#include "ensenso.hpp"
using namespace std;

int main()
{
    cout<<"Press 'c' to get calibration images, \nor 'p' to get the pointcloud"<<endl;
    char antwoord;
    cin >> antwoord;
    pcl::PointCloud<pcl::PointXYZ> a;
    get_en_image(a);
    if(a.points.size()>0)
    {
        pcl::PLYWriter plywriter;
        pcl::io::savePCDFileBinary("./calib_en/Ensenso.pcd", a);
        plywriter.write("./calib_en/Ensenso.ply", a, false);
    }
    return 0;
}
