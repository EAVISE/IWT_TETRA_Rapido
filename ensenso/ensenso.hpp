#include <vector>
#include <string>
#include <sstream>
#include <iostream>

#include <pcl/io/ensenso_grabber.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>

using namespace pcl;
using namespace std;

///Ensenso:
void get_en_image(pcl::PointCloud<pcl::PointXYZ>& cloud);
pcl::PointCloud<pcl::PointXYZ> get_en_cloud();
