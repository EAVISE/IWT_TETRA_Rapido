Nodige libraries voor Ensenso:

PCL 1.8
Boost : builden met vlaggen -lboost_system en -lboost_thread

Andere vlaggen voor de linker: -lNxLib64 (voor de Ensenso SDK) -lueye_api (voor de IDS camera)

Compiler moet zoeken in directories:
/usr/local/pcl-1.8  (of waar je het geinstalleerd hebt)
/usr/include/eigen3
/usr/include/vtk-5.8
calib_en (bevat de nodige headers om de Ensenso aan de praat te krijgen)

Voorwaarden: 
	/

Functies:

get_en_image
	Als input heeft deze functie een pointcloud, die ingevuld zal worden.
	De pointcloud heeft als eenheid meters.
	De images van beide camera's worden opgeslagen in de folder calib_en.
	Deze functie neemt ook een beeld van de RGB camera (van IDS). De afbeelding wordt ook 		opgeslagen in calib_en.

get_en_cloud
	Deze functie werkt met de Ensensograbber class van PCL.
	Momenteel werkt deze niet. Pointclouds kunnen verkregen worden met de get_en_image().
	


