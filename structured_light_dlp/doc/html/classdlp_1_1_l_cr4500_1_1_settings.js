var classdlp_1_1_l_cr4500_1_1_settings =
[
    [ "DisplayMode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode" ],
    [ "File", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file.html", [
      [ "DLPC350_Firmware", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware" ],
      [ "DLPC350_FlashParameters", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters" ],
      [ "PatternSequenceFirmware", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware" ]
    ] ],
    [ "FlashImage", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image" ],
    [ "ImageFlip", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip" ],
    [ "InputDataSwap", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap" ],
    [ "InputSource", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source" ],
    [ "InvertData", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data" ],
    [ "Led", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led" ],
    [ "ParallelClockSelect", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select" ],
    [ "ParallelPortWidth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width" ],
    [ "Pattern", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern.html", [
      [ "Bitdepth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth" ],
      [ "Display", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display" ],
      [ "Exposure", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_exposure.html", null ],
      [ "FlashImage", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image" ],
      [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert" ],
      [ "Led", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led" ],
      [ "Number", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number" ],
      [ "ShareExposure", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure" ],
      [ "Source", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source" ],
      [ "Trigger", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger" ]
    ] ],
    [ "PowerStandby", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby" ],
    [ "TestPattern", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern" ],
    [ "UseDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default" ]
];