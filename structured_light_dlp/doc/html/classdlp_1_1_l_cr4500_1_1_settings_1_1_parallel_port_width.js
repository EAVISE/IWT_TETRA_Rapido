var classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width =
[
    [ "Width", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634", [
      [ "BITS_30", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634ab8480c5a94e5bc93a8b7f23a69047bbe", null ],
      [ "BITS_24", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634afde08340ffb4dc6e708b33c9d8fbc6d6", null ],
      [ "BITS_20", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634aaf1800d265a2221d633dd3237a62cd82", null ],
      [ "BITS_16", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634a27819f40142c0f0c0c479c4a55926d11", null ],
      [ "BITS_10", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634a0c6ee858fe403cf4baa95398b76d4c26", null ],
      [ "BITS_8", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634a551e192f85a76c98a8f65d09c5d49fe0", null ]
    ] ],
    [ "ParallelPortWidth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a69c71b9b10ad51427728c38f95a79e4f", null ],
    [ "ParallelPortWidth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a56719dabdfae78c0ad7d7f173d3be769", null ],
    [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#ae867bd5a0adbbfd9f6d6caae1545eec6", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#acbdf5010c6a2b74ee35c63b8179e6415", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#aeb4532f4a43255e46c90bf736dfaa6d6", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a0f9d40b2cf463108e3627b0a743eb348", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a5cc95cbdbf27da32f6e8f2b50ed56ce8", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#aa2720b97c443f72c7b977056d81d7e38", null ]
];