var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led =
[
    [ "Color", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71", [
      [ "NONE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a8a6c91cb399ffe7ecdbe37588d056979", null ],
      [ "RED", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a17c7015a0f0fdfbbfd193b2fa29cdbc1", null ],
      [ "GREEN", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a0afd22c0d58558582a37d1fc6cb18a3c", null ],
      [ "YELLOW", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a9522616a0f3f081983855b62e3a2a216", null ],
      [ "BLUE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a00046021bd828932be41aa6d61a9593f", null ],
      [ "MAGENTA", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71ad36f3468e8708a7d95be110fd23e8d47", null ],
      [ "CYAN", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a6e21f168ff6b0464a3084be2ea9d9dec", null ],
      [ "WHITE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a09bbbe57ce3ba54f394ba2f516dea57d", null ],
      [ "INVALID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a040654bb9e36d5812b8ff2312b3a76a9", null ]
    ] ],
    [ "Led", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a369a827432d040478899221d0f077e2a", null ],
    [ "Led", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a0ba0bc48900a8788efa9f7330068057d", null ],
    [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a9630a101cea232ad1b67eb7a0e01bd0c", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ae5ba53e0d72c2bbea770202d74594abb", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab572731c1b55ae1ae6b1a128b6657c6a", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a08b2695863c5a8941356e7f6ab4e0d5f", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a1488dd6d91aa95a9b1ab8e4e1a42414d", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a056e0ff659abb18dd99bf14925058b75", null ]
];