var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth =
[
    [ "Format", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770", [
      [ "MONO_1BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a653afd33388c4fe53f407ac397843870", null ],
      [ "MONO_2BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a102166659427846300d6275a3e8e5eb5", null ],
      [ "MONO_3BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a49a74b3d4b849a6f9ea657d2d457fd4e", null ],
      [ "MONO_4BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a316432c4fe930ff3f08b03f5e78c7f17", null ],
      [ "MONO_5BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a7260a1e7030da0437eec28023f59ad46", null ],
      [ "MONO_6BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770aa393c59de050ad0eaedca937f5a7714c", null ],
      [ "MONO_7BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770af2e5cd6931d8c5f0677d0f9e94afd948", null ],
      [ "MONO_8BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a64696d5d2987b0f698d88c10d654b991", null ],
      [ "INVALID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770aad34df8147ce3123c55d105f891d450e", null ]
    ] ],
    [ "Bitdepth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a1ddbba47d30a224f5214eae5184a87ff", null ],
    [ "Bitdepth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#aa74f9dc0f0070729f17a61ab2c34f437", null ],
    [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#ac1442abb6315c7bc8caeb2a14417c297", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a961ee5734d71c70cd63b2d13bc7e9cfc", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a39d9286ca2cb5a238df870be61dd888c", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a9d8e69c664b31a28281896d69dbda059", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#af85dcfa7bf4b7e763d5c91bce6a57cca", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#ab0d7640d13b8751c3c423495e0bcdc57", null ]
];