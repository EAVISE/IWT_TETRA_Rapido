var classdlp_1_1_d_l_p___platform =
[
    [ "Settings", "classdlp_1_1_d_l_p___platform_1_1_settings.html", [
      [ "PatternSequence", "classdlp_1_1_d_l_p___platform_1_1_settings_1_1_pattern_sequence.html", [
        [ "Exposure", "classdlp_1_1_d_l_p___platform_1_1_settings_1_1_pattern_sequence_1_1_exposure.html", "classdlp_1_1_d_l_p___platform_1_1_settings_1_1_pattern_sequence_1_1_exposure" ],
        [ "Period", "classdlp_1_1_d_l_p___platform_1_1_settings_1_1_pattern_sequence_1_1_period.html", "classdlp_1_1_d_l_p___platform_1_1_settings_1_1_pattern_sequence_1_1_period" ],
        [ "Prepared", "classdlp_1_1_d_l_p___platform_1_1_settings_1_1_pattern_sequence_1_1_prepared.html", "classdlp_1_1_d_l_p___platform_1_1_settings_1_1_pattern_sequence_1_1_prepared" ]
      ] ]
    ] ],
    [ "Mirror", "classdlp_1_1_d_l_p___platform.html#a0c2764722341c024b62f77bb691da787", [
      [ "ORTHOGONAL", "classdlp_1_1_d_l_p___platform.html#a0c2764722341c024b62f77bb691da787ac9643b6ef9ceab12abacac795c0543ec", null ],
      [ "DIAMOND", "classdlp_1_1_d_l_p___platform.html#a0c2764722341c024b62f77bb691da787a337f43db5edddda90ef2156a9baaed09", null ],
      [ "INVALID", "classdlp_1_1_d_l_p___platform.html#a0c2764722341c024b62f77bb691da787accc0377a8afbf50e7094f5c23a8af223", null ]
    ] ],
    [ "Platform", "classdlp_1_1_d_l_p___platform.html#a5d3b41f658d5801dedd89bbf65c3818e", [
      [ "LIGHTCRAFTER_3000", "classdlp_1_1_d_l_p___platform.html#a5d3b41f658d5801dedd89bbf65c3818eae9df6d646288e5db206f1fccb6759a59", null ],
      [ "LIGHTCRAFTER_4500", "classdlp_1_1_d_l_p___platform.html#a5d3b41f658d5801dedd89bbf65c3818eaeede28ef77b357f4dda717990aab6449", null ],
      [ "INVALID", "classdlp_1_1_d_l_p___platform.html#a5d3b41f658d5801dedd89bbf65c3818eaccc0377a8afbf50e7094f5c23a8af223", null ]
    ] ],
    [ "DLP_Platform", "classdlp_1_1_d_l_p___platform.html#a2057ee70023b9194030862fbf0ccd012", null ],
    [ "Connect", "classdlp_1_1_d_l_p___platform.html#a104325b2dada8d185fceba5690bc2305", null ],
    [ "Disconnect", "classdlp_1_1_d_l_p___platform.html#a5bb8c3eff12299781a413195c1305527", null ],
    [ "DisplayPatternInSequence", "classdlp_1_1_d_l_p___platform.html#aab835e34c222cc89f4df22752387e039", null ],
    [ "GetColumns", "classdlp_1_1_d_l_p___platform.html#a5f7fe9afe8c00191f3cae342e0d50297", null ],
    [ "GetDevice", "classdlp_1_1_d_l_p___platform.html#a019fda3c05ccbe52e84c1e03218361f6", null ],
    [ "GetEffectiveMirrorSize", "classdlp_1_1_d_l_p___platform.html#a21a55f55eba70422305fb6d9596332c6", null ],
    [ "GetMirrorType", "classdlp_1_1_d_l_p___platform.html#a62da10bb375067ace61ecbeb1f3fd06e", null ],
    [ "GetPlatform", "classdlp_1_1_d_l_p___platform.html#aa66f13e6b9d6e1d0e0c90e4a81b75b8e", null ],
    [ "GetRows", "classdlp_1_1_d_l_p___platform.html#a3704e59d03e7035495e3678dee886978", null ],
    [ "GetSetup", "classdlp_1_1_d_l_p___platform.html#a9d717933511579696ad6d49aa8f5e3aa", null ],
    [ "ImageResolutionCorrect", "classdlp_1_1_d_l_p___platform.html#a7c6d9c3401d63ac9374954681cd1ef15", null ],
    [ "ImageResolutionCorrect", "classdlp_1_1_d_l_p___platform.html#a2d6caaafbebb1d29a613920801e3b1b3", null ],
    [ "isConnected", "classdlp_1_1_d_l_p___platform.html#ab9c20f1e8216985a5fb59b825a577a59", null ],
    [ "isPlatformSetup", "classdlp_1_1_d_l_p___platform.html#a45ab08e93004137f3ac3ef3bc3ac1df2", null ],
    [ "isSetup", "classdlp_1_1_d_l_p___platform.html#a3f88ae6c3ba55e6a12a76f6ac38184fb", null ],
    [ "PreparePatternSequence", "classdlp_1_1_d_l_p___platform.html#adf9baf8b8782b8ea07e1b10a016060f9", null ],
    [ "ProjectSolidBlackPattern", "classdlp_1_1_d_l_p___platform.html#a7f52ad737e9e58ef5665f9947b33e5c9", null ],
    [ "ProjectSolidWhitePattern", "classdlp_1_1_d_l_p___platform.html#a151bdbca3991d35503620c402b5eeb49", null ],
    [ "SetDebugEnable", "classdlp_1_1_d_l_p___platform.html#a9c9f02c1691ea89bbaccd5bbcd2d2285", null ],
    [ "SetDebugLevel", "classdlp_1_1_d_l_p___platform.html#a9f64b042bd8c8be7de68d0b23a633ef4", null ],
    [ "SetDebugOutput", "classdlp_1_1_d_l_p___platform.html#a1400ea6e49ef0fa49896437dbaa67672", null ],
    [ "SetDevice", "classdlp_1_1_d_l_p___platform.html#aaaa97a4cfe25768b6b39fedd8e8982b1", null ],
    [ "SetPlatform", "classdlp_1_1_d_l_p___platform.html#a3145e35fff7c797b44476d6cc5d3c5a5", null ],
    [ "Setup", "classdlp_1_1_d_l_p___platform.html#a018706cc4f67a3c1ae67c9b7ee4e8435", null ],
    [ "StartPatternSequence", "classdlp_1_1_d_l_p___platform.html#af0b160459c7eb69aae68c6c743ae9ca3", null ],
    [ "StopPatternSequence", "classdlp_1_1_d_l_p___platform.html#a4241e3cb61e323521732a12c2b926fde", null ],
    [ "debug_", "classdlp_1_1_d_l_p___platform.html#ada002617e188f389b066b3663cc62fd9", null ],
    [ "is_setup_", "classdlp_1_1_d_l_p___platform.html#aac4cdea7919bcd50453b21847fca2556", null ],
    [ "sequence_exposure_", "classdlp_1_1_d_l_p___platform.html#adda39121bc188b7c978d46a46c4e8c85", null ],
    [ "sequence_period_", "classdlp_1_1_d_l_p___platform.html#a7808e68de1363e467fc94359d730f9c2", null ],
    [ "sequence_prepared_", "classdlp_1_1_d_l_p___platform.html#acf53d6f5ed7d2e221e53677ae3f3b371", null ]
];