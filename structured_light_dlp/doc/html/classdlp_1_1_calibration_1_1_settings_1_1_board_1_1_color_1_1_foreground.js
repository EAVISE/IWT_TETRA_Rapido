var classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_foreground =
[
    [ "Foreground", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_foreground.html#ad8f85a525278fb8258ad6ac5d790461f", null ],
    [ "Foreground", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_foreground.html#a4f083d061ed8b11a0f87483d5ce2924a", null ],
    [ "Get", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_foreground.html#a71ddffd4211756e68d5674ba4fa6d141", null ],
    [ "GetEntryDefault", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_foreground.html#a3e69ef33fe5128e95a88b9bb0c2aeaa4", null ],
    [ "GetEntryName", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_foreground.html#a0cdb6ab665fbdc0efb6ff5b796f54917", null ],
    [ "GetEntryValue", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_foreground.html#a6fa3e8f7f26be9421db878719bcf09e6", null ],
    [ "Set", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_foreground.html#a5680a06d7f42694d3bba9484cafe2d27", null ],
    [ "SetEntryValue", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_foreground.html#a20d0a201228f63fd63cc72fb1eb19f07", null ]
];