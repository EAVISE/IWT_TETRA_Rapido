var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode =
[
    [ "Trigger", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135", [
      [ "MODE_0_VSYNC", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135a23c3189ca6b62372e4c90c9a9d8f7bc7", null ],
      [ "MODE_1_INT_OR_EXT", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135ac61c51c7f8115514a7d5c1fb3be65f3d", null ],
      [ "MODE_2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135a89de2f5c6940a653f7f35efee5b58e2c", null ],
      [ "MODE_3_EXP_INT_OR_EXT", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135a3b6763a6c72c94948817c7cbbafdbf8a", null ],
      [ "MODE_4_EXP_VSYNC", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135a62b37285f23cee405047e234c0f199e6", null ],
      [ "INVALID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135adde0da922c9b3094ea42ccf921b663ad", null ]
    ] ],
    [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#af3a5ad5df2db11aff42637100cb400a8", null ],
    [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a77fb744227fb4f1932ccb35b0947e1a5", null ],
    [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a95e023e19c01ecda7a519da563c0821f", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a0ece5007ff9a71da9a1f41aff972320a", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a181c675af881ea2396f12002f0411d9e", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#ae95ede26b37ee8898620d5b56a3000e6", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#aa3a3e9b3b6287fc837b55ef3923f63fd", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#af056292ffde44a7e445a7a02fe4b012e", null ]
];