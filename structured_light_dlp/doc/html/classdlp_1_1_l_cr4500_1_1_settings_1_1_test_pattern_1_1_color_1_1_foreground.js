var classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground =
[
    [ "Foreground", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#a8d5f1f68e5ca78689ead9d7d4b7bf55d", null ],
    [ "Foreground", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#aee39ce6883996fcebb08faa0247feb7a", null ],
    [ "GetBlue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#ac8c8b2bcfa52e5c7412b7379b69f3cb2", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#ac3e34a4d1f2ba34dfc29f1a53d5b6e24", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#aa643c69991c7c7cb611f55fa85ec1cf8", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#af618054c94f9e91b8476efe013230658", null ],
    [ "GetGreen", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#a2f000babddaa64172e2c24b87c0fc091", null ],
    [ "GetRed", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#a34cc00079dd2a6b54d93bbf9bd88a1da", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#a5a56441b2e2812267a563f068bc41297", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#a28ce453eeb1703f3d1b646a8729214b7", null ]
];