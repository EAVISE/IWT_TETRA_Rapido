var geometry_8hpp =
[
    [ "Geometry", "classdlp_1_1_geometry.html", "classdlp_1_1_geometry" ],
    [ "Settings", "classdlp_1_1_geometry_1_1_settings.html", [
      [ "FilterViewpointRays", "classdlp_1_1_geometry_1_1_settings_1_1_filter_viewpoint_rays.html", "classdlp_1_1_geometry_1_1_settings_1_1_filter_viewpoint_rays" ],
      [ "OriginPointDistance", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance.html", [
        [ "Maximum", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_maximum.html", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_maximum" ],
        [ "Minimum", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_minimum.html", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_minimum" ]
      ] ],
      [ "ScaleXYZ", "classdlp_1_1_geometry_1_1_settings_1_1_scale_x_y_z.html", "classdlp_1_1_geometry_1_1_settings_1_1_scale_x_y_z" ]
    ] ],
    [ "FilterViewpointRays", "classdlp_1_1_geometry_1_1_settings_1_1_filter_viewpoint_rays.html", "classdlp_1_1_geometry_1_1_settings_1_1_filter_viewpoint_rays" ],
    [ "MaximumPercentError", "classdlp_1_1_geometry_1_1_settings_1_1_filter_viewpoint_rays_1_1_maximum_percent_error.html", "classdlp_1_1_geometry_1_1_settings_1_1_filter_viewpoint_rays_1_1_maximum_percent_error" ],
    [ "OriginPointDistance", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance.html", [
      [ "Maximum", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_maximum.html", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_maximum" ],
      [ "Minimum", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_minimum.html", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_minimum" ]
    ] ],
    [ "Maximum", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_maximum.html", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_maximum" ],
    [ "Minimum", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_minimum.html", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_minimum" ],
    [ "ScaleXYZ", "classdlp_1_1_geometry_1_1_settings_1_1_scale_x_y_z.html", "classdlp_1_1_geometry_1_1_settings_1_1_scale_x_y_z" ],
    [ "ViewPoint", "classdlp_1_1_geometry_1_1_view_point.html", "classdlp_1_1_geometry_1_1_view_point" ],
    [ "GEOMETRY_CALIBRATION_NOT_COMPLETE", "geometry_8hpp.html#a3e9312822e6b55996153be400d6f69ef", null ],
    [ "GEOMETRY_DISPARITY_MAP_RESOLUTION_INVALID", "geometry_8hpp.html#a18bdf8c316ab39a6851ddbaa3123422b", null ],
    [ "GEOMETRY_NO_ORIGIN_SET", "geometry_8hpp.html#a32313e324f5131349696cc154855f892", null ],
    [ "GEOMETRY_NULL_POINTER", "geometry_8hpp.html#aa4394afb0383803842826c6ef722fbf1", null ],
    [ "GEOMETRY_ORIGIN_RAY_OUT_OF_RANGE", "geometry_8hpp.html#a9e1a14f8696dd22887d99b2f5fe592a5", null ],
    [ "GEOMETRY_POINT_CLOUD_EMPTY", "geometry_8hpp.html#a88a0ec61e8d171284c0bc3afa7bd387f", null ],
    [ "GEOMETRY_SETTINGS_EMPTY", "geometry_8hpp.html#a785c117beb5ebd0d9e07729623365530", null ],
    [ "GEOMETRY_VIEWPORT_ID_OUT_OF_RANGE", "geometry_8hpp.html#af1063a18124914f768ccd992d0377157", null ],
    [ "GEOMETRY_VIEWPORT_RAY_OUT_OF_RANGE", "geometry_8hpp.html#a8fb5b3f6bba2b0bd999398dde3dc7503", null ]
];