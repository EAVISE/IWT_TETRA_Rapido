var classdlp_1_1_return_code =
[
    [ "Add", "classdlp_1_1_return_code.html#a56cb860efbe51e824806651cb5f38710", null ],
    [ "AddError", "classdlp_1_1_return_code.html#a4b1aab187f442dfc1da473863023381f", null ],
    [ "AddWarning", "classdlp_1_1_return_code.html#a646473de39fbeb95ca397714df0b0f49", null ],
    [ "ContainsError", "classdlp_1_1_return_code.html#ad44cabfe99fdb64db630693b69a09677", null ],
    [ "ContainsWarning", "classdlp_1_1_return_code.html#a713b14b6e5d5ca4f02660dbd192c2a3c", null ],
    [ "GetErrorCount", "classdlp_1_1_return_code.html#a0e6cae33c275ffdaf1abe87f522c6659", null ],
    [ "GetErrors", "classdlp_1_1_return_code.html#ad557a82c5c9666d80c94d079eead778e", null ],
    [ "GetWarningCount", "classdlp_1_1_return_code.html#ac82c684effceb43e3c3082448ed0ac56", null ],
    [ "GetWarnings", "classdlp_1_1_return_code.html#acc5787c4fb1715a59e0db1007b1e6699", null ],
    [ "hasErrors", "classdlp_1_1_return_code.html#a00198ee7e9875b250e9c916b3c581c36", null ],
    [ "hasWarnings", "classdlp_1_1_return_code.html#a0dacab974302d4422ce060a69f3d5f18", null ],
    [ "operator bool", "classdlp_1_1_return_code.html#a5e7df632a3c3e3b54402ae28de81ea20", null ],
    [ "ToString", "classdlp_1_1_return_code.html#af3069bd6af2a2c0d517e84af62601732", null ]
];