var classdlp_1_1_geometry =
[
    [ "Settings", "classdlp_1_1_geometry_1_1_settings.html", [
      [ "FilterViewpointRays", "classdlp_1_1_geometry_1_1_settings_1_1_filter_viewpoint_rays.html", "classdlp_1_1_geometry_1_1_settings_1_1_filter_viewpoint_rays" ],
      [ "OriginPointDistance", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance.html", [
        [ "Maximum", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_maximum.html", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_maximum" ],
        [ "Minimum", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_minimum.html", "classdlp_1_1_geometry_1_1_settings_1_1_origin_point_distance_1_1_minimum" ]
      ] ],
      [ "ScaleXYZ", "classdlp_1_1_geometry_1_1_settings_1_1_scale_x_y_z.html", "classdlp_1_1_geometry_1_1_settings_1_1_scale_x_y_z" ]
    ] ],
    [ "ViewPoint", "classdlp_1_1_geometry_1_1_view_point.html", "classdlp_1_1_geometry_1_1_view_point" ],
    [ "Geometry", "classdlp_1_1_geometry.html#a36992fe4596cd6fde382b2503327266b", null ],
    [ "~Geometry", "classdlp_1_1_geometry.html#aacaaebc8441ad47c97344d1dfd2adec2", null ],
    [ "AddView", "classdlp_1_1_geometry.html#a2ee40e5c4910c142ccc25db9980e6415", null ],
    [ "CalculateFlatness", "classdlp_1_1_geometry.html#a374ee85adde2b354c1ce5187667be8b6", null ],
    [ "Clear", "classdlp_1_1_geometry.html#a07dbbc2b6f56e0828694a55f079ab664", null ],
    [ "Find3dLineIntersection", "classdlp_1_1_geometry.html#acb7943186c210699652e2138848eb4cb", null ],
    [ "GeneratePointCloud", "classdlp_1_1_geometry.html#aba3609d20687b1d2193262c6d2bcb384", null ],
    [ "GetNumberOfViews", "classdlp_1_1_geometry.html#aa13b3df4358e6afd77958f7f30c65259", null ],
    [ "GetSetup", "classdlp_1_1_geometry.html#aed829cf40bbc665dab13bd6c3d744053", null ],
    [ "isSetup", "classdlp_1_1_geometry.html#ade0a231aa180867e327030963f78c053", null ],
    [ "SetDebugEnable", "classdlp_1_1_geometry.html#a9c9f02c1691ea89bbaccd5bbcd2d2285", null ],
    [ "SetDebugLevel", "classdlp_1_1_geometry.html#a9f64b042bd8c8be7de68d0b23a633ef4", null ],
    [ "SetDebugOutput", "classdlp_1_1_geometry.html#a1400ea6e49ef0fa49896437dbaa67672", null ],
    [ "SetOriginView", "classdlp_1_1_geometry.html#ad0d286e9f51b9cc1889128e41ea7c4f5", null ],
    [ "Setup", "classdlp_1_1_geometry.html#a8f91a3588db051dbbf664d0bc61e334b", null ],
    [ "Unsafe_Find3dLineIntersection", "classdlp_1_1_geometry.html#a87b33bca3c76b0894d29b3f3f3314b81", null ],
    [ "debug_", "classdlp_1_1_geometry.html#ada002617e188f389b066b3663cc62fd9", null ],
    [ "is_setup_", "classdlp_1_1_geometry.html#aac4cdea7919bcd50453b21847fca2556", null ]
];