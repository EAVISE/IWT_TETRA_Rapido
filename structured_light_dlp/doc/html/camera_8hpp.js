var camera_8hpp =
[
    [ "Settings", "classdlp_1_1_camera_1_1_settings.html", [
      [ "Trigger", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html", "classdlp_1_1_camera_1_1_settings_1_1_trigger" ]
    ] ],
    [ "Trigger", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html", "classdlp_1_1_camera_1_1_settings_1_1_trigger" ],
    [ "CAMERA_EXPOSURE_INVALID", "camera_8hpp.html#a54868c76f4f4a63332359a07b8ceb3f3", null ],
    [ "CAMERA_FRAME_GRAB_FAILED", "camera_8hpp.html#a762166a5970ce8370dbb2b1b4d08367c", null ],
    [ "CAMERA_FRAME_RATE_INVALID", "camera_8hpp.html#a37d6b35a8d19e75987882080f916c098", null ],
    [ "CAMERA_NOT_CONNECTED", "camera_8hpp.html#a4d6f37a7d2ece417d02b29c7b0df9d79", null ],
    [ "CAMERA_NOT_DISCONNECTED", "camera_8hpp.html#ad3645d6b015c75bddedee4126d051096", null ],
    [ "CAMERA_NOT_SETUP", "camera_8hpp.html#adde47f29238b59e54e0d9c87dbc09f7d", null ],
    [ "CAMERA_NOT_STARTED", "camera_8hpp.html#ac41aa822d15c0acb36697070e797a8bc", null ],
    [ "CAMERA_NOT_STOPPED", "camera_8hpp.html#a136682e4472efa339db46a1c0bdf55e3", null ],
    [ "CAMERA_RESOLUTION_INVALID", "camera_8hpp.html#a922d76ce845b0ab171b6eb42a6251945", null ],
    [ "CAMERA_TRIGGER_INVALID", "camera_8hpp.html#a76e6a83cad6dad5f02f650a7e6e2605e", null ]
];