var classdlp_1_1_l_cr4500_1_1_settings_1_1_led =
[
    [ "Blue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue.html", [
      [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current" ],
      [ "EdgeDelay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay.html", [
        [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling" ],
        [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising" ]
      ] ],
      [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable" ]
    ] ],
    [ "Green", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green.html", [
      [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current" ],
      [ "EdgeDelay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay.html", [
        [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling" ],
        [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising" ]
      ] ],
      [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable" ]
    ] ],
    [ "InvertPWM", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m" ],
    [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red.html", [
      [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current" ],
      [ "EdgeDelay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay.html", [
        [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling" ],
        [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising" ]
      ] ],
      [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable" ]
    ] ],
    [ "Sequence", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence" ]
];