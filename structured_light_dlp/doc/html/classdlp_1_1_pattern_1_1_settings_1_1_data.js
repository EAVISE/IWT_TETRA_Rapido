var classdlp_1_1_pattern_1_1_settings_1_1_data =
[
    [ "Type", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#ae31af5d562ae7be3971f7d4a63a0fa56", [
      [ "IMAGE_FILE", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#ae31af5d562ae7be3971f7d4a63a0fa56a68f06c4f04b60df9dc1eb70fd9468558", null ],
      [ "IMAGE_DATA", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#ae31af5d562ae7be3971f7d4a63a0fa56adc0a5d4243f38ec51f61fd0f90fbc42d", null ],
      [ "PARAMETERS", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#ae31af5d562ae7be3971f7d4a63a0fa56aeb178264802ebbd52cccc8feadb72a6f", null ],
      [ "INVALID", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#ae31af5d562ae7be3971f7d4a63a0fa56accc0377a8afbf50e7094f5c23a8af223", null ]
    ] ],
    [ "Data", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#ace1b6c942d5c7b8ebda7004c8a76136e", null ],
    [ "Data", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#a57583301bdaf87cda594e7bf3162ecf5", null ],
    [ "Get", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#ae1c9f02e8c831b78ea2d656f355dceab", null ],
    [ "GetEntryDefault", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#a00e0bdabaf6ffba42f157fca4dc35ad6", null ],
    [ "GetEntryName", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#af9658a8acec4042ce3900492f10164d0", null ],
    [ "GetEntryValue", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#ad7806cc3172db40c4ebafb2762c44166", null ],
    [ "Set", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#aa4e7eec97bfafc28c3d13a35bee9d033", null ],
    [ "SetEntryValue", "classdlp_1_1_pattern_1_1_settings_1_1_data.html#aedd5a02b08655a4ea5e32442bb921b7d", null ]
];