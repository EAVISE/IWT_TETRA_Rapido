var classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode =
[
    [ "Options", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a55690836b07006c296c64e83da17a45f", [
      [ "MODE0_FULL_RESOLUTION", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a55690836b07006c296c64e83da17a45fa105529d2fabf159331d9c887047514bd", null ],
      [ "MODE4_HALF_RESOLUTION", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a55690836b07006c296c64e83da17a45fab607392347bec64075dd2463e29b903c", null ],
      [ "INVALID", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a55690836b07006c296c64e83da17a45faccc0377a8afbf50e7094f5c23a8af223", null ]
    ] ],
    [ "VideoMode", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a0561369e603d7c32fdd58012d9e336d6", null ],
    [ "VideoMode", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#acbde74c379c0b5cb840f669401b6c5d8", null ],
    [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a3044a022bd466fe22d71db5b6ec070b5", null ],
    [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a922be2bf9beded2c3ec55499ea53f3bb", null ],
    [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a3e2eed63d2f8a0e2e24f3919b29063d7", null ],
    [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a0b96a9fcf073c1d624943373fcd09bc8", null ],
    [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#aa4a44fe54bd7d2a3206a54c1e1980d15", null ],
    [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a9a6c059b67047d5a1259e1b884efddf9", null ]
];