var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number =
[
    [ "Blue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue" ],
    [ "Green", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green" ],
    [ "Mono_1BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p" ],
    [ "Mono_2BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p" ],
    [ "Mono_3BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p" ],
    [ "Mono_4BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p" ],
    [ "Mono_5BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p" ],
    [ "Mono_6BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p" ],
    [ "Mono_7BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__7_b_p_p.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__7_b_p_p" ],
    [ "Mono_8BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__8_b_p_p.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__8_b_p_p" ],
    [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red" ],
    [ "Number", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#a2b853dc66055e02cc879de89a8a57019", null ],
    [ "Number", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#af72f318d074e4df5ddae319a588b8a50", null ],
    [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#a9dde2baf4193fb657cecf9ebe4ee59dd", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#a19183446e0ff9c3e717e96f8c763c327", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#a42f31bbc22f47bf8266ec391a98539c3", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#a0c24c6a1f58e67bbc4eaa256d542491b", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#acc9fd77110cf183df42cb30a9f7e5023", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#aba456c0d73ecd634a4cb1a7dd3ab071c", null ]
];