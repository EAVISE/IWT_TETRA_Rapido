var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p =
[
    [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71", [
      [ "G2_G1_G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a9a82bf198bcca32d735a8406146b180c", null ],
      [ "G5_G4_G3", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a11abf5e6e1a188b24b96ceb72fea4aee", null ],
      [ "R0_G7_G6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a9ae7144b12606d6c2ab99b58cd263e89", null ],
      [ "R3_R2_R1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a7cca54551a78bf5d0cd3427400a69590", null ],
      [ "R6_R5_R4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a80d3b9148bc89dd5ffe93a69c8a537cc", null ],
      [ "B1_B0_R7", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a4ec1a58a22923b72e70cdea62e49336a", null ],
      [ "B4_B3_B2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a6a4ea1135a9fb71a4abb8ea9cd7b0320", null ],
      [ "B7_B6_B5", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a294122a4be1bccfd4bd759fd1ab198f5", null ]
    ] ]
];