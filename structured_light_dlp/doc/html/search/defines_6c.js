var searchData=
[
  ['lcr4500_5fcalibration_5fpatterns_5fnot_5fprepared',['LCR4500_CALIBRATION_PATTERNS_NOT_PREPARED',['../lcr4500_8hpp.html#af8e787fa7f0bcd294ef1f34bb8487e64',1,'lcr4500.hpp']]],
  ['lcr4500_5fcommand_5ffailed',['LCR4500_COMMAND_FAILED',['../lcr4500_8hpp.html#a60e0634eab9387478009765914050797',1,'lcr4500.hpp']]],
  ['lcr4500_5fconnection_5ffailed',['LCR4500_CONNECTION_FAILED',['../lcr4500_8hpp.html#a93dd68b0a264debfd33850f06672af2a',1,'lcr4500.hpp']]],
  ['lcr4500_5fdisplay_5fmode_5fdefault',['LCR4500_DISPLAY_MODE_DEFAULT',['../lcr4500__defaults_8hpp.html#a01c95328d6e658b757318f63a96e3d98',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fdlpc350_5ffirmware_5ffile_5fnot_5ffound',['LCR4500_DLPC350_FIRMWARE_FILE_NOT_FOUND',['../lcr4500_8hpp.html#a02e8f3f7b44d6c46840ccc7fa0accbb7',1,'lcr4500.hpp']]],
  ['lcr4500_5ffalse',['LCR4500_FALSE',['../lcr4500_8hpp.html#ada510ed9ad20704835d1869ab5695048',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5fchecksum_5fmismatch',['LCR4500_FIRMWARE_CHECKSUM_MISMATCH',['../lcr4500_8hpp.html#a822e9f486f98b44ddd40fcc37a13a3bb',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5fchecksum_5fverification_5ffailed',['LCR4500_FIRMWARE_CHECKSUM_VERIFICATION_FAILED',['../lcr4500_8hpp.html#a20680be4a9c211fb90040737c0b117b4',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5ffile_5finvalid',['LCR4500_FIRMWARE_FILE_INVALID',['../lcr4500_8hpp.html#aec7c6dd62626b1ee9dd12a7e906376a8',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5ffile_5fname_5finvalid',['LCR4500_FIRMWARE_FILE_NAME_INVALID',['../lcr4500_8hpp.html#a8c5cf78627a8d0b89a83a27204c288e6',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5ffile_5fnot_5ffound',['LCR4500_FIRMWARE_FILE_NOT_FOUND',['../lcr4500_8hpp.html#abcd5f21caaed42fb6d41bb52e0057385',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5fflash_5ferase_5ffailed',['LCR4500_FIRMWARE_FLASH_ERASE_FAILED',['../lcr4500_8hpp.html#a70a79e246b777db342db96c563bc8781',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5fflash_5fparameters_5ffile_5fnot_5ffound',['LCR4500_FIRMWARE_FLASH_PARAMETERS_FILE_NOT_FOUND',['../lcr4500_8hpp.html#a3ce55cd8e98c860bdb46b697609cb99d',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5fimage_5fbasename_5fempty',['LCR4500_FIRMWARE_IMAGE_BASENAME_EMPTY',['../lcr4500_8hpp.html#a4545ab9f6c8af152da6e921bb2e35bc8',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5fmemory_5fallocation_5ffailed',['LCR4500_FIRMWARE_MEMORY_ALLOCATION_FAILED',['../lcr4500_8hpp.html#aa60f5bdf6e691f12304f82c8bdbf1c10',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5fnot_5fenough_5fmemory',['LCR4500_FIRMWARE_NOT_ENOUGH_MEMORY',['../lcr4500_8hpp.html#a7e859835fb5de735703bd9400236aee4',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5fupload_5ffailed',['LCR4500_FIRMWARE_UPLOAD_FAILED',['../lcr4500_8hpp.html#a42e98cf33cd397cdc80ad3a45149504a',1,'lcr4500.hpp']]],
  ['lcr4500_5ffirmware_5fupload_5fin_5fprogress',['LCR4500_FIRMWARE_UPLOAD_IN_PROGRESS',['../lcr4500_8hpp.html#a900f58ec50b12b9111549a00229e9068',1,'lcr4500.hpp']]],
  ['lcr4500_5fflash_5ffw_5fversion_5faddress',['LCR4500_FLASH_FW_VERSION_ADDRESS',['../lcr4500_8hpp.html#ad6499cfd07539b64b29cc32062b270f9',1,'lcr4500.hpp']]],
  ['lcr4500_5fflash_5fimage_5findex_5finvalid',['LCR4500_FLASH_IMAGE_INDEX_INVALID',['../lcr4500_8hpp.html#ac921783d5c509de62616234ff7ea3fb7',1,'lcr4500.hpp']]],
  ['lcr4500_5fflashdevice_5fparameters_5fnot_5ffound',['LCR4500_FLASHDEVICE_PARAMETERS_NOT_FOUND',['../lcr4500_8hpp.html#a46afb32b2d390e42707ee2ff148ef0aa',1,'lcr4500.hpp']]],
  ['lcr4500_5fget_5fflash_5fdevice_5fid_5ffailed',['LCR4500_GET_FLASH_DEVICE_ID_FAILED',['../lcr4500_8hpp.html#a78e0b977631ea249b9cde7ae6dba1aa9',1,'lcr4500.hpp']]],
  ['lcr4500_5fget_5fflash_5fmanufacturer_5fid_5ffailed',['LCR4500_GET_FLASH_MANUFACTURER_ID_FAILED',['../lcr4500_8hpp.html#afaf7e1a38ca6599b611ae4c9676ba338',1,'lcr4500.hpp']]],
  ['lcr4500_5fget_5fstatus_5ffailed',['LCR4500_GET_STATUS_FAILED',['../lcr4500_8hpp.html#a31f265ddb99bae405d863dac366b21bd',1,'lcr4500.hpp']]],
  ['lcr4500_5fimage_5ffile_5fformat_5finvalid',['LCR4500_IMAGE_FILE_FORMAT_INVALID',['../lcr4500_8hpp.html#a440c086de417e147cfb2493d2b18efd4',1,'lcr4500.hpp']]],
  ['lcr4500_5fimage_5fflip_5fdefault',['LCR4500_IMAGE_FLIP_DEFAULT',['../lcr4500__defaults_8hpp.html#a9be75a6cd5ffb70af28c673c327d52ba',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fimage_5fformat_5finvalid',['LCR4500_IMAGE_FORMAT_INVALID',['../lcr4500_8hpp.html#abed95edc0306e40f30b197182b2af1c2',1,'lcr4500.hpp']]],
  ['lcr4500_5fimage_5flist_5ftoo_5flong',['LCR4500_IMAGE_LIST_TOO_LONG',['../lcr4500_8hpp.html#a8f7877b8dd5766f6fce32d980e5783bf',1,'lcr4500.hpp']]],
  ['lcr4500_5fimage_5fmemory_5fallocation_5ffailed',['LCR4500_IMAGE_MEMORY_ALLOCATION_FAILED',['../lcr4500_8hpp.html#a9a3b99d6cf041677c7f7f2eee33c7d3b',1,'lcr4500.hpp']]],
  ['lcr4500_5fimage_5fresolution_5finvalid',['LCR4500_IMAGE_RESOLUTION_INVALID',['../lcr4500_8hpp.html#a7d01db21b8f84caeac397e3ad35977bd',1,'lcr4500.hpp']]],
  ['lcr4500_5fin_5fcalibration_5fmode',['LCR4500_IN_CALIBRATION_MODE',['../lcr4500_8hpp.html#a1ed9856e6f0f553861752a3340b0a162',1,'lcr4500.hpp']]],
  ['lcr4500_5finvert_5fdata_5fdefault',['LCR4500_INVERT_DATA_DEFAULT',['../lcr4500__defaults_8hpp.html#ab18aed4d7481b646c47dcac1a3f49e1a',1,'LCR4500_INVERT_DATA_DEFAULT():&#160;lcr4500_defaults.hpp'],['../lcr4500__defaults_8hpp.html#ab18aed4d7481b646c47dcac1a3f49e1a',1,'LCR4500_INVERT_DATA_DEFAULT():&#160;lcr4500_defaults.hpp']]],
  ['lcr4500_5fled_5fcurrent_5fblue_5fdefault',['LCR4500_LED_CURRENT_BLUE_DEFAULT',['../lcr4500__defaults_8hpp.html#a00d5feb28d712ac54e0bb5b47c1e8af9',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fled_5fcurrent_5fgreen_5fdefault',['LCR4500_LED_CURRENT_GREEN_DEFAULT',['../lcr4500__defaults_8hpp.html#a38936e35dfc55dbccd9f06241511a582',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fled_5fcurrent_5fred_5fdefault',['LCR4500_LED_CURRENT_RED_DEFAULT',['../lcr4500__defaults_8hpp.html#a5c1c9a76b5e8e2c2fed8aef9194f2dc6',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fled_5fedge_5fdelay_5fdefault',['LCR4500_LED_EDGE_DELAY_DEFAULT',['../lcr4500__defaults_8hpp.html#acc1ccced70c00788311b8d85ead0997f',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fled_5fenable_5fdefault',['LCR4500_LED_ENABLE_DEFAULT',['../lcr4500__defaults_8hpp.html#a34261252ac1366e7faa5fc619aff723e',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fled_5fpwm_5finvert_5fdefault',['LCR4500_LED_PWM_INVERT_DEFAULT',['../lcr4500__defaults_8hpp.html#a7aaed4eaf54797ee5b6de7e11fde584b',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fled_5fsequence_5fauto_5fdefault',['LCR4500_LED_SEQUENCE_AUTO_DEFAULT',['../lcr4500__defaults_8hpp.html#afd3bbd4c1cc679244dc58e1324045c27',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fmeasure_5fflash_5fload_5ftiming_5ffailed',['LCR4500_MEASURE_FLASH_LOAD_TIMING_FAILED',['../lcr4500_8hpp.html#a1c5ce124975f17713fa817f2e0f20ba3',1,'lcr4500.hpp']]],
  ['lcr4500_5fnot_5fconnected',['LCR4500_NOT_CONNECTED',['../lcr4500_8hpp.html#a1ed661f06743ab8597d4e61fa9296af2',1,'lcr4500.hpp']]],
  ['lcr4500_5fnull_5fpoint_5fargument_5fminimum_5fexposure',['LCR4500_NULL_POINT_ARGUMENT_MINIMUM_EXPOSURE',['../lcr4500_8hpp.html#abf2138a804f2e5dbc7dd7b8c06e3bbad',1,'lcr4500.hpp']]],
  ['lcr4500_5fnull_5fpoint_5fargument_5fparameters',['LCR4500_NULL_POINT_ARGUMENT_PARAMETERS',['../lcr4500_8hpp.html#a176d81d283b7aa5cce96d6222f7b4aa3',1,'lcr4500.hpp']]],
  ['lcr4500_5fpattern_5fbuffer_5fswap_5fdefault',['LCR4500_PATTERN_BUFFER_SWAP_DEFAULT',['../lcr4500__defaults_8hpp.html#a9362386c81fc0d35b6e8aa38095f1750',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fpattern_5fdisplay_5fmode_5fdefault',['LCR4500_PATTERN_DISPLAY_MODE_DEFAULT',['../lcr4500__defaults_8hpp.html#a58d0fbd33bc1b87e290079bcd459339c',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fpattern_5fflash_5findex_5fparameter_5fmissing',['LCR4500_PATTERN_FLASH_INDEX_PARAMETER_MISSING',['../lcr4500_8hpp.html#a6a734e45248aff56cf482b5d7d171986',1,'lcr4500.hpp']]],
  ['lcr4500_5fpattern_5fimage_5fdefault',['LCR4500_PATTERN_IMAGE_DEFAULT',['../lcr4500__defaults_8hpp.html#a74522ebd1a8f5a431813564699fd1a3f',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fpattern_5finvert_5fdefault',['LCR4500_PATTERN_INVERT_DEFAULT',['../lcr4500__defaults_8hpp.html#a602850cf778f79c958277bcb6e80be6b',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fpattern_5fnumber_5fdefault',['LCR4500_PATTERN_NUMBER_DEFAULT',['../lcr4500__defaults_8hpp.html#a9193d7fc94649acf542b0558b51327b7',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fpattern_5fnumber_5fparameter_5fmissing',['LCR4500_PATTERN_NUMBER_PARAMETER_MISSING',['../lcr4500_8hpp.html#afc6b97fe15681c51cdde8f8ea6014f43',1,'lcr4500.hpp']]],
  ['lcr4500_5fpattern_5fpost_5fblack_5ffill_5fdefault',['LCR4500_PATTERN_POST_BLACK_FILL_DEFAULT',['../lcr4500__defaults_8hpp.html#a737780b05bb7a68925160988c32c118d',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fpattern_5fsequence_5fbufferswap_5ftime_5ferror',['LCR4500_PATTERN_SEQUENCE_BUFFERSWAP_TIME_ERROR',['../lcr4500_8hpp.html#adb8d12439d35cb37c34a694cd4dc905b',1,'lcr4500.hpp']]],
  ['lcr4500_5fpattern_5fsequence_5fnot_5fprepared',['LCR4500_PATTERN_SEQUENCE_NOT_PREPARED',['../lcr4500_8hpp.html#a3517b0d37a39c134c48ad65e654ebde7',1,'lcr4500.hpp']]],
  ['lcr4500_5fpattern_5fsequence_5fstart_5ffailed',['LCR4500_PATTERN_SEQUENCE_START_FAILED',['../lcr4500_8hpp.html#ae2948fb1796f1772b51a3156c7214b66',1,'lcr4500.hpp']]],
  ['lcr4500_5fpattern_5fsequence_5fvalidation_5ffailed',['LCR4500_PATTERN_SEQUENCE_VALIDATION_FAILED',['../lcr4500_8hpp.html#aa9c3eecc807380c06016d4fa67c489fa',1,'lcr4500.hpp']]],
  ['lcr4500_5fpattern_5ftrigger_5fdefault',['LCR4500_PATTERN_TRIGGER_DEFAULT',['../lcr4500__defaults_8hpp.html#a53d63f6d87dbf915134e6e8e60fc1344',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fread_5fflash_5fload_5ftiming_5ffailed',['LCR4500_READ_FLASH_LOAD_TIMING_FAILED',['../lcr4500_8hpp.html#ac4f247c56fac4d70d1cecef449bbfdd3',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fdata_5fswap_5ffailed',['LCR4500_SETUP_DATA_SWAP_FAILED',['../lcr4500_8hpp.html#afd545ff43484258b753f82c088d97c50',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fdefault_5fdefault',['LCR4500_SETUP_DEFAULT_DEFAULT',['../lcr4500__defaults_8hpp.html#a7d50ccde221cf1a31004bee14903dd1a',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5fsetup_5fdisplay_5fmode_5ffailed',['LCR4500_SETUP_DISPLAY_MODE_FAILED',['../lcr4500_8hpp.html#ae8279095bf6db519c691e58db2a2245b',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5ffailure',['LCR4500_SETUP_FAILURE',['../lcr4500_8hpp.html#ac87eb783bcdbac1bb0bb4c67b22c0398',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fflash_5fimage_5ffailed',['LCR4500_SETUP_FLASH_IMAGE_FAILED',['../lcr4500_8hpp.html#a2fd99a6ea54e80b919f181759776a8db',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5finput_5fsource_5ffailed',['LCR4500_SETUP_INPUT_SOURCE_FAILED',['../lcr4500_8hpp.html#aa3e20db6e1ee4d31cd197e50d40168ad',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5finvert_5fdata_5ffailed',['LCR4500_SETUP_INVERT_DATA_FAILED',['../lcr4500_8hpp.html#ab22c61674422a30d6fa5b0f23ef2a94f',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5finvert_5fled_5fpwm_5ffailed',['LCR4500_SETUP_INVERT_LED_PWM_FAILED',['../lcr4500_8hpp.html#ad3a4616ab8d7c4ae9a78ef3ed6535587',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fled_5fblue_5fedge_5fdelays_5ffailed',['LCR4500_SETUP_LED_BLUE_EDGE_DELAYS_FAILED',['../lcr4500_8hpp.html#acc8bd3ec3d118867b9caa32e682be23d',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fled_5fcurrents_5ffailed',['LCR4500_SETUP_LED_CURRENTS_FAILED',['../lcr4500_8hpp.html#a5751cfdd08c773c723e93039c4893e44',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fled_5fgreen_5fedge_5fdelays_5ffailed',['LCR4500_SETUP_LED_GREEN_EDGE_DELAYS_FAILED',['../lcr4500_8hpp.html#ab92eb6e976ac11fe2c173709a8a6f33f',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fled_5fred_5fedge_5fdelays_5ffailed',['LCR4500_SETUP_LED_RED_EDGE_DELAYS_FAILED',['../lcr4500_8hpp.html#a095f20c96868e98e4a23a5b51ffa9992',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fled_5fsequence_5fand_5fenables_5ffailed',['LCR4500_SETUP_LED_SEQUENCE_AND_ENABLES_FAILED',['../lcr4500_8hpp.html#ad8add6c83e198927b8b81347cbb33385',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5flong_5faxis_5fflip_5ffailed',['LCR4500_SETUP_LONG_AXIS_FLIP_FAILED',['../lcr4500_8hpp.html#a116e6f18fef69f02ab477227d5eb4a9a',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fparallel_5fport_5fclock_5ffailed',['LCR4500_SETUP_PARALLEL_PORT_CLOCK_FAILED',['../lcr4500_8hpp.html#a202893282adde0485eb3e27eb0a19ad4',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fpower_5fstandby_5ffailed',['LCR4500_SETUP_POWER_STANDBY_FAILED',['../lcr4500_8hpp.html#a9f4469bb24b6df216117248cedcf30fb',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5fshort_5faxis_5fflip_5ffailed',['LCR4500_SETUP_SHORT_AXIS_FLIP_FAILED',['../lcr4500_8hpp.html#a647fa7a0e8583825b3a80e0eff04ba2f',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5ftest_5fpattern_5fcolor_5ffailed',['LCR4500_SETUP_TEST_PATTERN_COLOR_FAILED',['../lcr4500_8hpp.html#a437d3c384a2c2671e58ff9ec3fb3c84a',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5ftest_5fpattern_5ffailed',['LCR4500_SETUP_TEST_PATTERN_FAILED',['../lcr4500_8hpp.html#af5bc265860612567126cda3dd73af25d',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5ftrigger_5finput_5f1_5fdelay_5ffailed',['LCR4500_SETUP_TRIGGER_INPUT_1_DELAY_FAILED',['../lcr4500_8hpp.html#a5479dbb1e821128e7a060f0f4e1d4272',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5ftrigger_5foutput_5f1_5ffailed',['LCR4500_SETUP_TRIGGER_OUTPUT_1_FAILED',['../lcr4500_8hpp.html#a567d48dd59025639df396d12573c513f',1,'lcr4500.hpp']]],
  ['lcr4500_5fsetup_5ftrigger_5foutput_5f2_5ffailed',['LCR4500_SETUP_TRIGGER_OUTPUT_2_FAILED',['../lcr4500_8hpp.html#adeb40c0383abbff3f9d8546ba02dad4d',1,'lcr4500.hpp']]],
  ['lcr4500_5ftrigger_5fin_5f1_5fdelay_5fdefault',['LCR4500_TRIGGER_IN_1_DELAY_DEFAULT',['../lcr4500__defaults_8hpp.html#a512a6640c9eaf4c3d32f35b814949299',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5ftrigger_5fout_5f1_5finvert_5fdefault',['LCR4500_TRIGGER_OUT_1_INVERT_DEFAULT',['../lcr4500__defaults_8hpp.html#ac18a10b699f38d9ff5f7d9c7f400f88b',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5ftrigger_5fout_5f2_5finvert_5fdefault',['LCR4500_TRIGGER_OUT_2_INVERT_DEFAULT',['../lcr4500__defaults_8hpp.html#a6cb4a90ee04b5676509ed2b3f2ddc7ee',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5ftrigger_5fout_5fedge_5fdelay_5fdefault',['LCR4500_TRIGGER_OUT_EDGE_DELAY_DEFAULT',['../lcr4500__defaults_8hpp.html#aba4782a043dcab4acc7dc1e9bb544e4c',1,'lcr4500_defaults.hpp']]],
  ['lcr4500_5ftrue',['LCR4500_TRUE',['../lcr4500_8hpp.html#a4fa744170a175d439a999fbca9297ff8',1,'lcr4500.hpp']]],
  ['lcr4500_5funable_5fto_5fenter_5fprogramming_5fmode',['LCR4500_UNABLE_TO_ENTER_PROGRAMMING_MODE',['../lcr4500_8hpp.html#a9ec517c6332435ba6c2ddd8072ba74dd',1,'lcr4500.hpp']]]
];
