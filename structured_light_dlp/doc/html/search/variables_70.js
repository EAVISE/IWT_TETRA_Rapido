var searchData=
[
  ['parameters',['parameters',['../classdlp_1_1_capture_1_1_sequence.html#aa73ab62ae5f7c63e199e5a77a29b8028',1,'dlp::Capture::Sequence::parameters()'],['../classdlp_1_1_pattern_1_1_sequence.html#abbab22270ee05ed386c744735eac3b54',1,'dlp::Pattern::Sequence::parameters()'],['../classdlp_1_1_pattern.html#a06faf402de8c977d0c67bfe656cf819d',1,'dlp::Pattern::parameters()']]],
  ['pattern_5fcolor_5f',['pattern_color_',['../group___structured_light.html#ga7663d73ab46fb4a3627dd54d0cebd1f5',1,'dlp::StructuredLight']]],
  ['pattern_5fcolumns_5f',['pattern_columns_',['../group___structured_light.html#ga3c79ed73d987c0eadecd155f48861a8d',1,'dlp::StructuredLight']]],
  ['pattern_5fid',['pattern_id',['../classdlp_1_1_capture.html#aa448736f5d0df361f2c8f44d7c86e900',1,'dlp::Capture']]],
  ['pattern_5flut_5fsize',['PATTERN_LUT_SIZE',['../classdlp_1_1_l_cr4500_1_1_settings.html#a1950b64c474338ee92cb968a8d7f00dd',1,'dlp::LCr4500::Settings']]],
  ['pattern_5fnumber',['pattern_number',['../structdlp_1_1_l_c_r4500___l_u_t___entry.html#a43ad20ef2a327dd81c846a7636cee48e',1,'dlp::LCR4500_LUT_Entry']]],
  ['pattern_5forientation_5f',['pattern_orientation_',['../group___structured_light.html#ga220db49fbac6c0ab6a3e57048a0edebe',1,'dlp::StructuredLight']]],
  ['pattern_5frows_5f',['pattern_rows_',['../group___structured_light.html#ga797e6cc84f4d871f4f852920c7c8876b',1,'dlp::StructuredLight']]],
  ['period',['period',['../classdlp_1_1_pattern.html#a4613000aaf658a66dfd36862079c2eb2',1,'dlp::Pattern::period()'],['../structdlp_1_1_l_c_r4500___l_u_t___entry.html#a750d707e9c4dc725327510bc01c01bcc',1,'dlp::LCR4500_LUT_Entry::period()']]],
  ['period_5fdifference_5fminimum',['PERIOD_DIFFERENCE_MINIMUM',['../classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_exposure.html#abddcedda79947b767585d9093c65010b',1,'dlp::LCr4500::Settings::Pattern::Exposure']]],
  ['pixel_5fformat_5f',['pixel_format_',['../classdlp_1_1_p_g___fly_cap2___c.html#ac5e474cae53e44d7eba69a7cb964deb0',1,'dlp::PG_FlyCap2_C']]],
  ['pixel_5fsize_5f',['pixel_size_',['../classdlp_1_1_calibration_1_1_projector.html#ae9ae831045ed7c90c3cee0fa52d56fa3',1,'dlp::Calibration::Projector']]],
  ['projecter_5fset_5f',['projecter_set_',['../classdlp_1_1_calibration_1_1_projector.html#a991f9e2ec2d9928de2f9e516351967ce',1,'dlp::Calibration::Projector']]],
  ['projector_5fmirror_5ftype_5f',['projector_mirror_type_',['../classdlp_1_1_calibration_1_1_projector.html#a6f71dfa656716554ad9d913b955f2726',1,'dlp::Calibration::Projector']]],
  ['projector_5fset_5f',['projector_set_',['../group___structured_light.html#ga303924487429027ad3705c53f4504554',1,'dlp::StructuredLight']]]
];
