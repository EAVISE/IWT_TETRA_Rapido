var searchData=
[
  ['geometry_5fcalibration_5fnot_5fcomplete',['GEOMETRY_CALIBRATION_NOT_COMPLETE',['../geometry_8hpp.html#a3e9312822e6b55996153be400d6f69ef',1,'geometry.hpp']]],
  ['geometry_5fdisparity_5fmap_5fresolution_5finvalid',['GEOMETRY_DISPARITY_MAP_RESOLUTION_INVALID',['../geometry_8hpp.html#a18bdf8c316ab39a6851ddbaa3123422b',1,'geometry.hpp']]],
  ['geometry_5fno_5forigin_5fset',['GEOMETRY_NO_ORIGIN_SET',['../geometry_8hpp.html#a32313e324f5131349696cc154855f892',1,'geometry.hpp']]],
  ['geometry_5fnull_5fpointer',['GEOMETRY_NULL_POINTER',['../geometry_8hpp.html#aa4394afb0383803842826c6ef722fbf1',1,'geometry.hpp']]],
  ['geometry_5forigin_5fray_5fout_5fof_5frange',['GEOMETRY_ORIGIN_RAY_OUT_OF_RANGE',['../geometry_8hpp.html#a9e1a14f8696dd22887d99b2f5fe592a5',1,'geometry.hpp']]],
  ['geometry_5fpoint_5fcloud_5fempty',['GEOMETRY_POINT_CLOUD_EMPTY',['../geometry_8hpp.html#a88a0ec61e8d171284c0bc3afa7bd387f',1,'geometry.hpp']]],
  ['geometry_5fsettings_5fempty',['GEOMETRY_SETTINGS_EMPTY',['../geometry_8hpp.html#a785c117beb5ebd0d9e07729623365530',1,'geometry.hpp']]],
  ['geometry_5fviewport_5fid_5fout_5fof_5frange',['GEOMETRY_VIEWPORT_ID_OUT_OF_RANGE',['../geometry_8hpp.html#af1063a18124914f768ccd992d0377157',1,'geometry.hpp']]],
  ['geometry_5fviewport_5fray_5fout_5fof_5frange',['GEOMETRY_VIEWPORT_RAY_OUT_OF_RANGE',['../geometry_8hpp.html#a8fb5b3f6bba2b0bd999398dde3dc7503',1,'geometry.hpp']]],
  ['gray_5fcode_5fpixel_5fthreshold_5fmissing',['GRAY_CODE_PIXEL_THRESHOLD_MISSING',['../gray__code_8hpp.html#aa2dc9757c7ffbfa6fa3a31904eabfe30',1,'gray_code.hpp']]]
];
