var classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis =
[
    [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a6f5345ac49993b4164416ff4dea9d3d9", [
      [ "FLIP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a6f5345ac49993b4164416ff4dea9d3d9a142dd90a3ccb2553c726d360253dccef", null ],
      [ "NORMAL", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a6f5345ac49993b4164416ff4dea9d3d9af0dde6b11fc060a54eec03e2fee7b9ee", null ]
    ] ],
    [ "ShortAxis", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#add3874838997696f3f5a2992215d437b", null ],
    [ "ShortAxis", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#ad414991c6113ea52048cbc6145e3b432", null ],
    [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#abe1faaf0023a1c12dadb98a04f8ab2c5", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#ab6d122c9c141d9fd752a500f4e0279e3", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a177d4ecb75f45a5c9ebbfd6c5c0867df", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a4a2333800e2efbc70858f3d071e7ebbc", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a92e74b63f32ee2fde0cb23a4113e48c8", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a92e4c05ac4c98f586eb18acb2373f7ca", null ]
];