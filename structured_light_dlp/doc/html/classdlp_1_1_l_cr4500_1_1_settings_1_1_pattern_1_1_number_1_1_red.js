var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red =
[
    [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#a533a79d2c33b6001c44910edc12c0b38", null ],
    [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#a215482c8126ce401c09b29207b9d3d09", null ],
    [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#a58761b7e00e7d6a6f8c61235d89a94f6", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#a2559c39dfe1f6191e636aebe8b104341", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#acafd82ed4f240459e51a526805018f15", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#af8cf81cb0caf12b6bacfc8ede646039e", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#a0f260940f75df734b33eb406a9df6439", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#af82edb4b485864bc831ec6630d735770", null ]
];