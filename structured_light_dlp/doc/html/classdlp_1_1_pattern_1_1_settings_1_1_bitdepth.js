var classdlp_1_1_pattern_1_1_settings_1_1_bitdepth =
[
    [ "Format", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647a", [
      [ "MONO_1BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aa48e0b17914a982ee18877e7ff8e7eeb5", null ],
      [ "MONO_2BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aae51824b3d9b2852ce1bac4d743ba2df1", null ],
      [ "MONO_3BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aa047f202e9129eebc9a560f707b17eac7", null ],
      [ "MONO_4BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aab8fbd4c1a4eed3ec6395f3846b331afe", null ],
      [ "MONO_5BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aa71b60c6df97aea7029233df465ddebd4", null ],
      [ "MONO_6BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aae98f13ec029532226bdfea757bd15fbd", null ],
      [ "MONO_7BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aa5aa669d4ab7039c12bb63fe9ebd1a2fb", null ],
      [ "MONO_8BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aabb4cdc8aee453d3ce523f0458e6fc102", null ],
      [ "RGB_3BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aa9be577c0f6f3dda3c8945d86fb89de4f", null ],
      [ "RGB_6BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aaf7e1e51f5b8baaa9dac8f38bc2c7b339", null ],
      [ "RGB_9BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aa8e4c87f4e7b996f27b8a2e82009bd75f", null ],
      [ "RGB_12BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aa06076ce291ec48c2a0c4d049eea803d8", null ],
      [ "RGB_15BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aac35c485a24feddf071c5f5659a2292fd", null ],
      [ "RGB_18BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aa148742aa89f7a8a808487a67b8e569b4", null ],
      [ "RGB_21BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aa9db82c1b8ebbfdbbd16eafaa6786bc79", null ],
      [ "RGB_24BPP", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aa6d75f33f665756be28bc8c1cc6954e83", null ],
      [ "INVALID", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a1e36f15390268f3047f55a064b5d647aaccc0377a8afbf50e7094f5c23a8af223", null ]
    ] ],
    [ "Bitdepth", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#ae03302143e5f97d05a8f282d7e4de499", null ],
    [ "Bitdepth", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#af3d7f8db53c84bbf5b42109b6a921450", null ],
    [ "Get", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a4cd0a8494669c8f7913ce67390bf827d", null ],
    [ "GetEntryDefault", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#abdd021fc04c14f640d0e064054185886", null ],
    [ "GetEntryName", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a126899ff4fdf5f0916451c2b88916ead", null ],
    [ "GetEntryValue", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a3f109cc279ea7391c96c3a5b3c0e72b8", null ],
    [ "Set", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a6a539a378445883094ca1aa95fcd5de8", null ],
    [ "SetEntryValue", "classdlp_1_1_pattern_1_1_settings_1_1_bitdepth.html#a15e064bb97b27209c42b6e2f2e265986", null ]
];