var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p =
[
    [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05c", [
      [ "G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca433444527c1cfbc74afda0894f2069f7", null ],
      [ "G1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05cad5dc26cb3cff9139dc80eb9e0fed96ea", null ],
      [ "G2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca02ac87fc14907ef96b33a55666618433", null ],
      [ "G3", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca53251e4b2df88a35e79f4563af6b4305", null ],
      [ "G4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05caf399d7f930fd364d21e07308a9305f4d", null ],
      [ "G5", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca3f2a8a6a0215d5f0332cb6474cc67d94", null ],
      [ "G6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca7f7e41aa93269acfc3b0bd9e80049e6e", null ],
      [ "G7", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05caa4a740cb35ada0e67cb383779890cf25", null ],
      [ "R0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05cae9d7f26f3d8acddea71ca3c6be0f794d", null ],
      [ "R1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca539e0d8eaf77629d8fa0d8924a0f03fe", null ],
      [ "R2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca0e5a21716829fe39ea935596848d5171", null ],
      [ "R3", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca395dff42417fe103740bb7ee12ea1b01", null ],
      [ "R4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05caab9c5a9e18622561b40abc2601c4afa9", null ],
      [ "R5", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca3087260da138d14871a9b70743993960", null ],
      [ "R6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca233d5ae2d96a8312402b50975e53a9ca", null ],
      [ "R7", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca284a59cfc52613eca0fb2d20c41ce21a", null ],
      [ "B0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca5bc0789ac911ceb669ce5c37840ff241", null ],
      [ "B1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05caf1975e034efa1cf6400b677533024039", null ],
      [ "B2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05caece4e90e272a3bd8cf1dd958171e4f7d", null ],
      [ "B3", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca553d39b1200542e62a5976b09d3464a4", null ],
      [ "B4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca94bbbe7261340049eb2fd4e8572fcd59", null ],
      [ "B5", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05cab9d279ffbae84bdae891f7592754dcc8", null ],
      [ "B6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05cadca55895190691ff1deb5af081bd9de8", null ],
      [ "B7", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca67bf3ddffc8331dfc959caaef9bcabdc", null ],
      [ "BLACK", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca2f0ba0638fd520529ceb66099a161d3b", null ]
    ] ]
];