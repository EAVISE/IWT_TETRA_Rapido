var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p =
[
    [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html#a83ad4d2e3725dcc736eea29570fc8544", [
      [ "G5_G4_G3_G2_G1_G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html#a83ad4d2e3725dcc736eea29570fc8544a77d197e01fc78a08f836a78f79b46d57", null ],
      [ "R3_R2_R1_R0_G7_G6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html#a83ad4d2e3725dcc736eea29570fc8544a405ca80d70c4338eeadb22948e6c26cf", null ],
      [ "B1_B0_R7_R6_R5_R4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html#a83ad4d2e3725dcc736eea29570fc8544ae3dae2bec9097d8c9be26d40c0e60b94", null ],
      [ "B7_B6_B5_B4_B3_B2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html#a83ad4d2e3725dcc736eea29570fc8544a840b4f1c9b27dce095b641ea5d7b0395", null ]
    ] ]
];