var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p =
[
    [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html#a0f6ff769d177e6efeff6714c5f48e7d0", [
      [ "G5_G4_G3_G2_G1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html#a0f6ff769d177e6efeff6714c5f48e7d0a50b4a28c09632bbbb6ad06e8290d9a69", null ],
      [ "R3_R2_R1_R0_G7", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html#a0f6ff769d177e6efeff6714c5f48e7d0afb54b9a5c59f068f003af90330a2f8fe", null ],
      [ "B1_B0_R7_R6_R5", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html#a0f6ff769d177e6efeff6714c5f48e7d0a51653af92516e793e991dd32c04dfe88", null ],
      [ "B7_B6_B5_B4_B3", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html#a0f6ff769d177e6efeff6714c5f48e7d0a9953e4437b5f8057da1afee6027123d9", null ]
    ] ]
];