var group___camera =
[
    [ "Camera", "classdlp_1_1_camera.html", [
      [ "Settings", "classdlp_1_1_camera_1_1_settings.html", [
        [ "Trigger", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html", [
          [ "Type", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#a655381b0984008f7db802ab7a12d8f54", [
            [ "SOFTWARE_SLAVE", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#a655381b0984008f7db802ab7a12d8f54ad8621ef71dd0f912f8f17735be8932dc", null ],
            [ "HARDWARE_SLAVE", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#a655381b0984008f7db802ab7a12d8f54ad45bdf55f149b4fca6587cb2f51e636f", null ],
            [ "HARDWARE_MASTER", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#a655381b0984008f7db802ab7a12d8f54afd8978e4b222b452f4004e473b100c49", null ],
            [ "INVALID", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#a655381b0984008f7db802ab7a12d8f54accc0377a8afbf50e7094f5c23a8af223", null ]
          ] ],
          [ "Trigger", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#ae1442ec62de79e7ead7550207aaf304b", null ],
          [ "Trigger", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#a0916d290c233eab6408c408dd2142677", null ],
          [ "Get", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#acf62e4989623db4440a005c43c9fd391", null ],
          [ "GetEntryDefault", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#a922ba895f6857582aa34b766c4906b0d", null ],
          [ "GetEntryName", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#ab8489dd598b2bf162ffb679954bb145a", null ],
          [ "GetEntryValue", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#a31af44693371b91d67f5fabb3bc812c5", null ],
          [ "Set", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#afbe960b1336def4a22b51f1b035edeb3", null ],
          [ "SetEntryValue", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#a1cc7cddcc206f8fffbc68000b6e1c4f9", null ],
          [ "Camera", "classdlp_1_1_camera_1_1_settings_1_1_trigger.html#ad8bd9afbbd7af19d996da80e9d25890d", null ]
        ] ]
      ] ],
      [ "Connect", "classdlp_1_1_camera.html#a07afcdc82816bf0fbc1cdaac4a2c03d1", null ],
      [ "Disconnect", "classdlp_1_1_camera.html#a1ee33eecc08f0fee420c68750aa343a7", null ],
      [ "GetCaptureSequence", "classdlp_1_1_camera.html#aa359ba8b17fd85eb95fd9049bc00421c", null ],
      [ "GetColumns", "classdlp_1_1_camera.html#a25fb451eb22f8a68c438b9b53708c5ad", null ],
      [ "GetExposure", "classdlp_1_1_camera.html#ad622a9911244b25b4bc7dc65e3b8fd88", null ],
      [ "GetFrame", "classdlp_1_1_camera.html#a272272ee0d456b46b1bc7adc279268a6", null ],
      [ "GetFrameRate", "classdlp_1_1_camera.html#a3972041ce6149c2abaf9218947422c3b", null ],
      [ "GetID", "classdlp_1_1_camera.html#ab42d64e281c3926de1155566efc75334", null ],
      [ "GetRows", "classdlp_1_1_camera.html#a34820cc1e1755a57c30af38c341b0515", null ],
      [ "GetSetup", "classdlp_1_1_camera.html#a9d717933511579696ad6d49aa8f5e3aa", null ],
      [ "GetTrigger", "classdlp_1_1_camera.html#a43b9bee574823a6395c7438ef810c874", null ],
      [ "isConnected", "classdlp_1_1_camera.html#a38a7e7e36d2918e8297bd2fa6d97c317", null ],
      [ "isSetup", "classdlp_1_1_camera.html#ade0a231aa180867e327030963f78c053", null ],
      [ "isStarted", "classdlp_1_1_camera.html#aa9721d9615891e3c09ab9b3c9c54b1b3", null ],
      [ "SetDebugEnable", "classdlp_1_1_camera.html#a9c9f02c1691ea89bbaccd5bbcd2d2285", null ],
      [ "SetDebugLevel", "classdlp_1_1_camera.html#a9f64b042bd8c8be7de68d0b23a633ef4", null ],
      [ "SetDebugOutput", "classdlp_1_1_camera.html#a1400ea6e49ef0fa49896437dbaa67672", null ],
      [ "Setup", "classdlp_1_1_camera.html#a018706cc4f67a3c1ae67c9b7ee4e8435", null ],
      [ "Start", "classdlp_1_1_camera.html#a13754942911a69ad1550a69454f540f6", null ],
      [ "Stop", "classdlp_1_1_camera.html#a79658569a1908c6ff5c3afed356bfc06", null ],
      [ "debug_", "classdlp_1_1_camera.html#ada002617e188f389b066b3663cc62fd9", null ],
      [ "is_setup_", "classdlp_1_1_camera.html#aac4cdea7919bcd50453b21847fca2556", null ]
    ] ],
    [ "Settings", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings.html", [
      [ "Columns", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_columns.html", [
        [ "Columns", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_columns.html#a76d13f62da0915bd4b18638c66d26917", null ],
        [ "Columns", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_columns.html#a11aae9345637ba9033e09f792b7eed57", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_columns.html#a7011fc49a9ccaf02157a8a7543f33c0e", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_columns.html#a902841ead17ba5a557d1aa2565e07476", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_columns.html#a54c649a96a7d2c453857f68b7c00dec6", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_columns.html#adf57e047fdb3f06345c7f1f6842c07b1", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_columns.html#a2d30d495685a1ea5fb771089bf482445", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_columns.html#a9d8c6f06f10be1b74fddc1063d1f9bf8", null ]
      ] ],
      [ "Exposure", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_exposure.html", [
        [ "Exposure", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_exposure.html#aaadaff21e86e21a3d44147de5c013272", null ],
        [ "Exposure", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_exposure.html#ab8f16f0ef6f74e26b3ab3059792faf76", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_exposure.html#af509b1ff4a9bfa33eef6a5e624078507", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_exposure.html#a985f59f547df27c0fe8a6b6a3a2b97e9", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_exposure.html#ac5a05f2be483077dcefbae46fcdb13eb", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_exposure.html#acb4ecab94ee297e29164dac64a4a446f", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_exposure.html#af39224165c97f5cd82c4220a6bee49e7", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_exposure.html#ac2a1a1eeda82272b983dc48bee1d68ee", null ]
      ] ],
      [ "FrameRate", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_frame_rate.html", [
        [ "FrameRate", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_frame_rate.html#a04123fb8f6b92c4d199759f96b39096f", null ],
        [ "FrameRate", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_frame_rate.html#ad97a68fa594dd0cea11cb6c48943cfca", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_frame_rate.html#a7c6d6ea1c0839ea73e4606e01820146f", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_frame_rate.html#a4c4f08aae3107e39605bfb430be1c384", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_frame_rate.html#a3c9e0e35e07d35d613c3dffc07fd9c2b", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_frame_rate.html#a4ca94c1073e33c3212dfee074adf0fac", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_frame_rate.html#a6dc2d7f23258a4ba31f68a29ba17e5fe", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_frame_rate.html#a81ec26bc40e7a1c6ab9229f423de21e4", null ]
      ] ],
      [ "Gain", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_gain.html", [
        [ "Gain", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_gain.html#a88aca0a052f2310d805e678f18486c7a", null ],
        [ "Gain", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_gain.html#af1f47cb7579c5666a03031903a5e58e6", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_gain.html#a8544369c7993b4ac01a6a03c8eaae513", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_gain.html#aa089c28dee4ffc942b87a4f3b0bf1bb7", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_gain.html#a92e19fb9004eb698cf186c6fc6395d31", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_gain.html#acb8b460033fd17bdd33b9a0d1ca7b805", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_gain.html#aae1c952ed7a52ac6f3afd0f5db2ed606", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_gain.html#a252408145408c0fd342280c7b4c286bb", null ]
      ] ],
      [ "ImageFormat", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html", [
        [ "Options", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#a971640f05ea4eeb5590a84eb4813145e", [
          [ "IMAGE_FORMAT_MONO8", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#a971640f05ea4eeb5590a84eb4813145eae8ce33914411bab314ffee5e38a4364a", null ],
          [ "IMAGE_FORMAT_RGB8", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#a971640f05ea4eeb5590a84eb4813145ea9ec0882fcd361f0255f29384ff14678c", null ],
          [ "INVALID", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#a971640f05ea4eeb5590a84eb4813145eaccc0377a8afbf50e7094f5c23a8af223", null ]
        ] ],
        [ "ImageFormat", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#a60b6f1a3fe17d2ecf1f9acf3cbc65b92", null ],
        [ "ImageFormat", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#a0da86889b6d5c0be8870164438c1e54e", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#a6a66b1752a0b27279d485fefb161e0a4", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#ac89d6bcb3ba8828453e2f63c981b6848", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#ab68f9b287198164e492822a70d39c245", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#a043498bf2a307d8068854e556ae548ab", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#adfb2cf3748eff497c3fbdeb3f8ae8f1d", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_image_format.html#ae63bb3bb19c0700be1597d11fc4325f3", null ]
      ] ],
      [ "OffsetX", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_x.html", [
        [ "OffsetX", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_x.html#a507aa52a132ef95720c24a844811ebda", null ],
        [ "OffsetX", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_x.html#ae4feebc668d82af4ecf8835d44d83c77", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_x.html#afe03192799782bc82da674e556a68568", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_x.html#a64a0b296281256ad809216008895829b", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_x.html#a30f0181a1a6eb813ebdf9cbabd2c76a5", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_x.html#a157b089878f98ab27eac775342e0fd58", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_x.html#a21280465ddd9f28b98a5f16689c7562a", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_x.html#a764415ac4d9108bd60bbcdeb85994d09", null ]
      ] ],
      [ "OffsetY", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_y.html", [
        [ "OffsetY", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_y.html#a2244814ee00d4e183dd7312b5d355175", null ],
        [ "OffsetY", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_y.html#a734aa0e55fd5ca192c181681b6c13729", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_y.html#a5d66429c456f8f0ec7eae8e30d579891", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_y.html#a5b53aadc43273b95b6757a7ad6a87d29", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_y.html#a35c80e76e85033d4e2f3b9dcaadcc202", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_y.html#ad72df498509e02a9361b9c7f74be6ab3", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_y.html#a4e8bfacc6b7d8ea815c2a73d91c4ed8b", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_offset_y.html#a73b4c812d59d11aa834a2df1c2e0544b", null ]
      ] ],
      [ "PixelFormat", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html", [
        [ "Options", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a3462450419390e636c112a627516fd7a", [
          [ "PIXEL_FORMAT_RAW8", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a3462450419390e636c112a627516fd7aa173a2ef25cb634ae77f73990518c1fb5", null ],
          [ "PIXEL_FORMAT_MONO8", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a3462450419390e636c112a627516fd7aa615e299bd84085fb3777165d86934911", null ],
          [ "PIXEL_FORMAT_RGB8", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a3462450419390e636c112a627516fd7aa45cbebdb64f887d097805c769cf7602c", null ],
          [ "INVALID", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a3462450419390e636c112a627516fd7aaccc0377a8afbf50e7094f5c23a8af223", null ]
        ] ],
        [ "PixelFormat", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#acb0055bf3fcb507fda0012709f5c62d8", null ],
        [ "PixelFormat", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a5a502384f2b4cd515fbb7eba44c38f22", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a46e45d6c20b82d4d081917c77fe1923a", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a42560294f775b8d1c2975f68c4e5471e", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a1d660a8a2cc3d472aeb7bf751bd4b352", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a6c95905a3f104ca9ec9aefbaf054a131", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#abc19884c1ee8a6f610c12da47fd4a392", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_pixel_format.html#a628323a69c07fa3a82f0ab61af7aee41", null ]
      ] ],
      [ "Rows", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_rows.html", [
        [ "Rows", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_rows.html#a6810e269eda1dedd85cf80a9aebd92f0", null ],
        [ "Rows", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_rows.html#ad45702aaf1ffe6a7347e12ceb66edfd1", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_rows.html#a2766bfef6c2a3a3650d8e074ff6fec9c", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_rows.html#a2256fec265af80d06826aeb47615d93e", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_rows.html#abf50eaa5b95de3cd8e14ae6dc006c340", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_rows.html#a55482bdd20955990fbaa1801364df618", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_rows.html#a653f8bdecf8f80c84bb78c9982f468e9", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_rows.html#aa63328836daf309c1e2ccb8803dba8ea", null ]
      ] ],
      [ "Sharpness", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_sharpness.html", [
        [ "Sharpness", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_sharpness.html#a2e9761dd6319ad6ee74181194a148eae", null ],
        [ "Sharpness", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_sharpness.html#ae81ca318e0f47c4d6e6710de8df0aa4d", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_sharpness.html#a25a2d6da1a54834e8b1a7ea7b5735b11", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_sharpness.html#ada22e5156fdf5cce32b574d8b795ad91", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_sharpness.html#a4522415b386afe772f5dab7421966102", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_sharpness.html#aa0ce15f816f0c9b70af48b83571b7f36", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_sharpness.html#aae11d9a2f40dc677e478b97f67c78cb7", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_sharpness.html#ac76f9091b15d7655936efef7d386accd", null ]
      ] ],
      [ "ShutterSpeed", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_shutter_speed.html", [
        [ "ShutterSpeed", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_shutter_speed.html#a99598ea98e72f23786b58ef79730a106", null ],
        [ "ShutterSpeed", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_shutter_speed.html#afe9ccd21d23d84f465432cd8c1f2ba1d", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_shutter_speed.html#aa061499fcb8fcdf9238b3c0a3eec7aac", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_shutter_speed.html#a0e37504343831e02535dd023a5009808", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_shutter_speed.html#ab39250667069fb649e8c9370a4c5d431", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_shutter_speed.html#a32b853d4b534931a39b1c866adc3fe4a", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_shutter_speed.html#a5112a1ebdbc9ea9a8ad511d279bdc35a", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_shutter_speed.html#aa619f8b5b5b2baa598bf014b37fc1bd1", null ]
      ] ],
      [ "StrobeEnable", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_enable.html", [
        [ "StrobeEnable", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_enable.html#a584fc77384e51739448280a1aa199d11", null ],
        [ "StrobeEnable", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_enable.html#a32906946f3446b2ad7293133f021a604", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_enable.html#ad3cd4aa1f742c71fd0db01e43d85a93d", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_enable.html#a5d5942eb08e5455d26cc6944d07ae1e9", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_enable.html#a45652cf7a1d5f198f7b328adbe7f5370", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_enable.html#a64764e8657744d0c9482824fc0b8de5f", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_enable.html#a3a431ecb3ff312a44e91e7bd12fad836", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_enable.html#abb2a141a1e1f8accbb68239ca9336d90", null ]
      ] ],
      [ "StrobePolarity", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html", [
        [ "Options", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#a53dea26b298e3c1a3854dc5eb48c9583", [
          [ "LOW", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#a53dea26b298e3c1a3854dc5eb48c9583a41bc94cbd8eebea13ce0491b2ac11b88", null ],
          [ "HIGH", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#a53dea26b298e3c1a3854dc5eb48c9583ab89de3b4b81c4facfac906edf29aec8c", null ],
          [ "INVALID", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#a53dea26b298e3c1a3854dc5eb48c9583accc0377a8afbf50e7094f5c23a8af223", null ]
        ] ],
        [ "StrobePolarity", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#aaea358ca490a754f798b3222cd4b5edd", null ],
        [ "StrobePolarity", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#abbce9cf7434c17a6d0dd0caeb3bd453d", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#a2218e984d0c3991316540c51875f7ef9", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#a0721c0f33017fa6e2a928212d83a5022", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#a4cdaceda5eeb2490b652c37536fe4ece", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#a7440c7e9acc0662e1294d0371a2a1f99", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#ab70c4af0b8a92aa32f075dd593ee2ec8", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_polarity.html#a85f7401c9adbef93e9390d18f3dad65c", null ]
      ] ],
      [ "StrobeSource", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html", [
        [ "Options", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a64bc88aaad2569ce6cb3b6d5e0c57143", [
          [ "GPIO_1", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a64bc88aaad2569ce6cb3b6d5e0c57143ab194974c5b97f28a3cfde54bf285fde3", null ],
          [ "GPIO_2", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a64bc88aaad2569ce6cb3b6d5e0c57143a3e4bed05a2e4e069bb52d131ae37ed2b", null ],
          [ "GPIO_3", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a64bc88aaad2569ce6cb3b6d5e0c57143acf55ad0f6139228d122be42bb79810c3", null ],
          [ "INVALID", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a64bc88aaad2569ce6cb3b6d5e0c57143accc0377a8afbf50e7094f5c23a8af223", null ]
        ] ],
        [ "StrobeSource", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a46d967b75e378982aee7cc8aa6f7c578", null ],
        [ "StrobeSource", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a6f499f0d98fe68c31ed6dfce8f211e9d", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a2fc0e1e10d88e00e8363e4f16286537e", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a902e800a15bc20f60edc3beca65e5118", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a5f6984eea65df15682188a2fab5d3d66", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a412f56803084ff314a3b5e424cce345e", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#a7907539f558d01d58b6940e15104ad5f", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_strobe_source.html#afda3a8f677c801a5dccbb4eb33f09099", null ]
      ] ],
      [ "TriggerEnable", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_enable.html", [
        [ "TriggerEnable", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_enable.html#a9db2f18e4f5b30eb82e1fd4bbc8b002c", null ],
        [ "TriggerEnable", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_enable.html#abf53703ee02419b3ec4fc20420648f22", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_enable.html#a004db66cd4ab69f147f2b9d42a80b3ae", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_enable.html#a4d06414b7b589910b65794d5bd4e4953", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_enable.html#a6c2aa2844dc2de23aa07965270106f34", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_enable.html#ab6f60aacd8df16c5b5a664a96eecd1fc", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_enable.html#adbc3235278dd026ee67197785bddc1b3", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_enable.html#ad02acd0780d1e1abdd01b145058502f2", null ]
      ] ],
      [ "TriggerPolarity", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html", [
        [ "Options", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a57a26d1ad805d0b67f3c6975c13f4ac0", [
          [ "FALLING_EDGE", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a57a26d1ad805d0b67f3c6975c13f4ac0a297f5ea19073f4b51715b029f48d5d52", null ],
          [ "RISING_EDGE", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a57a26d1ad805d0b67f3c6975c13f4ac0ad85243a747ac5537ef0b7adde99c3e98", null ],
          [ "INVALID", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a57a26d1ad805d0b67f3c6975c13f4ac0accc0377a8afbf50e7094f5c23a8af223", null ]
        ] ],
        [ "TriggerPolarity", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a0b91331b08594c5812b548fc0aa9bbee", null ],
        [ "TriggerPolarity", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#ac001334ef03800429be01b24d3415869", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#acb7b212e219b006a8ecb327c6913dbbf", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a0a8ea6d44dc5beb208a998c93f095d18", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#aafa01551b090758a5988f78ea520c13d", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a807f9e791232aefbe83acf07e8ead2d5", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#ad917b6102d1aa03d0f9928750cbfd555", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#aaa50d5460a3b2ff0e6ba094074b9b1ce", null ]
      ] ],
      [ "VideoMode", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html", [
        [ "Options", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a55690836b07006c296c64e83da17a45f", [
          [ "MODE0_FULL_RESOLUTION", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a55690836b07006c296c64e83da17a45fa105529d2fabf159331d9c887047514bd", null ],
          [ "MODE4_HALF_RESOLUTION", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a55690836b07006c296c64e83da17a45fab607392347bec64075dd2463e29b903c", null ],
          [ "INVALID", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a55690836b07006c296c64e83da17a45faccc0377a8afbf50e7094f5c23a8af223", null ]
        ] ],
        [ "VideoMode", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a0561369e603d7c32fdd58012d9e336d6", null ],
        [ "VideoMode", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#acbde74c379c0b5cb840f669401b6c5d8", null ],
        [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a3044a022bd466fe22d71db5b6ec070b5", null ],
        [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a922be2bf9beded2c3ec55499ea53f3bb", null ],
        [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a3e2eed63d2f8a0e2e24f3919b29063d7", null ],
        [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a0b96a9fcf073c1d624943373fcd09bc8", null ],
        [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#aa4a44fe54bd7d2a3206a54c1e1980d15", null ],
        [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_video_mode.html#a9a6c059b67047d5a1259e1b884efddf9", null ]
      ] ]
    ] ]
];