var classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_background =
[
    [ "Background", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_background.html#a93d38f682d9c7dfa57532c2d5f405b9c", null ],
    [ "Background", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_background.html#af849df5fbe20a7a5719bc156519cb844", null ],
    [ "Get", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_background.html#aa50b807c2fd5e1e606e799b9e106a093", null ],
    [ "GetEntryDefault", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_background.html#a4a4b0f68d5cd05837ede3c5df94dd4af", null ],
    [ "GetEntryName", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_background.html#a33c21eb821c8e64b90cb7807b38eade1", null ],
    [ "GetEntryValue", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_background.html#a8b781e0a6fa5ba605e235f6c4064c2d1", null ],
    [ "Set", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_background.html#abc14697ef2308dba48667eb8e4535053", null ],
    [ "SetEntryValue", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_color_1_1_background.html#a7cc0ffef8659849f3420192a71274565", null ]
];