var classdlp_1_1_pattern_1_1_settings_1_1_color =
[
    [ "Type", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651", [
      [ "NONE", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651ab50339a10e1de285ac99d4c3990b8693", null ],
      [ "BLACK", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651a08d0012388564e95c3b4a7407cf04965", null ],
      [ "RED", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651aa2d9547b5d3dd9f05984475f7c926da0", null ],
      [ "GREEN", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651a9de0e5dd94e861317e74964bed179fa0", null ],
      [ "BLUE", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651a1b3e1ee9bff86431dea6b181365ba65f", null ],
      [ "CYAN", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651a344dd8cd533280795b9db82ef0c92749", null ],
      [ "YELLOW", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651a8a568e5f41b7e4da88fe5c4a00aad34e", null ],
      [ "MAGENTA", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651ac634ffea7195608364671ac52ee59a61", null ],
      [ "WHITE", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651ab5bf627e448384cf3a4c35121ca6008d", null ],
      [ "RGB", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651a889574aebacda6bfd3e534e2b49b8028", null ],
      [ "INVALID", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a238a90fb14ee9c43c41cdc0495733651accc0377a8afbf50e7094f5c23a8af223", null ]
    ] ],
    [ "Color", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#aa60edd54a452f19ff9fcda3aaf985732", null ],
    [ "Color", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a2cc8d8d78d272be6c851aa33eb2f90bf", null ],
    [ "Get", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#af17afc944ac98d18378867f4d8c3ae77", null ],
    [ "GetEntryDefault", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#aab8adeef4eecab8689a003c49a3bd18e", null ],
    [ "GetEntryName", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#ae42cdf29c38f29f15d2e8e158a8a729e", null ],
    [ "GetEntryValue", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#a58521ff626ba25b29af39c84b4fe615d", null ],
    [ "Set", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#ac7b6676ac8d8a389f46cbdddd85375ca", null ],
    [ "SetEntryValue", "classdlp_1_1_pattern_1_1_settings_1_1_color.html#ae30d210763163a080c265a86ee81df1b", null ]
];