var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger =
[
    [ "Input1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1.html", [
      [ "Delay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay" ]
    ] ],
    [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode" ],
    [ "Output1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1.html", [
      [ "EdgeDelay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay" ],
      [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert" ]
    ] ],
    [ "Output2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2.html", [
      [ "EdgeDelay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay" ],
      [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert" ]
    ] ],
    [ "Output", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a8ef05969a0efa1c8877913053c4e7dc4", [
      [ "TRIGGER_OUT_1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a8ef05969a0efa1c8877913053c4e7dc4abbe4f9e0c45bb63c54312e59615df3f7", null ],
      [ "TRIGGER_OUT_2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a8ef05969a0efa1c8877913053c4e7dc4ac676ceddfe52beed833bc7696c77cce0", null ]
    ] ],
    [ "Type", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444", [
      [ "INTERNAL", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444afab87a569419a80285a5c8e27a56ec8e", null ],
      [ "EXTERNAL_POSITIVE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444ab8dd1baf165b1285f5846ad6a5ba0762", null ],
      [ "EXTERNAL_NEGATIVE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444a2785c5c8fe43894805e2ca14e54b6f12", null ],
      [ "NONE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444af6ef3b5ab0881b847e5003bf1c0b64f2", null ],
      [ "INVALID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444a839016869abec49b9a541dddd813409b", null ]
    ] ],
    [ "Trigger", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#ac296a72c249497e52f7ca1a0edfa6bd4", null ],
    [ "Trigger", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a572fcd10ef58d92cab8e89b415c1e579", null ],
    [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a8e2516130be16949ab87d8db55c087d6", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a1f0e7da52dcf3bc252e8c2fab54cf7ad", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#ac75bc90f111f22442732ac1fffb71c85", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#abf72616e81e25427b7f46802d1d84248", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a43e18189401447ef5201eac93318a2cc", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#ad28a5d1cb7b8b7c7e9afb78b115dc089", null ]
];