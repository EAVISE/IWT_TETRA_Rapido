var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p =
[
    [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37", [
      [ "G3_G2_G1_G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37af57b1c3156b461f6e9cf419d353ce177", null ],
      [ "G7_G6_G5_G4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37a67300b7f6ca98e08370154b623dbdf87", null ],
      [ "R3_R2_R1_R0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37ab7e2977b557dffb6b4f1d3ee73803d43", null ],
      [ "R7_R6_R5_R4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37a43a286bfe6e8855afb6d75389245bd5d", null ],
      [ "B3_B2_B1_B0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37af907b6c4fcf9a936d94a6c73a94986f0", null ],
      [ "B7_B6_B5_B4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37a31c277350dbdcdf717d456468c59fd7c", null ]
    ] ]
];