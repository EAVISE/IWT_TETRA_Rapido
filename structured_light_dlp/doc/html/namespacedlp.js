var namespacedlp =
[
    [ "File", "namespacedlp_1_1_file.html", null ],
    [ "Number", "namespacedlp_1_1_number.html", null ],
    [ "String", "namespacedlp_1_1_string.html", null ],
    [ "Time", "namespacedlp_1_1_time.html", "namespacedlp_1_1_time" ],
    [ "Calibration", "classdlp_1_1_calibration.html", null ],
    [ "Camera", "classdlp_1_1_camera.html", "classdlp_1_1_camera" ],
    [ "Capture", "classdlp_1_1_capture.html", "classdlp_1_1_capture" ],
    [ "Debug", "classdlp_1_1_debug.html", "classdlp_1_1_debug" ],
    [ "DisparityMap", "classdlp_1_1_disparity_map.html", "classdlp_1_1_disparity_map" ],
    [ "DLP_Platform", "classdlp_1_1_d_l_p___platform.html", "classdlp_1_1_d_l_p___platform" ],
    [ "Geometry", "classdlp_1_1_geometry.html", "classdlp_1_1_geometry" ],
    [ "GrayCode", "classdlp_1_1_gray_code.html", "classdlp_1_1_gray_code" ],
    [ "Image", "classdlp_1_1_image.html", "classdlp_1_1_image" ],
    [ "LCr4500", "classdlp_1_1_l_cr4500.html", "classdlp_1_1_l_cr4500" ],
    [ "LCR4500_LUT_Entry", "structdlp_1_1_l_c_r4500___l_u_t___entry.html", "structdlp_1_1_l_c_r4500___l_u_t___entry" ],
    [ "Module", "classdlp_1_1_module.html", "classdlp_1_1_module" ],
    [ "Parameters", "classdlp_1_1_parameters.html", "classdlp_1_1_parameters" ],
    [ "Pattern", "classdlp_1_1_pattern.html", "classdlp_1_1_pattern" ],
    [ "PG_FlyCap2_C", "classdlp_1_1_p_g___fly_cap2___c.html", "classdlp_1_1_p_g___fly_cap2___c" ],
    [ "PixelRGB", "classdlp_1_1_pixel_r_g_b.html", "classdlp_1_1_pixel_r_g_b" ],
    [ "Point", "classdlp_1_1_point.html", "classdlp_1_1_point" ],
    [ "ReturnCode", "classdlp_1_1_return_code.html", "classdlp_1_1_return_code" ],
    [ "StructuredLight", "classdlp_1_1_structured_light.html", "classdlp_1_1_structured_light" ]
];