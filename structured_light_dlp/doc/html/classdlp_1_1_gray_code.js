var classdlp_1_1_gray_code =
[
    [ "Settings", "classdlp_1_1_gray_code_1_1_settings.html", [
      [ "Pixel", "classdlp_1_1_gray_code_1_1_settings_1_1_pixel.html", [
        [ "Threshold", "classdlp_1_1_gray_code_1_1_settings_1_1_pixel_1_1_threshold.html", "classdlp_1_1_gray_code_1_1_settings_1_1_pixel_1_1_threshold" ]
      ] ]
    ] ],
    [ "GrayCode", "classdlp_1_1_gray_code.html#ae0cad8c99bcd3f8ddef6ea8c746c5331", null ],
    [ "~GrayCode", "classdlp_1_1_gray_code.html#ae320261a71c3aa19c9dccb831d213309", null ],
    [ "DecodeCaptureSequence", "classdlp_1_1_gray_code.html#a2b31944ae8bbeb8ab548731eff3badca", null ],
    [ "GeneratePatternSequence", "classdlp_1_1_gray_code.html#a74a73a068ce638267f1ea52802d251da", null ],
    [ "GetSetup", "classdlp_1_1_gray_code.html#ada65e938ff59bc751dfe098c6c640218", null ],
    [ "GetTotalPatternCount", "classdlp_1_1_gray_code.html#gabc01c4eed1a9ea6dacee122ab8f89ddd", null ],
    [ "isSetup", "classdlp_1_1_gray_code.html#ade0a231aa180867e327030963f78c053", null ],
    [ "SetDebugEnable", "classdlp_1_1_gray_code.html#a9c9f02c1691ea89bbaccd5bbcd2d2285", null ],
    [ "SetDebugLevel", "classdlp_1_1_gray_code.html#a9f64b042bd8c8be7de68d0b23a633ef4", null ],
    [ "SetDebugOutput", "classdlp_1_1_gray_code.html#a1400ea6e49ef0fa49896437dbaa67672", null ],
    [ "SetDlpPlatform", "classdlp_1_1_gray_code.html#ga018a4bd931412647133f0f4d3dd4cfb9", null ],
    [ "Setup", "classdlp_1_1_gray_code.html#a01981c202b9d736424db870e9e80dbd0", null ],
    [ "debug_", "classdlp_1_1_gray_code.html#ada002617e188f389b066b3663cc62fd9", null ],
    [ "disparity_map_", "classdlp_1_1_gray_code.html#gafdb643ffaa77626e331f1f0b812d5e0d", null ],
    [ "is_decoded_", "classdlp_1_1_gray_code.html#ga24b590f3f97c196823e8c77313b1572c", null ],
    [ "is_setup_", "classdlp_1_1_gray_code.html#aac4cdea7919bcd50453b21847fca2556", null ],
    [ "pattern_color_", "classdlp_1_1_gray_code.html#ga7663d73ab46fb4a3627dd54d0cebd1f5", null ],
    [ "pattern_columns_", "classdlp_1_1_gray_code.html#ga3c79ed73d987c0eadecd155f48861a8d", null ],
    [ "pattern_orientation_", "classdlp_1_1_gray_code.html#ga220db49fbac6c0ab6a3e57048a0edebe", null ],
    [ "pattern_rows_", "classdlp_1_1_gray_code.html#ga797e6cc84f4d871f4f852920c7c8876b", null ],
    [ "projector_set_", "classdlp_1_1_gray_code.html#ga303924487429027ad3705c53f4504554", null ],
    [ "sequence_count_", "classdlp_1_1_gray_code.html#ga0e862b5b06843d1fb980fdb04e2d5e9e", null ],
    [ "sequence_count_total_", "classdlp_1_1_gray_code.html#ga1f47dbe7038449a92cec3bbcca4cab00", null ],
    [ "sequence_include_inverted_", "classdlp_1_1_gray_code.html#ga41daa649d9126a033e3e21099bf3c684", null ]
];