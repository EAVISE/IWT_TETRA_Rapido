var classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity =
[
    [ "Options", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a57a26d1ad805d0b67f3c6975c13f4ac0", [
      [ "FALLING_EDGE", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a57a26d1ad805d0b67f3c6975c13f4ac0a297f5ea19073f4b51715b029f48d5d52", null ],
      [ "RISING_EDGE", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a57a26d1ad805d0b67f3c6975c13f4ac0ad85243a747ac5537ef0b7adde99c3e98", null ],
      [ "INVALID", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a57a26d1ad805d0b67f3c6975c13f4ac0accc0377a8afbf50e7094f5c23a8af223", null ]
    ] ],
    [ "TriggerPolarity", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a0b91331b08594c5812b548fc0aa9bbee", null ],
    [ "TriggerPolarity", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#ac001334ef03800429be01b24d3415869", null ],
    [ "Get", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#acb7b212e219b006a8ecb327c6913dbbf", null ],
    [ "GetEntryDefault", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a0a8ea6d44dc5beb208a998c93f095d18", null ],
    [ "GetEntryName", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#aafa01551b090758a5988f78ea520c13d", null ],
    [ "GetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#a807f9e791232aefbe83acf07e8ead2d5", null ],
    [ "Set", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#ad917b6102d1aa03d0f9928750cbfd555", null ],
    [ "SetEntryValue", "classdlp_1_1_p_g___fly_cap2___c_1_1_settings_1_1_trigger_polarity.html#aaa50d5460a3b2ff0e6ba094074b9b1ce", null ]
];