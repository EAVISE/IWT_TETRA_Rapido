var classdlp_1_1_capture =
[
    [ "Settings", "classdlp_1_1_capture_1_1_settings.html", [
      [ "Data", "classdlp_1_1_capture_1_1_settings_1_1_data.html", "classdlp_1_1_capture_1_1_settings_1_1_data" ]
    ] ],
    [ "Capture", "classdlp_1_1_capture.html#a1b2e63505758b6439f2d7bf376aeeb91", null ],
    [ "~Capture", "classdlp_1_1_capture.html#aeb583d767f2e0cecff09cdbea7d3d98c", null ],
    [ "camera_id", "classdlp_1_1_capture.html#a92840841b8d8d26d392937872d80a94f", null ],
    [ "image_data", "classdlp_1_1_capture.html#a2750fbec8b9c54ccfd82039e024c5b58", null ],
    [ "image_file", "classdlp_1_1_capture.html#a880c53dc44bab9dac3fe9afa234a0a3e", null ],
    [ "image_type", "classdlp_1_1_capture.html#a32731761c9d232e107bba6e69de3e1fd", null ],
    [ "pattern_id", "classdlp_1_1_capture.html#aa448736f5d0df361f2c8f44d7c86e900", null ]
];