var group___d_l_p___platforms =
[
    [ "LCr4500", "classdlp_1_1_l_cr4500.html", [
      [ "Settings", "classdlp_1_1_l_cr4500_1_1_settings.html", [
        [ "DisplayMode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html", [
          [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#a56051146817e79ed7f5b2e51e15889da", [
            [ "PATTERN_SEQUENCE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#a56051146817e79ed7f5b2e51e15889daafa42a611655701fdcdfe32778754f518", null ],
            [ "VIDEO", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#a56051146817e79ed7f5b2e51e15889daa31af3e3ded0cca6436bdeb9463891cf0", null ]
          ] ],
          [ "DisplayMode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#a5844348451bf913e6a5352ab2409e96d", null ],
          [ "DisplayMode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#aad10a18459d55804890735e65c30cf4d", null ],
          [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#aa7f32dcd7f8fe22dd45ff9463257833b", null ],
          [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#afd20f74064c9297dbaa1c25b87fbbdff", null ],
          [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#a2634a0da6f156cbc35c42eeae36afaf9", null ],
          [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#ad2736a82aef86d3d5ce99648faf215d7", null ],
          [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#afcde4bb62e2c659aa1ffb36be8c084fe", null ],
          [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_display_mode.html#aa2afb0706dbe08f6ad81de5fb2476bfa", null ]
        ] ],
        [ "File", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file.html", [
          [ "DLPC350_Firmware", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware.html", [
            [ "DLPC350_Firmware", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware.html#a446b5ea641c3d683559db1e22852cab0", null ],
            [ "DLPC350_Firmware", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware.html#a292cfd790cacfad8389fc8e32767e5a9", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware.html#a299994bdcb8f1d9f77859df2de3bb85f", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware.html#a75260135ee38f7e3e9a39c88c1845587", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware.html#a4eb360afc2d95e1652f0964cb77b8775", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware.html#a08a6713c0c73e2c8f8c932233e7eacdf", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware.html#ad1a52ef890292847e3d91aed906b8d6a", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___firmware.html#aec71ab06232fbe1be5ea18e32ae8481e", null ]
          ] ],
          [ "DLPC350_FlashParameters", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters.html", [
            [ "DLPC350_FlashParameters", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters.html#a108a34b15cd919d5442f60de2fac4c37", null ],
            [ "DLPC350_FlashParameters", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters.html#a1a18bfd7c3965ce06948da6296f8e9e1", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters.html#a1cd8c500beddb8238421da89cb934cf1", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters.html#abc3d37edb1359515b06830baeaea32e9", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters.html#ac860359d2c9fd21501f3e100801949d9", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters.html#a55467fefaeb432391710777a890df24f", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters.html#aca3b8038eeafa404d26bf1602a030400", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_d_l_p_c350___flash_parameters.html#a3dcb3870c345365e520077f32befb650", null ]
          ] ],
          [ "PatternSequenceFirmware", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware.html", [
            [ "PatternSequenceFirmware", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware.html#a3f0ffa92e4b5b8984d634574bf60955b", null ],
            [ "PatternSequenceFirmware", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware.html#a804d87009735e47f6ee416b29aee90b6", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware.html#a09367d0194863f625e43d7bb9743f56d", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware.html#a828018f39faddd0b7d1ea81d0be94210", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware.html#addc0225d3bb909b4ff955a421409fde0", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware.html#a5ef4817fda7437bd7a567e2ae8332273", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware.html#aa0bf0f3d14f20069adccfca17e3294a3", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_file_1_1_pattern_sequence_firmware.html#a4bddf85e15f1256274307dae4b99da49", null ]
          ] ]
        ] ],
        [ "FlashImage", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image.html", [
          [ "FlashImage", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image.html#af5f27ae493783b89911ef0f23919f127", null ],
          [ "FlashImage", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image.html#a2722f76866de379d74d6a01a99e14748", null ],
          [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image.html#a696d9578a8fa44c9b390a62862830a53", null ],
          [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image.html#a8e206e421e850f5fa6aafd2abb805e64", null ],
          [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image.html#aa7aa82547f9c3d0a635dc53d926f73eb", null ],
          [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image.html#a37136c5f66e768349c884ce801f4d7bc", null ],
          [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image.html#a75f6668a67bf12e6a3693a28d5ad0c08", null ],
          [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_flash_image.html#aab8dbc0e3f95fab00008f01a2ca9d0af", null ]
        ] ],
        [ "ImageFlip", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip.html", [
          [ "LongAxis", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html", [
            [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#a127b20e4ba5ee2610f3259d602bd86f0", [
              [ "FLIP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#a127b20e4ba5ee2610f3259d602bd86f0a9a9229c026bc50b26330b70492b733f6", null ],
              [ "NORMAL", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#a127b20e4ba5ee2610f3259d602bd86f0a5d8de3859ba11aef0bb404b4e520b9dc", null ]
            ] ],
            [ "LongAxis", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#a5b586a71440b94698f22eacd25cd0f51", null ],
            [ "LongAxis", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#aef1e590266984af81ba718d278a5b6d7", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#ab040f9fc0972355834a8bff5bf7d4514", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#acbb8e8e79a633247058971b619e90150", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#a824d48de6b93b9b15b5fa5433f971010", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#ab2da4ca4a16d2dbdfecdf309cf400309", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#ad0c3654bcdd3354a23ea2ee057af3fc4", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_long_axis.html#a35dc9a13b7d4fcdab537dab5df5c0b69", null ]
          ] ],
          [ "ShortAxis", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html", [
            [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a6f5345ac49993b4164416ff4dea9d3d9", [
              [ "FLIP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a6f5345ac49993b4164416ff4dea9d3d9a142dd90a3ccb2553c726d360253dccef", null ],
              [ "NORMAL", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a6f5345ac49993b4164416ff4dea9d3d9af0dde6b11fc060a54eec03e2fee7b9ee", null ]
            ] ],
            [ "ShortAxis", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#add3874838997696f3f5a2992215d437b", null ],
            [ "ShortAxis", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#ad414991c6113ea52048cbc6145e3b432", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#abe1faaf0023a1c12dadb98a04f8ab2c5", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#ab6d122c9c141d9fd752a500f4e0279e3", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a177d4ecb75f45a5c9ebbfd6c5c0867df", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a4a2333800e2efbc70858f3d071e7ebbc", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a92e74b63f32ee2fde0cb23a4113e48c8", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip_1_1_short_axis.html#a92e4c05ac4c98f586eb18acb2373f7ca", null ]
          ] ],
          [ "EastWest", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip.html#a4edd5333b79bcd5b4332565d20ed8fd2", null ],
          [ "NorthSouth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_image_flip.html#a1264fec4eba92b80d81405a7f2d3d49d", null ]
        ] ],
        [ "InputDataSwap", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html", [
          [ "DataSubChannels", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#afc46054dab1bd036e4668c750cc05f3f", [
            [ "ABC_TO_ABC", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#afc46054dab1bd036e4668c750cc05f3faba267aa29da62d08c81f18ddd807f752", null ],
            [ "ABC_TO_CAB", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#afc46054dab1bd036e4668c750cc05f3fa561acfa5c67899e3238c0534d31e3237", null ],
            [ "ABC_TO_BCA", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#afc46054dab1bd036e4668c750cc05f3fa532ee6948856f60f393941fe7b3b3bae", null ],
            [ "ABC_TO_ACB", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#afc46054dab1bd036e4668c750cc05f3fab6bd44c0cad0b9e7040b8476e953ccc1", null ],
            [ "ABC_TO_BAC", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#afc46054dab1bd036e4668c750cc05f3fab08457c84eb2ed8901e4dba7554d1ba4", null ],
            [ "ABC_TO_CBA", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#afc46054dab1bd036e4668c750cc05f3facfcbc28f79a0543feb9fa3e94a4f0fe3", null ],
            [ "INVALID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#afc46054dab1bd036e4668c750cc05f3fade03abb1030ea11a1c07e51505912169", null ]
          ] ],
          [ "Port", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#a7cf01c42520bcefb9a17b95cc1d7f34d", [
            [ "PARALLEL_INTERFACE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#a7cf01c42520bcefb9a17b95cc1d7f34da29877968307094b982c9690757832784", null ],
            [ "FPD_LINK", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#a7cf01c42520bcefb9a17b95cc1d7f34dae817ffa041d3f47fdfc669483bdbd1a3", null ]
          ] ],
          [ "InputDataSwap", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#aa1f91b7d6ab2b8f4b615a4f5ac3c84b2", null ],
          [ "InputDataSwap", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#a89c74e866bb2e8cdd7646fbcb6d8426a", null ],
          [ "GetDataChannel", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#a9e7b2785942a2114042665c6990da822", null ],
          [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#abc1f994027b0b9555a21d2c48a165f0a", null ],
          [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#a9628cf7d88ea70aa1a5b36ebc0a17396", null ],
          [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#a6cfd12aa4e89470bef17d403b422bcba", null ],
          [ "GetPort", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#a9731ee1463ccf49a67b78a1f04d1434a", null ],
          [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#affc64de21063700ec4ee4f3bdd85c960", null ],
          [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_data_swap.html#a3732342e537eeb70113ce2c01e0114bb", null ]
        ] ],
        [ "InputSource", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html", [
          [ "Source", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#a995c7434302dfb87a60d539823e8aaae", [
            [ "PARALLEL_INTERFACE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#a995c7434302dfb87a60d539823e8aaaea107bb96c6e348b8b059a73656235ad9b", null ],
            [ "INTERNAL_TEST_PATTERNS", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#a995c7434302dfb87a60d539823e8aaaeac08eba2280db34d49db54960f4cb30e9", null ],
            [ "FLASH_IMAGES", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#a995c7434302dfb87a60d539823e8aaaea6f264c3e2406721c5e6d3651fbfb4669", null ],
            [ "FPD_LINK", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#a995c7434302dfb87a60d539823e8aaaea0a40ba39fe536f2341f5821e4139d6a0", null ]
          ] ],
          [ "InputSource", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#a183c6b71ba950e23fc2b3bc58d770177", null ],
          [ "InputSource", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#aef495fc4f6b15c65f496a41f3985e341", null ],
          [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#aeb7f28076bb2f90e0bdbb7982ee0f067", null ],
          [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#aa19e1b983f8b4adb76aa8a8408885f9f", null ],
          [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#ac2bbdde28e674e9fda42b96a08e1ead3", null ],
          [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#a0af8710a4b6408414aa55811f48eff36", null ],
          [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#a0707058b1c3444a7edc401f68197afbc", null ],
          [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_input_source.html#ae59c277d2f1321fb197059f9bfaee867", null ]
        ] ],
        [ "InvertData", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data.html", [
          [ "InvertData", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data.html#a87f190023b9c917389ef29ef1f18ab8c", null ],
          [ "InvertData", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data.html#adc4d8da76d57e832eeb5af949bf78e4c", null ],
          [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data.html#aa82a6dc3e96246a7adb0b58879e56fc4", null ],
          [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data.html#ab8cba74e7a7d40eb5a9717822afb794e", null ],
          [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data.html#a7ff886469b65c8f914ac02867678a619", null ],
          [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data.html#aa8b5a91cb35dc31b71a91b0f18869132", null ],
          [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data.html#a51b55a2b7a2a8a057e72dc255baea310", null ],
          [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_invert_data.html#a91d9c158405ae2f5667978855f490098", null ]
        ] ],
        [ "Led", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led.html", [
          [ "Blue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue.html", [
            [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current.html", [
              [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current.html#a3c8a01f086565c4f75d84fc9abde582c", null ],
              [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current.html#a47819a8c313863781e07508bf5904255", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current.html#a6fc1212a5f4696fa3efdf3478298db6c", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current.html#adaf691036614ce485fd5f48fb242c278", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current.html#aa281e8d6f05e82acc5401f093399f3c5", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current.html#a56670292e295ef72d1e816b0861d7465", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current.html#a9f3db6afef67e18b2b05f65c86fc9efc", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_current.html#a2a7c1a8ae47b4c9ea43844a9ec0cbf2c", null ]
            ] ],
            [ "EdgeDelay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay.html", [
              [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling.html", [
                [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling.html#afe3cced016aa78a6331257471b7f65d1", null ],
                [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling.html#ae8df1ea482c09b5c57ff79c7ed1a62bd", null ],
                [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling.html#a12c70441e64e43d0727a7f17e8e8ac90", null ],
                [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling.html#a3a66eaedf042e20a2af7958bfbc396ae", null ],
                [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling.html#af1cffd6ce2e416c22bbc4a3ab0bb08d8", null ],
                [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling.html#a6d82976516b14cbd6d4e2fbf1cf1b4b6", null ],
                [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling.html#a6275a2d42ebb15398fb1ce5f449a4686", null ],
                [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_falling.html#a61bc7e636806100d9c19455161952b9c", null ]
              ] ],
              [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising.html", [
                [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising.html#a9e6562aaddb3d42ede21379a58fb4bae", null ],
                [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising.html#ad39ea84ff9fa2e0294641a5ef7af8b67", null ],
                [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising.html#ac28f94e5d9f5f5623b0a9e8b1d763dbe", null ],
                [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising.html#a557b013083a979205dcb0b57316bb020", null ],
                [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising.html#a1d1adbea0aa27223ddd4b27f550151c1", null ],
                [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising.html#af0b1fc298e4194a7835bc01e5dd36e6c", null ],
                [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising.html#a118e04f1efc50fb49f28d5c94d09a957", null ],
                [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_edge_delay_1_1_rising.html#a76dc2cd7723625f57cf8823b690d229f", null ]
              ] ]
            ] ],
            [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable.html", [
              [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable.html#a41826751d45ad14765fd5a31baa9010e", null ],
              [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable.html#affe5e4c2afceb4b41423f82f016e93c2", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable.html#a2b975ce55f6f9cd5df3e6c853e2b8b76", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable.html#ab8fb95d20fc67c1ba0865c985c3fc0de", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable.html#afb6521127bdf604730f35d90df0eb7c6", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable.html#a47db81bac227b33f187aee38e3af28a3", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable.html#a9b2f0c55df7cc86cdfb5c54e40ba3445", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_blue_1_1_enable.html#ac1982009f1641467a81f86ac2a0415e7", null ]
            ] ]
          ] ],
          [ "Green", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green.html", [
            [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current.html", [
              [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current.html#ad01f49094a131758f30cdcd374d4cfd7", null ],
              [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current.html#a15df9b3c3373cc9e4b6b6f87fd60017f", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current.html#a59bab9e858a44aec801bdda718c8537a", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current.html#abff2b2fd8a08bccfa1e23a2401484dcd", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current.html#ac8c9adaf508443a3a42323b1fc029340", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current.html#a6193c4a7fc870a21e9b13d72c82d13d2", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current.html#a5f3d48e9793dd35fa1e929d435208f81", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_current.html#a04d28412dff3ed12b7f8cd835401d8e2", null ]
            ] ],
            [ "EdgeDelay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay.html", [
              [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling.html", [
                [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling.html#ad7ff8f10ea0104a07997a1d277cb5863", null ],
                [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling.html#aac043add0468598a33e4191e6fcd2356", null ],
                [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling.html#a7de6cb4bcdf219d3651d40b764ae25c3", null ],
                [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling.html#abf0bfac67bac2768f5ec217482aaeb50", null ],
                [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling.html#ab175bcee474aec38766d49c5c8a7cc14", null ],
                [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling.html#adae1ea4308b85198390f6c9e31a86902", null ],
                [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling.html#a1468eb55a25126bfdc89e0149df6d0ce", null ],
                [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_falling.html#ae70a16e1126f242a85366aeb0a237ea6", null ]
              ] ],
              [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising.html", [
                [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising.html#aa0b35f496b26e71b48b144b6a5d6c87e", null ],
                [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising.html#a2dd6f64e47e04e121b27c0f52121a763", null ],
                [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising.html#a2e9377364598859fba2daac01802b281", null ],
                [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising.html#a6acda2c388a9f7e6d5be043ed4fe50b0", null ],
                [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising.html#a4917b9e9cc1169a1f4fca94955f7248e", null ],
                [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising.html#a9236c6be4b230e6523bc22b02600bfe8", null ],
                [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising.html#a8e430f975798ca3c4d1a9908685ab8f8", null ],
                [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_edge_delay_1_1_rising.html#a0603298c3b064dce79964c7c9d687143", null ]
              ] ]
            ] ],
            [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable.html", [
              [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable.html#ab2c62b9d6c8e654307512d677eba846c", null ],
              [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable.html#a524d641182c440ccdcc1640a416c1d12", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable.html#a7f74f10bf37a0d18d51a8aa1bf490a3d", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable.html#ab32e2559a99167771782cbb87a4db296", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable.html#a21035f09d12e5cfaffabd8866e32d008", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable.html#ab81b654e85745bcac8b8b8030876459e", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable.html#ab315eb4833f0911d2d849d455774321c", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_green_1_1_enable.html#a87b5e06d1e2c38787126af37ea148ad5", null ]
            ] ]
          ] ],
          [ "InvertPWM", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m.html", [
            [ "InvertPWM", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m.html#a770ab1b4e892803b55cb2574c9aed1ee", null ],
            [ "InvertPWM", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m.html#a0c6a6f5cc11a20684acd2e95171400c7", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m.html#af10fe80438e75bb395da5e59500ccd63", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m.html#a38c170efea56f9b8b61163f5329e88c3", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m.html#a22486898ff9eb4bb0586495fb17f918a", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m.html#a75fdf578cfcb32fc636ba4e3ce0ba907", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m.html#ad459ecd59e5955f5cbb66dae8389e8bc", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_invert_p_w_m.html#ab26deec9e6222312670eaf68d4d64825", null ]
          ] ],
          [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red.html", [
            [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current.html", [
              [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current.html#a809cf0880b5573bb67ab051748690948", null ],
              [ "Current", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current.html#a4ccc974b13f6e595c9ded4235b869820", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current.html#a3f50a8ebe7b2fd4bb74c6419f58df3a7", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current.html#a5416f0c1320c4ba82bf5123511b858ea", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current.html#a916d527c7a1a919c8bb04564961ebdc5", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current.html#af6c9aae301089dc81b3ce080b562a7af", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current.html#a99b640d10bd321f68cb09caa58d28ea4", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_current.html#a77629c1b6400d7e4e5e8d9d147e813c0", null ]
            ] ],
            [ "EdgeDelay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay.html", [
              [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling.html", [
                [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling.html#aad73cb8ccf8000bd12d65c659251ce81", null ],
                [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling.html#a5fe2c213575717de99c926d3f1289327", null ],
                [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling.html#a1d6ff3f603703f73f1d345670dc9fc7f", null ],
                [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling.html#ac770bcfa0f3d22785a47d0ff799239ec", null ],
                [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling.html#ac3e6ef5ad417a0f37f7455398391310e", null ],
                [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling.html#abecdc1c8d33c817663887d925fba9642", null ],
                [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling.html#a40cbf75ed37744503fcae58cc4940bdd", null ],
                [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_falling.html#a7ee28d897e78f7c371e4c2f244690ff9", null ]
              ] ],
              [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising.html", [
                [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising.html#ae98e29f534ad8f9615e038251aababd1", null ],
                [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising.html#a0e3cea3a987b6e3af1f75daf15f75500", null ],
                [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising.html#a83e33cda7f5aabdacaeee978b75e7d1a", null ],
                [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising.html#adfca56b9bb02e74c5ae10ea052587e81", null ],
                [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising.html#acc457480653576bdcc3d3181f7054acc", null ],
                [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising.html#ac04dd24c03cd592c66d1692758211f28", null ],
                [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising.html#a12b36cb4eca311bb2761926d7c93f288", null ],
                [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_edge_delay_1_1_rising.html#afea274c5aed3010ae4d85a7bc6298cad", null ]
              ] ]
            ] ],
            [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable.html", [
              [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable.html#a59935d5d1121c5445d7006a66f5fe969", null ],
              [ "Enable", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable.html#ae92cd54dbd0708c5ac5823807b34e51e", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable.html#ad0cc24db86e6051219a91748867067d7", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable.html#abcd909b36440999dd8bd40038b2acdd5", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable.html#a913ce79e1112542932d072f0ae0a5810", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable.html#aa7e06cb3d59f491eab1c74da3a78009b", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable.html#afebd3c13ad2e46418f1372d320bec5e4", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_red_1_1_enable.html#a377bd85f0f224c656fb6d5fa7166a4cd", null ]
            ] ]
          ] ],
          [ "Sequence", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html", [
            [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#a0bb27f2036317bbbff5e1c1595f516e6", [
              [ "AUTO", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#a0bb27f2036317bbbff5e1c1595f516e6a8d351eed15f063882a1c0620592f5b73", null ],
              [ "MANUAL", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#a0bb27f2036317bbbff5e1c1595f516e6ab432d04f79ec8bcaa9245fe71c309658", null ]
            ] ],
            [ "Sequence", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#a26a75df350a94c00f96388fb58687378", null ],
            [ "Sequence", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#a86cc48b5f1776ee8637795b4ea5a6b6d", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#a2cc2e077afe5ee2b79b64ef62d97f1b9", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#ac64a0c396a321f7dfffc7276aaa9c035", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#a21287768e80875fa4006f163b40fc2fa", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#a358f66e07e4b675bb1a2d166e459c35e", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#a36fd1d43f06b0060181b977ac1f6819d", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_led_1_1_sequence.html#a10892f7a0318ebd2569f23aa2a961a38", null ]
          ] ]
        ] ],
        [ "ParallelClockSelect", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html", [
          [ "ClockPort", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#ab1739538444a8363f3704747da2d0876", [
            [ "PORT_1_CLOCK_A", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#ab1739538444a8363f3704747da2d0876a188e2da7b125052a807fdb5ebad6f22d", null ],
            [ "PORT_1_CLOCK_B", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#ab1739538444a8363f3704747da2d0876a26019d491d9e92039d49a7f835cc4684", null ],
            [ "PORT_1_CLOCK_C", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#ab1739538444a8363f3704747da2d0876a484821157d4293606b52c8d41ce8ffad", null ]
          ] ],
          [ "ParallelClockSelect", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#acd24a7f3f16f903972a13677bbbd1e7b", null ],
          [ "ParallelClockSelect", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#a0856e8dd2d4975673089f57c7e505595", null ],
          [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#ab1b80768bca21ac9dc044b4d8ede8245", null ],
          [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#ab4e564dd9e09fc002bed199598358d74", null ],
          [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#a6876735790230ccc849107a00ab3e198", null ],
          [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#adf45abc40ca05fcb1e1ce65f8da9d914", null ],
          [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#a8014c6bde9cf1f22f62209cf704cbf47", null ],
          [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_clock_select.html#a8f79cad6385be811d447d58d0c15bf6c", null ]
        ] ],
        [ "ParallelPortWidth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html", [
          [ "Width", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634", [
            [ "BITS_30", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634ab8480c5a94e5bc93a8b7f23a69047bbe", null ],
            [ "BITS_24", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634afde08340ffb4dc6e708b33c9d8fbc6d6", null ],
            [ "BITS_20", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634aaf1800d265a2221d633dd3237a62cd82", null ],
            [ "BITS_16", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634a27819f40142c0f0c0c479c4a55926d11", null ],
            [ "BITS_10", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634a0c6ee858fe403cf4baa95398b76d4c26", null ],
            [ "BITS_8", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a92ad5ab23617166e260c113d165ff634a551e192f85a76c98a8f65d09c5d49fe0", null ]
          ] ],
          [ "ParallelPortWidth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a69c71b9b10ad51427728c38f95a79e4f", null ],
          [ "ParallelPortWidth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a56719dabdfae78c0ad7d7f173d3be769", null ],
          [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#ae867bd5a0adbbfd9f6d6caae1545eec6", null ],
          [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#acbdf5010c6a2b74ee35c63b8179e6415", null ],
          [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#aeb4532f4a43255e46c90bf736dfaa6d6", null ],
          [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a0f9d40b2cf463108e3627b0a743eb348", null ],
          [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#a5cc95cbdbf27da32f6e8f2b50ed56ce8", null ],
          [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_parallel_port_width.html#aa2720b97c443f72c7b977056d81d7e38", null ]
        ] ],
        [ "Pattern", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern.html", [
          [ "Bitdepth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html", [
            [ "Format", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770", [
              [ "MONO_1BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a653afd33388c4fe53f407ac397843870", null ],
              [ "MONO_2BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a102166659427846300d6275a3e8e5eb5", null ],
              [ "MONO_3BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a49a74b3d4b849a6f9ea657d2d457fd4e", null ],
              [ "MONO_4BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a316432c4fe930ff3f08b03f5e78c7f17", null ],
              [ "MONO_5BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a7260a1e7030da0437eec28023f59ad46", null ],
              [ "MONO_6BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770aa393c59de050ad0eaedca937f5a7714c", null ],
              [ "MONO_7BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770af2e5cd6931d8c5f0677d0f9e94afd948", null ],
              [ "MONO_8BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770a64696d5d2987b0f698d88c10d654b991", null ],
              [ "INVALID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a18614464ec3676ea23c1f2d342ea2770aad34df8147ce3123c55d105f891d450e", null ]
            ] ],
            [ "Bitdepth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a1ddbba47d30a224f5214eae5184a87ff", null ],
            [ "Bitdepth", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#aa74f9dc0f0070729f17a61ab2c34f437", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#ac1442abb6315c7bc8caeb2a14417c297", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a961ee5734d71c70cd63b2d13bc7e9cfc", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a39d9286ca2cb5a238df870be61dd888c", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#a9d8e69c664b31a28281896d69dbda059", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#af85dcfa7bf4b7e763d5c91bce6a57cca", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_bitdepth.html#ab0d7640d13b8751c3c423495e0bcdc57", null ]
          ] ],
          [ "Display", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display.html", [
            [ "Repeat", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_repeat.html", [
              [ "Repeat", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_repeat.html#a4c902184838f3c80c560107a0955599b", null ],
              [ "Repeat", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_repeat.html#a84b441fe66df94513adfc59e8c7b1ae6", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_repeat.html#ad1a0c82838ee58f8b399b719eff6bd60", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_repeat.html#a5d779fddab294df060f1d1c8939097f1", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_repeat.html#ad43612be80cea1a2280f68d837fc3036", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_repeat.html#a3cfcd390c65233aeefe289b1662e6823", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_repeat.html#a716b294715ac51f24d7d8029b348f59f", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_repeat.html#aeed32a2c6b19ed75bf4c2fbee6869d5f", null ]
            ] ],
            [ "VerifyImageLoadTimes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_verify_image_load_times.html", [
              [ "VerifyImageLoadTimes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_verify_image_load_times.html#a79f2f00f32838a56f1a13da71931839c", null ],
              [ "VerifyImageLoadTimes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_verify_image_load_times.html#a45c8928f7dc201409bddfcc702f82908", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_verify_image_load_times.html#aefb199adf1a505733fad992cf8c109ae", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_verify_image_load_times.html#ad2ed6be15833dbd55c45b7370822af49", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_verify_image_load_times.html#a0eb698e0f4ae8da73dbe79a146440859", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_verify_image_load_times.html#a1a0a6d2fcf390188fbc7223d84fd76c0", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_verify_image_load_times.html#a136b33eb90d9cd5ced4a19537009e2f7", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display_1_1_verify_image_load_times.html#aff529a25490b7e108c4ba8c467dd2869", null ]
            ] ],
            [ "Control", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display.html#a7dfc08555b9836aedc5f7add05266df3", [
              [ "STOP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display.html#a7dfc08555b9836aedc5f7add05266df3acebbd403caee32092cf945b4c9dd2456", null ],
              [ "PAUSE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display.html#a7dfc08555b9836aedc5f7add05266df3a5f304b77057563f3a3786a563cb58c79", null ],
              [ "START", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_display.html#a7dfc08555b9836aedc5f7add05266df3ab427807dafc59bc934191b1dfcaeaebc", null ]
            ] ]
          ] ],
          [ "Exposure", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_exposure.html", null ],
          [ "FlashImage", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image.html", [
            [ "Blue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_blue.html", [
              [ "Blue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_blue.html#aa546cf4d724992b7b32c3822689ea432", null ],
              [ "Blue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_blue.html#a641d2980249e22a62ab676b98a913abf", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_blue.html#a2f6a98c836cdea14d10b1d26f7182491", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_blue.html#aa28fcbe589df19567f8357ff9028aacb", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_blue.html#abffcb543dfa5e6a794650d7d8f065238", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_blue.html#a9e943476253841c67e84bf1419fe2d85", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_blue.html#a4791f87ecf7dd37273bcff163e452289", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_blue.html#ae6984de217eaa15b9aa6d7419817e9d5", null ]
            ] ],
            [ "Green", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_green.html", [
              [ "Green", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_green.html#a495594767be9b75f42a3554aa6cf8e9d", null ],
              [ "Green", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_green.html#a914cc88f2e5e934226976b784498c81f", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_green.html#a262cf6351e45d4d4fff51bdd623498be", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_green.html#ab5c502b22a3795ef28cfc428c00e0b99", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_green.html#ab176b2028ce4753e873704e3991ba1ab", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_green.html#a54d39669597f0ccaa05ebb83254413c9", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_green.html#acc11de308d2247f8dcb5ed629e313eb1", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_green.html#acfb4897b8b578986ae41ec5c9c6e1e05", null ]
            ] ],
            [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_red.html", [
              [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_red.html#a344230b5dcd64488735ee6658cdda06c", null ],
              [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_red.html#abc729c377594691613d80c9baed06b23", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_red.html#a9ad76abf2abe1ed80a06e69592a21568", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_red.html#a47bc4c58e08fa944e514647d6423eb40", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_red.html#a19a6cd800a0d093ee39d3345e9053f98", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_red.html#a7d91f2c0d5620639023f2cf19ca81b68", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_red.html#a4a5b6a34753e030387a666867977719c", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image_1_1_red.html#a1b42fe9d57e6a6f6903ef154b86920e9", null ]
            ] ],
            [ "FlashImage", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image.html#af62bb7d45c5535f9deae7d07b3bb7b9f", null ],
            [ "FlashImage", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image.html#a37bc2b189ad9e4c96cd8dc8b802bc55c", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image.html#a480b4b8890e4ba268106c72543b122c8", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image.html#abae9aa4d7d397498c7b09cbcd798dfbc", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image.html#ad825f056b69280543d302bcc5ab630c6", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image.html#af78cc435245ac517eee9f1c96ce951cc", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image.html#a9a99631881fe52374c8b9c1e3fe24d83", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_flash_image.html#ae11a697ce20183b3bc1835dda90d0f80", null ]
          ] ],
          [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert.html", [
            [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert.html#a0ad083d4da33344262f59fd168b0ef8c", null ],
            [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert.html#ac41de73b211e4b67e81e12fdd1a4926c", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert.html#ad23ac76e36dd00c9e089a7fe69a28974", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert.html#a9c7467a7dc9fda893ed4b45cdc35e26b", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert.html#a2700cfeda4d99d1abadb021aaab06d80", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert.html#a86092a00e9fabb0d96193df1b09a37e9", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert.html#a283e62e9b832399d67288370ae09cb4f", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_invert.html#a3bbb327bf00f796665d8069e83a882ac", null ]
          ] ],
          [ "Led", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html", [
            [ "Color", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71", [
              [ "NONE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a8a6c91cb399ffe7ecdbe37588d056979", null ],
              [ "RED", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a17c7015a0f0fdfbbfd193b2fa29cdbc1", null ],
              [ "GREEN", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a0afd22c0d58558582a37d1fc6cb18a3c", null ],
              [ "YELLOW", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a9522616a0f3f081983855b62e3a2a216", null ],
              [ "BLUE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a00046021bd828932be41aa6d61a9593f", null ],
              [ "MAGENTA", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71ad36f3468e8708a7d95be110fd23e8d47", null ],
              [ "CYAN", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a6e21f168ff6b0464a3084be2ea9d9dec", null ],
              [ "WHITE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a09bbbe57ce3ba54f394ba2f516dea57d", null ],
              [ "INVALID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab66ce978236dfa1a3d90b8764dd32a71a040654bb9e36d5812b8ff2312b3a76a9", null ]
            ] ],
            [ "Led", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a369a827432d040478899221d0f077e2a", null ],
            [ "Led", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a0ba0bc48900a8788efa9f7330068057d", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a9630a101cea232ad1b67eb7a0e01bd0c", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ae5ba53e0d72c2bbea770202d74594abb", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#ab572731c1b55ae1ae6b1a128b6657c6a", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a08b2695863c5a8941356e7f6ab4e0d5f", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a1488dd6d91aa95a9b1ab8e4e1a42414d", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_led.html#a056e0ff659abb18dd99bf14925058b75", null ]
          ] ],
          [ "Number", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html", [
            [ "Blue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue.html", [
              [ "Blue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue.html#a4e1c1a2b012ccafbb37ccba7214037f3", null ],
              [ "Blue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue.html#a6ef89372be7b2a62ac878099f65a49af", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue.html#acb222e6a2b24120b5b9f914e69e5e396", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue.html#a6c17347f2a3ffffa46db66a37bf85006", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue.html#a1b5842fc37a00d8af74a59b83ba6c8b3", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue.html#a84c8d619fe0848126d12630690c69ec7", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue.html#aca853a1a084a7f207312b99e0233dbb3", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_blue.html#a35c4a208e5e17810f32d8bc031d09f97", null ]
            ] ],
            [ "Green", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green.html", [
              [ "Green", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green.html#a137abd32ee11643760bc6e0cc03db405", null ],
              [ "Green", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green.html#a0c4879d53a3d65099c1e751194787de6", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green.html#a95d44285b1ab7782204843f01acdc894", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green.html#a4e70b2e11fab3f53c428c478e4997e76", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green.html#af6fdcd6f171207839889c7c192e44b36", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green.html#a71477843bfd3d2c4a579ce72ef4aca56", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green.html#a89a1ae1a9c524e94c0b8d5ab083f10e6", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_green.html#ae229153fba13708047ca60c284dcffac", null ]
            ] ],
            [ "Mono_1BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html", [
              [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05c", [
                [ "G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca433444527c1cfbc74afda0894f2069f7", null ],
                [ "G1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05cad5dc26cb3cff9139dc80eb9e0fed96ea", null ],
                [ "G2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca02ac87fc14907ef96b33a55666618433", null ],
                [ "G3", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca53251e4b2df88a35e79f4563af6b4305", null ],
                [ "G4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05caf399d7f930fd364d21e07308a9305f4d", null ],
                [ "G5", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca3f2a8a6a0215d5f0332cb6474cc67d94", null ],
                [ "G6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca7f7e41aa93269acfc3b0bd9e80049e6e", null ],
                [ "G7", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05caa4a740cb35ada0e67cb383779890cf25", null ],
                [ "R0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05cae9d7f26f3d8acddea71ca3c6be0f794d", null ],
                [ "R1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca539e0d8eaf77629d8fa0d8924a0f03fe", null ],
                [ "R2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca0e5a21716829fe39ea935596848d5171", null ],
                [ "R3", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca395dff42417fe103740bb7ee12ea1b01", null ],
                [ "R4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05caab9c5a9e18622561b40abc2601c4afa9", null ],
                [ "R5", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca3087260da138d14871a9b70743993960", null ],
                [ "R6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca233d5ae2d96a8312402b50975e53a9ca", null ],
                [ "R7", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca284a59cfc52613eca0fb2d20c41ce21a", null ],
                [ "B0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca5bc0789ac911ceb669ce5c37840ff241", null ],
                [ "B1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05caf1975e034efa1cf6400b677533024039", null ],
                [ "B2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05caece4e90e272a3bd8cf1dd958171e4f7d", null ],
                [ "B3", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca553d39b1200542e62a5976b09d3464a4", null ],
                [ "B4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca94bbbe7261340049eb2fd4e8572fcd59", null ],
                [ "B5", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05cab9d279ffbae84bdae891f7592754dcc8", null ],
                [ "B6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05cadca55895190691ff1deb5af081bd9de8", null ],
                [ "B7", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca67bf3ddffc8331dfc959caaef9bcabdc", null ],
                [ "BLACK", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__1_b_p_p.html#afc1fa90bdd6c64a9d900803b59e2d05ca2f0ba0638fd520529ceb66099a161d3b", null ]
              ] ]
            ] ],
            [ "Mono_2BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html", [
              [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47f", [
                [ "G1_G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa4304009fdf7d424af08555b708abac48", null ],
                [ "G3_G2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa8816ff0ae84d6e955d7ac375f945924b", null ],
                [ "G5_G4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa079a705aa32ebdb9fa3ce22345bc1313", null ],
                [ "G7_G6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa4477ed17b5f748682ca11cfbbdcae9cc", null ],
                [ "R1_R0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa0fb7253116c942a62401679d78af97ae", null ],
                [ "R3_R2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa5b165efa2bb0462495442452fa4c60e7", null ],
                [ "R5_R4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa4e9d7dea6f67cb6993dd16a86818c047", null ],
                [ "R7_R6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fade5a694c448cb1276e1cb808680987fc", null ],
                [ "B1_B0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47facdef61d2f9341d59d6701bff35c9d91c", null ],
                [ "B3_B2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fad5c5ea7211e54eb221a4c88c00ec4480", null ],
                [ "B5_B4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fae69f8ea417330e7a06c4edfe7f75a121", null ],
                [ "B7_B6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa0198f5fb5947b2021103bde491c0f2be", null ]
              ] ]
            ] ],
            [ "Mono_3BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html", [
              [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71", [
                [ "G2_G1_G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a9a82bf198bcca32d735a8406146b180c", null ],
                [ "G5_G4_G3", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a11abf5e6e1a188b24b96ceb72fea4aee", null ],
                [ "R0_G7_G6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a9ae7144b12606d6c2ab99b58cd263e89", null ],
                [ "R3_R2_R1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a7cca54551a78bf5d0cd3427400a69590", null ],
                [ "R6_R5_R4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a80d3b9148bc89dd5ffe93a69c8a537cc", null ],
                [ "B1_B0_R7", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a4ec1a58a22923b72e70cdea62e49336a", null ],
                [ "B4_B3_B2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a6a4ea1135a9fb71a4abb8ea9cd7b0320", null ],
                [ "B7_B6_B5", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__3_b_p_p.html#a5927ca46f880dcf47a13b963cd3ddb71a294122a4be1bccfd4bd759fd1ab198f5", null ]
              ] ]
            ] ],
            [ "Mono_4BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html", [
              [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37", [
                [ "G3_G2_G1_G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37af57b1c3156b461f6e9cf419d353ce177", null ],
                [ "G7_G6_G5_G4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37a67300b7f6ca98e08370154b623dbdf87", null ],
                [ "R3_R2_R1_R0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37ab7e2977b557dffb6b4f1d3ee73803d43", null ],
                [ "R7_R6_R5_R4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37a43a286bfe6e8855afb6d75389245bd5d", null ],
                [ "B3_B2_B1_B0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37af907b6c4fcf9a936d94a6c73a94986f0", null ],
                [ "B7_B6_B5_B4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__4_b_p_p.html#aa7f22c15a88189b3e0ad534a4b880c37a31c277350dbdcdf717d456468c59fd7c", null ]
              ] ]
            ] ],
            [ "Mono_5BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html", [
              [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html#a0f6ff769d177e6efeff6714c5f48e7d0", [
                [ "G5_G4_G3_G2_G1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html#a0f6ff769d177e6efeff6714c5f48e7d0a50b4a28c09632bbbb6ad06e8290d9a69", null ],
                [ "R3_R2_R1_R0_G7", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html#a0f6ff769d177e6efeff6714c5f48e7d0afb54b9a5c59f068f003af90330a2f8fe", null ],
                [ "B1_B0_R7_R6_R5", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html#a0f6ff769d177e6efeff6714c5f48e7d0a51653af92516e793e991dd32c04dfe88", null ],
                [ "B7_B6_B5_B4_B3", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__5_b_p_p.html#a0f6ff769d177e6efeff6714c5f48e7d0a9953e4437b5f8057da1afee6027123d9", null ]
              ] ]
            ] ],
            [ "Mono_6BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html", [
              [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html#a83ad4d2e3725dcc736eea29570fc8544", [
                [ "G5_G4_G3_G2_G1_G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html#a83ad4d2e3725dcc736eea29570fc8544a77d197e01fc78a08f836a78f79b46d57", null ],
                [ "R3_R2_R1_R0_G7_G6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html#a83ad4d2e3725dcc736eea29570fc8544a405ca80d70c4338eeadb22948e6c26cf", null ],
                [ "B1_B0_R7_R6_R5_R4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html#a83ad4d2e3725dcc736eea29570fc8544ae3dae2bec9097d8c9be26d40c0e60b94", null ],
                [ "B7_B6_B5_B4_B3_B2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__6_b_p_p.html#a83ad4d2e3725dcc736eea29570fc8544a840b4f1c9b27dce095b641ea5d7b0395", null ]
              ] ]
            ] ],
            [ "Mono_7BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__7_b_p_p.html", [
              [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__7_b_p_p.html#ab44e11904562829f1e078756839b1187", [
                [ "G7_G6_G5_G4_G3_G2_G1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__7_b_p_p.html#ab44e11904562829f1e078756839b1187ae0c6280a3bbcc0e2f5b366a1cb5ebb40", null ],
                [ "R7_R6_R5_R4_R3_R2_R1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__7_b_p_p.html#ab44e11904562829f1e078756839b1187a023ce734673976e9469497008ad509d3", null ],
                [ "B7_B6_B5_B4_B3_B2_B1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__7_b_p_p.html#ab44e11904562829f1e078756839b1187add8bfc0a51a5916ab16d9a389afd157c", null ]
              ] ]
            ] ],
            [ "Mono_8BPP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__8_b_p_p.html", [
              [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__8_b_p_p.html#a3fb072c472a28f5cfe1e1e46adabeae8", [
                [ "G7_G6_G5_G4_G3_G2_G1_G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__8_b_p_p.html#a3fb072c472a28f5cfe1e1e46adabeae8ad5aeac5cf90cfe6baecc84d580e0e015", null ],
                [ "R7_R6_R5_R4_R3_R2_R1_R0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__8_b_p_p.html#a3fb072c472a28f5cfe1e1e46adabeae8aee4b7e9e89ede10161f6096995426cad", null ],
                [ "B7_B6_B5_B4_B3_B2_B1_B0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__8_b_p_p.html#a3fb072c472a28f5cfe1e1e46adabeae8a98f105df163f90c1bea5172b6d0eb4b7", null ]
              ] ]
            ] ],
            [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html", [
              [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#a533a79d2c33b6001c44910edc12c0b38", null ],
              [ "Red", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#a215482c8126ce401c09b29207b9d3d09", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#a58761b7e00e7d6a6f8c61235d89a94f6", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#a2559c39dfe1f6191e636aebe8b104341", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#acafd82ed4f240459e51a526805018f15", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#af8cf81cb0caf12b6bacfc8ede646039e", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#a0f260940f75df734b33eb406a9df6439", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_red.html#af82edb4b485864bc831ec6630d735770", null ]
            ] ],
            [ "Number", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#a2b853dc66055e02cc879de89a8a57019", null ],
            [ "Number", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#af72f318d074e4df5ddae319a588b8a50", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#a9dde2baf4193fb657cecf9ebe4ee59dd", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#a19183446e0ff9c3e717e96f8c763c327", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#a42f31bbc22f47bf8266ec391a98539c3", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#a0c24c6a1f58e67bbc4eaa256d542491b", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#acc9fd77110cf183df42cb30a9f7e5023", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number.html#aba456c0d73ecd634a4cb1a7dd3ab071c", null ]
          ] ],
          [ "ShareExposure", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure.html", [
            [ "ShareExposure", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure.html#a20b2a632fbdbc0c939f7e460259a99bb", null ],
            [ "ShareExposure", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure.html#a4ca42a6f0f4ca79c4b4b91a2b4cc2c92", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure.html#ab84cad5704d6d8e9cc90ee2759783861", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure.html#a6adcae431ae1334397d5ef259f7ab5d0", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure.html#abaefba5ee77d021e10f9786c9c509650", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure.html#a503cf313447d3ff9ebd84b53ff00c726", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure.html#a24f8c0682a9ed5f88a77d1180ec18e25", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_share_exposure.html#ae15f77b4d5ae68c560bbce7eb7a2900c", null ]
          ] ],
          [ "Source", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html", [
            [ "Input", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#a26ab1373ce044902a237d40e77ee716c", [
              [ "FLASH_IMAGES", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#a26ab1373ce044902a237d40e77ee716ca2f52a1a55f60bffebe127cf216ad35d0", null ],
              [ "VIDEO_PORT", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#a26ab1373ce044902a237d40e77ee716ca3ecd2edf81c3cd3e2c9c7c66aa1619bf", null ]
            ] ],
            [ "Source", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#a883574a01cc414aa1b6dbc23c375337f", null ],
            [ "Source", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#adf7159d9a2a33a539fd8a90017d2fee9", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#a59786da39809e15312fdadefc75651f6", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#a1c9f17eddfd35029524a917ef2bdc627", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#a1d1ede20f987bd65fcc2a40b587c468e", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#a202b4f1e9cd99da8dcc7badfc1523266", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#a59375b096efa3a41663e7200cdb72485", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_source.html#ac1763e45fd0bb003b3dd77021fa8d423", null ]
          ] ],
          [ "Trigger", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html", [
            [ "Input1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1.html", [
              [ "Delay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html", [
                [ "Delay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html#aa266518d2d481edf1e2efd891c907f76", null ],
                [ "Delay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html#a9f742cc91691d4843e41553349ee7093", null ],
                [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html#ae33a402d76951fc8436d20ac45f8936b", null ],
                [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html#a1cb41e7099926cfd8a15d6b217119feb", null ],
                [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html#a3d290b1d55a2940253a2cc76af71b31e", null ],
                [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html#a7b37ec4c6f8233ff717c128bb51ccbab", null ],
                [ "GetNanoSeconds", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html#a647a8bced98d3693acbc90ce99951cd4", null ],
                [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html#a24b27666cf587a4dcdd6b66d92625fde", null ],
                [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_input1_1_1_delay.html#a6448351812bd1de381f7e457c865c081", null ]
              ] ]
            ] ],
            [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html", [
              [ "Trigger", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135", [
                [ "MODE_0_VSYNC", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135a23c3189ca6b62372e4c90c9a9d8f7bc7", null ],
                [ "MODE_1_INT_OR_EXT", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135ac61c51c7f8115514a7d5c1fb3be65f3d", null ],
                [ "MODE_2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135a89de2f5c6940a653f7f35efee5b58e2c", null ],
                [ "MODE_3_EXP_INT_OR_EXT", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135a3b6763a6c72c94948817c7cbbafdbf8a", null ],
                [ "MODE_4_EXP_VSYNC", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135a62b37285f23cee405047e234c0f199e6", null ],
                [ "INVALID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a19dbac7a958466090c320f1b3e592135adde0da922c9b3094ea42ccf921b663ad", null ]
              ] ],
              [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#af3a5ad5df2db11aff42637100cb400a8", null ],
              [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a77fb744227fb4f1932ccb35b0947e1a5", null ],
              [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a95e023e19c01ecda7a519da563c0821f", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a0ece5007ff9a71da9a1f41aff972320a", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#a181c675af881ea2396f12002f0411d9e", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#ae95ede26b37ee8898620d5b56a3000e6", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#aa3a3e9b3b6287fc837b55ef3923f63fd", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_mode.html#af056292ffde44a7e445a7a02fe4b012e", null ]
            ] ],
            [ "Output1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1.html", [
              [ "EdgeDelay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay.html", [
                [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_falling.html", [
                  [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_falling.html#a3c5876a9faa2ab126c89901247a62cc7", null ],
                  [ "Falling", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_falling.html#a21ea21fcdca2d225df2e4aef70ac941e", null ],
                  [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_falling.html#a63cd5bdab2e3b736f544862949583158", null ],
                  [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_falling.html#a11bbb5e1420fc7df16b806c0cb92f652", null ],
                  [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_falling.html#a7364fcf439f19beb1c65528b305c1cc3", null ],
                  [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_falling.html#a43179e2fe402e8579c43d0df074b70db", null ],
                  [ "GetNanoSeconds", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_falling.html#abae936b6f796ba0d076cc11885350dc7", null ],
                  [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_falling.html#a8d1f48a689eb0999af81a86f473013ed", null ],
                  [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_falling.html#a5656b78a89270e3204e58f566303ad6a", null ]
                ] ],
                [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_rising.html", [
                  [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_rising.html#a95078378ccb3ff383f6b038504acc0f0", null ],
                  [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_rising.html#affd9c5041b48911a1d9e7ccca1668864", null ],
                  [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_rising.html#a3415577e3cbebfeff97693e28d6cfa52", null ],
                  [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_rising.html#addf71a7e5a34d7bf6f300f2b49425789", null ],
                  [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_rising.html#a73d7ed594e10198af46a5495a0c4d43f", null ],
                  [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_rising.html#a9588dd1d332e3da4b033816880b2312a", null ],
                  [ "GetNanoSeconds", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_rising.html#af4eafa78dd8450cb3d7e52f15e725d4f", null ],
                  [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_rising.html#a48a00aac1fdefe8c4dc5e1cd5dea1ee1", null ],
                  [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_edge_delay_1_1_rising.html#a17a3d9a88880c386afe69617855debfb", null ]
                ] ]
              ] ],
              [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert.html", [
                [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert.html#a627d18d747567d1b3debde7883e13a38", null ],
                [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert.html#ac6a30590a21b5f01dd73df6e5ca273bd", null ],
                [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert.html#abeb30d37665e408ce1b07d130120dd3f", null ],
                [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert.html#ab65883a7e98aa861c4cc03faed19a5e8", null ],
                [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert.html#a468a835988b97f21d3adaf132f11d084", null ],
                [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert.html#ae134f1143cb9690e07fc2bcfbb5bc7a6", null ],
                [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert.html#a36ea8e98cd241eac5acce009aacaceea", null ],
                [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output1_1_1_invert.html#a23b1c0a5493e1a855dab69baccf689fa", null ]
              ] ]
            ] ],
            [ "Output2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2.html", [
              [ "EdgeDelay", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay.html", [
                [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay_1_1_rising.html", [
                  [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay_1_1_rising.html#a65d6c8518d9f198b26b72068a08dec9f", null ],
                  [ "Rising", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay_1_1_rising.html#a52787e2b924ee26cd39905b60ef2f8d8", null ],
                  [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay_1_1_rising.html#a60665f8bc78c7b44831c29e892b6b5df", null ],
                  [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay_1_1_rising.html#a4c584bb1bd8082fa87424949a17f343e", null ],
                  [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay_1_1_rising.html#af2cb8d253c02f0864976d9fec914f1bf", null ],
                  [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay_1_1_rising.html#a282d40ce11a11658b73f794892275c24", null ],
                  [ "GetNanoSeconds", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay_1_1_rising.html#a0da6614af89a50cc84b6856d37c73fa3", null ],
                  [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay_1_1_rising.html#a47e167aebb4dd69f2a94b315fb0b8d7f", null ],
                  [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_edge_delay_1_1_rising.html#a53f631936353378d4dd711c32b9d1300", null ]
                ] ]
              ] ],
              [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert.html", [
                [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert.html#aefd234c72559db0790286f927802be59", null ],
                [ "Invert", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert.html#ad5e8c3e1658eb6b7f739f9600962788d", null ],
                [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert.html#a91473d08889bdfb582c62a921b94f7a8", null ],
                [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert.html#a24627dbc8e3db16c2d25755fd9bf0f9a", null ],
                [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert.html#a69bacf4e76b9477166e680af3e573201", null ],
                [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert.html#a542bdb7a43b1bccea2ee1b43b39f5e75", null ],
                [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert.html#aa99282c01075120f3f2bccf0aabc3ed4", null ],
                [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger_1_1_output2_1_1_invert.html#afe6fa105e58bc7362325f032c9296955", null ]
              ] ]
            ] ],
            [ "Output", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a8ef05969a0efa1c8877913053c4e7dc4", [
              [ "TRIGGER_OUT_1", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a8ef05969a0efa1c8877913053c4e7dc4abbe4f9e0c45bb63c54312e59615df3f7", null ],
              [ "TRIGGER_OUT_2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a8ef05969a0efa1c8877913053c4e7dc4ac676ceddfe52beed833bc7696c77cce0", null ]
            ] ],
            [ "Type", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444", [
              [ "INTERNAL", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444afab87a569419a80285a5c8e27a56ec8e", null ],
              [ "EXTERNAL_POSITIVE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444ab8dd1baf165b1285f5846ad6a5ba0762", null ],
              [ "EXTERNAL_NEGATIVE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444a2785c5c8fe43894805e2ca14e54b6f12", null ],
              [ "NONE", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444af6ef3b5ab0881b847e5003bf1c0b64f2", null ],
              [ "INVALID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a65fd74b8880b74560d0ba6c76456c444a839016869abec49b9a541dddd813409b", null ]
            ] ],
            [ "Trigger", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#ac296a72c249497e52f7ca1a0edfa6bd4", null ],
            [ "Trigger", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a572fcd10ef58d92cab8e89b415c1e579", null ],
            [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a8e2516130be16949ab87d8db55c087d6", null ],
            [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a1f0e7da52dcf3bc252e8c2fab54cf7ad", null ],
            [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#ac75bc90f111f22442732ac1fffb71c85", null ],
            [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#abf72616e81e25427b7f46802d1d84248", null ],
            [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#a43e18189401447ef5201eac93318a2cc", null ],
            [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_trigger.html#ad28a5d1cb7b8b7c7e9afb78b115dc089", null ]
          ] ]
        ] ],
        [ "PowerStandby", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html", [
          [ "Mode", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#adea53e48eeca22fcdaf5f68115c5ee2d", [
            [ "STANDBY", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#adea53e48eeca22fcdaf5f68115c5ee2da558e9d492171240884f1e21d718684f4", null ],
            [ "NORMAL", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#adea53e48eeca22fcdaf5f68115c5ee2da2c9ed2bd8aa03c800d57a6847504bd8f", null ]
          ] ],
          [ "PowerStandby", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#a003280e97455b7760492bc8974dbffee", null ],
          [ "PowerStandby", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#ae7fb9c37e8c750d25cfd287d02cc62d6", null ],
          [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#a109538de9cc3ed3075c2dcd7030fdff5", null ],
          [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#a028809663811139ebcaae6505885f803", null ],
          [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#a37678e87da56f0e7aeffc9cbcf22def1", null ],
          [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#a369686cbdbac151b57a90ce256ba1f52", null ],
          [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#a8c0837840b9eba6b36dbb6efa4da86ac", null ],
          [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_power_standby.html#a88f44b87020d7930f045181b8f1792e4", null ]
        ] ],
        [ "TestPattern", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html", [
          [ "Color", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color.html", [
            [ "Background", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html", [
              [ "Background", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a89d5bd0e5ea108f44fecf1a801d3b9d6", null ],
              [ "Background", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a44e5bebb3d68ddbdb426accc75337cac", null ],
              [ "GetBlue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a9e433fa137c516fdf398ab25655ec9c8", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a621af84f8b7c300fd32d08995ef152ac", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a9012ff8e121fdd1ca28545c062515879", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a5785ae3a60c517cbaab6b9d0f5b0d173", null ],
              [ "GetGreen", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a60fb3134e3390b1dd8389870dd7d0437", null ],
              [ "GetRed", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a83d8aff7c72e3ae4beaf24bf66e5d25d", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a82fbbb606dc762304fb44b3e136974e2", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a22fe06f5e2b3560f02b350de056ec2ff", null ]
            ] ],
            [ "Foreground", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html", [
              [ "Foreground", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#a8d5f1f68e5ca78689ead9d7d4b7bf55d", null ],
              [ "Foreground", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#aee39ce6883996fcebb08faa0247feb7a", null ],
              [ "GetBlue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#ac8c8b2bcfa52e5c7412b7379b69f3cb2", null ],
              [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#ac3e34a4d1f2ba34dfc29f1a53d5b6e24", null ],
              [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#aa643c69991c7c7cb611f55fa85ec1cf8", null ],
              [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#af618054c94f9e91b8476efe013230658", null ],
              [ "GetGreen", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#a2f000babddaa64172e2c24b87c0fc091", null ],
              [ "GetRed", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#a34cc00079dd2a6b54d93bbf9bd88a1da", null ],
              [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#a5a56441b2e2812267a563f068bc41297", null ],
              [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_foreground.html#a28ce453eeb1703f3d1b646a8729214b7", null ]
            ] ]
          ] ],
          [ "Pattern", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4", [
            [ "SOLID_FIELD", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4ae9b5674f37c2e6921869209a4523a808", null ],
            [ "HORIZONTAL_RAMP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a4264cd9d8e2b048244c6550ebc2d717a", null ],
            [ "VERTICAL_RAMP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a493fbb3b3bdcf6ad7ea354ae5e3d1ffb", null ],
            [ "HORIZONTAL_LINES", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a3bcffdde3719e1ea188d60aaded8340e", null ],
            [ "DIAGONAL_LINES", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4abcff03184682fda8b0be4b4803e86742", null ],
            [ "VERTICAL_LINES", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a2e3e3c8c87ef59de82639fed8d437464", null ],
            [ "GRID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a22916681bcd1a9cfbe9dfbe1278949ba", null ],
            [ "CHECKERBOARD", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a36085a45786216163ad5c779bea9c245", null ],
            [ "RGB_RAMP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a90ba0ea130bf63c8636dd982f25830e0", null ],
            [ "COLOR_BARS", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4af2a218f8b911732c90235b5d689f55c7", null ],
            [ "STEP_BARS", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4aa20475c19c29247666016219b939fe2f", null ]
          ] ],
          [ "TestPattern", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a3e444786c7d91557afeec191bee4747e", null ],
          [ "TestPattern", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a6cd900d200c25294629b31e7127a26eb", null ],
          [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a2331e2f7cbcfae28da7fdb016d7c8bbf", null ],
          [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a4ae262ae6bf30f6c7d6bfe227a5be92d", null ],
          [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a04b37a116255d54451a7302933bdfded", null ],
          [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#ac1ab8085966cc3c55c33326a0a942e89", null ],
          [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a1e325109a463e9c41c109b6e6222c1b7", null ],
          [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#aaeaef70f1de097c8e0f147fb1349db41", null ]
        ] ],
        [ "UseDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default.html", [
          [ "UseDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default.html#a3c507ea0ae773c5106a229a1fb30713a", null ],
          [ "UseDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default.html#a70961f8ef67cf32a5d4aee26ba9048da", null ],
          [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default.html#a39cb5452f2290b3136c61e8ee75c70a6", null ],
          [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default.html#ad3fd659cfbe092e0ed5e2b9a323164ba", null ],
          [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default.html#aad41b76ebee48aed8c1569ccc3ee0262", null ],
          [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default.html#a059fd9fc137cdfc2dae88c397ffe4bbc", null ],
          [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default.html#adc287f57e4df250ade2b92f7560a9e56", null ],
          [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_use_default.html#acef36d9e6c04af69d53ff1e1f069a1ed", null ]
        ] ]
      ] ],
      [ "Mirror", "classdlp_1_1_l_cr4500.html#a0c2764722341c024b62f77bb691da787", [
        [ "ORTHOGONAL", "classdlp_1_1_l_cr4500.html#a0c2764722341c024b62f77bb691da787ac9643b6ef9ceab12abacac795c0543ec", null ],
        [ "DIAMOND", "classdlp_1_1_l_cr4500.html#a0c2764722341c024b62f77bb691da787a337f43db5edddda90ef2156a9baaed09", null ],
        [ "INVALID", "classdlp_1_1_l_cr4500.html#a0c2764722341c024b62f77bb691da787accc0377a8afbf50e7094f5c23a8af223", null ]
      ] ],
      [ "Platform", "classdlp_1_1_l_cr4500.html#a5d3b41f658d5801dedd89bbf65c3818e", [
        [ "LIGHTCRAFTER_3000", "classdlp_1_1_l_cr4500.html#a5d3b41f658d5801dedd89bbf65c3818eae9df6d646288e5db206f1fccb6759a59", null ],
        [ "LIGHTCRAFTER_4500", "classdlp_1_1_l_cr4500.html#a5d3b41f658d5801dedd89bbf65c3818eaeede28ef77b357f4dda717990aab6449", null ],
        [ "INVALID", "classdlp_1_1_l_cr4500.html#a5d3b41f658d5801dedd89bbf65c3818eaccc0377a8afbf50e7094f5c23a8af223", null ]
      ] ],
      [ "LCr4500", "classdlp_1_1_l_cr4500.html#a6e2d77e212a6ded62c0ab9839c5d7e26", null ],
      [ "Connect", "classdlp_1_1_l_cr4500.html#ad424142a8361d60c963112a32e2204f6", null ],
      [ "CreateFirmware", "classdlp_1_1_l_cr4500.html#a8b2f2705b529a3031295d684d1ff5c98", null ],
      [ "CreateFirmwareImages", "classdlp_1_1_l_cr4500.html#afe0fa868e6f0274f391b83750e3cabe9", null ],
      [ "Disconnect", "classdlp_1_1_l_cr4500.html#a5b6d8c0af77fd885333d0e5ef02eb02c", null ],
      [ "DisplayPatternInSequence", "classdlp_1_1_l_cr4500.html#a2aa7168859e4e8feb847c8fdbabaa926", null ],
      [ "FirmwareUploadInProgress", "classdlp_1_1_l_cr4500.html#a62752543ef9050a1ee6d7aa6b096ec5d", null ],
      [ "GetColumns", "classdlp_1_1_l_cr4500.html#a5f7fe9afe8c00191f3cae342e0d50297", null ],
      [ "GetDevice", "classdlp_1_1_l_cr4500.html#a019fda3c05ccbe52e84c1e03218361f6", null ],
      [ "GetEffectiveMirrorSize", "classdlp_1_1_l_cr4500.html#a21a55f55eba70422305fb6d9596332c6", null ],
      [ "GetFirmwareFlashEraseComplete", "classdlp_1_1_l_cr4500.html#aee9630f69a581362951e9ffdfbe1f2fb", null ],
      [ "GetFirmwareUploadPercentComplete", "classdlp_1_1_l_cr4500.html#a3d6ecc7a6e2ace5cdd10d8e37bab901d", null ],
      [ "GetImageLoadTime", "classdlp_1_1_l_cr4500.html#a6d8c730584a387c7fc887bd8bba480e1", null ],
      [ "GetMirrorType", "classdlp_1_1_l_cr4500.html#a62da10bb375067ace61ecbeb1f3fd06e", null ],
      [ "GetPlatform", "classdlp_1_1_l_cr4500.html#aa66f13e6b9d6e1d0e0c90e4a81b75b8e", null ],
      [ "GetRows", "classdlp_1_1_l_cr4500.html#a3704e59d03e7035495e3678dee886978", null ],
      [ "GetSetup", "classdlp_1_1_l_cr4500.html#a09c9e617f549784e84e8ca7938f0941b", null ],
      [ "ImageResolutionCorrect", "classdlp_1_1_l_cr4500.html#a7c6d9c3401d63ac9374954681cd1ef15", null ],
      [ "ImageResolutionCorrect", "classdlp_1_1_l_cr4500.html#a2d6caaafbebb1d29a613920801e3b1b3", null ],
      [ "isConnected", "classdlp_1_1_l_cr4500.html#a7480b5a0abcfb977e58c01bf6b456ac7", null ],
      [ "isPlatformSetup", "classdlp_1_1_l_cr4500.html#a45ab08e93004137f3ac3ef3bc3ac1df2", null ],
      [ "isSetup", "classdlp_1_1_l_cr4500.html#a3f88ae6c3ba55e6a12a76f6ac38184fb", null ],
      [ "PatternSettingsValid", "classdlp_1_1_l_cr4500.html#a68e05ecb1976cbc8a3f834ccad3c34ec", null ],
      [ "PreparePatternSequence", "classdlp_1_1_l_cr4500.html#a2e0e462f4ff41bd29eac9db105849135", null ],
      [ "ProjectSolidBlackPattern", "classdlp_1_1_l_cr4500.html#a521be846f7c43cb30c779af133f8b85a", null ],
      [ "ProjectSolidWhitePattern", "classdlp_1_1_l_cr4500.html#aeb7d8949b4fa19a0533d515d5fa61635", null ],
      [ "SetDebugEnable", "classdlp_1_1_l_cr4500.html#a9c9f02c1691ea89bbaccd5bbcd2d2285", null ],
      [ "SetDebugLevel", "classdlp_1_1_l_cr4500.html#a9f64b042bd8c8be7de68d0b23a633ef4", null ],
      [ "SetDebugOutput", "classdlp_1_1_l_cr4500.html#a1400ea6e49ef0fa49896437dbaa67672", null ],
      [ "SetDevice", "classdlp_1_1_l_cr4500.html#aaaa97a4cfe25768b6b39fedd8e8982b1", null ],
      [ "SetPlatform", "classdlp_1_1_l_cr4500.html#a3145e35fff7c797b44476d6cc5d3c5a5", null ],
      [ "Setup", "classdlp_1_1_l_cr4500.html#ab4d84e3ccce8c185f944fa4521334d81", null ],
      [ "StartPatternImageStorage", "classdlp_1_1_l_cr4500.html#aa31e55c06bb844e8acfc280bf15bdcfb", null ],
      [ "StartPatternSequence", "classdlp_1_1_l_cr4500.html#a2f4636023049efa5685525b9e990b24a", null ],
      [ "StopPatternSequence", "classdlp_1_1_l_cr4500.html#a50599fe092b9f775da9c7a0a4df5b0a4", null ],
      [ "UploadFirmware", "classdlp_1_1_l_cr4500.html#afc0f3e61b5dd1c20e38b0c2684674db5", null ],
      [ "debug_", "classdlp_1_1_l_cr4500.html#ada002617e188f389b066b3663cc62fd9", null ],
      [ "is_setup_", "classdlp_1_1_l_cr4500.html#aac4cdea7919bcd50453b21847fca2556", null ],
      [ "sequence_exposure_", "classdlp_1_1_l_cr4500.html#adda39121bc188b7c978d46a46c4e8c85", null ],
      [ "sequence_period_", "classdlp_1_1_l_cr4500.html#a7808e68de1363e467fc94359d730f9c2", null ],
      [ "sequence_prepared_", "classdlp_1_1_l_cr4500.html#acf53d6f5ed7d2e221e53677ae3f3b371", null ]
    ] ]
];