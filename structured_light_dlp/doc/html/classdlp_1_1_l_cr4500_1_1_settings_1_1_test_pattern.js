var classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern =
[
    [ "Color", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color.html", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color" ],
    [ "Pattern", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4", [
      [ "SOLID_FIELD", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4ae9b5674f37c2e6921869209a4523a808", null ],
      [ "HORIZONTAL_RAMP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a4264cd9d8e2b048244c6550ebc2d717a", null ],
      [ "VERTICAL_RAMP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a493fbb3b3bdcf6ad7ea354ae5e3d1ffb", null ],
      [ "HORIZONTAL_LINES", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a3bcffdde3719e1ea188d60aaded8340e", null ],
      [ "DIAGONAL_LINES", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4abcff03184682fda8b0be4b4803e86742", null ],
      [ "VERTICAL_LINES", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a2e3e3c8c87ef59de82639fed8d437464", null ],
      [ "GRID", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a22916681bcd1a9cfbe9dfbe1278949ba", null ],
      [ "CHECKERBOARD", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a36085a45786216163ad5c779bea9c245", null ],
      [ "RGB_RAMP", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4a90ba0ea130bf63c8636dd982f25830e0", null ],
      [ "COLOR_BARS", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4af2a218f8b911732c90235b5d689f55c7", null ],
      [ "STEP_BARS", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#adf907331ff7a1cf34237f9bc5fe527b4aa20475c19c29247666016219b939fe2f", null ]
    ] ],
    [ "TestPattern", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a3e444786c7d91557afeec191bee4747e", null ],
    [ "TestPattern", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a6cd900d200c25294629b31e7127a26eb", null ],
    [ "Get", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a2331e2f7cbcfae28da7fdb016d7c8bbf", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a4ae262ae6bf30f6c7d6bfe227a5be92d", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a04b37a116255d54451a7302933bdfded", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#ac1ab8085966cc3c55c33326a0a942e89", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#a1e325109a463e9c41c109b6e6222c1b7", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern.html#aaeaef70f1de097c8e0f147fb1349db41", null ]
];