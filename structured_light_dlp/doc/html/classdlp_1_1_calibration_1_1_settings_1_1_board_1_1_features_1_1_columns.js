var classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns =
[
    [ "Distance", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance.html", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance" ],
    [ "DistanceInPixels", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels.html", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels" ],
    [ "Columns", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns.html#ab8a24c5f314b2dafa239695f18e61b66", null ],
    [ "Columns", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns.html#a6af87149be0c59a7ffb33064ef8f7c6f", null ],
    [ "Get", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns.html#a7ec88cc7fd165a435c42649aa48664ae", null ],
    [ "GetEntryDefault", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns.html#a1df9ecdddd81d2792066d91905f90c04", null ],
    [ "GetEntryName", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns.html#a85c718a32def7cb8159688b930931798", null ],
    [ "GetEntryValue", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns.html#aec74ca4fcc0848506091b62a55f511a2", null ],
    [ "Set", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns.html#a9a9ce3ca2578acd7b54038f7cb919470", null ],
    [ "SetEntryValue", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns.html#a8413e4a16c922b5016d1ff4bc3b8362e", null ]
];