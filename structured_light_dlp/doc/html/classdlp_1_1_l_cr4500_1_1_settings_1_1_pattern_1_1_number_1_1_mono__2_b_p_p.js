var classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p =
[
    [ "Bitplanes", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47f", [
      [ "G1_G0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa4304009fdf7d424af08555b708abac48", null ],
      [ "G3_G2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa8816ff0ae84d6e955d7ac375f945924b", null ],
      [ "G5_G4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa079a705aa32ebdb9fa3ce22345bc1313", null ],
      [ "G7_G6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa4477ed17b5f748682ca11cfbbdcae9cc", null ],
      [ "R1_R0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa0fb7253116c942a62401679d78af97ae", null ],
      [ "R3_R2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa5b165efa2bb0462495442452fa4c60e7", null ],
      [ "R5_R4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa4e9d7dea6f67cb6993dd16a86818c047", null ],
      [ "R7_R6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fade5a694c448cb1276e1cb808680987fc", null ],
      [ "B1_B0", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47facdef61d2f9341d59d6701bff35c9d91c", null ],
      [ "B3_B2", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fad5c5ea7211e54eb221a4c88c00ec4480", null ],
      [ "B5_B4", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fae69f8ea417330e7a06c4edfe7f75a121", null ],
      [ "B7_B6", "classdlp_1_1_l_cr4500_1_1_settings_1_1_pattern_1_1_number_1_1_mono__2_b_p_p.html#a596ad6bc20d2e46079a228331eeed47fa0198f5fb5947b2021103bde491c0f2be", null ]
    ] ]
];