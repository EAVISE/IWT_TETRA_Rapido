var classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background =
[
    [ "Background", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a89d5bd0e5ea108f44fecf1a801d3b9d6", null ],
    [ "Background", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a44e5bebb3d68ddbdb426accc75337cac", null ],
    [ "GetBlue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a9e433fa137c516fdf398ab25655ec9c8", null ],
    [ "GetEntryDefault", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a621af84f8b7c300fd32d08995ef152ac", null ],
    [ "GetEntryName", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a9012ff8e121fdd1ca28545c062515879", null ],
    [ "GetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a5785ae3a60c517cbaab6b9d0f5b0d173", null ],
    [ "GetGreen", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a60fb3134e3390b1dd8389870dd7d0437", null ],
    [ "GetRed", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a83d8aff7c72e3ae4beaf24bf66e5d25d", null ],
    [ "Set", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a82fbbb606dc762304fb44b3e136974e2", null ],
    [ "SetEntryValue", "classdlp_1_1_l_cr4500_1_1_settings_1_1_test_pattern_1_1_color_1_1_background.html#a22fe06f5e2b3560f02b350de056ec2ff", null ]
];