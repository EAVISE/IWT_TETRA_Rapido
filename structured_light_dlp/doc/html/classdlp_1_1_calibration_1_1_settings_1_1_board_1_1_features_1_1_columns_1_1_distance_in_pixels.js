var classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels =
[
    [ "DistanceInPixels", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels.html#a6b0c631553509c55988a5fd6517f79b4", null ],
    [ "DistanceInPixels", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels.html#a199ec40801614d69a30954fa675d31d1", null ],
    [ "Get", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels.html#aaebd9d904c69ead5b86d76f204a9bb11", null ],
    [ "GetEntryDefault", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels.html#af54ca60a0da7748189edf4f8a138bd73", null ],
    [ "GetEntryName", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels.html#afbcc6eaa80bcc1e2f3c55221db97c977", null ],
    [ "GetEntryValue", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels.html#ab74ea7fa16900a459c51641fb2610171", null ],
    [ "Set", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels.html#a9ad967955932cbec783174e31b236c31", null ],
    [ "SetEntryValue", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels.html#aca76c3af9ed39a6cae5667e3d63cf1c1", null ],
    [ "Calibration", "classdlp_1_1_calibration_1_1_settings_1_1_board_1_1_features_1_1_columns_1_1_distance_in_pixels.html#accd04959b6e0c466ae7023839b342b00", null ]
];