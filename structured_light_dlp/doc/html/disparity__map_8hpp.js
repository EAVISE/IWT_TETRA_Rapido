var disparity__map_8hpp =
[
    [ "DISPARITY_MAP_EMPTY", "disparity__map_8hpp.html#ad66aeecd7408164141f7a5c3bdc0276f", null ],
    [ "DISPARITY_MAP_NULL_POINTER_COLUMNS", "disparity__map_8hpp.html#a6000ed2263480a25389b198c2578afe9", null ],
    [ "DISPARITY_MAP_NULL_POINTER_ROWS", "disparity__map_8hpp.html#a45cd3c99f6c942bd4ce992973722eb3e", null ],
    [ "DISPARITY_MAP_PIXEL_OUT_OF_RANGE", "disparity__map_8hpp.html#a7b6fc38c0503dd5e53ba99063e9aff4a", null ],
    [ "DISPARITY_MAP_PIXEL_VALUE_INVALID", "disparity__map_8hpp.html#acbedf2267c80fbabfda3bcdb9c33ad70", null ]
];