var classdlp_1_1_capture_1_1_settings_1_1_data =
[
    [ "Type", "classdlp_1_1_capture_1_1_settings_1_1_data.html#a39b1591d3cf9de9a1923a2ec8deb3932", [
      [ "IMAGE_FILE", "classdlp_1_1_capture_1_1_settings_1_1_data.html#a39b1591d3cf9de9a1923a2ec8deb3932a68f06c4f04b60df9dc1eb70fd9468558", null ],
      [ "IMAGE_DATA", "classdlp_1_1_capture_1_1_settings_1_1_data.html#a39b1591d3cf9de9a1923a2ec8deb3932adc0a5d4243f38ec51f61fd0f90fbc42d", null ],
      [ "INVALID", "classdlp_1_1_capture_1_1_settings_1_1_data.html#a39b1591d3cf9de9a1923a2ec8deb3932accc0377a8afbf50e7094f5c23a8af223", null ]
    ] ],
    [ "Data", "classdlp_1_1_capture_1_1_settings_1_1_data.html#aea1cdbdab11e78e4c86a4ab52b97c8d0", null ],
    [ "Data", "classdlp_1_1_capture_1_1_settings_1_1_data.html#ad4b01fe7f8dd32b7eae69e14c69e4e4d", null ],
    [ "Get", "classdlp_1_1_capture_1_1_settings_1_1_data.html#a4f24b4e62a3382f12496bdee145a00b6", null ],
    [ "GetEntryDefault", "classdlp_1_1_capture_1_1_settings_1_1_data.html#a1bf95dcafd692248c091e0f025baa7a2", null ],
    [ "GetEntryName", "classdlp_1_1_capture_1_1_settings_1_1_data.html#ab18d12eda7cb4ca828f5f70648883e4e", null ],
    [ "GetEntryValue", "classdlp_1_1_capture_1_1_settings_1_1_data.html#a778caa9f7a9a85e698c2414117e3bb1e", null ],
    [ "Set", "classdlp_1_1_capture_1_1_settings_1_1_data.html#a6035890f6146a5e37f7288ed0263cc7b", null ],
    [ "SetEntryValue", "classdlp_1_1_capture_1_1_settings_1_1_data.html#ab569242b235bff17da019ebd6727fda3", null ]
];