var classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion =
[
    [ "Options", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#ab9230a1220646ca53f4591089f569ec6", [
      [ "SET_TO_ZERO", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#ab9230a1220646ca53f4591089f569ec6a603ae046eadf375b94f7746bb80f7431", null ],
      [ "NORMAL", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#ab9230a1220646ca53f4591089f569ec6a1e23852820b9154316c7c06e2b7ba051", null ]
    ] ],
    [ "TangentDistortion", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#adf42a18079865d0388169df4bf274b42", null ],
    [ "TangentDistortion", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#aee5d1cc5f4c2be0f3855cd92c7588ded", null ],
    [ "Get", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#a3ea91b681deabed5d8bb02bb0564dc3b", null ],
    [ "GetEntryDefault", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#a880e0fac5d667720e241ed5a83555eb5", null ],
    [ "GetEntryName", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#ab521b14c200e4846e5884ab8a30d1031", null ],
    [ "GetEntryValue", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#ad2c5e5529a8e8fd3f79eeb2e670df011", null ],
    [ "Set", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#a5b0b02f65ad81217842dbe1c934cccc0", null ],
    [ "SetEntryValue", "classdlp_1_1_calibration_1_1_settings_1_1_tangent_distortion.html#ac27c979a0afa7e29574339c9b346888c", null ]
];