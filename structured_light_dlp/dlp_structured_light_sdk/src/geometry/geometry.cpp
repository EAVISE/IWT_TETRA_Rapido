/** \file   geometry.cpp
 *  \brief  Contains methods for setting geometry origins and finding intersections
 *  of lines in 3D space to determine point locations from disparity map and calibration data.
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/capture/capture.hpp>
#include <common/pattern/pattern.hpp>
#include <common/point_cloud.hpp>
#include <calibration/calibration.hpp>
#include <geometry/geometry.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>


#include <vector>
#include <string>
#include <math.h>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief Default Constructor origin set = false, set name GEOMETRY_DEBUG*/
Geometry::Geometry(){
    this->origin_set_ = false;
    this->is_setup_ = false;
    this->debug_.SetName("GEOMETRY_DEBUG (" + dlp::Number::ToString(this)+ "): ");
}

/** \brief Destroys object */
Geometry::~Geometry(){
    this->Clear();
}

/** \brief Releases all cv::Mat data objects */
void Geometry::Clear(){
    this->origin_set_ = false;
    this->origin_calibration_.Clear();
    this->origin_.ray.release();

    for(unsigned int iView = 0; iView < this->viewport_.size(); iView++){
        this->viewport_.at(iView).ray.release();
    }
}

/** \brief  Method to set point cloud scaling and distance thresholds
 *  \param[in] settings  \ref dlp::Parameters object with \ref Setting::Entry objects
 */
ReturnCode Geometry::Setup(const dlp::Parameters &settings){
    ReturnCode ret;

    if(settings.isEmpty())
        return ret.AddError(GEOMETRY_SETTINGS_EMPTY);

    settings.Get(&this->filter_viewpoint_rays_);
    settings.Get(&this->max_percent_error_);
    settings.Get(&this->point_scale_percent_);
    settings.Get(&this->max_distance_);
    settings.Get(&this->min_distance_);

    this->is_setup_ = true;

    return ret;
}

/**
 * \brief Retrieves object settings
 * \param[out] settings         \ref dlp::Parameters object is empty
 * \retval GEOMETRY_NULL_POINTER    Return argument is NULL
 */
ReturnCode Geometry::GetSetup(dlp::Parameters *settings)const{
    ReturnCode ret;

    if(!settings)
        return ret.AddError(GEOMETRY_NULL_POINTER);


    settings->Set(this->filter_viewpoint_rays_);
    settings->Set(this->max_percent_error_);
    settings->Set(this->point_scale_percent_);
    settings->Set(this->max_distance_);
    settings->Set(this->min_distance_);

    return ret;
}


/** \brief  Creates System Geometry Origin/Reference point using \ref dlp::Calibration::Data
 *          and generates optical rays
 *  \retval GEOMETRY_CALIBRATION_NOT_COMPLETE   Supplied calibration data is NOT complete
 */
ReturnCode Geometry::SetOriginView(const dlp::Calibration::Data &origin_calib){
    ReturnCode ret;

    this->debug_.Msg("Setting origin viewpoint...");

    // Check that calibration is compelete
    if(!origin_calib.isComplete())
        return ret.AddError(GEOMETRY_CALIBRATION_NOT_COMPLETE);

    // Save the origin claibration
    this->origin_calibration_ = origin_calib;

    // Set the origin view center to 0,0,0
    this->debug_.Msg("Setting origin center to 0,0,0");
    this->origin_.center.x = 0.0;
    this->origin_.center.y = 0.0;
    this->origin_.center.z = 0.0;

    // Get calibration resolution
    unsigned int columns;
    unsigned int rows;
    unsigned int ray_count;

    origin_calib.GetModelResolution(&columns,&rows);
    this->debug_.Msg("Model resolution = " + dlp::Number::ToString(columns) +" by " + dlp::Number::ToString(rows));

    ray_count = columns * rows;
    this->debug_.Msg("Total number of rays = " + dlp::Number::ToString(ray_count));

    // Get calibration data
    cv::Mat intrinsic;
    cv::Mat distortion;
    cv::Mat extrinsic;
    double  error;

    origin_calib.GetData(&intrinsic,&extrinsic,&distortion,&error);

    std::stringstream msg;
    msg << "Retrieved origin calibration" <<
           "\n\nIntrinsic (pixels) = \n" << intrinsic  <<
           "\n\nDistortion(pixels) = \n" << distortion <<
           "\n\nExtrinsic (real) = \n"   << extrinsic  << std::endl;
    this->debug_.Msg(msg);

    // Compute the optical rays
    this->debug_.Msg("Computing the optical rays...");
    cv::Mat original_rays(    ray_count, 1, CV_64FC2);
    cv::Mat undistorted_rays( ray_count, 1, CV_64FC2);

    // If diamond array projector add offset of 0.5 for every other row
    if(!origin_calib.isCamera() && (rows > columns)){
        this->debug_.Msg( "Projector has DMD with diamond array");
    }

    unsigned int xCol;
    unsigned int yRow;
    unsigned int iRay = 0;
    for(     yRow = 0; yRow < rows;    yRow++){
        for( xCol = 0; xCol < columns; xCol++){
            cv::Point2d temp;
            temp.x = (double)xCol;
            temp.y = (double)yRow;

            // If diamond array projector add offset of 0.5 for every other row
            if(!origin_calib.isCamera() && (rows > columns)){
                // Correct for row compression
                temp.y = temp.y * 0.5;

                // Correct for row shifting
                if((yRow % 2) == 1) temp.x = temp.x + 0.5;
            }

            original_rays.at<cv::Point2d>(iRay) = temp;
            iRay++;
        }
    }

    // Undistort the optical rays
    this->debug_.Msg("Undistorting optical rays...");
    cv::undistortPoints(original_rays, undistorted_rays, intrinsic, distortion);


    // Translate to 3 dimensional rays
    this->origin_.ray.release();
    this->origin_.ray.create(rows,columns,CV_64FC3);

    dlp::Point::Cloud ray_cloud;

    xCol = 0;
    yRow = 0;
    this->debug_.Msg("Normalizing optical rays...");
    for(iRay = 0; iRay < ray_count;iRay++){
        cv::Point2d ray             = undistorted_rays.at<cv::Point2d>(iRay);   // Not normalized
        double      ray_length      = sqrt(pow(ray.x,2)+pow(ray.y,2)+1.0);      // Assume unit length for z
        cv::Point3d ray_normalized;
        dlp::Point  ray_point;

        ray_normalized.x = ray.x / ray_length;
        ray_normalized.y = ray.y / ray_length;
        ray_normalized.z = 1.0   / ray_length;

        ray_point.x = ray_normalized.x;
        ray_point.y = ray_normalized.y;
        ray_point.z = ray_normalized.z;

        ray_cloud.Add(ray_point);

        // Save the normalized ray in origin rays
        this->origin_.ray.at<cv::Point3d>(yRow,xCol) = ray_normalized;

        // Increment pixel location counters
        xCol++;

        if(xCol == columns){
            xCol = 0;
            yRow++;
        }
    }


    ray_cloud.Add(dlp::Point(0,0,0));
    //ray_cloud.SaveXYZ("ray_cloud_origin.xyz",' ');

    this->debug_.Msg("Origin viewport set");
    this->origin_set_ = true;

    return ret;
}

/** \brief  Creates a new \ref dlp::Geometry::ViewPort with supplied
 *          \ref dlp::Calibration::Data. This method allows for multiple
 *          view ports which allows for multi-camera setups
 *  \retval GEOMETRY_NULL_POINTER               Return argument is NULL
 *  \retval GEOMETRY_CALIBRATION_NOT_COMPLETE   Supplied calibration data is NOT complete
 *  \retval GEOMETRY_NO_ORIGIN_SET              Origin view point has NOT been created
 */
ReturnCode Geometry::AddView(const dlp::Calibration::Data &viewport_calib,
                                             unsigned int *ret_viewport_id){

    ReturnCode ret;

    this->debug_.Msg("Adding viewport...");

    // Check that return viewport id int is not null
    if(!ret_viewport_id)
        return ret.AddError(GEOMETRY_NULL_POINTER);

    // Check that calibration is compelete
    if(!viewport_calib.isComplete())
        return ret.AddError(GEOMETRY_CALIBRATION_NOT_COMPLETE);

    // Check that the origin has been set
    if(!this->origin_set_)
        return ret.AddError(GEOMETRY_NO_ORIGIN_SET);

    Geometry::ViewPoint viewport_temp;

    // Get origin calibration data
    cv::Mat origin_intrinsic;
    cv::Mat origin_distortion;
    cv::Mat origin_extrinsic;
    double  origin_error;
    cv::Mat viewport_intrinsic;
    cv::Mat viewport_distortion;
    cv::Mat viewport_extrinsic;
    double  viewport_error;

    // Get origin calibration data
    this->origin_calibration_.GetData(&origin_intrinsic,
                                      &origin_extrinsic,
                                      &origin_distortion,
                                      &origin_error);




    // Get viewport calibration data
    viewport_calib.GetData(&viewport_intrinsic,
                           &viewport_extrinsic,
                           &viewport_distortion,
                           &viewport_error);

    std::stringstream msg;
    msg << "Retrieved new viewport calibration" <<
           "\n\nIntrinsic (pixels) = \n" << viewport_intrinsic  <<
           "\n\nDistortion(pixels) = \n" << viewport_distortion <<
           "\n\nExtrinsic (real) = \n"   << viewport_extrinsic  << std::endl;
    this->debug_.Msg(msg);

    // Get origin rotation and translation data.
    cv::Mat origin_rotation;
    cv::Mat origin_rotation_transposed;
    cv::Mat origin_rotation_3x3(3,3,CV_64FC1);
    cv::Mat origin_translation;
    cv::Mat origin_translation_transposed;
    cv::Mat viewport_rotation;
    cv::Mat viewport_rotation_transposed;
    cv::Mat viewport_rotation_3x3(3,3,CV_64FC1);
    cv::Mat viewport_translation;
    cv::Mat viewport_translation_transposed;
    cv::Mat viewport_center(3,1,CV_64FC1);
    cv::Mat empty_3x3(3,3,CV_64FC1);

    // Get extrinsic translation and rotation data
    origin_rotation    = origin_extrinsic.row(dlp::Calibration::Data::EXTRINSIC_ROW_ROTATION).clone();
    origin_translation = origin_extrinsic.row(dlp::Calibration::Data::EXTRINSIC_ROW_TRANSLATION).clone();


    viewport_rotation    = viewport_extrinsic.row(dlp::Calibration::Data::EXTRINSIC_ROW_ROTATION).clone();
    viewport_translation = viewport_extrinsic.row(dlp::Calibration::Data::EXTRINSIC_ROW_TRANSLATION).clone();

    // Convert the rotation data to 3x3 matrices
    this->debug_.Msg("Converting 3x1 extrinsic data to 3x3 matricies...");
    cv::Rodrigues(origin_rotation,origin_rotation_3x3);
    cv::Rodrigues(viewport_rotation,viewport_rotation_3x3);

    // Transpose origin rotation and translation
    cv::transpose(origin_rotation,origin_rotation_transposed);
    cv::transpose(origin_translation,origin_translation_transposed);

    // Transpose viewport rotation and translation
    cv::transpose(viewport_rotation,viewport_rotation_transposed);
    cv::transpose(viewport_translation,viewport_translation_transposed);

    // Calculate the viewports center point
    this->debug_.Msg("Calculating new viewport center location...");
    cv::gemm(viewport_rotation_3x3, viewport_translation_transposed, -1, empty_3x3, 0, viewport_center, CV_GEMM_A_T);
    cv::gemm(origin_rotation_3x3, viewport_center, 1, origin_translation_transposed, 1, viewport_center, 0);

    viewport_temp.center.x = viewport_center.at<double>(0);
    viewport_temp.center.y = viewport_center.at<double>(1);
    viewport_temp.center.z = viewport_center.at<double>(2);


    msg.str(std::string());
    msg << "Origin viewport center = " << this->origin_.center << std::endl;
    msg << "New viewport center    = " << viewport_temp.center << std::endl;
    this->debug_.Msg(msg);

    // Get viewport calibration resolution
    unsigned int columns;
    unsigned int rows;
    unsigned int ray_count;

    viewport_calib.GetModelResolution(&columns,&rows);
    this->debug_.Msg("New model resolution = " + dlp::Number::ToString(columns) +" by " + dlp::Number::ToString(rows));

    ray_count = columns * rows;
    this->debug_.Msg("Total number of rays = " + dlp::Number::ToString(ray_count));

    // Compute the optical rays
    this->debug_.Msg("Computing the optical rays...");
    cv::Mat original_rays(    ray_count, 1, CV_64FC2);
    cv::Mat undistorted_rays( ray_count, 1, CV_64FC2);

    unsigned int xCol;
    unsigned int yRow;
    unsigned int iRay = 0;
    for(     yRow = 0; yRow < rows;    yRow++){
        for( xCol = 0; xCol < columns; xCol++){
            cv::Point2d temp;
            temp.x = (double)xCol;
            temp.y = (double)yRow;

            // If diamond array projector add offset of 0.5 for every other row
            if(!viewport_calib.isCamera() && (rows > columns)){
                // Correct for row compression
                temp.y = temp.y * 0.5;

                // Correct for row shifting
                if((yRow % 2) == 1) temp.x = temp.x + 0.5;
            }

            original_rays.at<cv::Point2d>(iRay) = temp;
            iRay++;
        }
    }

    // Undistort the optical rays
    this->debug_.Msg("Undistorting optical rays...");
    cv::undistortPoints(original_rays, undistorted_rays, viewport_intrinsic, viewport_distortion);

    // Translate to 3 dimensional rays
    this->debug_.Msg("Normalizing optical rays...");
    viewport_temp.ray.create(rows,columns,CV_64FC3);
    cv::Mat viewport_rays(3, ray_count, CV_64FC1);

    dlp::Point::Cloud ray_cloud;
    for(iRay = 0; iRay < ray_count;iRay++){
        cv::Point2d ray             = undistorted_rays.at<cv::Point2d>(iRay);   // Not normalized
        double      ray_length      = sqrt(pow(ray.x,2)+pow(ray.y,2)+1.0);      // Assume unit length for z
        dlp::Point ray_point;

        // Save the normalized ray in origin rays
        viewport_rays.at<double>(0,iRay) = ray.x / ray_length;
        viewport_rays.at<double>(1,iRay) = ray.y / ray_length;
        viewport_rays.at<double>(2,iRay) = 1.0   / ray_length;

        ray_point.x = viewport_rays.at<double>(0,iRay);
        ray_point.y = viewport_rays.at<double>(1,iRay);
        ray_point.z = viewport_rays.at<double>(2,iRay);
        ray_cloud.Add(ray_point);
    }


    // Rotate projector optical rays into the origin coordinate system.
    this->debug_.Msg("Rotating and transposing new viewport rays into origin coordinate system...");
    cv::Mat R(3, 3, CV_64FC1);

    cv::gemm(origin_rotation_3x3, viewport_rotation_3x3, 1, empty_3x3, 0, R, CV_GEMM_B_T);
    cv::gemm(R, viewport_rays, 1, empty_3x3, 0,viewport_rays, 0);



    xCol = 0;
    yRow = 0;
    for(iRay = 0; iRay < ray_count;iRay++){
        cv::Point3d ray;

        ray.x = viewport_rays.at<double>(0,iRay);
        ray.y = viewport_rays.at<double>(1,iRay);
        ray.z = viewport_rays.at<double>(2,iRay);



        // Save the normalized ray in origin rays
        viewport_temp.ray.at<cv::Point3d>(yRow,xCol) = ray;


        // Increment pixel location counters
        xCol++;

        if(xCol == columns){
            xCol = 0;
            yRow++;
        }
    }
    this->viewport_.push_back(viewport_temp);

    (*ret_viewport_id) = this->viewport_.size() - 1;


    ray_cloud.Add(dlp::Point(0,0,0));

//    ray_cloud.SaveXYZ("ray_cloud_viewport_" + dlp::Number::ToString(*ret_viewport_id) +  ".xyz",' ');

    this->debug_.Msg("Viewport " + dlp::Number::ToString((*ret_viewport_id)) + " added");

    return ret;

}

/** \brief Finds 3D line intersections using a line from the origin point and a line from the view point
*   \param[in] origin_x     Origin_x is the decoded value from the vertical structured light patterns
*   \param[in] origin_y     Origin_y is the decoded value from the horizontal structured light patterns
*   \param[in] viewport_id  Is the ID for the camera to calculate intersections from
*   \param[in] viewport_x   Viewport x is the camera column pixel location
*   \param[in] viewport_y   Viewport y is the camera row pixel location
*   \param[out] ret_xyz         Pointer to return dlp::Point
*   \param[out] ret_distance    Pointer to return dlp::Point distance from origin
*   \retval GEOMETRY_VIEWPORT_ID_OUT_OF_RANGE   Requested viewport does NOT exist
*   \retval GEOMETRY_ORIGIN_RAY_OUT_OF_RANGE    Attempting to access origin optical ray that does NOT exist
*   \retval GEOMETRY_VIEWPORT_RAY_OUT_OF_RANGE  Attempting to access view point optical ray that does NOT exist
*   \retval GEOMETRY_NULL_POINTER               Return pointer argument is NULL
*/
ReturnCode Geometry::Find3dLineIntersection(const unsigned int &origin_x, const unsigned int &origin_y,
                                            const unsigned int &viewport_id,
                                            const unsigned int &viewport_x, const unsigned int &viewport_y,
                                                         Point *ret_xyz, double *ret_distance){

    ReturnCode ret;

    // Check viewport id
    if(viewport_id >= this->viewport_.size())
        return ret.AddError(GEOMETRY_VIEWPORT_ID_OUT_OF_RANGE);


    // Check  origin ray positions
    if((origin_x >= (unsigned int) this->origin_.ray.cols) ||
       (origin_y >= (unsigned int) this->origin_.ray.rows))
        return ret.AddError(GEOMETRY_ORIGIN_RAY_OUT_OF_RANGE);

    // Check  viewport ray positions
    if((viewport_x >= (unsigned int) this->viewport_.at(viewport_id).ray.cols) ||
       (viewport_y >= (unsigned int) this->viewport_.at(viewport_id).ray.rows))
        return ret.AddError(GEOMETRY_VIEWPORT_RAY_OUT_OF_RANGE);


    // Check pointers
    if(!ret_xyz)
        return ret.AddError(GEOMETRY_NULL_POINTER);

    if(!ret_distance)
        return ret.AddError(GEOMETRY_NULL_POINTER);


    // Find the intersection and distance
    this->Unsafe_Find3dLineIntersection(origin_x,    origin_y,
                                        viewport_id, viewport_x, viewport_y,
                                        ret_xyz, ret_distance);

    return ret;
}

/** \brief Finds 3D line intersections using a line from the origin point and a line from the view point
*   \param[in] origin_x     Origin_x is the decoded value from the vertical structured light patterns
*   \param[in] origin_y     Origin_y is the decoded value from the horizontal structured light patterns
*   \param[in] viewport_id  Is the ID for the camera to calculate intersections from
*   \param[in] viewport_x   Viewport x is the camera column pixel location
*   \param[in] viewport_y   Viewport y is the camera row pixel location
*   \param[out] ret_xyz         Pointer to return dlp::Point
*   \param[out] ret_distance    Pointer to return dlp::Point distance from origin
*/
void Geometry::Unsafe_Find3dLineIntersection(const unsigned int &origin_x, const unsigned int &origin_y,
                                             const unsigned int &viewport_id,
                                             const unsigned int &viewport_x, const unsigned int &viewport_y,
                                                  dlp::Point *ret_xyz,
                                                      double *ret_distance){

    // Origin offset and vector
    cv::Point3d q1 = this->origin_.center;
    cv::Point3d v1 = this->origin_.ray.at<cv::Point3d>(origin_y,origin_x);

    // Viewport offset and vector
    cv::Point3d q2 = this->viewport_.at(viewport_id).center;
    cv::Point3d v2 = this->viewport_.at(viewport_id).ray.at<cv::Point3d>(viewport_y,viewport_x);

    //std::cout<<"q1 v1 q2 v2 "<<q1<<" "<<v1<<" "<<q2<<" "<<v2<<std::endl;
    // Find difference between origin and viewport center

    cv::Point3d q_diff2 = q2 - q1;
    cv::Point3d q_diff = q1 - q2;

    cv::Point3d n = v1.cross(v2);
    cv::Point3d n1 = v1.cross(n);
    cv::Point3d n2 = v2.cross(n);


    cv::Point3d c1 = q1 + (q_diff2.dot(n2)/v1.dot(n2)*v1);
    cv::Point3d c2 = q2 + (q_diff.dot(n1)/v2.dot(n1)*v2);

    cv::Point3d intersection = (c1+c2)*0.5/**0.436619*/;


//    // Calculate dot products
//    double v1_dot_v1     = v1.dot(v1);
//    double v1_dot_v2     = v1.dot(v2);
//    double v2_dot_v2     = v2.dot(v2);
//    double q_diff_dot_v1 = q_diff.dot(v1);
//    double q_diff_dot_v2 = q_diff.dot(v2);
//
//    // Calculate scale factors
//    double denom =  (v1_dot_v1*v2_dot_v2) - (v1_dot_v2*v1_dot_v2);
//    double s     =  (v1_dot_v2/denom)*q_diff_dot_v2 - (v2_dot_v2/denom)*q_diff_dot_v1;
//    double t     = -(v1_dot_v2/denom)*q_diff_dot_v1 + (v1_dot_v1/denom)*q_diff_dot_v2;
//
//    // Calculate closest XYZ point between the two lines
//    cv::Point3d intersection = ((q1+s*v1) + (q2+t*v2)) * 0.5;//*0.436619;

    // Convert the cv::Point3d to dlp::PointXYZ
    ret_xyz->x = intersection.x * this->point_scale_percent_.Get();
    ret_xyz->y = intersection.y * this->point_scale_percent_.Get();
    ret_xyz->z = intersection.z * this->point_scale_percent_.Get();

    //std::cout<<"TI: "<<intersection2<<" Eavise "<<intersection<<std::endl;

    // Calculate the distance from the origin
    (*ret_distance) = v1.dot(intersection-q1);
}

/** \brief Generates \ref dlp::Point::Cloud by finding the line intersections
 *         between the requested view port rays and the origin rays
 *  \param[in]  viewport_id         \ref dlp::Geometry::ViewPoint ID to select correct optical rays
 *  \param[in]  column_disparity    \ref dlp::DisparityMap generated from dlp::StructuredLight module set for \ref dlp::StructuredLight::Settings::Pattern::Orientation::Type::VERTICAL
 *  \param[in]  row_disparity       \ref dlp::DisparityMap generated from dlp::StructuredLight module set for \ref dlp::StructuredLight::Settings::Pattern::Orientation::Type::HORIZONTAL
 *  \param[out] ret_cloud           Pointer to return \ref dlp::Point::Cloud
 *  \param[out] ret_depthmap        Pointer to return \ref dlp::Image depth map
 *  \retval GEOMETRY_VIEWPORT_ID_OUT_OF_RANGE   Requested view port does NOT exist
 *  \retval GEOMETRY_NULL_POINTER               Return argument is NULL
 */
ReturnCode Geometry::GeneratePointCloud(const unsigned int &viewport_id,
                                        dlp::DisparityMap  &column_disparity,
                                        dlp::DisparityMap  &row_disparity,
                                        dlp::Point::Cloud  *ret_cloud,
                                        dlp::Image         *ret_depthmap){
    ReturnCode ret;

    // Check viewport id
    if(viewport_id >= this->viewport_.size())
        return ret.AddError(GEOMETRY_VIEWPORT_ID_OUT_OF_RANGE);

    // Check pointers
    if(!ret_cloud)
        return ret.AddError(GEOMETRY_NULL_POINTER);

    if(!ret_depthmap)
        return ret.AddError(GEOMETRY_NULL_POINTER);


    // Check  viewport resolution with column disparity
    unsigned int disparity_image_columns;
    unsigned int disparity_image_rows;

    column_disparity.GetColumns(&disparity_image_columns);
    column_disparity.GetRows(&disparity_image_rows);

    if((disparity_image_columns > (unsigned int) this->viewport_.at(viewport_id).ray.cols) ||
       (disparity_image_rows    > (unsigned int) this->viewport_.at(viewport_id).ray.rows))
        return ret.AddError(GEOMETRY_DISPARITY_MAP_RESOLUTION_INVALID);

    row_disparity.GetColumns(&disparity_image_columns);
    row_disparity.GetRows(&disparity_image_rows);

    if((disparity_image_columns > (unsigned int) this->viewport_.at(viewport_id).ray.cols) ||
       (disparity_image_rows    > (unsigned int) this->viewport_.at(viewport_id).ray.rows))
        return ret.AddError(GEOMETRY_DISPARITY_MAP_RESOLUTION_INVALID);

    // Allocate memory for the distance map
    double min_distance = 0;
    double max_distance = 0;
    dlp::Image distance_map(disparity_image_columns,
                            disparity_image_rows,
                            Image::Format::MONO_DOUBLE);
    distance_map.FillImage((double)0.0);

    // Create point map to track all points to specfic origin rays
    // rows     // columns  // points
    std::vector<std::vector<std::vector<dlp::Point> > > point_cloud;
    std::vector<std::vector<std::vector<  double  > > > point_cloud_distance;

    // Check if viewpoint rays should be filtered
    if(this->filter_viewpoint_rays_.Get()){
        // Size the vectors to the origin dimensions
        point_cloud.resize(this->origin_.ray.rows);
        point_cloud_distance.resize(this->origin_.ray.rows);
        for( int yRow = 0; yRow < this->origin_.ray.rows; yRow++){
            point_cloud[yRow].resize(this->origin_.ray.cols);
            point_cloud_distance[yRow].resize(this->origin_.ray.cols);
        }
    }

    // Clear the point cloud
    ret_cloud->Clear();
    double max_origin_distance = this->max_distance_.Get();
    double min_origin_distance = this->min_distance_.Get();
    bool   check_distance      = (max_origin_distance != min_origin_distance);

    for(    unsigned int yRow = 0; yRow < disparity_image_rows;    yRow++){
        for(unsigned int xCol = 0; xCol < disparity_image_columns; xCol++){
            int value_column_disparity;
            int value_row_disparity;

            // Get the disparity values
            column_disparity.GetPixel(xCol, yRow, &value_column_disparity);
            row_disparity.GetPixel(   xCol, yRow, &value_row_disparity);

            // Check that neither pixels are invalid
            if((value_column_disparity != DisparityMap::INVALID_PIXEL) &&
               (value_row_disparity    != DisparityMap::INVALID_PIXEL) &&
               (value_row_disparity     < this->origin_.ray.rows) &&
               (value_column_disparity  < this->origin_.ray.cols)){


                // Calculate the point in space
                dlp::Point point;
                double     distance;

                this->Unsafe_Find3dLineIntersection((unsigned int) value_column_disparity,
                                                    (unsigned int) value_row_disparity,    // Origin ray
                                                    viewport_id, xCol, yRow,                        // Viewport ray
                                                    &point, &distance);                             // Return point and distance from origin


                // Check that z is greater than zero
                if(point.z > 0){

                    // If the distance is within the set min and max origin distances
                    // or no distance check is needed add the point to the cloud
                    if( (!check_distance) ||
                       ((distance <= max_origin_distance) &&
                       (distance >= min_origin_distance))){

                        // Save the distance
                        distance_map.SetPixel(xCol,yRow,distance);

                        // Check if the min and max distances have not been set before
                        if(max_distance == 0 && min_distance == 0){
                            max_distance = distance;
                            min_distance = distance;
                        }

                        // Check the point distance for the depth map
                        if(distance < min_distance) min_distance = distance;
                        if(distance > max_distance) max_distance = distance;

                        // Check if viewpoint rays should be filtered
                        if(this->filter_viewpoint_rays_.Get()){
                            // Save the point and origin distance
                            point_cloud[value_row_disparity][value_column_disparity].push_back(point);
                            point_cloud_distance[value_row_disparity][value_column_disparity].push_back(distance);
                        }
                        else{
                            // Add the point
                            ret_cloud->Add(point);
                        }
                    }
                }
            }
        }
    }

    // If viewpoint rays should be filtered reprocess the cloud
    if(this->filter_viewpoint_rays_.Get()){

        // Claculate maximum error
        double max_error = this->max_percent_error_.Get() / 100;

        for(     int yRow = 0; yRow < this->origin_.ray.rows; yRow++){
            for( int xCol = 0; xCol < this->origin_.ray.cols; xCol++){

                // Check that there are enough points
                //std::cout<<"pointcloud size "<<point_cloud[yRow][xCol].size()<<std::endl;
                if(point_cloud[yRow][xCol].size() > 0){

                    // Calculate average
                    double sum     = 0;
                    double average = 0;
                    for(unsigned int iPoint = 0; iPoint < point_cloud[yRow][xCol].size(); iPoint++){
                        sum = sum + point_cloud_distance[yRow][xCol][iPoint];
                    }
                    average = sum / (double)point_cloud[yRow][xCol].size();

                    // Calculate the error for each point and keep points within threshold
                    std::vector<dlp::Point> valid_points;
                    for(unsigned int iPoint = 0; iPoint < point_cloud[yRow][xCol].size(); iPoint++){
                        double percent_error = std::abs(average - point_cloud_distance[yRow][xCol][iPoint])
                                               / point_cloud_distance[yRow][xCol][iPoint];

                        // If the error is below threshold save point
                        if(percent_error <= max_error){
                            valid_points.push_back(point_cloud[yRow][xCol][iPoint]);
                        }
                    }

                    // Calculate the average point location if more than zero points
                    if(valid_points.size() > 0){
                        dlp::Point valid_point;
                        for(unsigned int iPoint = 0; iPoint < valid_points.size(); iPoint++){
                            valid_point.x += valid_points[iPoint].x;
                            valid_point.y += valid_points[iPoint].y;
                            valid_point.z += valid_points[iPoint].z;
                        }
                        valid_point.x = valid_point.x / valid_points.size();
                        valid_point.y = valid_point.y / valid_points.size();
                        valid_point.z = valid_point.z / valid_points.size();
                        //std::cout<<"valid point z "<<valid_point.z<<std::endl;
                        // Save the point
                        ret_cloud->Add(valid_point);
                    }
                }
            }
        }
    }


    // Create the depth map based
    ret_depthmap->Create(disparity_image_columns,disparity_image_rows,Image::Format::RGB_UCHAR);
    ret_depthmap->FillImage(PixelRGB(0,0,0));

    // Convert distance to color
    for(    unsigned int xCol = 0; xCol < disparity_image_columns; xCol++){
        for(unsigned int yRow = 0; yRow < disparity_image_rows; yRow++){
            double          distance;
            PixelRGB        depth;
            unsigned char   depth_temp;

            // Get the distamce value
            distance_map.GetPixel(   xCol, yRow, &distance);

            // Check that distance is greater than zero
            if(distance > 0){
                // Scale the distance point so that it is a value between 0 and 255
                depth_temp = (unsigned char) ((max_distance - distance) * 255 / (max_distance - min_distance));

                // Determine the color
                if(depth_temp < 43){
                    depth.r = depth_temp * 6;
                    depth.g = 0;
                    depth.b = depth_temp * 6;
                }
                if(depth_temp > 42 && depth_temp < 85){
                    depth.r = 255 - (depth_temp - 43) * 6;
                    depth.g = 0;
                    depth.b = 255;
                }
                if(depth_temp > 84 && depth_temp < 128){
                    depth.r = 0;
                    depth.g = (depth_temp - 85) * 6;
                    depth.b = 255;
                }
                if(depth_temp > 127 && depth_temp < 169){
                    depth.r = 0;
                    depth.g = 255;
                    depth.b = 255 - (depth_temp - 128) * 6;
                }
                if(depth_temp > 168 && depth_temp < 212){
                    depth.r = (depth_temp - 169) * 6;
                    depth.g = 255;
                    depth.b = 0;
                }
                if(depth_temp > 211 && depth_temp < 254){
                    depth.r = 255;
                    depth.g = 255 - (depth_temp - 212) * 6;
                    depth.b = 0;
                }
                if(depth_temp > 253){
                    depth.r = 255;
                    depth.g = 0;
                    depth.b = 0;
                }

                // Save the value
                ret_depthmap->SetPixel(xCol,yRow,depth);
            }
        }
    }

    return ret;
}



ReturnCode Geometry::CalculateFlatness(const dlp::Point::Cloud &cloud, double *flatness){
    ReturnCode ret;

    // Check pointer
    if(!flatness)
        return ret.AddError(GEOMETRY_NULL_POINTER);

    // Check that point cloud is NOT empty
    if(cloud.GetCount() == 0)
        return ret.AddError(GEOMETRY_POINT_CLOUD_EMPTY);

    // Convert point cloud to cv::Mat
    cv::Mat cloud_matrix(cloud.GetCount(),3,CV_64FC1);
    for(unsigned long long iPoint = 0; iPoint < cloud.GetCount(); iPoint++){
        dlp::Point temp;
        cloud.Get(iPoint,&temp);
        cloud_matrix.at<double>(iPoint,0) = temp.x;
        cloud_matrix.at<double>(iPoint,1) = temp.y;
        cloud_matrix.at<double>(iPoint,2) = temp.z;
    }

    // Calculate the covariance
    cv::Mat covariance;
    cv::Mat mean;
    cv::calcCovarMatrix(cloud_matrix, covariance, mean, CV_COVAR_NORMAL | CV_COVAR_ROWS);

    // Calculate the eigen values and vectors
    cv::Mat eigen_values;
    cv::Mat eigen_vectors;
    cv::eigen(covariance,eigen_values,eigen_vectors);

    std::cout << "Eigen Values = " << eigen_values << std::endl;

    // Return the flatness
    (*flatness) = eigen_values.at<double>(0,2);

    return ret;
}






}
