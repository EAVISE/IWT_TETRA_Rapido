/** \file   lcr4500_settings.cpp
 *  \brief  Contains setting entry objects for the LightCrafter 4500 EVM
 */

#include <common/debug.hpp>
#include <common/other.hpp>
#include <common/image/image.hpp>
#include <common/parameters.hpp>
#include <common/pattern/pattern.hpp>

#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <ctime>


#include <dlp_platforms/dlp_platform.hpp>
#include <dlp_platforms/lightcrafter_4500/lcr4500.hpp>

#include <dlp_platforms/lightcrafter_4500/dlpc350_api.hpp>
#include <dlp_platforms/lightcrafter_4500/common.hpp>
#include <dlp_platforms/lightcrafter_4500/error.hpp>
#include <dlp_platforms/lightcrafter_4500/dlpc350_usb.hpp>
#include <dlp_platforms/lightcrafter_4500/dlpc350_firmware.hpp>
#include <dlp_platforms/lightcrafter_4500/flashdevice.hpp>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{


const unsigned int LCr4500::Settings::PATTERN_LUT_SIZE  = MAX_EXP_PAT_LUT_ENTRIES;
const unsigned int LCr4500::Settings::IMAGE_LUT_SIZE    = MAX_EXP_IMAGE_LUT_ENTRIES;
const unsigned int LCr4500::Settings::BUFFER_IMAGE_SIZE =   2;


LCr4500::Settings::File::DLPC350_Firmware::DLPC350_Firmware(){
    this->value_ = "resources/lcr4500/DLPR350PROM_v2.0.0_no_images.bin";
}

LCr4500::Settings::File::DLPC350_Firmware::DLPC350_Firmware(const std::string &file){
    if(dlp::File::Exists(file)){
        this->value_ = file;
    }
    else{
        this->value_ = "resources/lcr4500/DLPR350PROM_v2.0.0_no_images.bin";
    }
}

void LCr4500::Settings::File::DLPC350_Firmware::Set(const std::string &file){
    if(dlp::File::Exists(file)){
        this->value_ = file;
    }
    else{
        this->value_ = "resources/lcr4500/DLPR350PROM_v2.0.0_no_images.bin";
    }
}

std::string LCr4500::Settings::File::DLPC350_Firmware::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_DLPC350_FIRMWARE_FILE
 */
std::string LCr4500::Settings::File::DLPC350_Firmware::GetEntryName() const{
    return "LCR4500_SETTINGS_DLPC350_FIRMWARE_FILE";
}

/** \brief  Gets the value of the entry for filename of base firmware file */
std::string LCr4500::Settings::File::DLPC350_Firmware::GetEntryValue() const{
    return this->value_;
}

/** \brief  Gets the default entry for the DLPC350 base firmware entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::File::DLPC350_Firmware::GetEntryDefault() const{
    return "resources/lcr4500/DLPR350PROM_v2.0.0_no_images.bin";
}

/** \brief  Sets the DLPC350 base firmware entry setting on LightCrafter 4500*/
void LCr4500::Settings::File::DLPC350_Firmware::SetEntryValue(std::string value){
    if(dlp::File::Exists(value)){
        this->value_ = value;
    }
    else{
        this->value_ = "resources/lcr4500/DLPR350PROM_v2.0.0_no_images.bin";
    }
}



LCr4500::Settings::File::DLPC350_FlashParameters::DLPC350_FlashParameters(){
    this->value_ = "resources/lcr4500/DLPC350_FlashDeviceParameters.txt";
}

LCr4500::Settings::File::DLPC350_FlashParameters::DLPC350_FlashParameters(const std::string &file){
    if(dlp::File::Exists(file)){
        this->value_ = file;
    }
    else{
        this->value_ = "resources/lcr4500/DLPC350_FlashDeviceParameters.txt";
    }
}

void LCr4500::Settings::File::DLPC350_FlashParameters::Set(const std::string &file){
    if(dlp::File::Exists(file)){
        this->value_ = file;
    }
    else{
        this->value_ = "resources/lcr4500/DLPC350_FlashDeviceParameters.txt";
    }
}

std::string LCr4500::Settings::File::DLPC350_FlashParameters::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_DLPC350_FIRWMARE_FILE
 */
std::string LCr4500::Settings::File::DLPC350_FlashParameters::GetEntryName() const{
    return "LCR4500_SETTINGS_DLPC350_FLASH_PARAMETERS_FILE";
}

/** \brief  Gets the path and filename of base flash parameter file */
std::string LCr4500::Settings::File::DLPC350_FlashParameters::GetEntryValue() const{
    return this->value_;
}

/** \brief  Gets the default entry for the base flash parameter file name entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::File::DLPC350_FlashParameters::GetEntryDefault() const{
    return "resources/lcr4500/DLPC350_FlashDeviceParameters.txt";
}

/** \brief  Sets the base flash parameter file name entry setting on LightCrafter 4500*/
void LCr4500::Settings::File::DLPC350_FlashParameters::SetEntryValue(std::string value){
    if(dlp::File::Exists(value)){
        this->value_ = value;
    }
    else{
        this->value_ = "resources/lcr4500/DLPC350_FlashDeviceParameters.txt";
    }
}



LCr4500::Settings::File::PatternSequenceFirmware::PatternSequenceFirmware(){
    this->value_ = "lcr4500_pattern_sequence.bin";
}

LCr4500::Settings::File::PatternSequenceFirmware::PatternSequenceFirmware(const std::string &file){
    this->value_ = file;
}

void LCr4500::Settings::File::PatternSequenceFirmware::Set(const std::string &file){
    this->value_ = file;
}

std::string LCr4500::Settings::File::PatternSequenceFirmware::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_SEQUENCE_FIRMWARE
 */
std::string LCr4500::Settings::File::PatternSequenceFirmware::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_SEQUENCE_FIRMWARE";
}

/** \brief  Gets the output DLPC350 Firmware object path and file name*/
std::string LCr4500::Settings::File::PatternSequenceFirmware::GetEntryValue() const{
    return this->value_;
}

/** \brief  Gets the default entry for the output DLPC350 firmware image filename entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::File::PatternSequenceFirmware::GetEntryDefault() const{
    return "lcr4500_pattern_sequence.bin";
}

/** \brief  Sets the output DLPC350 firmware image filename entry setting on LightCrafter 4500*/
void LCr4500::Settings::File::PatternSequenceFirmware::SetEntryValue(std::string value){
    this->value_ = value;
}



LCr4500::Settings::UseDefault::UseDefault(){
    this->value_ = true;
}

LCr4500::Settings::UseDefault::UseDefault(const bool &setup){
    this->value_ = setup;
}

void LCr4500::Settings::UseDefault::Set(const bool &setup){
    this->value_ = setup;
}

bool LCr4500::Settings::UseDefault::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_USE_DEFAULT
 */
std::string LCr4500::Settings::UseDefault::GetEntryName() const{
    return "LCR4500_SETTINGS_USE_DEFAULT";
}

/** \brief  Gets the use of default settings entry for LightCrafter 4500 EVM*/
std::string LCr4500::Settings::UseDefault::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for LightCrafter 4500 EVM default settings*/
std::string LCr4500::Settings::UseDefault::GetEntryDefault() const{
    return dlp::Number::ToString(true);
}

/** \brief  Sets the use of default settings entries for LightCrafter 4500 EVM*/
void LCr4500::Settings::UseDefault::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::PowerStandby::PowerStandby(){
    this->value_ = NORMAL;
}

LCr4500::Settings::PowerStandby::PowerStandby(const bool &mode){
    this->value_ = mode;
}

void LCr4500::Settings::PowerStandby::Set(const bool &mode){
    this->value_ = mode;
}

bool LCr4500::Settings::PowerStandby::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_POWER_STANDBY
 */
std::string LCr4500::Settings::PowerStandby::GetEntryName() const{
    return "LCR4500_SETTINGS_POWER_STANDBY";
}

/** \brief  Gets the LightCrafter 4500 Power mode*/
std::string LCr4500::Settings::PowerStandby::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for LightCrafter 4500 Power mode*/
std::string LCr4500::Settings::PowerStandby::GetEntryDefault() const{
    return dlp::Number::ToString(NORMAL);
}

/** \brief  Object to set the LightCrafter 4500 into Power Standby.  True sets power standby, false sets normal mode*/
void LCr4500::Settings::PowerStandby::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::DisplayMode::DisplayMode(){
    this->value_ = PATTERN_SEQUENCE;
}

LCr4500::Settings::DisplayMode::DisplayMode(const bool &mode){
    this->value_ = mode;
}

void LCr4500::Settings::DisplayMode::Set(const bool &mode){
    this->value_ = mode;
}

bool LCr4500::Settings::DisplayMode::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_DISPLAY_MODE
 */
std::string LCr4500::Settings::DisplayMode::GetEntryName() const{
    return "LCR4500_SETTINGS_DISPLAY_MODE";
}

/** \brief  Gets the LightCrafter 4500 Display Mode*/
std::string LCr4500::Settings::DisplayMode::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for LightCrafter 4500 Display Mode*/
std::string LCr4500::Settings::DisplayMode::GetEntryDefault() const{
    return dlp::Number::ToString(PATTERN_SEQUENCE);
}

/** \brief  Object to set the LightCrafter 4500 Display Mode. True sets pattern sequence mode, false sets video mode*/
void LCr4500::Settings::DisplayMode::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::InvertData::InvertData(){
    this->value_ = false;
}

LCr4500::Settings::InvertData::InvertData(const bool &invert){
    this->value_ = invert;
}

void LCr4500::Settings::InvertData::Set(const bool &invert){
    this->value_ = invert;
}

bool LCr4500::Settings::InvertData::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_INVERT_DATA
 */
std::string LCr4500::Settings::InvertData::GetEntryName() const{
    return "LCR4500_SETTINGS_INVERT_DATA";
}

/** \brief  Gets the setting entry for images color inversion*/
std::string LCr4500::Settings::InvertData::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default setting entry for images color inversion*/
std::string LCr4500::Settings::InvertData::GetEntryDefault() const{
    return dlp::Number::ToString(false);
}

/** \brief  Object to invert image color displayed by LightCrafter 4500, true inverts image data*/
void LCr4500::Settings::InvertData::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::ImageFlip::ShortAxis::ShortAxis(){
    this->value_ = NORMAL;
}

LCr4500::Settings::ImageFlip::ShortAxis::ShortAxis(const bool &mode){
    this->value_ = mode;
}

void LCr4500::Settings::ImageFlip::ShortAxis::Set(const bool &mode){
    this->value_ = mode;
}

bool LCr4500::Settings::ImageFlip::ShortAxis::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_IMAGE_FLIP_SHORT_AXIS
 */
std::string LCr4500::Settings::ImageFlip::ShortAxis::GetEntryName() const{
    return "LCR4500_SETTINGS_IMAGE_FLIP_SHORT_AXIS";
}

/** \brief  Gets the short axis image flip on LightCrafter 4500*/
std::string LCr4500::Settings::ImageFlip::ShortAxis::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for short axis image flip on LightCrafter 4500*/
std::string LCr4500::Settings::ImageFlip::ShortAxis::GetEntryDefault() const{
    return dlp::Number::ToString(NORMAL);
}

/** \brief  Sets the short axis image flip on LightCrafter 4500*/
void LCr4500::Settings::ImageFlip::ShortAxis::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::ImageFlip::LongAxis::LongAxis(){
    this->value_ = NORMAL;
}

LCr4500::Settings::ImageFlip::LongAxis::LongAxis(const bool &mode){
    this->value_ = mode;
}

void LCr4500::Settings::ImageFlip::LongAxis::Set(const bool &mode){
    this->value_ = mode;
}

bool LCr4500::Settings::ImageFlip::LongAxis::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_IMAGE_FLIP_LONG_AXIS
 */
std::string LCr4500::Settings::ImageFlip::LongAxis::GetEntryName() const{
    return "LCR4500_SETTINGS_IMAGE_FLIP_LONG_AXIS";
}

/** \brief  Gets the long axis image flip on LightCrafter 4500*/
std::string LCr4500::Settings::ImageFlip::LongAxis::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for long axis image flip on LightCrafter 4500*/
std::string LCr4500::Settings::ImageFlip::LongAxis::GetEntryDefault() const{
    return dlp::Number::ToString(NORMAL);
}

/** \brief  Sets the long axis image flip on LightCrafter 4500*/
void LCr4500::Settings::ImageFlip::LongAxis::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



const unsigned char LCr4500::Settings::Led::MAXIMUM_CURRENT = 255;

LCr4500::Settings::Led::Sequence::Sequence(){
    this->value_ = AUTO;
}

LCr4500::Settings::Led::Sequence::Sequence(const bool &mode){
    this->value_ = mode;
}

void LCr4500::Settings::Led::Sequence::Set(const bool &mode){
    this->value_ = mode;
}

bool LCr4500::Settings::Led::Sequence::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_SEQUENCE_MODE
 */
std::string LCr4500::Settings::Led::Sequence::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_SEQUENCE_MODE";
}

/** \brief  Gets the led sequence control on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Sequence::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for led sequence control on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Sequence::GetEntryDefault() const{
    return dlp::Number::ToString(AUTO);
}

/** \brief  Sets the led sequence control on LightCrafter 4500*/
void LCr4500::Settings::Led::Sequence::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::Led::InvertPWM::InvertPWM(){
    this->value_ = true;
}

LCr4500::Settings::Led::InvertPWM::InvertPWM(const bool &invert){
    this->value_ = invert;
}

void LCr4500::Settings::Led::InvertPWM::Set(const bool &invert){
    this->value_ = invert;
}

bool LCr4500::Settings::Led::InvertPWM::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_INVERT_PWM
 */
std::string LCr4500::Settings::Led::InvertPWM::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_INVERT_PWM";
}

/** \brief  Gets the Invert PWM entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::InvertPWM::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Invert PWM entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::InvertPWM::GetEntryDefault() const{
    return dlp::Number::ToString(false);
}

/** \brief  Sets the Invert PWM entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::InvertPWM::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::Led::Red::Enable::Enable(){
    this->value_ = true;
}

LCr4500::Settings::Led::Red::Enable::Enable(const bool &enable){
    this->value_ = enable;
}

void LCr4500::Settings::Led::Red::Enable::Set(const bool &enable){
    this->value_ = enable;
}

bool LCr4500::Settings::Led::Red::Enable::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_RED_ENABLE
 */
std::string LCr4500::Settings::Led::Red::Enable::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_RED_ENABLE";
}

/** \brief  Gets the Red LED enable entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Red::Enable::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Red LED enable entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Red::Enable::GetEntryDefault() const{
    return dlp::Number::ToString(true);
}

/** \brief  Sets the Red LED enable entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Red::Enable::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::Led::Red::Current::Current(){
    this->value_ = 104;
}

LCr4500::Settings::Led::Red::Current::Current(const unsigned char &current){
    this->value_ = current;
}

void LCr4500::Settings::Led::Red::Current::Set(const unsigned char &current){
    this->value_ = current;
}

unsigned char LCr4500::Settings::Led::Red::Current::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_RED_CURRENT
 */
std::string LCr4500::Settings::Led::Red::Current::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_RED_CURRENT";
}

/** \brief  Gets the Red LED current entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Red::Current::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Red LED current entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Red::Current::GetEntryDefault() const{
    return dlp::Number::ToString(104);
}

/** \brief  Sets the Red LED current entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Red::Current::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned char>(value);
}



LCr4500::Settings::Led::Red::EdgeDelay::Rising::Rising(){
    this->value_ = 187;
}

LCr4500::Settings::Led::Red::EdgeDelay::Rising::Rising(const unsigned char &delay){
    this->value_ = delay;
}

void LCr4500::Settings::Led::Red::EdgeDelay::Rising::Set(const unsigned char &delay){
    this->value_ = delay;
}

unsigned char LCr4500::Settings::Led::Red::EdgeDelay::Rising::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_RED_EDGE_DELAY_RISING
 */
std::string LCr4500::Settings::Led::Red::EdgeDelay::Rising::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_RED_EDGE_DELAY_RISING";
}

/** \brief  Gets the Red LED rising edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Red::EdgeDelay::Rising::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Red LED rising edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Red::EdgeDelay::Rising::GetEntryDefault() const{
    return dlp::Number::ToString(187);
}

/** \brief  Sets the Red LED rising edge delay entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Red::EdgeDelay::Rising::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned char>(value);
}



LCr4500::Settings::Led::Red::EdgeDelay::Falling::Falling(){
    this->value_ = 187;
}

LCr4500::Settings::Led::Red::EdgeDelay::Falling::Falling(const unsigned char &delay){
    this->value_ = delay;
}

void LCr4500::Settings::Led::Red::EdgeDelay::Falling::Set(const unsigned char &delay){
    this->value_ = delay;
}

unsigned char LCr4500::Settings::Led::Red::EdgeDelay::Falling::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_RED_EDGE_DELAY_FALLING
 */
std::string LCr4500::Settings::Led::Red::EdgeDelay::Falling::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_RED_EDGE_DELAY_FALLING";
}

/** \brief  Gets the Red LED falling edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Red::EdgeDelay::Falling::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Red LED falling edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Red::EdgeDelay::Falling::GetEntryDefault() const{
    return dlp::Number::ToString(187);
}

/** \brief  Sets the Red LED falling edge delay entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Red::EdgeDelay::Falling::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned char>(value);
}




LCr4500::Settings::Led::Green::Enable::Enable(){
    this->value_ = true;
}

LCr4500::Settings::Led::Green::Enable::Enable(const bool &enable){
    this->value_ = enable;
}

void LCr4500::Settings::Led::Green::Enable::Set(const bool &enable){
    this->value_ = enable;
}

bool LCr4500::Settings::Led::Green::Enable::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_GREEN_ENABLE
 */
std::string LCr4500::Settings::Led::Green::Enable::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_GREEN_ENABLE";
}

/** \brief  Gets the Green LED enable entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Green::Enable::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Green LED enable entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Green::Enable::GetEntryDefault() const{
    return dlp::Number::ToString(true);
}

/** \brief  Sets the Green LED enable entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Green::Enable::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::Led::Green::Current::Current(){
    this->value_ = 135;
}

LCr4500::Settings::Led::Green::Current::Current(const unsigned char &current){
    this->value_ = current;
}

void LCr4500::Settings::Led::Green::Current::Set(const unsigned char &current){
    this->value_ = current;
}

unsigned char LCr4500::Settings::Led::Green::Current::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_GREEN_CURRENT
 */
std::string LCr4500::Settings::Led::Green::Current::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_GREEN_CURRENT";
}

/** \brief  Gets the Green LED current entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Green::Current::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Green LED current entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Green::Current::GetEntryDefault() const{
    return dlp::Number::ToString(135);
}

/** \brief  Sets the Green LED current entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Green::Current::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned char>(value);
}



LCr4500::Settings::Led::Green::EdgeDelay::Rising::Rising(){
    this->value_ = 187;
}

LCr4500::Settings::Led::Green::EdgeDelay::Rising::Rising(const unsigned char &delay){
    this->value_ = delay;
}

void LCr4500::Settings::Led::Green::EdgeDelay::Rising::Set(const unsigned char &delay){
    this->value_ = delay;
}

unsigned char LCr4500::Settings::Led::Green::EdgeDelay::Rising::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_GREEN_EDGE_DELAY_RISING
 */
std::string LCr4500::Settings::Led::Green::EdgeDelay::Rising::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_GREEN_EDGE_DELAY_RISING";
}

/** \brief  Gets the Green LED rising edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Green::EdgeDelay::Rising::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Green LED rising edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Green::EdgeDelay::Rising::GetEntryDefault() const{
    return dlp::Number::ToString(187);
}

/** \brief  Sets the Green LED rising edge delay entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Green::EdgeDelay::Rising::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned char>(value);
}



LCr4500::Settings::Led::Green::EdgeDelay::Falling::Falling(){
    this->value_ = 187;
}

LCr4500::Settings::Led::Green::EdgeDelay::Falling::Falling(const unsigned char &delay){
    this->value_ = delay;
}

void LCr4500::Settings::Led::Green::EdgeDelay::Falling::Set(const unsigned char &delay){
    this->value_ = delay;
}

unsigned char LCr4500::Settings::Led::Green::EdgeDelay::Falling::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_GREEN_EDGE_DELAY_FALLING
 */
std::string LCr4500::Settings::Led::Green::EdgeDelay::Falling::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_GREEN_EDGE_DELAY_FALLING";
}

/** \brief  Gets the Green LED falling edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Green::EdgeDelay::Falling::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Green LED falling edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Green::EdgeDelay::Falling::GetEntryDefault() const{
    return dlp::Number::ToString(187);
}

/** \brief  Sets the Green LED falling edge delay entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Green::EdgeDelay::Falling::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned char>(value);
}



LCr4500::Settings::Led::Blue::Enable::Enable(){
    this->value_ = true;
}

LCr4500::Settings::Led::Blue::Enable::Enable(const bool &enable){
    this->value_ = enable;
}

void LCr4500::Settings::Led::Blue::Enable::Set(const bool &enable){
    this->value_ = enable;
}

bool LCr4500::Settings::Led::Blue::Enable::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_BLUE_ENABLE
 */
std::string LCr4500::Settings::Led::Blue::Enable::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_BLUE_ENABLE";
}

/** \brief  Gets the Blue LED enable entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Blue::Enable::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Blue LED enable entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Blue::Enable::GetEntryDefault() const{
    return dlp::Number::ToString(true);
}

/** \brief  Sets the Blue LED enable entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Blue::Enable::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::Led::Blue::Current::Current(){
    this->value_ = 135;
}

LCr4500::Settings::Led::Blue::Current::Current(const unsigned char &current){
    this->value_ = current;
}

void LCr4500::Settings::Led::Blue::Current::Set(const unsigned char &current){
    this->value_ = current;
}

unsigned char LCr4500::Settings::Led::Blue::Current::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_BLUE_CURRENT
 */
std::string LCr4500::Settings::Led::Blue::Current::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_BLUE_CURRENT";
}

/** \brief  Gets the Blue LED current entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Blue::Current::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Blue LED current entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Blue::Current::GetEntryDefault() const{
    return dlp::Number::ToString(135);
}

/** \brief  Sets the Blue LED current entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Blue::Current::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned char>(value);
}



LCr4500::Settings::Led::Blue::EdgeDelay::Rising::Rising(){
    this->value_ = 187;
}

LCr4500::Settings::Led::Blue::EdgeDelay::Rising::Rising(const unsigned char &delay){
    this->value_ = delay;
}

void LCr4500::Settings::Led::Blue::EdgeDelay::Rising::Set(const unsigned char &delay){
    this->value_ = delay;
}

unsigned char LCr4500::Settings::Led::Blue::EdgeDelay::Rising::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_BLUE_EDGE_DELAY_RISING
 */
std::string LCr4500::Settings::Led::Blue::EdgeDelay::Rising::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_BLUE_EDGE_DELAY_RISING";
}

/** \brief  Gets the Blue LED rising edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Blue::EdgeDelay::Rising::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Blue LED rising edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Blue::EdgeDelay::Rising::GetEntryDefault() const{
    return dlp::Number::ToString(187);
}

/** \brief  Sets the Blue LED rising edge delay entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Blue::EdgeDelay::Rising::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned char>(value);
}



LCr4500::Settings::Led::Blue::EdgeDelay::Falling::Falling(){
    this->value_ = 187;
}

LCr4500::Settings::Led::Blue::EdgeDelay::Falling::Falling(const unsigned char &delay){
    this->value_ = delay;
}

void LCr4500::Settings::Led::Blue::EdgeDelay::Falling::Set(const unsigned char &delay){
    this->value_ = delay;
}

unsigned char LCr4500::Settings::Led::Blue::EdgeDelay::Falling::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_LED_BLUE_EDGE_DELAY_FALLING
 */
std::string LCr4500::Settings::Led::Blue::EdgeDelay::Falling::GetEntryName() const{
    return "LCR4500_SETTINGS_LED_BLUE_EDGE_DELAY_FALLING";
}

/** \brief  Gets the Blue LED falling edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Blue::EdgeDelay::Falling::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Blue LED falling edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Led::Blue::EdgeDelay::Falling::GetEntryDefault() const{
    return dlp::Number::ToString(187);
}

/** \brief  Sets the Blue LED falling edge delay entry setting on LightCrafter 4500*/
void LCr4500::Settings::Led::Blue::EdgeDelay::Falling::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned char>(value);
}









LCr4500::Settings::TestPattern::TestPattern(){
    this->value_ = SOLID_FIELD;
}

LCr4500::Settings::TestPattern::TestPattern(const unsigned int &pattern){
    if(pattern <= STEP_BARS){
        this->value_ = pattern;
    }
    else{
        this->value_ = SOLID_FIELD;
    }
}

void LCr4500::Settings::TestPattern::Set(const unsigned int &pattern){
    if(pattern <= STEP_BARS){
        this->value_ = pattern;
    }
    else{
        this->value_ = SOLID_FIELD;
    }
}

unsigned int LCr4500::Settings::TestPattern::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_TEST_PATTERN
 */
std::string LCr4500::Settings::TestPattern::GetEntryName() const{
    return "LCR4500_SETTINGS_TEST_PATTERN";
}

/** \brief  Gets the test pattern entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::TestPattern::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for the test pattern entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::TestPattern::GetEntryDefault() const{
    return dlp::Number::ToString(SOLID_FIELD);
}

/** \brief  Sets the test pattern entry setting on LightCrafter 4500*/
void LCr4500::Settings::TestPattern::SetEntryValue(std::string value){
    if(dlp::String::ToNumber<unsigned int>(value) <= STEP_BARS){
        this->value_ = dlp::String::ToNumber<unsigned int>(value);
    }
    else{
        this->value_ = SOLID_FIELD;
    }
}



const unsigned int LCr4500::Settings::TestPattern::Color::MAXIMUM = 1023;

LCr4500::Settings::TestPattern::Color::Foreground::Foreground(){
    this->value_red_   = MAXIMUM;
    this->value_green_ = MAXIMUM;
    this->value_blue_  = MAXIMUM;
}

LCr4500::Settings::TestPattern::Color::Foreground::Foreground(const unsigned int &red, const unsigned int &green, const unsigned int &blue){
    this->value_red_    = red;
    this->value_green_  = green;
    this->value_blue_   = blue;


    if(red   > MAXIMUM) this->value_red_    = MAXIMUM;
    if(green > MAXIMUM) this->value_green_  = MAXIMUM;
    if(blue  > MAXIMUM) this->value_blue_   = MAXIMUM;
}

void LCr4500::Settings::TestPattern::Color::Foreground::Set(const unsigned int &red, const unsigned int &green, const unsigned int &blue){
    this->value_red_    = red;
    this->value_green_  = green;
    this->value_blue_   = blue;


    if(red   > MAXIMUM) this->value_red_    = MAXIMUM;
    if(green > MAXIMUM) this->value_green_  = MAXIMUM;
    if(blue  > MAXIMUM) this->value_blue_   = MAXIMUM;
}

unsigned int LCr4500::Settings::TestPattern::Color::Foreground::GetRed() const{
    return this->value_red_;
}

unsigned int LCr4500::Settings::TestPattern::Color::Foreground::GetGreen() const{
    return this->value_green_;
}

unsigned int LCr4500::Settings::TestPattern::Color::Foreground::GetBlue() const{
    return this->value_blue_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_TEST_PATTERN_COLOR_FOREGROUND
 */
std::string LCr4500::Settings::TestPattern::Color::Foreground::GetEntryName() const{
    return "LCR4500_SETTINGS_TEST_PATTERN_COLOR_FOREGROUND";
}

/** \brief  Gets the test pattern foreground color entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::TestPattern::Color::Foreground::GetEntryValue() const{
    std::string ret = "";

    ret += dlp::Number::ToString(this->value_red_)   + ", ";
    ret += dlp::Number::ToString(this->value_green_) + ", ";
    ret += dlp::Number::ToString(this->value_blue_);

    return ret;
}

/** \brief  Gets the default entry for the test pattern foreground color entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::TestPattern::Color::Foreground::GetEntryDefault() const{
    std::string ret = "";

    ret += dlp::Number::ToString(MAXIMUM) + ", ";
    ret += dlp::Number::ToString(MAXIMUM) + ", ";
    ret += dlp::Number::ToString(MAXIMUM);

    return ret;
}

/** \brief  Sets the test pattern foreground color entry setting on LightCrafter 4500*/
void LCr4500::Settings::TestPattern::Color::Foreground::SetEntryValue(std::string value){
    std::vector<std::string> colors;

    colors = dlp::String::SeparateDelimited(value,',');

    if(colors.size() == 3){
        this->value_red_    = dlp::String::ToNumber<unsigned int>(colors.at(0));
        this->value_green_  = dlp::String::ToNumber<unsigned int>(colors.at(1));
        this->value_blue_   = dlp::String::ToNumber<unsigned int>(colors.at(2));

        if(this->value_red_   > MAXIMUM) this->value_red_   = MAXIMUM;
        if(this->value_green_ > MAXIMUM) this->value_green_ = MAXIMUM;
        if(this->value_blue_  > MAXIMUM) this->value_blue_  = MAXIMUM;
    }
    else{
        this->value_red_    = MAXIMUM;
        this->value_green_  = MAXIMUM;
        this->value_blue_   = MAXIMUM;
    }
}













LCr4500::Settings::TestPattern::Color::Background::Background(){
    this->value_red_   = MAXIMUM;
    this->value_green_ = MAXIMUM;
    this->value_blue_  = MAXIMUM;
}

LCr4500::Settings::TestPattern::Color::Background::Background(const unsigned int &red, const unsigned int &green, const unsigned int &blue){
    this->value_red_    = red;
    this->value_green_  = green;
    this->value_blue_   = blue;


    if(red   > MAXIMUM) this->value_red_    = MAXIMUM;
    if(green > MAXIMUM) this->value_green_  = MAXIMUM;
    if(blue  > MAXIMUM) this->value_blue_   = MAXIMUM;
}

void LCr4500::Settings::TestPattern::Color::Background::Set(const unsigned int &red, const unsigned int &green, const unsigned int &blue){
    this->value_red_    = red;
    this->value_green_  = green;
    this->value_blue_   = blue;


    if(red   > MAXIMUM) this->value_red_    = MAXIMUM;
    if(green > MAXIMUM) this->value_green_  = MAXIMUM;
    if(blue  > MAXIMUM) this->value_blue_   = MAXIMUM;
}

unsigned int LCr4500::Settings::TestPattern::Color::Background::GetRed() const{
    return this->value_red_;
}

unsigned int LCr4500::Settings::TestPattern::Color::Background::GetGreen() const{
    return this->value_green_;
}

unsigned int LCr4500::Settings::TestPattern::Color::Background::GetBlue() const{
    return this->value_blue_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_TEST_PATTERN_COLOR_BACKGROUND
 */
std::string LCr4500::Settings::TestPattern::Color::Background::GetEntryName() const{
    return "LCR4500_SETTINGS_TEST_PATTERN_COLOR_BACKGROUND";
}

/** \brief  Gets the test pattern background color entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::TestPattern::Color::Background::GetEntryValue() const{
    std::string ret = "";

    ret += dlp::Number::ToString(this->value_red_)   + ", ";
    ret += dlp::Number::ToString(this->value_green_) + ", ";
    ret += dlp::Number::ToString(this->value_blue_);

    return ret;
}

/** \brief  Gets the default entry for the test pattern background color entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::TestPattern::Color::Background::GetEntryDefault() const{
    std::string ret = "";

    ret += dlp::Number::ToString(MAXIMUM) + ", ";
    ret += dlp::Number::ToString(MAXIMUM) + ", ";
    ret += dlp::Number::ToString(MAXIMUM);

    return ret;
}

/** \brief  Sets the test pattern background color entry setting on LightCrafter 4500*/
void LCr4500::Settings::TestPattern::Color::Background::SetEntryValue(std::string value){
    std::vector<std::string> colors;

    colors = dlp::String::SeparateDelimited(value, ',');

    if(colors.size() == 3){
        this->value_red_    = dlp::String::ToNumber<unsigned int>(colors.at(0));
        this->value_green_  = dlp::String::ToNumber<unsigned int>(colors.at(1));
        this->value_blue_   = dlp::String::ToNumber<unsigned int>(colors.at(2));

        if(this->value_red_   > MAXIMUM) this->value_red_   = MAXIMUM;
        if(this->value_green_ > MAXIMUM) this->value_green_ = MAXIMUM;
        if(this->value_blue_  > MAXIMUM) this->value_blue_  = MAXIMUM;
    }
    else{
        this->value_red_    = MAXIMUM;
        this->value_green_  = MAXIMUM;
        this->value_blue_   = MAXIMUM;
    }
}







LCr4500::Settings::InputSource::InputSource(){
    this->value_ = PARALLEL_INTERFACE;
}

LCr4500::Settings::InputSource::InputSource(const unsigned int &source){
    if(source <= FPD_LINK){
        this->value_ = source;
    }
    else{
        this->value_ = PARALLEL_INTERFACE;
    }
}

void LCr4500::Settings::InputSource::Set(const unsigned int &source){
    if(source <= FPD_LINK){
        this->value_ = source;
    }
    else{
        this->value_ = PARALLEL_INTERFACE;
    }
}

unsigned int LCr4500::Settings::InputSource::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_INPUT_SOURCE
 */
std::string LCr4500::Settings::InputSource::GetEntryName() const{
    return "LCR4500_SETTINGS_INPUT_SOURCE";
}

/** \brief  Gets the input source entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::InputSource::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for input source entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::InputSource::GetEntryDefault() const{
    return dlp::Number::ToString(PARALLEL_INTERFACE);
}

/** \brief  Sets the input source entry setting on LightCrafter 4500*/
void LCr4500::Settings::InputSource::SetEntryValue(std::string value){
    if(dlp::String::ToNumber<unsigned int>(value) <= FPD_LINK){
        this->value_ = dlp::String::ToNumber<unsigned int>(value);
    }
    else{
        this->value_ = PARALLEL_INTERFACE;
    }
}



LCr4500::Settings::ParallelPortWidth::ParallelPortWidth(){
    this->value_ = BITS_24;
}

LCr4500::Settings::ParallelPortWidth::ParallelPortWidth(const unsigned int &width){
    if(width <= BITS_8){
        this->value_ = width;
    }
    else{
        this->value_ = BITS_24;
    }
}

void LCr4500::Settings::ParallelPortWidth::Set(const unsigned int &width){
    if(width <= BITS_8){
        this->value_ = width;
    }
    else{
        this->value_ = BITS_24;
    }
}

unsigned int LCr4500::Settings::ParallelPortWidth::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PARALLEL_PORT_WIDTH
 */
std::string LCr4500::Settings::ParallelPortWidth::GetEntryName() const{
    return "LCR4500_SETTINGS_PARALLEL_PORT_WIDTH";
}

/** \brief  Gets the parallel port width entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::ParallelPortWidth::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for parallel port width entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::ParallelPortWidth::GetEntryDefault() const{
    return dlp::Number::ToString(BITS_24);
}

/** \brief  Sets the parallel port width entry setting on LightCrafter 4500*/
void LCr4500::Settings::ParallelPortWidth::SetEntryValue(std::string value){
    if(dlp::String::ToNumber<unsigned int>(value) <= BITS_8){
        this->value_ = dlp::String::ToNumber<unsigned int>(value);
    }
    else{
        this->value_ = BITS_24;
    }
}



const unsigned int LCr4500::Settings::FlashImage::MAXIMUM_INDEX = MAX_SPLASH_IMAGES;

LCr4500::Settings::FlashImage::FlashImage(){
    this->value_ = 0;
}

LCr4500::Settings::FlashImage::FlashImage(const unsigned int &index){
    this->value_ = index;

    if(index > MAXIMUM_INDEX) this->value_ = MAXIMUM_INDEX;
}

void LCr4500::Settings::FlashImage::Set(const unsigned int &index){
    this->value_ = index;

    if(index > MAXIMUM_INDEX) this->value_ = MAXIMUM_INDEX;
}

unsigned int LCr4500::Settings::FlashImage::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_FLASH_IMAGE
 */
std::string LCr4500::Settings::FlashImage::GetEntryName() const{
    return "LCR4500_SETTINGS_FLASH_IMAGE";
}

/** \brief  Gets the flash image entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::FlashImage::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for the flash image entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::FlashImage::GetEntryDefault() const{
    return "0";
}

/** \brief  Sets the flash image entry setting on LightCrafter 4500*/
void LCr4500::Settings::FlashImage::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);

    if(this->value_ > MAXIMUM_INDEX) this->value_ = MAXIMUM_INDEX;
}








LCr4500::Settings::Pattern::Trigger::Trigger(){
    this->value_ = INTERNAL;
}

LCr4500::Settings::Pattern::Trigger::Trigger(const unsigned int &trigger){
    this->value_ = trigger;

    if(this->value_ >= INVALID) this->value_ = INTERNAL;
}

void LCr4500::Settings::Pattern::Trigger::Set(const unsigned int &trigger){
    this->value_ = trigger;

    if(this->value_ >= INVALID) this->value_ = INTERNAL;
}

unsigned int LCr4500::Settings::Pattern::Trigger::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_TRIGGER
 */
std::string LCr4500::Settings::Pattern::Trigger::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_TRIGGER";
}

/** \brief  Gets the pattern trigger entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for pattern trigger entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::GetEntryDefault() const{
    return dlp::Number::ToString(INTERNAL);
}

/** \brief  Sets the pattern trigger entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Trigger::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
    if(this->value_ >= INVALID) this->value_ = INTERNAL;
}



LCr4500::Settings::Pattern::Trigger::Mode::Mode(){
    this->value_ = MODE_1_INT_OR_EXT;
}

LCr4500::Settings::Pattern::Trigger::Mode::Mode(const unsigned int &mode){
    this->value_ = mode;

    if(this->value_ >= INVALID) this->value_ = MODE_1_INT_OR_EXT;
}

void LCr4500::Settings::Pattern::Trigger::Mode::Set(const unsigned int &mode){
    this->value_ = mode;

    if(this->value_ >= INVALID) this->value_ = MODE_1_INT_OR_EXT;
}

unsigned int LCr4500::Settings::Pattern::Trigger::Mode::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_TRIGGER_MODE
 */
std::string LCr4500::Settings::Pattern::Trigger::Mode::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_TRIGGER_MODE";
}

/** \brief  Gets the pattern trigger mode entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Mode::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for pattern trigger mode entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Mode::GetEntryDefault() const{
    return dlp::Number::ToString(MODE_1_INT_OR_EXT);
}

/** \brief  Sets the pattern trigger mode entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Trigger::Mode::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);

    if(this->value_ >= INVALID) this->value_ = MODE_1_INT_OR_EXT;
}



LCr4500::Settings::Pattern::ShareExposure::ShareExposure(){
    this->value_ = false;
}

LCr4500::Settings::Pattern::ShareExposure::ShareExposure(const bool &share){
    this->value_ = share;
}

void LCr4500::Settings::Pattern::ShareExposure::Set(const bool &share){
    this->value_ = share;
}

bool LCr4500::Settings::Pattern::ShareExposure::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_SHARE_EXPOSURE
 */
std::string LCr4500::Settings::Pattern::ShareExposure::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_SHARE_EXPOSURE";
}

/** \brief  Gets the share exposure entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::ShareExposure::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for the share exposure entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::ShareExposure::GetEntryDefault() const{
    return dlp::Number::ToString(false);
}

/** \brief  Sets the share exposure entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::ShareExposure::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
}


LCr4500::Settings::Pattern::Display::VerifyImageLoadTimes::VerifyImageLoadTimes(){
    this->value_ = 1;
}

LCr4500::Settings::Pattern::Display::VerifyImageLoadTimes::VerifyImageLoadTimes(const unsigned int &count){
    this->value_ = count;
}

void LCr4500::Settings::Pattern::Display::VerifyImageLoadTimes::Set(const unsigned int &count){
    this->value_ = count;
}

unsigned int LCr4500::Settings::Pattern::Display::VerifyImageLoadTimes::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_DISPLAY_VERIFY_LOAD_TIMES
 */
std::string LCr4500::Settings::Pattern::Display::VerifyImageLoadTimes::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_DISPLAY_VERIFY_LOAD_TIMES";
}

/** \brief  Gets the repeat sequence display entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Display::VerifyImageLoadTimes::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for repeat sequence display entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Display::VerifyImageLoadTimes::GetEntryDefault() const{
    return dlp::Number::ToString(1);
}

/** \brief  Sets the repeat sequence display entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Display::VerifyImageLoadTimes::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
}















LCr4500::Settings::Pattern::Display::Repeat::Repeat(){
    this->value_ = false;
}

LCr4500::Settings::Pattern::Display::Repeat::Repeat(const bool &repeat){
    this->value_ = repeat;
}

void LCr4500::Settings::Pattern::Display::Repeat::Set(const bool &repeat){
    this->value_ = repeat;
}

bool LCr4500::Settings::Pattern::Display::Repeat::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_DISPLAY_REPEAT
 */
std::string LCr4500::Settings::Pattern::Display::Repeat::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_DISPLAY_REPEAT";
}

/** \brief  Gets the repeat sequence display entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Display::Repeat::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for repeat sequence display entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Display::Repeat::GetEntryDefault() const{
    return dlp::Number::ToString(false);
}

/** \brief  Sets the repeat sequence display entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Display::Repeat::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
}



LCr4500::Settings::Pattern::FlashImage::FlashImage(){
    this->value_ = 0;
}

LCr4500::Settings::Pattern::FlashImage::FlashImage(const unsigned int &index){
    this->value_ = index;

    if(index > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}

void LCr4500::Settings::Pattern::FlashImage::Set(const unsigned int &index){
    this->value_ = index;

    if(index > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}

unsigned int LCr4500::Settings::Pattern::FlashImage::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_FLASH_IMAGE
 */
std::string LCr4500::Settings::Pattern::FlashImage::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_FLASH_IMAGE";
}

/** \brief  Gets the flash image pattern entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::FlashImage::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for flash image pattern entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::FlashImage::GetEntryDefault() const{
    return "0";
}

/** \brief  Sets the flash image pattern entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::FlashImage::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
    if(this->value_ > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}



LCr4500::Settings::Pattern::FlashImage::Red::Red(){
    this->value_ = 0;
}

LCr4500::Settings::Pattern::FlashImage::FlashImage::Red::Red(const unsigned int &index){
    this->value_ = index;

    if(index > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}

void LCr4500::Settings::Pattern::FlashImage::Red::Set(const unsigned int &index){
    this->value_ = index;

    if(index > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}

unsigned int LCr4500::Settings::Pattern::FlashImage::Red::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_FLASH_IMAGE_RED
 */
std::string LCr4500::Settings::Pattern::FlashImage::Red::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_FLASH_IMAGE_RED";
}

/** \brief  Gets the Red flash image pattern entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::FlashImage::Red::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Red flash image pattern entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::FlashImage::Red::GetEntryDefault() const{
    return "0";
}

/** \brief  Sets the Red flash image pattern entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::FlashImage::Red::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
    if(this->value_ > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}



LCr4500::Settings::Pattern::FlashImage::Green::Green(){
    this->value_ = 0;
}

LCr4500::Settings::Pattern::FlashImage::FlashImage::Green::Green(const unsigned int &index){
    this->value_ = index;

    if(index > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}

void LCr4500::Settings::Pattern::FlashImage::Green::Set(const unsigned int &index){
    this->value_ = index;

    if(index > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}

unsigned int LCr4500::Settings::Pattern::FlashImage::Green::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_FLASH_IMAGE_GREEN
 */
std::string LCr4500::Settings::Pattern::FlashImage::Green::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_FLASH_IMAGE_GREEN";
}

/** \brief  Gets the Green flash image pattern entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::FlashImage::Green::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Green flash image pattern entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::FlashImage::Green::GetEntryDefault() const{
    return "0";
}

/** \brief  Sets the Green flash image pattern entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::FlashImage::Green::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
    if(this->value_ > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}



LCr4500::Settings::Pattern::FlashImage::Blue::Blue(){
    this->value_ = 0;
}

LCr4500::Settings::Pattern::FlashImage::FlashImage::Blue::Blue(const unsigned int &index){
    this->value_ = index;

    if(index > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}

void LCr4500::Settings::Pattern::FlashImage::Blue::Set(const unsigned int &index){
    this->value_ = index;

    if(index > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}

unsigned int LCr4500::Settings::Pattern::FlashImage::Blue::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_FLASH_IMAGE_BLUE
 */
std::string LCr4500::Settings::Pattern::FlashImage::Blue::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_FLASH_IMAGE_BLUE";
}

/** \brief  Gets the Blue flash image pattern entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::FlashImage::Blue::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for Blue flash image pattern entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::FlashImage::Blue::GetEntryDefault() const{
    return "0";
}

/** \brief  Sets the Blue flash image pattern entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::FlashImage::Blue::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
    if(this->value_ > Settings::FlashImage::MAXIMUM_INDEX) this->value_ = Settings::FlashImage::MAXIMUM_INDEX;
}



LCr4500::Settings::Pattern::Led::Led(){
    this->value_ = NONE;
}

LCr4500::Settings::Pattern::Led::Led(const unsigned int &led){
    this->value_ = led;

    if(this->value_ >= INVALID) this->value_ = NONE;
}

void LCr4500::Settings::Pattern::Led::Set(const unsigned int &led){
    this->value_ = led;

    if(this->value_ >= INVALID) this->value_ = NONE;
}

unsigned int LCr4500::Settings::Pattern::Led::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_LED
 */
std::string LCr4500::Settings::Pattern::Led::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_LED";
}

/** \brief  Gets the pattern LED entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Led::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for pattern LED entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Led::GetEntryDefault() const{
    return dlp::Number::ToString(NONE);
}

/** \brief  Sets the pattern LED entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Led::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);

    if(this->value_ >= INVALID) this->value_ = NONE;
}



LCr4500::Settings::Pattern::Bitdepth::Bitdepth(){
    this->value_ = MONO_1BPP;
}

LCr4500::Settings::Pattern::Bitdepth::Bitdepth(const unsigned int &format){
    this->value_ = format;

    if(this->value_ >= INVALID) this->value_ = MONO_1BPP;
}

void LCr4500::Settings::Pattern::Bitdepth::Set(const unsigned int &format){
    this->value_ = format;

    if(this->value_ >= INVALID) this->value_ = MONO_1BPP;
}

unsigned int LCr4500::Settings::Pattern::Bitdepth::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_BITDEPTH
 */
std::string LCr4500::Settings::Pattern::Bitdepth::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_BITDEPTH";
}

/** \brief  Gets the pattern bitdepth entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Bitdepth::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for pattern bitdepth entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Bitdepth::GetEntryDefault() const{
    return dlp::Number::ToString(MONO_1BPP);
}

/** \brief  Sets the pattern bitdepth entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Bitdepth::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);

    if(this->value_ >= INVALID) this->value_ = MONO_1BPP;
}



LCr4500::Settings::Pattern::Source::Source(){
    this->value_ = FLASH_IMAGES;
}

LCr4500::Settings::Pattern::Source::Source(const bool &source){
    this->value_ = source;
}

void LCr4500::Settings::Pattern::Source::Set(const bool &source){
    this->value_ = source;
}

bool LCr4500::Settings::Pattern::Source::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_SOURCE
 */
std::string LCr4500::Settings::Pattern::Source::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_SOURCE";
}

/** \brief  Gets the pattern source entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Source::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for pattern source entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Source::GetEntryDefault() const{
    return dlp::Number::ToString(FLASH_IMAGES);
}

/** \brief  Sets the pattern source entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Source::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::Pattern::Invert::Invert(){
    this->value_ = false;
}

LCr4500::Settings::Pattern::Invert::Invert(const bool &invert){
    this->value_ = invert;
}

void LCr4500::Settings::Pattern::Invert::Set(const bool &invert){
    this->value_ = invert;
}

bool LCr4500::Settings::Pattern::Invert::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_INVERT
 */
std::string LCr4500::Settings::Pattern::Invert::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_INVERT";
}

/** \brief  Gets the pattern invert entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Invert::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for pattern invert entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Invert::GetEntryDefault() const{
    return dlp::Number::ToString(false);
}

/** \brief  Sets the pattern invert entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Invert::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}




const unsigned long int LCr4500::Settings::Pattern::Exposure::MAXIMUM = 200000000;

const unsigned long int LCr4500::Settings::Pattern::Exposure::PERIOD_DIFFERENCE_MINIMUM = 230;

unsigned long int LCr4500::Settings::Pattern::Exposure::MININUM(const dlp::Pattern::Settings::Bitdepth::Format &bitdepth){
    switch(bitdepth){
    case dlp::Pattern::Settings::Bitdepth::Format::MONO_1BPP:   return     235;
    case dlp::Pattern::Settings::Bitdepth::Format::MONO_2BPP:   return     700;
    case dlp::Pattern::Settings::Bitdepth::Format::MONO_3BPP:   return    1570;
    case dlp::Pattern::Settings::Bitdepth::Format::MONO_4BPP:   return    1700;
    case dlp::Pattern::Settings::Bitdepth::Format::MONO_5BPP:   return    2000;
    case dlp::Pattern::Settings::Bitdepth::Format::MONO_6BPP:   return    2500;
    case dlp::Pattern::Settings::Bitdepth::Format::MONO_7BPP:   return    4500;
    case dlp::Pattern::Settings::Bitdepth::Format::MONO_8BPP:   return    8333;
    case dlp::Pattern::Settings::Bitdepth::Format::RGB_3BPP:    return     705;
    case dlp::Pattern::Settings::Bitdepth::Format::RGB_6BPP:    return    2100;
    case dlp::Pattern::Settings::Bitdepth::Format::RGB_9BPP:    return    4710;
    case dlp::Pattern::Settings::Bitdepth::Format::RGB_12BPP:   return    5100;
    case dlp::Pattern::Settings::Bitdepth::Format::RGB_15BPP:   return    6000;
    case dlp::Pattern::Settings::Bitdepth::Format::RGB_18BPP:   return    7500;
    case dlp::Pattern::Settings::Bitdepth::Format::RGB_21BPP:   return   13500;
    case dlp::Pattern::Settings::Bitdepth::Format::RGB_24BPP:   return   24999;
    case dlp::Pattern::Settings::Bitdepth::Format::INVALID:
    default:
        return MAXIMUM;
    }
}


//class Input1{ //Each bit adds 107.136 ns delay 4 Bytes = 4294967295



double LCr4500::Settings::Pattern::Trigger::Input1::Delay::GetNanoSeconds(){
    return (107.2*((double)this->value_));
}

LCr4500::Settings::Pattern::Trigger::Input1::Delay::Delay(){
    this->value_ = 0;
}

LCr4500::Settings::Pattern::Trigger::Input1::Delay::Delay(const unsigned int &delay){
    this->value_ = delay;
}

void LCr4500::Settings::Pattern::Trigger::Input1::Delay::Set(const unsigned int &delay){
    this->value_ = delay;
}

unsigned int LCr4500::Settings::Pattern::Trigger::Input1::Delay::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_TRIGGER_INPUT1_DELAY
 */
std::string LCr4500::Settings::Pattern::Trigger::Input1::Delay::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_TRIGGER_INPUT1_DELAY";
}

/** \brief  Gets the input trigger 1 delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Input1::Delay::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for input trigger 1 delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Input1::Delay::GetEntryDefault() const{
    return dlp::Number::ToString(0);
}

/** \brief  Sets the input trigger 1 delay entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Trigger::Input1::Delay::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
}



LCr4500::Settings::Pattern::Trigger::Output1::Invert::Invert(){
    this->value_ = false;
}

LCr4500::Settings::Pattern::Trigger::Output1::Invert::Invert(const bool &invert){
    this->value_ = invert;
}

void LCr4500::Settings::Pattern::Trigger::Output1::Invert::Set(const bool &invert){
    this->value_ = invert;
}

bool LCr4500::Settings::Pattern::Trigger::Output1::Invert::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_TRIGGER_OUTPUT1_INVERT
 */
std::string LCr4500::Settings::Pattern::Trigger::Output1::Invert::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_TRIGGER_OUTPUT1_INVERT";
}

/** \brief  Gets the output trigger 1 invert entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Output1::Invert::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for output trigger 1 invert entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Output1::Invert::GetEntryDefault() const{
    return dlp::Number::ToString(false);
}

/** \brief  Sets the output trigger 1 invert entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Trigger::Output1::Invert::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



LCr4500::Settings::Pattern::Trigger::Output2::Invert::Invert(){
    this->value_ = false;
}

LCr4500::Settings::Pattern::Trigger::Output2::Invert::Invert(const bool &invert){
    this->value_ = invert;
}

void LCr4500::Settings::Pattern::Trigger::Output2::Invert::Set(const bool &invert){
    this->value_ = invert;
}

bool LCr4500::Settings::Pattern::Trigger::Output2::Invert::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_TRIGGER_OUTPUT2_INVERT
 */
std::string LCr4500::Settings::Pattern::Trigger::Output2::Invert::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_TRIGGER_OUTPUT2_INVERT";
}

/** \brief  Gets the output trigger 2 invert entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Output2::Invert::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for output trigger 2 invert entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Output2::Invert::GetEntryDefault() const{
    return dlp::Number::ToString(false);
}

/** \brief  Sets the output trigger 2 invert entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Trigger::Output2::Invert::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}



const unsigned int LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::MAXIMUM = 0xD5;

LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Rising::Rising(){
    this->value_ = 187;
}

LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Rising::Rising(const unsigned int &delay){
    this->value_ = delay;

    if(this->value_ > MAXIMUM) this->value_ = MAXIMUM;
}

void LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Rising::Set(const unsigned int &delay){
    this->value_ = delay;

    if(this->value_ > MAXIMUM) this->value_ = MAXIMUM;
}

unsigned int LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Rising::Get() const{
    return this->value_;
}

double LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Rising::GetNanoSeconds(){
    return (107.2*((double)this->value_) - 20050);
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_TRIGGER_OUTPUT1_EDGE_DELAY_RISING
 */
std::string LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Rising::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_TRIGGER_OUTPUT1_EDGE_DELAY_RISING";
}

/** \brief  Gets the output trigger 1 rising edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Rising::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for output trigger 1 rising edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Rising::GetEntryDefault() const{
    return dlp::Number::ToString(187);
}

/** \brief  Sets the output trigger 1 rising edge delay entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Rising::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);

    if(this->value_ > MAXIMUM) this->value_ = MAXIMUM;
}



LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Falling::Falling(){
    this->value_ = 187;
}

LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Falling::Falling(const unsigned int &delay){
    this->value_ = delay;

    if(this->value_ > MAXIMUM) this->value_ = MAXIMUM;
}

void LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Falling::Set(const unsigned int &delay){
    this->value_ = delay;

    if(this->value_ > MAXIMUM) this->value_ = MAXIMUM;
}

unsigned int LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Falling::Get() const{
    return this->value_;
}

double LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Falling::GetNanoSeconds(){
    return (107.2*((double)this->value_) - 20050);
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_TRIGGER_OUTPUT1_EDGE_DELAY_FALLING
 */
std::string LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Falling::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_TRIGGER_OUTPUT1_EDGE_DELAY_FALLING";
}

/** \brief  Gets the output trigger 1 falling edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Falling::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for output trigger 1 falling edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Falling::GetEntryDefault() const{
    return dlp::Number::ToString(187);
}

/** \brief  Sets the output trigger 1 falling edge delay entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Trigger::Output1::EdgeDelay::Falling::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);

    if(this->value_ > MAXIMUM) this->value_ = MAXIMUM;
}



const unsigned int LCr4500::Settings::Pattern::Trigger::Output2::EdgeDelay::MAXIMUM = 0xFF;

LCr4500::Settings::Pattern::Trigger::Output2::EdgeDelay::Rising::Rising(){
    this->value_ = 187;
}

LCr4500::Settings::Pattern::Trigger::Output2::EdgeDelay::Rising::Rising(const unsigned int &delay){
    this->value_ = delay;

    if(this->value_ > MAXIMUM) this->value_ = MAXIMUM;
}

void LCr4500::Settings::Pattern::Trigger::Output2::EdgeDelay::Rising::Set(const unsigned int &delay){
    this->value_ = delay;

    if(this->value_ > MAXIMUM) this->value_ = MAXIMUM;
}

unsigned int LCr4500::Settings::Pattern::Trigger::Output2::EdgeDelay::Rising::Get() const{
    return this->value_;
}

double LCr4500::Settings::Pattern::Trigger::Output2::EdgeDelay::Rising::GetNanoSeconds(){
    return (107.2*((double)this->value_) - 20050);
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_TRIGGER_OUTPUT2_EDGE_DELAY_RISING
 */
std::string LCr4500::Settings::Pattern::Trigger::Output2::EdgeDelay::Rising::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_TRIGGER_OUTPUT2_EDGE_DELAY_RISING";
}

/** \brief  Gets the output trigger 2 rising edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Output2::EdgeDelay::Rising::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for output trigger 2 rising edge delay entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Trigger::Output2::EdgeDelay::Rising::GetEntryDefault() const{
    return dlp::Number::ToString(187);
}

/** \brief  Sets the output trigger 2 rising edge delay entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Trigger::Output2::EdgeDelay::Rising::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);

    if(this->value_ > MAXIMUM) this->value_ = MAXIMUM;
}



LCr4500::Settings::Pattern::Number::Number(){
    this->value_ = 0;
}

LCr4500::Settings::Pattern::Number::Number(const unsigned int &index){
    this->value_ = index;
}

void LCr4500::Settings::Pattern::Number::Set(const unsigned int &index){
    this->value_ = index;
}

unsigned int LCr4500::Settings::Pattern::Number::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_NUMBER
 */
std::string LCr4500::Settings::Pattern::Number::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_NUMBER";
}

/** \brief  Gets the pattern number entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Number::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for pattern number entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Number::GetEntryDefault() const{
    return "0";
}

/** \brief  Sets the pattern number entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Number::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
}



LCr4500::Settings::Pattern::Number::Number::Red::Red(){
    this->value_ = 0;
}

LCr4500::Settings::Pattern::Number::Number::Red::Red(const unsigned int &index){
    this->value_ = index;
}

void LCr4500::Settings::Pattern::Number::Red::Set(const unsigned int &index){
    this->value_ = index;
}

unsigned int LCr4500::Settings::Pattern::Number::Red::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_NUMBER_RED
 */
std::string LCr4500::Settings::Pattern::Number::Red::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_NUMBER_RED";
}

/** \brief  Gets the red pattern number entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Number::Red::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for the red pattern number entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Number::Red::GetEntryDefault() const{
    return "0";
}

/** \brief  Sets the red pattern number entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Number::Red::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
}



LCr4500::Settings::Pattern::Number::Number::Green::Green(){
    this->value_ = 0;
}

LCr4500::Settings::Pattern::Number::Number::Green::Green(const unsigned int &index){
    this->value_ = index;
}

void LCr4500::Settings::Pattern::Number::Green::Set(const unsigned int &index){
    this->value_ = index;
}

unsigned int LCr4500::Settings::Pattern::Number::Green::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_NUMBER_GREEN
 */
std::string LCr4500::Settings::Pattern::Number::Green::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_NUMBER_GREEN";
}

/** \brief  Gets the green pattern number entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Number::Green::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for the green pattern number entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Number::Green::GetEntryDefault() const{
    return "0";
}

/** \brief  Sets the green pattern number entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Number::Green::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
}






LCr4500::Settings::Pattern::Number::Number::Blue::Blue(){
    this->value_ = 0;
}

LCr4500::Settings::Pattern::Number::Number::Blue::Blue(const unsigned int &index){
    this->value_ = index;
}

void LCr4500::Settings::Pattern::Number::Blue::Set(const unsigned int &index){
    this->value_ = index;
}

unsigned int LCr4500::Settings::Pattern::Number::Blue::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PATTERN_NUMBER_BLUE
 */
std::string LCr4500::Settings::Pattern::Number::Blue::GetEntryName() const{
    return "LCR4500_SETTINGS_PATTERN_NUMBER_BLUE";
}

/** \brief  Gets the blue pattern number entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Number::Blue::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for the blue pattern number entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::Pattern::Number::Blue::GetEntryDefault() const{
    return "0";
}

/** \brief  Sets the blue pattern number entry setting on LightCrafter 4500*/
void LCr4500::Settings::Pattern::Number::Blue::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
}








LCr4500::Settings::ParallelClockSelect::ParallelClockSelect(){
    this->value_ = PORT_1_CLOCK_A;
}

LCr4500::Settings::ParallelClockSelect::ParallelClockSelect(const unsigned int &clock){
    this->value_ = clock;

    if(clock >= PORT_1_CLOCK_C) this->value_ = PORT_1_CLOCK_A;
}

void LCr4500::Settings::ParallelClockSelect::Set(const unsigned int &clock){
    this->value_ = clock;

    if(clock >= PORT_1_CLOCK_C) this->value_ = PORT_1_CLOCK_A;
}

unsigned int LCr4500::Settings::ParallelClockSelect::Get() const{
    return this->value_;
}

/** \brief  Gets the string name for the setting entry
 *  \retval LCR4500_SETTINGS_PARALLEL_CLOCK_SELECT
 */
std::string LCr4500::Settings::ParallelClockSelect::GetEntryName() const{
    return "LCR4500_SETTINGS_PARALLEL_CLOCK_SELECT";
}

/** \brief  Gets the parallel port clock select entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::ParallelClockSelect::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief  Gets the default entry for the parallel port clock select entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::ParallelClockSelect::GetEntryDefault() const{
    return dlp::Number::ToString(PORT_1_CLOCK_A);
}

/** \brief  Sets the parallel port clock select entry setting on LightCrafter 4500*/
void LCr4500::Settings::ParallelClockSelect::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);

    if(this->value_ >= PORT_1_CLOCK_C) this->value_ = PORT_1_CLOCK_A;
}





LCr4500::Settings::InputDataSwap::InputDataSwap(){
    this->value_port_         = PARALLEL_INTERFACE;
    this->value_data_channel_ = ABC_TO_BAC; // LightCrafter 4500 default based on hardware
}

LCr4500::Settings::InputDataSwap::InputDataSwap(const bool &port, const unsigned int &data_channel){
    this->value_port_         = port;
    this->value_data_channel_ = data_channel;

    if(this->value_data_channel_ >= INVALID) this->value_data_channel_ = ABC_TO_BAC;
}

void LCr4500::Settings::InputDataSwap::Set(const bool &port, const unsigned int &data_channel){
    this->value_port_         = port;
    this->value_data_channel_ = data_channel;

    if(this->value_data_channel_ >= INVALID) this->value_data_channel_ = ABC_TO_BAC;
}

bool LCr4500::Settings::InputDataSwap::GetPort() const{
    return this->value_port_;
}

unsigned int LCr4500::Settings::InputDataSwap::GetDataChannel() const{
    return this->value_data_channel_;
}

/** \brief  Gets the string name for the setting entry
  * \retval LCR4500_SETTINGS_INPUT_DATA_SWAP
  */
std::string LCr4500::Settings::InputDataSwap::GetEntryName() const{
    return "LCR4500_SETTINGS_INPUT_DATA_SWAP";
}

/** \brief  Gets the input data subchannel swap entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::InputDataSwap::GetEntryValue() const{
    return dlp::Number::ToString(this->value_port_) + ", " + dlp::Number::ToString(this->value_data_channel_);
}

/** \brief  Gets the default entry for the input data subchannel swap entry setting on LightCrafter 4500*/
std::string LCr4500::Settings::InputDataSwap::GetEntryDefault() const{
    return dlp::Number::ToString(PARALLEL_INTERFACE) + ", " + dlp::Number::ToString(ABC_TO_BAC);
}

/** \brief  Sets the input data subchannel swap entry setting on LightCrafter 4500*/
void LCr4500::Settings::InputDataSwap::SetEntryValue(std::string value){
    std::vector<std::string> settings;

    settings = dlp::String::SeparateDelimited(value, ',');

    if(settings.size() == 2){
        this->value_port_         = dlp::String::ToNumber<unsigned int>(settings.at(0));
        this->value_data_channel_ = dlp::String::ToNumber<unsigned int>(settings.at(1));

        if(this->value_data_channel_ >= INVALID) this->value_data_channel_ = ABC_TO_BAC;
    }
    else{
        this->value_port_         = PARALLEL_INTERFACE;
        this->value_data_channel_ = ABC_TO_BAC; // LightCrafter 4500 default based on hardware
    }
}
}

