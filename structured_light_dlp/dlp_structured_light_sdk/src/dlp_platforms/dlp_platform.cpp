/** \file dlp_platform.cpp
 *  \brief Contains methods for DLP_Platform class
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#include <iostream>

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/other.hpp>
#include <common/parameters.hpp>

#include <dlp_platforms/dlp_platform.hpp>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{


/** \brief Constructs object */
DLP_Platform::DLP_Platform(){
    this->device_   = 0;

    this->rows_     = 0;
    this->columns_  = 0;

    this->platform_ = Platform::INVALID;
    this->mirror_   = Mirror::INVALID;
    this->mirror_effective_size_um_ = 0.0;

    this->is_platform_set_ = false;
}


/**
 * \brief   Sets the object's \ref DLP_Platform::Platform
 * \retval  DLP_PLATFORM_NOT_SETUP  An invalid \ref DLP_Platform::Platform was supplied or has NOT been implemented in SDK
 */
ReturnCode DLP_Platform::SetPlatform(Platform arg_platform){
    ReturnCode ret;

    switch(arg_platform){
    case Platform::LIGHTCRAFTER_3000:
        this->platform_ = arg_platform;
        this->mirror_   = Mirror::DIAMOND;
        this->mirror_effective_size_um_ = 10.8;
        this->rows_     = 684;
        this->columns_  = 608;
        this->is_platform_set_    = true;
        break;
    case Platform::LIGHTCRAFTER_4500:
        this->platform_ = arg_platform;
        this->mirror_   = Mirror::DIAMOND;
        this->mirror_effective_size_um_ = 10.8;
        this->rows_     = 1140;
        this->columns_  = 912;
        this->is_platform_set_    = true;
        break;
    default:
        this->is_platform_set_ = false;
        ret.AddError(DLP_PLATFORM_NOT_SETUP);
    }

    return ret;
}

/** \brief Saves the device identifier */
void DLP_Platform::SetDevice(unsigned int device){
    this->device_ = device;
}

/** \brief  Returns the object's identifier
 *  \retval  DLP_PLATFORM_NOT_SETUP  An invalid \ref DLP_Platform::Platform was supplied or has NOT been implemented in SDK
 */
ReturnCode DLP_Platform::GetDevice(unsigned int *ret_device) const{
    ReturnCode ret;

    if(!this->isPlatformSetup()){
        ret.AddError(DLP_PLATFORM_NOT_SETUP);
    }

    (*ret_device) = this->device_;

    return ret;
}

/** \brief Returns true if SetPlatform() was previously called successfully */
bool DLP_Platform::isPlatformSetup() const{
    return this->is_platform_set_;
}

/** \brief Returns true if SetPlatform() and Setup() were previously called successfully */
bool DLP_Platform::isSetup() const{
    return (this->is_setup_ & this->is_platform_set_);
}

/** \brief Returns true if the supplied image file has the same resolution as the DLP_Platform object */
bool DLP_Platform::ImageResolutionCorrect(const std::string &arg_image_filename) const{
    bool ret;
    Image test_image;

    // Load the image and check it it loaded correctly
    if(!test_image.Load(arg_image_filename).hasErrors()){

        // Check the resolution
        this->debug_.Msg(1,"Image loaded, comparing DMD and Image resolution...");
        ret = this->ImageResolutionCorrect(test_image);

        // Clear the image
        test_image.Clear();
    }
    else{
        // Image did NOT load properly
        this->debug_.Msg(1,"Image failed to load, cannot compare DMD and Image resolution");
        ret = false;
    }
    // Return the value
    return ret;
}

/** \brief Returns true if the supplied \ref dlp::Image has the same resolution as the DLP_Platform object */
bool DLP_Platform::ImageResolutionCorrect(const Image &arg_image)const{
    bool ret;
    unsigned int image_rows = 0;
    unsigned int image_cols = 0;
    unsigned int dmd_rows = 0;
    unsigned int dmd_cols = 0;

    // Get DMD columns and rows
    this->GetColumns(&dmd_cols);
    this->GetRows(&dmd_rows);

    this->debug_.Msg(1, "Compare DMD and Image resolution...");

    this->debug_.Msg(1, "DMD rows    = " + dlp::Number::ToString(dmd_rows));
    this->debug_.Msg(1, "DMD columns = " + dlp::Number::ToString(dmd_cols));

    // Get Flash Image columns and rows
    arg_image.GetRows(&image_rows);
    arg_image.GetColumns(&image_cols);


    this->debug_.Msg(1, "Image rows    = " + dlp::Number::ToString(image_rows));
    this->debug_.Msg(1, "Image columns = " + dlp::Number::ToString(image_cols));

    // Detmine if resoluations match
    ret = (image_rows == dmd_rows) && (image_cols == dmd_cols);

    if(ret){
        this->debug_.Msg(1, "DMD and Image resolutions match");
    }
    else{
        this->debug_.Msg(1, "DMD and Image resolutions do NOT match");
    }

    // Return value
    return ret;
}

/** \brief  Returns the object's \ref dlp::DLP_Platform::Platform
 *  \retval DLP_PLATFORM_NULL_INPUT_ARGUMENT    Input argument NULL
 *  \retval DLP_PLATFORM_NOT_SETUP              Object has NOT been setup
 */
ReturnCode DLP_Platform::GetPlatform(Platform *ret_platform) const{
    ReturnCode ret;

    // Check for NULL pointer
    if(!ret_platform)
        return ret.AddError(DLP_PLATFORM_NULL_INPUT_ARGUMENT);

    // Check that DLP Platform has been setup
    if(this->isPlatformSetup()){
        (*ret_platform) = this->platform_;
    }
    else{
        // Platform has NOT been setup
        ret.AddError(DLP_PLATFORM_NOT_SETUP);
    }

    return ret;
}

/** \brief  Returns the object's \ref dlp::DLP_Platform::Mirror
 *  \retval DLP_PLATFORM_NULL_INPUT_ARGUMENT    Input argument NULL
 *  \retval DLP_PLATFORM_NOT_SETUP              Object has NOT been setup
 */
ReturnCode DLP_Platform::GetMirrorType(Mirror *ret_mirror) const{
    ReturnCode ret;

    // Check for NULL pointer
    if(!ret_mirror)
        return ret.AddError(DLP_PLATFORM_NULL_INPUT_ARGUMENT);

    // Check that DLP Platform has been setup
    if(this->isPlatformSetup()){
        (*ret_mirror) = this->mirror_;
    }
    else{
        // Platform has NOT been setup
        ret.AddError(DLP_PLATFORM_NOT_SETUP);
    }

    return ret;
}


/** \brief  Returns the object's effective mirror size in micrometers (othogonal distance from pixel to pixel)
 *  \retval DLP_PLATFORM_NULL_INPUT_ARGUMENT    Input argument NULL
 *  \retval DLP_PLATFORM_NOT_SETUP              Object has NOT been setup
 */
ReturnCode DLP_Platform::GetEffectiveMirrorSize( float *ret_mirror_size) const{
    ReturnCode ret;

    // Check for NULL pointer
    if(!ret_mirror_size)
        return ret.AddError(DLP_PLATFORM_NULL_INPUT_ARGUMENT);

    // Check that DLP Platform has been setup
    if(this->isPlatformSetup()){
        (*ret_mirror_size) = this->mirror_effective_size_um_;
    }
    else{
        // Platform has NOT been setup
        ret.AddError(DLP_PLATFORM_NOT_SETUP);
    }

    return ret;
}


/** \brief  Returns the number of pixel rows (height)
 *  \retval DLP_PLATFORM_NULL_INPUT_ARGUMENT    Input argument NULL
 *  \retval DLP_PLATFORM_NOT_SETUP              Object has NOT been setup
 */
ReturnCode DLP_Platform::GetRows(unsigned int *ret_rows) const{
    ReturnCode ret;

    // Check for NULL pointer
    if(!ret_rows)
        return ret.AddError(DLP_PLATFORM_NULL_INPUT_ARGUMENT);

    // Check that DLP Platform has been setup
    if(this->isPlatformSetup()){
        (*ret_rows) = this->rows_;
    }
    else{
        // Platform has NOT been setup
        ret.AddError(DLP_PLATFORM_NOT_SETUP);
    }

    return ret;
}

/** \brief  Returns the number of pixel columns (width)
 *  \retval DLP_PLATFORM_NULL_INPUT_ARGUMENT    Input argument NULL
 *  \retval DLP_PLATFORM_NOT_SETUP              Object has NOT been setup
 */
ReturnCode DLP_Platform::GetColumns(unsigned int *ret_columns) const{
    ReturnCode ret;

    // Check for NULL pointer
    if(!ret_columns)
        return ret.AddError(DLP_PLATFORM_NULL_INPUT_ARGUMENT);


    // Check that DLP Platform has been setup
    if(this->isPlatformSetup()){
        (*ret_columns) = this->columns_;
    }
    else{
        // Platform has NOT been setup
        ret.AddError(DLP_PLATFORM_NOT_SETUP);
    }

    return ret;
}

}
