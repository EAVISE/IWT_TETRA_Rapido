/**
 *  \file dlp_platform.cpp
 *  \brief Contains methods for DLP_Platform class
 */

#include <iostream>

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/other.hpp>
#include <common/parameters.hpp>

#include <dlp_platforms/dlp_platform.hpp>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief Default constructor false*/
DLP_Platform::Settings::PatternSequence::Prepared::Prepared()
{
    this->prepared_ = false;
}

/** \brief construct prepared or not prepared*/
DLP_Platform::Settings::PatternSequence::Prepared::Prepared(bool prep)
{
    this->prepared_ = prep;
}

/** \brief set or clear*/
void DLP_Platform::Settings::PatternSequence::Prepared::Set(bool prep)
{
    this->prepared_ = prep;
}

/** \brief Is the PatternSequence prepared?*/
bool DLP_Platform::Settings::PatternSequence::Prepared::Get()const{
    return this->prepared_;
}

/** \brief Set the entry value from a string, which is converted to a bool
 *  \param[in] value String representation of a boolean*/
void DLP_Platform::Settings::PatternSequence::Prepared::SetEntryValue(std::string value)
{
    this->prepared_ = dlp::String::ToNumber<bool>(value);
}

/** \brief get entry name*/
std::string DLP_Platform::Settings::PatternSequence::Prepared::GetEntryName() const
{
    return "DLP_PLATFORM_SETTINGS_PATTERNSEQUENCE_PREPARED";
}

/** \brief get entry value*/
std::string DLP_Platform::Settings::PatternSequence::Prepared::GetEntryValue()const
{
    return dlp::Number::ToString(this->prepared_);
}


/** \brief default constructor sets time to 0*/
DLP_Platform::Settings::PatternSequence::Exposure::Exposure()
{
    this->exposure_ = 0;
}

/** \brief construct with exposure time*/
DLP_Platform::Settings::PatternSequence::Exposure::Exposure(unsigned long long time)
{
    this->exposure_ = time;
}

/** \brief get entry default*/
std::string DLP_Platform::Settings::PatternSequence::Prepared::GetEntryDefault() const{
    return dlp::Number::ToString(false);
}

/** \brief set time*/
void DLP_Platform::Settings::PatternSequence::Exposure::Set(unsigned long long time)
{
    this->exposure_ = time;
}

/** \brief Return the exposure as an unsigned long long*/
unsigned long long DLP_Platform::Settings::PatternSequence::Exposure::Get()const{
    return this->exposure_;
}

/** \brief set time from string*/
void DLP_Platform::Settings::PatternSequence::Exposure::SetEntryValue(std::string value)
{
    this->exposure_ = String::ToNumber<unsigned long long>(value);
}

/** \brief get entry name*/
std::string DLP_Platform::Settings::PatternSequence::Exposure::GetEntryName()const
{
    return "DLP_PLATFORM_SETTINGS_PATTERNSEQUENCE_EXPOSURE";
}

/** \brief get entry value*/
std::string DLP_Platform::Settings::PatternSequence::Exposure::GetEntryValue()const
{
    return Number::ToString(this->exposure_);
}

/** \brief get entry default of "16666"*/
std::string DLP_Platform::Settings::PatternSequence::Exposure::GetEntryDefault()const
{
    return "16666";
}

/** \brief default constructor sets time to 0*/
DLP_Platform::Settings::PatternSequence::Period::Period()
{
    this->period_ = 0;
}

/** \brief construct with exposure time*/
DLP_Platform::Settings::PatternSequence::Period::Period(unsigned long long time)
{
    this->period_ = time;
}

/** \brief set time*/
void DLP_Platform::Settings::PatternSequence::Period::Set(unsigned long long time)
{
    this->period_ = time;
}

unsigned long long DLP_Platform::Settings::PatternSequence::Period::Get()const{
    return this->period_;
}

/** \brief set time from string*/
void DLP_Platform::Settings::PatternSequence::Period::SetEntryValue(std::string value)
{
    this->period_ = dlp::String::ToNumber<unsigned long long>(value);
}

/** \brief get entry name*/
std::string DLP_Platform::Settings::PatternSequence::Period::GetEntryName()const
{
    return "DLP_PLATFORM_SETTINGS_PATTERNSEQUENCE_PERIOD";
}

/** \brief get entry value*/
std::string DLP_Platform::Settings::PatternSequence::Period::GetEntryValue()const
{
    return Number::ToString(this->period_);
}

/** \brief get entry default of "16666"*/
std::string DLP_Platform::Settings::PatternSequence::Period::GetEntryDefault()const
{
    return "16666";
}

}

