/** \file   pg_flycap2_settings_c.cpp
 *  \brief  Conatins Point Grey Research camera objects and methods for changing and storing camera configuration/setting entries
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/parameters.hpp>
#include <camera/pg_flycap2/pg_flycap2_c.hpp>

#include <vector>
#include <string>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief  Default Value 0*/
PG_FlyCap2_C::Settings::Rows::Rows(){
    this->value_ = 0;
}

/** \brief      Take row count parameter
 *  \param[in]   rows  Number of rows
 */
PG_FlyCap2_C::Settings::Rows::Rows(const unsigned int &rows){
    this->value_ = rows;
}

/** \brief      Row count Set Method
 *  \param[in]   rows  Number of rows
 */
void PG_FlyCap2_C::Settings::Rows::Set(const unsigned int &rows){
    this->value_ = rows;
}

/** \brief      Row count get method*/
unsigned int PG_FlyCap2_C::Settings::Rows::Get() const{
    return this->value_;
}

/** \brief      Returns the entry name as a string
 *  \retval     PG_FLYCAP_SETTINGS_ROWS Entry name for flycap settings
 */
std::string PG_FlyCap2_C::Settings::Rows::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_ROWS";

}

/** \brief      Returns entry value as a string*/
std::string PG_FlyCap2_C::Settings::Rows::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief      Returns default value of the entry as a string*/
std::string PG_FlyCap2_C::Settings::Rows::GetEntryDefault() const{
    return "0";
}

/** \brief      Set entry value
 *  \param[in]   value string to set the number of rows to after conversion to integer*/
void PG_FlyCap2_C::Settings::Rows::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}

/** \brief  Default Value 0*/
// Columns
PG_FlyCap2_C::Settings::Columns::Columns(){
    this->value_ = 0;
}

/** \brief      Take column count parameter
 *  \param[in]  columns  Number of columns
 */
PG_FlyCap2_C::Settings::Columns::Columns(const unsigned int &columns){
    this->value_ = columns;
}

/** \brief      Column count set method
 *  \param[in]   columns  Number of columns
 */
void PG_FlyCap2_C::Settings::Columns::Set(const unsigned int &columns){
    this->value_ = columns;
}
/** \brief      Columns count get method*/
unsigned int PG_FlyCap2_C::Settings::Columns::Get() const{
    return this->value_;
}


/** \brief  Get method for column entry name "PG_FLYCAP_SETTINGS_COLUMNS"
 *  \retval PG_FLYCAP_SETTINGS_COLUMNS Entry name for flycap settings
 */
std::string PG_FlyCap2_C::Settings::Columns::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_COLUMNS";

}
/** \brief Get entry value for number of columns as string*/
std::string PG_FlyCap2_C::Settings::Columns::GetEntryValue() const{
    return Number::ToString(this->value_);
}
/** \brief Get entry default "0" as string*/
std::string PG_FlyCap2_C::Settings::Columns::GetEntryDefault() const{
    return "0";
}

/** \brief Set entry value from string
 * \param[in] value String of a number to be converted to set value to*/
void PG_FlyCap2_C::Settings::Columns::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}

//OffsetX
/** \brief Default value 0*/
PG_FlyCap2_C::Settings::OffsetX::OffsetX(){
    this->value_ = 0;
}


/** \brief take Horizontal offset parameter
 *  \param[in] offsetX offsetX value in pixels*/
PG_FlyCap2_C::Settings::OffsetX::OffsetX(const unsigned int &offsetX){
    this->value_ = offsetX;
}

/** \brief OffsetX set method
 *  \param[in] OffsetX */
void PG_FlyCap2_C::Settings::OffsetX::Set(const unsigned int &offsetX){
    this->value_ = offsetX;
}


/** \brief OffsetX get method*/
unsigned int PG_FlyCap2_C::Settings::OffsetX::Get() const{
    return this->value_;
}


/**
 *  \brief      Retrieve the name of the OffsetX parameter in string
 *  \retval     PG_FLYCAP_SETTINGS_OFFSETX  Entry name for flycap settings
 */
std::string PG_FlyCap2_C::Settings::OffsetX::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_OFFSETX";

}
/** \brief Returns entry value as string*/
std::string PG_FlyCap2_C::Settings::OffsetX::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Returns default value of "0"*/
std::string PG_FlyCap2_C::Settings::OffsetX::GetEntryDefault() const{
    return "0";
}

/** \brief Set entry value
  *  \param[in] value String to set number of rows to after conversion to integer
  */
void PG_FlyCap2_C::Settings::OffsetX::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}

//OffsetY
/** \brief Default value 0*/
PG_FlyCap2_C::Settings::OffsetY::OffsetY(){
    this->value_ = 0;
}

/** \brief take Vertical offset parameter
 *  \param[in] offsetY offsetY value in pixels*/
PG_FlyCap2_C::Settings::OffsetY::OffsetY(const unsigned int &offsetY){
    this->value_ = offsetY;
}

/** \brief OffsetY set method
 *  \param[in] OffsetY */
void PG_FlyCap2_C::Settings::OffsetY::Set(const unsigned int &offsetY){
    this->value_ = offsetY;
}

/**
 *  \brief      Retrieve the name of the OffsetY parameter in string
 *  \retval     PG_FLYCAP_SETTINGS_OFFSETY
 */
unsigned int PG_FlyCap2_C::Settings::OffsetY::Get() const{
    return this->value_;
}

/** \brief Returns entry name as string*/
std::string PG_FlyCap2_C::Settings::OffsetY::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_OFFSETX";

}

/** \brief Returns entry value as string*/
std::string PG_FlyCap2_C::Settings::OffsetY::GetEntryValue() const{
    return Number::ToString(this->value_);
}
/** \brief Returns default value of "0"*/
std::string PG_FlyCap2_C::Settings::OffsetY::GetEntryDefault() const{
    return "0";
}
/** \brief Set entry value*/
void PG_FlyCap2_C::Settings::OffsetY::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}


// Shutter Speed

/** \brief Default value of 16.67 */
PG_FlyCap2_C::Settings::ShutterSpeed::ShutterSpeed(){
    this->value_ = 16.67;
}

/** \brief Take Shutter speed parameter
 *  \param[in] shutter_speed Shutterd speed in milliseconds
 */
PG_FlyCap2_C::Settings::ShutterSpeed::ShutterSpeed(const float &shutter_speed){
    this->value_ = shutter_speed;
}

/** \brief Shutter Speed Set Method
 *  \param[in] Shutter speed in milliseconds
 */
void PG_FlyCap2_C::Settings::ShutterSpeed::Set(const float &shutter_speed){
    this->value_ = shutter_speed;
}

/** \brief Retrieve the shutter speed value*/
float PG_FlyCap2_C::Settings::ShutterSpeed::Get() const{
    return this->value_;
}

/** \brief  Retrieve the name of Shutter speed parameter in string
 *  \retval PG_FLYCAP_SETTINGS_SHUTTER_SPEED    Entry name for flycap settings
 */
std::string PG_FlyCap2_C::Settings::ShutterSpeed::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_SHUTTER_SPEED";

}

/** \brief Returns entry value as string*/
std::string PG_FlyCap2_C::Settings::ShutterSpeed::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Returns default value of "16.67"*/
std::string PG_FlyCap2_C::Settings::ShutterSpeed::GetEntryDefault() const{
    return "16.67";
}

/** \brief Set entry value*/
void PG_FlyCap2_C::Settings::ShutterSpeed::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<float>(value);
}


/** \brief Default value of 60 frames per second */
PG_FlyCap2_C::Settings::FrameRate::FrameRate(){
    this->value_ = 60;
}

/** \brief Take frame rate parameter
 *  \param[in] frame_rate value in fps
 */
PG_FlyCap2_C::Settings::FrameRate::FrameRate(const float &frame_rate){
    this->value_ = frame_rate;
}

/** \brief Frame Rate Set method */
void PG_FlyCap2_C::Settings::FrameRate::Set(const float &frame_rate){
    this->value_ = frame_rate;
}

/** \brief Frame rate get method
 *  \retval  frame rate in frames per second
 */
float PG_FlyCap2_C::Settings::FrameRate::Get() const{
    return this->value_;
}

/** \brief Returns entry name as string
 *  \retval PG_FLYCAP_SETTINGS_FRAME_RATE   Entry name for flycap settings
 */
std::string PG_FlyCap2_C::Settings::FrameRate::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_FRAME_RATE";
}

/** \brief Returns entry value as string*/
std::string PG_FlyCap2_C::Settings::FrameRate::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Returns default value of "60"*/
std::string PG_FlyCap2_C::Settings::FrameRate::GetEntryDefault() const{
    return "60";
}


/** \brief Set entry value*/
void PG_FlyCap2_C::Settings::FrameRate::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<float>(value);
}



//Exposure

/** \brief Default value 1.0*/
PG_FlyCap2_C::Settings::Exposure::Exposure(){
    this->value_ = 1.0;
}

/** \brief take Exposure parameter
 *  \param[in] exposure exposure value*/
PG_FlyCap2_C::Settings::Exposure::Exposure(const float &exposure){
    this->value_ = exposure;
}

/** \brief exposure set method
 *  \param[in] Exposure */
void PG_FlyCap2_C::Settings::Exposure::Set(const float &exposure){
    this->value_ = exposure;
}

/** \brief Retrieve the exposure value in milliseconds*/
float PG_FlyCap2_C::Settings::Exposure::Get() const{
    return this->value_;
}

/** \brief Returns entry name as string
 *  \retval PG_FLYCAP_SETTINGS_EXPOSURE   Entry name for flycap settings
 */
std::string PG_FlyCap2_C::Settings::Exposure::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_EXPOSURE";

}

/** \brief Returns entry value as string*/
std::string PG_FlyCap2_C::Settings::Exposure::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Returns default value of "1.0"*/
std::string PG_FlyCap2_C::Settings::Exposure::GetEntryDefault() const{
    return "1.0";
}

/** \brief Set entry value*/
void PG_FlyCap2_C::Settings::Exposure::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<float>(value);
}

/** \brief Default value 1.0*/
PG_FlyCap2_C::Settings::Gain::Gain(){
    this->value_ = 1.0;
}

/** \brief take Gain parameter
 *  \param[in] gain gain value*/
PG_FlyCap2_C::Settings::Gain::Gain(const float &gain){
    this->value_ = gain;
}

/** \brief exposure set method
 *  \param[in] gain */
void PG_FlyCap2_C::Settings::Gain::Set(const float &gain){
    this->value_ = gain;
}

/** \brief Retrieve the gain value */
float PG_FlyCap2_C::Settings::Gain::Get() const{
    return this->value_;
}

/** \brief Returns entry name as string
 *  \retval PG_FLYCAP_SETTINGS_GAIN   Entry name for flycap settings
 */
std::string PG_FlyCap2_C::Settings::Gain::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_GAIN";

}

/** \brief Returns entry value as string*/
std::string PG_FlyCap2_C::Settings::Gain::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Returns default value of "1.0"*/
std::string PG_FlyCap2_C::Settings::Gain::GetEntryDefault() const{
    return "1.0";
}

/** \brief Set entry value*/
void PG_FlyCap2_C::Settings::Gain::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<float>(value);
}























//Sharpness

/** \brief Default value 1500*/
PG_FlyCap2_C::Settings::Sharpness::Sharpness(){
    this->value_ = 1500;
}

/** \brief take Sharpness parameter
 *  \param[in] sharpness value*/
PG_FlyCap2_C::Settings::Sharpness::Sharpness(const unsigned int &sharpness){
    this->value_ = sharpness;
}

/** \brief sharpness set method
 *  \param[in] Sharpness */
void PG_FlyCap2_C::Settings::Sharpness::Set(const unsigned int &sharpness){
    this->value_ = sharpness;
}

/** \brief Retrieve the sharpness value*/
unsigned int PG_FlyCap2_C::Settings::Sharpness::Get() const{
    return this->value_;
}

/** \brief Returns entry name as string
 *  \retval PG_FLYCAP_SETTINGS_SHARPNESS   Entry name for flycap settings
 */
std::string PG_FlyCap2_C::Settings::Sharpness::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_SHARPNESS";

}

/** \brief Returns entry value as string*/
std::string PG_FlyCap2_C::Settings::Sharpness::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Returns default value of "1500"*/
std::string PG_FlyCap2_C::Settings::Sharpness::GetEntryDefault() const{
    return "1500";
}

/** \brief Set entry value*/
void PG_FlyCap2_C::Settings::Sharpness::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<unsigned int>(value);
}


// Video Mode
/** \brief Default value of video mode set to Full resolution*/
PG_FlyCap2_C::Settings::VideoMode::VideoMode(){
    this->value_ = Options::MODE0_FULL_RESOLUTION;
}

/** \brief Take Video mode parameter
 *  \param[in] video_mode Video Mode in terms of full or half resolution
 */
PG_FlyCap2_C::Settings::VideoMode::VideoMode(const Options &video_mode){
    this->value_ = video_mode;
}

/** \brief Video Mode Set method */
void PG_FlyCap2_C::Settings::VideoMode::Set(const Options &video_mode){
    this->value_ = video_mode;
}

/** \brief Retrieve the Video mode */
PG_FlyCap2_C::Settings::VideoMode::Options PG_FlyCap2_C::Settings::VideoMode::Get() const{
    return this->value_;

}

/** \brief Retrieve entry name for Video mode
 *  \retval PG_FLYCAP_SETTINGS_VIDEO_MODE   Entry name for flycap settings
 */
std::string PG_FlyCap2_C::Settings::VideoMode::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_VIDEO_MODE";
}

/** \brief Retrieve the default value of "MODE0_FULL_RESOLUTION"
 *  \retval MODE0_FULL_RESOLUTION   Entry name for flycap settings
 */
std::string PG_FlyCap2_C::Settings::VideoMode::GetEntryDefault() const{
    return "MODE0_FULL_RESOLUTION";
}

/** \brief Retrieve the entry value for video_mode
 *  \retval MODE0_FULL_RESOLUTION   Full resolution video mode set
 *  \retval MODE4_HALF_RESOLUTION   Half resolution video mode set
 *  \retval INVALID                 An INVALID resolution video mode set
 */
std::string PG_FlyCap2_C::Settings::VideoMode::GetEntryValue() const{
    switch (this->value_){
    case Options::MODE0_FULL_RESOLUTION: return "MODE0_FULL_RESOLUTION";
        break;
    case Options::MODE4_HALF_RESOLUTION: return "MODE4_HALF_RESOLUTION";
        break;
    case Options::INVALID: return "INVALID";
        break;
    default:				return "MODE0_FULL_RESOLUTION";
    }
}

/** \brief Set entry value*/
void PG_FlyCap2_C::Settings::VideoMode::SetEntryValue(std::string value){
    if (value.compare("MODE0_FULL_RESOLUTION") == 0){
        this->value_ = Options::MODE0_FULL_RESOLUTION;
    }
    else if (value.compare("MODE4_HALF_RESOLUTION") == 0){
        this->value_ = Options::MODE4_HALF_RESOLUTION;
    }
    else
        this->value_ = Options::INVALID;
}


// Pixel format
/** \brief Default value of Pixel Format PIXEL_FORMAT_RAW8*/
PG_FlyCap2_C::Settings::PixelFormat::PixelFormat(){
    this->value_ = Options::PIXEL_FORMAT_RAW8;
}

PG_FlyCap2_C::Settings::PixelFormat::PixelFormat(const Options &pixel_format){
    this->value_ = pixel_format;
}

void PG_FlyCap2_C::Settings::PixelFormat::Set(const Options &pixel_format){
    this->value_ = pixel_format;
}

PG_FlyCap2_C::Settings::PixelFormat::Options PG_FlyCap2_C::Settings::PixelFormat::Get() const{
    return this->value_;
}

/** \brief Retrieve entry name for pixel format
     *  \retval PG_FLYCAP_SETTINGS_PIXEL_FORMAT   Entry name for flycap settings
     */
std::string PG_FlyCap2_C::Settings::PixelFormat::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_PIXEL_FORMAT";
}

/** \brief Retrieve default entry value for pixel format
     *  \retval MONO8   Default entry
     */
std::string PG_FlyCap2_C::Settings::PixelFormat::GetEntryDefault() const{
    return "MONO8";
}

/** \brief Retrieve entry value for pixel format
     *  \retval MONO8   8-bit monochromatic value set
     *  \retval RGB8    8-bit RGB value set
     *  \retval RAW8    8-bit raw format value set
     *  \retval INVALID An INVALID pixel value set
     */
std::string PG_FlyCap2_C::Settings::PixelFormat::GetEntryValue() const{
    switch (this->value_){

    case Options::PIXEL_FORMAT_RAW8:    return "RAW8";
        break;
    case Options::PIXEL_FORMAT_MONO8:   return "MONO8";
        break;
    case Options::PIXEL_FORMAT_RGB8:    return "RGB8";
        break;
    case Options::INVALID:              return "INVALID";
        break;
    default:                            return "MONO8";
    }
}

/** \brief Set entry value for pixel format*/
void PG_FlyCap2_C::Settings::PixelFormat::SetEntryValue(std::string value){
    if (value.compare("MONO8") == 0){
        this->value_ = Options::PIXEL_FORMAT_MONO8;
    }
    else if (value.compare("RGB8") == 0){
        this->value_ = Options::PIXEL_FORMAT_RGB8;
    }
    else if (value.compare("RAW8") == 0){
        this->value_ = Options::PIXEL_FORMAT_RAW8;
    }
    else
        this->value_ = Options::INVALID;
}







// Image format
/** \brief Default value of Image Format IMAGE_FORMAT_MONO8*/
PG_FlyCap2_C::Settings::ImageFormat::ImageFormat(){
    this->value_ = Options::IMAGE_FORMAT_MONO8;
}

PG_FlyCap2_C::Settings::ImageFormat::ImageFormat(const Options &pixel_format){
    this->value_ = pixel_format;
}

void PG_FlyCap2_C::Settings::ImageFormat::Set(const Options &pixel_format){
    this->value_ = pixel_format;
}

PG_FlyCap2_C::Settings::ImageFormat::Options PG_FlyCap2_C::Settings::ImageFormat::Get() const{
    return this->value_;
}

/** \brief Retrieve entry name for Image format
     *  \retval PG_FLYCAP_SETTINGS_IMAGE_FORMAT   Entry name for flycap settings
     */
std::string PG_FlyCap2_C::Settings::ImageFormat::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_IMAGE_FORMAT";
}

/** \brief Retrieve default entry value for image format
     *  \retval MONO8   Default entry
     */
std::string PG_FlyCap2_C::Settings::ImageFormat::GetEntryDefault() const{
    return "MONO8";
}

/** \brief Retrieve entry value for image format
     *  \retval MONO8   8-bit monochromatic value set
     *  \retval RGB8    8-bit RGB value set
     *  \retval RAW8    8-bit raw format value set
     *  \retval INVALID An INVALID pixel value set
     */
std::string PG_FlyCap2_C::Settings::ImageFormat::GetEntryValue() const{
    switch (this->value_){
    case Options::IMAGE_FORMAT_MONO8:   return "MONO8";
        break;
    case Options::IMAGE_FORMAT_RGB8:    return "RGB8";
        break;
    case Options::INVALID:              return "INVALID";
        break;
    default:                            return "MONO8";
    }
}

/** \brief Set entry value for image format*/
void PG_FlyCap2_C::Settings::ImageFormat::SetEntryValue(std::string value){
    if (value.compare("MONO8") == 0){
        this->value_ = Options::IMAGE_FORMAT_MONO8;
    }
    else if (value.compare("RGB8") == 0){
        this->value_ = Options::IMAGE_FORMAT_RGB8;
    }
    else
        this->value_ = Options::INVALID;
}



//Trigger Polarity
PG_FlyCap2_C::Settings::TriggerPolarity::TriggerPolarity(){
    this->value_ = Options::FALLING_EDGE;
}

PG_FlyCap2_C::Settings::TriggerPolarity::TriggerPolarity(const Options &trigger_polarity){
    this->value_ = trigger_polarity;
}

void PG_FlyCap2_C::Settings::TriggerPolarity::Set(const Options &trigger_polarity){
    this->value_ = trigger_polarity;
}

PG_FlyCap2_C::Settings::TriggerPolarity::Options PG_FlyCap2_C::Settings::TriggerPolarity::Get() const{
    return this->value_;
}

/** \brief Retrieve entry name for trigger polarity
     *  \retval pg_flycap_SETTINGS_TRIGGER_POLARITY   Entry name for flycap settings
     */
std::string PG_FlyCap2_C::Settings::TriggerPolarity::GetEntryName() const{
    return "pg_flycap_SETTINGS_TRIGGER_POLARITY";
}

/** \brief  Retrieve default entry value for trigger polarity
     *  \retval FALLING_EDGE   Default entry
     */
std::string PG_FlyCap2_C::Settings::TriggerPolarity::GetEntryDefault() const{
    return "FALLING_EDGE";
}

/** \brief Retrieve entry value for trigger polarity
     *  \retval FALLING_EDGE    Falling edge trigger polarity selected
     *  \retval RISING_EDGE     Rising edge trigger polarity selected
     *  \retval INVALID         An INVALID trigger polarity selected
     */
std::string PG_FlyCap2_C::Settings::TriggerPolarity::GetEntryValue() const{
    switch (this->value_){
    case Options::FALLING_EDGE: return "FALLING_EDGE";
    case Options::RISING_EDGE:  return "RISING_EDGE";
    case Options::INVALID:      return "INVALID";
    default:                    return "FALLING_EDGE";
    }
}

/** \brief Set entry value for trigger polarity*/
void PG_FlyCap2_C::Settings::TriggerPolarity::SetEntryValue(std::string value){
    if (value.compare("FALLING_EDGE") == 0){
        this->value_ = Options::FALLING_EDGE;
    }
    else if (value.compare("RISING_EDGE") == 0){
        this->value_ = Options::RISING_EDGE;
    }
    else
        this->value_ = Options::INVALID;
}

//Trigger Enable
PG_FlyCap2_C::Settings::TriggerEnable::TriggerEnable(){
    this->value_ = false;
}

PG_FlyCap2_C::Settings::TriggerEnable::TriggerEnable(const bool &trigger_enable){
    this->value_ = trigger_enable;
}

void PG_FlyCap2_C::Settings::TriggerEnable::Set(const bool &trigger_enable){
    this->value_ = trigger_enable;
}

bool PG_FlyCap2_C::Settings::TriggerEnable::Get() const{
    return this->value_;
}

/** \brief Retrieve entry name for trigger enable
     *  \retval pg_flycap_SETTINGS_TRIGGER_ENABLE   Entry name for flycap settings
     */
std::string PG_FlyCap2_C::Settings::TriggerEnable::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_TRIGGER_ENABLE";

}

/** \brief Retrieve entry value for trigger enable */
std::string PG_FlyCap2_C::Settings::TriggerEnable::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Retrieve default entry value for trigger enable
     *  \retval false   Default entry
     */
std::string PG_FlyCap2_C::Settings::TriggerEnable::GetEntryDefault() const{
    return "false";
}

void PG_FlyCap2_C::Settings::TriggerEnable::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<bool>(value);
}




// Strobe control

// Strobe Source

PG_FlyCap2_C::Settings::StrobeSource::StrobeSource(){
    this->value_ = Options::GPIO_2;
}

PG_FlyCap2_C::Settings::StrobeSource::StrobeSource(const Options &strobe_source){
    this->value_ = strobe_source;
}

void PG_FlyCap2_C::Settings::StrobeSource::Set(const Options &strobe_source){
    this->value_ = strobe_source;
}

PG_FlyCap2_C::Settings::StrobeSource::Options PG_FlyCap2_C::Settings::StrobeSource::Get() const{
    return this->value_;
}

/** \brief Retrieve entry name for strobe source
     *  \retval PG_FLYCAP_SETTINGS_STROBE_SOURCE   Entry name for flycap settings
     */
std::string PG_FlyCap2_C::Settings::StrobeSource::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_STROBE_SOURCE";
}

/** \brief Retrieve default entry value for strobe source
     *  \retval GPIO_2   Default entry
     */
std::string PG_FlyCap2_C::Settings::StrobeSource::GetEntryDefault() const{
    return "GPIO_2";
}

/** \brief Retrieve entry value for strobe source
     *  \retval GPIO_1      GPIO source 1 selected
     *  \retval GPIO_2      GPIO source 2 selected
     *  \retval GPIO_3      GPIO source 3 selected
     *  \retval INVALID     An INVALID strobe source value set
     */
std::string PG_FlyCap2_C::Settings::StrobeSource::GetEntryValue() const{
    switch (this->value_){
    case Options::GPIO_1: return "GPIO_1";
        break;
    case Options::GPIO_2: return "GPIO_2";
        break;
    case Options::GPIO_3: return "GPIO_3";
        break;
    case Options::INVALID: return "INVALID";
        break;
    default:				return "GPIO_2";
    }
}

/** \brief Set entry value for strobe source*/
void PG_FlyCap2_C::Settings::StrobeSource::SetEntryValue(std::string value){
    if (value.compare("GPIO_1") == 0){
        this->value_ = Options::GPIO_1;
    }
    else if (value.compare("GPIO_2") == 0){
        this->value_ = Options::GPIO_2;
    }
    else if (value.compare("GPIO_3") == 0){
        this->value_ = Options::GPIO_3;
    }
    else
        this->value_ = Options::INVALID;
}


//Strobe Polarity
PG_FlyCap2_C::Settings::StrobePolarity::StrobePolarity(){
    this->value_ = Options::HIGH;
}

PG_FlyCap2_C::Settings::StrobePolarity::StrobePolarity(const Options &strobe_polarity){
    this->value_ = strobe_polarity;
}

void PG_FlyCap2_C::Settings::StrobePolarity::Set(const Options &strobe_polarity){
    this->value_ = strobe_polarity;
}

PG_FlyCap2_C::Settings::StrobePolarity::Options PG_FlyCap2_C::Settings::StrobePolarity::Get() const{
    return this->value_;
}

/** \brief Retrieve entry name for strobe polarity
     *  \retval PG_FLYCAP_SETTINGS_STROBE_POLARITY   Entry name for flycap settings
     */
std::string PG_FlyCap2_C::Settings::StrobePolarity::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_STROBE_POLARITY";
}

/** \brief Retrieve default entry value for strobe polarity
     *  \retval HIGH   Default entry
     */
std::string PG_FlyCap2_C::Settings::StrobePolarity::GetEntryDefault() const{
    return "HIGH";
}

/** \brief Retrieve entry value for strobe polarity
     *  \retval LOW     Low strobe polarity set
     *  \retval HIGH    High strobe polarity set
     *  \retval INVALID An INVALID strobe polarity value set
     */
std::string PG_FlyCap2_C::Settings::StrobePolarity::GetEntryValue() const{
    switch (this->value_){
    case Options::LOW:      return "LOW";
        break;
    case Options::HIGH:     return "HIGH";
        break;
    case Options::INVALID:  return "INVALID";
        break;
    default:				return "HIGH";
    }
}

/** \brief Set entry value for strobe polarity*/
void PG_FlyCap2_C::Settings::StrobePolarity::SetEntryValue(std::string value){
    if (value.compare("LOW") == 0){
        this->value_ = Options::LOW;
    }
    else if (value.compare("HIGH") == 0){
        this->value_ = Options::HIGH;
    }
    else
        this->value_ = Options::INVALID;
}

//Strobe Enable
PG_FlyCap2_C::Settings::StrobeEnable::StrobeEnable(){
    this->value_ = false;
}

PG_FlyCap2_C::Settings::StrobeEnable::StrobeEnable(const bool &strobe_enable){
    this->value_ = strobe_enable;
}

void PG_FlyCap2_C::Settings::StrobeEnable::Set(const bool &strobe_enable){
    this->value_ = strobe_enable;
}

bool PG_FlyCap2_C::Settings::StrobeEnable::Get() const{
    return this->value_;
}

/** \brief Retrieve entry name for strobe enable
     *  \retval PG_FLYCAP_SETTINGS_STROBE_ENABLE   Entry name for flycap settings
     */
std::string PG_FlyCap2_C::Settings::StrobeEnable::GetEntryName() const{
    return "PG_FLYCAP_SETTINGS_STROBE_ENABLE";

}

/** \brief Retrieve entry value for strobe enable */
std::string PG_FlyCap2_C::Settings::StrobeEnable::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Retrieve default entry value for strobe enable
     *  \retval false   Default entry, strobe disabled
     */
std::string PG_FlyCap2_C::Settings::StrobeEnable::GetEntryDefault() const{
    return Number::ToString(false);
}

/** \brief Set entry value for strobe enable*/
void PG_FlyCap2_C::Settings::StrobeEnable::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<bool>(value);
}
}








