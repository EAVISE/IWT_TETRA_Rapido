/** \file   pattern_settings.cpp
 *  \brief  Contains \ref dlp::Pattern::Sequence methods and objects
 */

#include <string>
#include <vector>
#include <iostream>

#include <common/debug.hpp>
#include <common/image/image.hpp>
#include <common/other.hpp>
#include <common/parameters.hpp>
#include <common/pattern/pattern.hpp>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief Default Constructor, format INVALID*/
Pattern::Settings::Bitdepth::Bitdepth(){
    this->value_ = Format::INVALID;
}

/** \brief Format as in input constructor
 * \param[in] bitdepth sets internal depth to that of the \ref Format input*/
Pattern::Settings::Bitdepth::Bitdepth(const Format &bitdepth){
    this->value_ = bitdepth;
}

/** \brief Set bit depth
 * \param[in] bitdepth sets internal depth to that of the \ref Format input*/
void Pattern::Settings::Bitdepth::Set(const Format &bitdepth){
    this->value_ = bitdepth;
}

/** \brief Returns \ref BitDepth */
Pattern::Settings::Bitdepth::Format Pattern::Settings::Bitdepth::Get() const{
    return this->value_;
}

/** \brief Returns Entry Name "PATTERN_SETTINGS_BITDEPTH"
 *  \retval PATTERN_SETTINGS_BITDEPTH
 */
std::string Pattern::Settings::Bitdepth::GetEntryName() const{
    return "PATTERN_SETTINGS_BITDEPTH";
}

/** \brief Returns the bit depth as a string */
std::string Pattern::Settings::Bitdepth::GetEntryValue() const{
    switch(this->value_){
    case Format::MONO_1BPP:   return "MONO_1BPP";
    case Format::MONO_2BPP:   return "MONO_2BPP";
    case Format::MONO_3BPP:   return "MONO_3BPP";
    case Format::MONO_4BPP:   return "MONO_4BPP";
    case Format::MONO_5BPP:   return "MONO_5BPP";
    case Format::MONO_6BPP:   return "MONO_6BPP";
    case Format::MONO_7BPP:   return "MONO_7BPP";
    case Format::MONO_8BPP:   return "MONO_8BPP";
    case Format::RGB_3BPP:    return "RGB_3BPP";
    case Format::RGB_6BPP:    return "RGB_6BPP";
    case Format::RGB_9BPP:    return "RGB_9BPP";
    case Format::RGB_12BPP:   return "RGB_12BPP";
    case Format::RGB_15BPP:   return "RGB_15BPP";
    case Format::RGB_18BPP:   return "RGB_18BPP";
    case Format::RGB_21BPP:   return "RGB_21BPP";
    case Format::RGB_24BPP:   return "RGB_24BPP";
    case Format::INVALID:     break;
    }
    return "INVALID";
}

/** \brief Returns default entry value of "INVALID" as a string
 *  \retval INVALID
 */
std::string Pattern::Settings::Bitdepth::GetEntryDefault() const{
    return "INVALID";
}

/** \brief Sets the bit depth based on string input
 * \param[in] value String determining bit depth*/
void Pattern::Settings::Bitdepth::SetEntryValue(std::string value){
    if(value.compare("MONO_1BPP") == 0){
        this->value_ = Format::MONO_1BPP;
    }
    else if(value.compare("MONO_2BPP") == 0){
        this->value_ = Format::MONO_2BPP;
    }
    else if(value.compare("MONO_3BPP") == 0){
        this->value_ = Format::MONO_3BPP;
    }
    else if(value.compare("MONO_4BPP") == 0){
        this->value_ = Format::MONO_4BPP;
    }
    else if(value.compare("MONO_5BPP") == 0){
        this->value_ = Format::MONO_5BPP;
    }
    else if(value.compare("MONO_6BPP") == 0){
        this->value_ = Format::MONO_6BPP;
    }
    else if(value.compare("MONO_7BPP") == 0){
        this->value_ = Format::MONO_7BPP;
    }
    else if(value.compare("MONO_8BPP") == 0){
        this->value_ = Format::MONO_8BPP;
    }
    else if(value.compare("RGB_3BPP") == 0){
        this->value_ = Format::RGB_3BPP;
    }
    else if(value.compare("RGB_6BPP") == 0){
        this->value_ = Format::RGB_6BPP;
    }
    else if(value.compare("RGB_9BPP") == 0){
        this->value_ = Format::RGB_9BPP;
    }
    else if(value.compare("RGB_12BPP") == 0){
        this->value_ = Format::RGB_12BPP;
    }
    else if(value.compare("RGB_15BPP") == 0){
        this->value_ = Format::RGB_15BPP;
    }
    else if(value.compare("RGB_18BPP") == 0){
        this->value_ = Format::RGB_18BPP;
    }
    else if(value.compare("RGB_21BPP") == 0){
        this->value_ = Format::RGB_21BPP;
    }
    else if(value.compare("RGB_24BPP") == 0){
        this->value_ = Format::RGB_24BPP;
    }
    else{
        this->value_ = Format::INVALID;
    }
}

/** \brief Default color constructor, invalid*/
Pattern::Settings::Color::Color(){
    this->value_ = Type::INVALID;
}

/** \brief \ref Type color constructor
 * \param[in] color The \ref Type color*/
Pattern::Settings::Color::Color(const Type &color){
    this->value_ = color;
}

/** \brief \ref Type set the color
 * \param[in] color The \ref Type color to set to*/
void Pattern::Settings::Color::Set(const Type &color){
    this->value_ = color;
}

/** \brief Get color */
Pattern::Settings::Color::Type Pattern::Settings::Color::Get() const{
    return this->value_;
}

/** \brief Return entry name as string "PATTERN_SETTINGS_COLOR"
 *  \retval PATTERN_SETTINGS_COLOR
 */
std::string Pattern::Settings::Color::GetEntryName() const{
    return "PATTERN_SETTINGS_COLOR";
}

/** \brief Return Color as a string*/
std::string Pattern::Settings::Color::GetEntryValue() const{
    switch(this->value_){
    case Type::NONE:    return "NONE";
    case Type::RED:     return "RED";
    case Type::GREEN:   return "GREEN";
    case Type::BLUE:    return "BLUE";
    case Type::CYAN:    return "CYAN";
    case Type::YELLOW:  return "YELLOW";
    case Type::MAGENTA: return "MAGENTA";
    case Type::WHITE:   return "WHITE";
    case Type::BLACK:   return "BLACK";
    case Type::RGB:     return "RGB";
    case Type::INVALID: break;
    }
    return "INVALID";
}

/** \brief Return default value, "INVALID"*/
std::string Pattern::Settings::Color::GetEntryDefault() const{
    return "INVALID";
}

/** \brief Set Entry value based on string input
 * \param[in] value String that sets the color*/
void Pattern::Settings::Color::SetEntryValue(std::string value){
    if(value.compare("NONE") == 0){
        this->value_ = Type::NONE;
    }
    else if(value.compare("RED") == 0){
        this->value_ = Type::RED;
    }
    else if(value.compare("GREEN") == 0){
        this->value_ = Type::GREEN;
    }
    else if(value.compare("BLUE") == 0){
        this->value_ = Type::BLUE;
    }
    else if(value.compare("CYAN") == 0){
        this->value_ = Type::CYAN;
    }
    else if(value.compare("YELLOW") == 0){
        this->value_ = Type::YELLOW;
    }
    else if(value.compare("MAGENTA") == 0){
        this->value_ = Type::MAGENTA;
    }
    else if(value.compare("WHITE") == 0){
        this->value_ = Type::WHITE;
    }
    else if(value.compare("BLACK") == 0){
        this->value_ = Type::BLACK;
    }
    else if(value.compare("RGB") == 0){
        this->value_ = Type::RGB;
    }
    else{
        this->value_ = Type::INVALID;
    }
}

/** \brief Default constructor sets data type to INVALID*/
Pattern::Settings::Data::Data(){
    this->value_ = Type::INVALID;
}

/** \brief type constructor, sets value to the \ref Type specified
 * \param[in] type The type to construct with*/
Pattern::Settings::Data::Data(const Type &type){
    this->value_ = type;
}

/** \brief Set type
 * \param[in] the type to set to*/
void Pattern::Settings::Data::Set(const Type &type){
    this->value_ = type;
}

/** \brief returns the type*/
Pattern::Settings::Data::Type Pattern::Settings::Data::Get() const{
    return this->value_;
}

/** \brief returns entry name "PATTERN_SETTINGS_DATA_TYPE"
 *  \retval PATTERN_SETTINGS_DATA_TYPE
 */
std::string Pattern::Settings::Data::GetEntryName() const{
    return "PATTERN_SETTINGS_DATA_TYPE";
}

/** \brief returns entry value as a string*/
std::string Pattern::Settings::Data::GetEntryValue() const{
    switch(this->value_){
    case Type::IMAGE_DATA:  return "IMAGE_DATA";
    case Type::IMAGE_FILE:  return "IMAGE_FILE";
    case Type::PARAMETERS:  return "PARAMETERS";
    case Type::INVALID:     break;
    }
    return "INVALID";
}

/** \brief returns default entry string "INVALID"
 *  \retval INVALID
 */
std::string Pattern::Settings::Data::GetEntryDefault() const{
    return "INVALID";
}

/** \brief set entry value from string
 * \param[in] value String input to set entry*/
void Pattern::Settings::Data::SetEntryValue(std::string value){
    if(value.compare("IMAGE_DATA") == 0){
        this->value_ = Type::IMAGE_DATA;
    }
    else if(value.compare("IMAGE_FILE") == 0){
        this->value_ = Type::IMAGE_FILE;
    }
    else if(value.compare("PARAMETERS") == 0){
        this->value_ = Type::PARAMETERS;
    }
    else{
        this->value_ = Type::INVALID;
    }
}
}

