/** \file   pattern.cpp
 *  \brief  Contains dlp::Pattern methods
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#include <string>
#include <vector>
#include <iostream>

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/other.hpp>
#include <common/parameters.hpp>
#include <common/pattern/pattern.hpp>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief  Object constructor */
Pattern::Pattern(){
    this->id       = 0;
    this->exposure = 0;
    this->period   = 0;
    this->bitdepth.Set(Settings::Bitdepth::Format::INVALID);
    this->color.Set(Settings::Color::Type::INVALID);
    this->type.Set(Settings::Data::Type::INVALID);
    this->image_file = "";
    this->image_data.Clear();
    this->parameters.Clear();
}

/** \brief  Destroys object and releases image memory */
Pattern::~Pattern(){
    image_data.Clear();
    parameters.Clear();
}

/** \brief  Constructs and initializes object with data copied from supplied Pattern */
Pattern::Pattern(const Pattern &pattern){
    this->id       = pattern.id;
    this->exposure = pattern.exposure;
    this->period   = pattern.period;
    this->bitdepth.Set(pattern.bitdepth.Get());
    this->color.Set(pattern.color.Get());
    this->type.Set(pattern.type.Get());
    this->image_file = pattern.image_file;
    this->image_data.Clear();
    this->image_data.Create(pattern.image_data);
    this->parameters = pattern.parameters;
}

/** Copies all data (deep) from supplied Pattern */
Pattern& Pattern::operator=(const Pattern& pattern){
    this->id       = pattern.id;
    this->exposure = pattern.exposure;
    this->period   = pattern.period;
    this->bitdepth.Set(pattern.bitdepth.Get());
    this->color.Set(pattern.color.Get());
    this->type.Set(pattern.type.Get());
    this->image_file = pattern.image_file;
    this->image_data.Clear();
    this->image_data.Create(pattern.image_data);
    this->parameters = pattern.parameters;
    return *this;
}

}

