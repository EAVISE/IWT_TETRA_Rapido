/** \file   point_cloud.cpp
 *  \brief  Contains methods for accessing, modifying, and storing point
 *          data and point clouds.
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/point_cloud.hpp>

#include <string>
#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <pcl/io/ply_io.h>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief  Constructs point at 0,0,0 */
Point::Point(){
    this->x = 0.0;
    this->y = 0.0;
    this->z = 0.0;
}

/** \brief  Constructs point at supplied x, y, z location */
Point::Point(const Point::PointType &x_in, const PointType &y_in, const Point::PointType &z_in){
    this->x = x_in;
    this->y = y_in;
    this->z = z_in;
}

/** \brief  Constructs empty point cloud*/
Point::Cloud::Cloud(){
    this->Clear();
}

/** \brief Deallocates memory */
Point::Cloud::~Cloud(){
    this->Clear();
}

/** \brief Deallocates memory */
void Point::Cloud::Clear(){
    this->points_.clear();
}

/** \brief  Adds a point to a point cloud*/
void Point::Cloud::Add(Point new_point){
    this->points_.push_back(new_point);
}

/** \brief  Returns the number of points in a point cloud*/
unsigned long long Point::Cloud::GetCount() const{
    return this->points_.size();
}

/** \brief  Returns a point in a point cloud
 *  \param[in]  index       Point index in the point cloud
 *  \param[out] ret_point   Pointer to return \ref dlp::Point
 *  \retval POINT_CLOUD_EMPTY                   Point cloud has NO data
 *  \retval POINT_CLOUD_INDEX_OUT_OF_RANGE      Requested point does NOT exist
 *  \retval POINT_CLOUD_NULL_POINTER_ARGUMENT   Return argument NULL
 */
ReturnCode Point::Cloud::Get(unsigned long long index, Point *ret_point) const{
    ReturnCode ret;

    if(this->GetCount() == 0)
        return ret.AddError(POINT_CLOUD_EMPTY);

    if(index >= this->GetCount())
        return ret.AddError(POINT_CLOUD_INDEX_OUT_OF_RANGE);

    if(!ret_point)
        return ret.AddError(POINT_CLOUD_NULL_POINTER_ARGUMENT);

    (*ret_point) = this->points_.at(index);

    return ret;
}

/** \brief     Removes a point in a point cloud
 *  \param[in] index    Point index in the point cloud
 *  \retval POINT_CLOUD_EMPTY                   Point cloud has NO data
 *  \retval POINT_CLOUD_INDEX_OUT_OF_RANGE      Requested point does NOT exist
 */
ReturnCode Point::Cloud::Remove(unsigned long long index){
    ReturnCode ret;

    if(this->GetCount() == 0)
        return ret.AddError(POINT_CLOUD_EMPTY);

    if(index >= this->GetCount())
        return ret.AddError(POINT_CLOUD_INDEX_OUT_OF_RANGE);

    this->points_.erase(this->points_.begin() + index);

    return ret;
}

/** \brief  Saves point cloud data to a ply file
 *  \param[in] filename     Output file name
 *  \retval POINT_CLOUD_EMPTY               Point cloud has NO data
 */

ReturnCode Point::Cloud::SavePLY(const std::string &filename){
    ReturnCode ret;
    std::ofstream myfile;

    // Check that filename is not empty
    if(filename.empty())
        return ret.AddError(POINT_CLOUD_FILENAME_EMPTY);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

    for(unsigned long long i = 0; i < this->points_.size();i++){
        dlp::Point dlp_point = this->points_.at(i);


        float distance = 0;
        pcl::PointXYZ point;
        point.x = dlp_point.x;
        point.y = dlp_point.y;
        point.z = dlp_point.z;

        cloud->points.push_back(point);


    }

    pcl::PLYWriter plywriter;
    plywriter.write(filename, *cloud, false);

    return ret;

}

/** \brief  Saves point cloud data to a file and separates x, y,
 *          and z with the supplied delimiter
 *  \param[in] filename     Output file name
 *  \param[in] delimiter    Character to separate x, y, and z
 *  \retval POINT_CLOUD_EMPTY               Point cloud has NO data
 *  \retval POINT_CLOUD_FILE_SAVE_FAILED    Could NOT save file
 */
ReturnCode Point::Cloud::SaveXYZ(const std::string &filename, const unsigned char &delimiter){
    ReturnCode ret;
    std::ofstream myfile;

    // Check that filename is not empty
    if(filename.empty())
        return ret.AddError(POINT_CLOUD_FILENAME_EMPTY);

    // Open file
    myfile.open(filename);

    // Check that file opened
    if(!myfile.is_open())
        return ret.AddError(POINT_CLOUD_FILE_SAVE_FAILED);

    for(unsigned long long i = 0; i < this->points_.size();i++){
        dlp::Point point = this->points_.at(i);
        myfile << point.x << delimiter << point.y << delimiter << point.z  << "\n";
    }

    //Close file
    myfile.close();

    return ret;
}


}
