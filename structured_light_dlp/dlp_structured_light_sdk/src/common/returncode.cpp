/** \file   returncode.cpp
 *  \brief  Contains methods for \ref dlp::ReturnCode
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#include <common/returncode.hpp>
#include <common/other.hpp>

#include <vector>
#include <string>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief  Adds error message to object and returns itself */
ReturnCode& ReturnCode::AddError(const std::string &msg ){
    if(!msg.empty()){
        this->errors_.push_back(msg);
    }
    return *this;
}

/** \brief  Adds warning message to object and returns itself */
ReturnCode& ReturnCode::AddWarning(const std::string &msg ){
    if(!msg.empty()){
        this->warnings_.push_back(msg);
    }
    return *this;
}



/** \brief  Adds all error and warning messages from source \ref dlp::ReturnCode
 *          and returns itself
 */
ReturnCode& ReturnCode::Add(const ReturnCode &code){
    if(this != &code){
        for(unsigned int iError = 0; iError < code.errors_.size();iError++){
            this->errors_.push_back(code.errors_.at(iError));
        }
        for(unsigned int iWarning = 0; iWarning < code.warnings_.size();iWarning++){
            this->warnings_.push_back(code.warnings_.at(iWarning));
        }
    }
    return *this;
}

/** \brief Returns true if object has errors */
bool ReturnCode::hasErrors(){
    if(this->errors_.size() > 0)    return true;
    else                            return false;
}

/** \brief Returns true if object has warnings */
bool ReturnCode::hasWarnings(){
    if(this->warnings_.size() > 0)    return true;
    else                            return false;
}

/** \brief Returns true if object has the exact supplied string as error */
bool ReturnCode::ContainsError( std::string msg){
    bool contains_error = false;
    for(unsigned int iError = 0; (iError < this->errors_.size()) && (!contains_error); iError++){
        if(msg.compare(this->errors_.at(iError)) == 0) contains_error = true;
    }
    return contains_error;
}

/** \brief Returns true if object has the exact supplied string as warning */
bool ReturnCode::ContainsWarning( std::string msg){
    bool contains_warning = false;
    for(unsigned int iWarning = 0; (iWarning < this->warnings_.size()) && (!contains_warning); iWarning++){
        if(msg.compare(this->warnings_.at(iWarning)) == 0) contains_warning = true;
    }
    return contains_warning;
}

/** \brief Returns string vector of all object errors */
std::vector<std::string> ReturnCode::GetErrors(){
    return this->errors_;
}

/** \brief Returns string vector of all object warnings */
std::vector<std::string> ReturnCode::GetWarnings(){
    return this->warnings_;
}

/** \brief Returns number of errors */
unsigned int ReturnCode::GetErrorCount(){
    return this->errors_.size();
}

/** \brief Returns number of warnings */
unsigned int ReturnCode::GetWarningCount(){
    return this->warnings_.size();
}

/** \brief Returns multiline string where the first line lists the quantity of
 *         errors and warnings while all following lines list the individual
 *         errors and warnings
 */
std::string ReturnCode::ToString(){
    std::string ret;

    unsigned int errors   = this->GetErrorCount();
    unsigned int warnings = this->GetWarningCount();

    ret = "ERRORS: " + dlp::Number::ToString(errors) + " and WARNINGS: " + dlp::Number::ToString(warnings);

    if(errors > 0){
        for(unsigned int iError = 0; iError < errors; iError++){
            ret += "\nERROR: " + this->errors_.at(iError);
        }
    }

    if(warnings > 0){
        for(unsigned int iWarning = 0; iWarning < warnings; iWarning++){
            ret += "\nWARNING: " + this->errors_.at(iWarning);
        }
    }

    return ret;
}

}
