/** \file       pattern.hpp
 *  \ingroup    Common
 *  \brief      Contains \ref dlp::Pattern and \ref dlp::Pattern::Sequence
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#ifndef DLP_SDK_PATTERN_HPP
#define DLP_SDK_PATTERN_HPP

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/other.hpp>
#include <common/parameters.hpp>

#include <string>
#include <vector>
#include <iostream>

#define PATTERN_BITDEPTH_INVALID        "PATTERN_BITDEPTH_INVALID"
#define PATTERN_COLOR_INVALID           "PATTERN_COLOR_INVALID"
#define PATTERN_DATA_TYPE_INVALID       "PATTERN_DATA_TYPE_INVALID"
#define PATTERN_EXPOSURE_INVALID        "PATTERN_EXPOSURE_INVALID"
#define PATTERN_EXPOSURE_TOO_SHORT      "PATTERN_EXPOSURE_TOO_SHORT"
#define PATTERN_EXPOSURE_TOO_LONG       "PATTERN_EXPOSURE_TOO_LONG"
#define PATTERN_PERIOD_TOO_SHORT        "PATTERN_PERIOD_TOO_SHORT"
#define PATTERN_PERIOD_TOO_LONG         "PATTERN_PERIOD_TOO_LONG"
#define PATTERN_PARAMETERS_EMPTY        "PATTERN_PARAMETERS_EMPTY"
#define PATTERN_IMAGE_DATA_EMPTY        "PATTERN_IMAGE_DATA_EMPTY"
#define PATTERN_IMAGE_FILE_EMPTY        "PATTERN_IMAGE_FILE_EMPTY"

#define PATTERN_SEQUENCE_EMPTY                      "PATTERN_SEQUENCE_EMPTY"
#define PATTERN_SEQUENCE_TOO_LONG                   "PATTERN_SEQUENCE_TOO_LONG"
#define PATTERN_SEQUENCE_INDEX_OUT_OF_RANGE         "PATTERN_SEQUENCE_INDEX_OUT_OF_RANGE"
#define PATTERN_SEQUENCE_EXPOSURES_NOT_EQUAL        "PATTERN_SEQUENCE_EXPOSURES_NOT_EQUAL"
#define PATTERN_SEQUENCE_PERIODS_NOT_EQUAL          "PATTERN_SEQUENCE_PERIODS_NOT_EQUAL"
#define PATTERN_SEQUENCE_PATTERN_TYPES_NOT_EQUAL    "PATTERN_SEQUENCE_bPATTERN_TYPES_NOT_EQUAL"
#define PATTERN_SEQUENCE_NULL_POINTER_ARGUMENT      "PATTERN_SEQUENCE_NULL_POINTER_ARGUMENT"


/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{


/** \class      Pattern
 *  \ingroup    Common
 *  \brief      Container class for \ref dlp::Image data, image file name, or
 *              \ref dlp::Parameters transfer between \ref dlp::DLP_Platform,
 *              and \ref dlp::StructuredLight classes.
 */
class Pattern{
public:

    /** \class      Settings
     *  \brief      Contains all \ref dlp::Parameters::Entry objects needed for Pattern objects.
     */
    class Settings{
    public:

        /** \class Bitdepth
         *  \brief This object sets the pattern bit depth (i.e. pixel value range)
         */
        class Bitdepth: public dlp::Parameters::Entry{
        public:
            enum class Format{
                MONO_1BPP,      /**< Monochrome pattern with pixel values from 0 to 1 (also known as a binary pattern) */
                MONO_2BPP,      /**< Monochrome pattern with pixel values from 0 to 3   */
                MONO_3BPP,      /**< Monochrome pattern with pixel values from 0 to 7   */
                MONO_4BPP,      /**< Monochrome pattern with pixel values from 0 to 15  */
                MONO_5BPP,      /**< Monochrome pattern with pixel values from 0 to 31  */
                MONO_6BPP,      /**< Monochrome pattern with pixel values from 0 to 63  */
                MONO_7BPP,      /**< Monochrome pattern with pixel values from 0 to 127 */
                MONO_8BPP,      /**< Monochrome pattern with pixel values from 0 to 255 */
                RGB_3BPP,       /**< Color pattern created from three sequential MONO_1BPP patterns (red, green, and blue) */
                RGB_6BPP,       /**< Color pattern created from three sequential MONO_2BPP patterns (red, green, and blue) */
                RGB_9BPP,       /**< Color pattern created from three sequential MONO_3BPP patterns (red, green, and blue) */
                RGB_12BPP,      /**< Color pattern created from three sequential MONO_4BPP patterns (red, green, and blue) */
                RGB_15BPP,      /**< Color pattern created from three sequential MONO_5BPP patterns (red, green, and blue) */
                RGB_18BPP,      /**< Color pattern created from three sequential MONO_6BPP patterns (red, green, and blue) */
                RGB_21BPP,      /**< Color pattern created from three sequential MONO_7BPP patterns (red, green, and blue) */
                RGB_24BPP,      /**< Color pattern created from three sequential MONO_8BPP patterns (red, green, and blue) */
                INVALID
            };
            Bitdepth();
            Bitdepth(const Format &bitdepth);
            void Set(const Format &bitdepth);
            Format Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            Format value_;
        };

        /** \class Color
         *  \brief This object sets which color LEDs are used for the pattern
         */
        class Color: public dlp::Parameters::Entry{
        public:
            enum class Type{
                NONE,           /**< No LED on      */
                BLACK,          /**< No LED on      */
                RED,            /**< Red LED on     */
                GREEN,          /**< Green LED on   */
                BLUE,           /**< Blue LED on    */
                CYAN,           /**< Green and Blue LEDs on simultaneously      */
                YELLOW,         /**< Red and Green LEDs on simultaneously       */
                MAGENTA,        /**< Red and Blue LEDs on simultaneously        */
                WHITE,          /**< Red, Green and Blue LEDs on simultaneously */
                RGB,            /**< Red, Green and Blue LEDs on sequentially   */
                INVALID
            };
            Color();
            Color(const Type &color);
            void Set(const Type &color);
            Type Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            Type value_;
        };

        /** \class Data
         *  \brief Dictates if the Pattern uses image_data, image_file, or
         *         parameters are used
         */
        class Data: public dlp::Parameters::Entry{
        public:
            enum class Type{
                IMAGE_FILE,     /**< Pattern data stored in \ref dlp::Image with \ref image_data */
                IMAGE_DATA,     /**< Pattern data stored in image file with \ref image_file      */
                PARAMETERS,     /**< Pattern information stored in \ref parameters               */
                INVALID
            };
            Data();
            Data(const Type &type);
            void Set(const Type &type);
            Type Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            Type value_;
        };
    };


    /** \class      Sequence
     *  \ingroup    Common
     *  \brief      Container class used to group multiple \ref dlp::Pattern objects
     */
    class Sequence{
    public:
        Sequence();
        ~Sequence();
        Sequence(const Pattern &pattern);
        Sequence(const Sequence &pattern_seq);
        Sequence& operator=(const Sequence& pattern_seq);

        unsigned int GetCount() const;
        void Clear();

        ReturnCode Add( const Pattern &new_pattern);
        void Add( const Sequence &sequence);

        ReturnCode Get(const unsigned int &index, Pattern* ret_pattern) const;
        ReturnCode Set(      const unsigned int &index, Pattern &arg_pattern);
        ReturnCode Remove(   const unsigned int &index);

        ReturnCode SetBitDepths(const dlp::Pattern::Settings::Bitdepth::Format &bitdepth);
        ReturnCode SetColors(const dlp::Pattern::Settings::Color::Type &color);
        ReturnCode SetExposures(const unsigned int &exposure);
        ReturnCode SetPeriods(const unsigned int &period);

        bool EqualBitDepths() const;
        bool EqualColors() const;
        bool EqualDataTypes() const;
        bool EqualExposures() const;
        bool EqualPeriods() const;

        Parameters parameters;

    private:
        std::vector<dlp::Pattern> patterns_;
    };


    Pattern();
    ~Pattern();
    Pattern(const Pattern &pattern);
    Pattern& operator=(const Pattern& pattern);


    // Pattern information
    int                 id;                 /*!< None required pattern indentifier value                */
    unsigned long long  exposure;           /*!< Exposure time in microseconds (pattern display time)   */
    unsigned long long  period;             /*!< Period time in microseconds (time between patterns)    */
    Settings::Bitdepth  bitdepth;
    Settings::Color     color;
    Settings::Data      type;

    // Pattern data
    dlp::Parameters     parameters;
    dlp::Image          image_data;
    std::string         image_file;

};

}


#endif // DLP_SDK_PATTERN_HPP
