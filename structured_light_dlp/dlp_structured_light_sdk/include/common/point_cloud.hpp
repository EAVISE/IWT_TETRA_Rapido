/** \file   point_cloud.hpp
 *  \brief  Contains definitions for the DLP SDK point cloud class
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#ifndef DLP_SDK_POINTCLOUD_HPP
#define DLP_SDK_POINTCLOUD_HPP

#include <common/debug.hpp>
#include <common/returncode.hpp>

#include <string>
#include <vector>

#define POINT_CLOUD_EMPTY                   "POINT_CLOUD_EMPTY"
#define POINT_CLOUD_INDEX_OUT_OF_RANGE      "POINT_CLOUD_INDEX_OUT_OF_RANGE"
#define POINT_CLOUD_FILE_SAVE_FAILED        "POINT_CLOUD_FILE_SAVE_FAILED"
#define POINT_CLOUD_NULL_POINTER_ARGUMENT   "POINT_CLOUD_NULL_POINTER_ARGUMENT"
#define POINT_CLOUD_FILENAME_EMPTY          "POINT_CLOUD_FILENAME_EMPTY"

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \class  Point
 *  \brief  Stores a point in real space in x, y, z format
 */
class Point{
public:

    /** \class  Cloud
     *  \brief  Stores a collection of points and methods to clear the cloud and
     *          add individual points.
     */
    class Cloud{
    public:

        Cloud();
        ~Cloud();

        void Clear();
        void Add(Point new_point);

        unsigned long long GetCount() const;

        ReturnCode Get(unsigned long long index, Point *ret_point)const;
        ReturnCode Remove(unsigned long long index);

        ReturnCode SavePLY(const std::string &filename);
        ReturnCode SaveXYZ(const std::string &filename, const unsigned char &delimiter);

    private:
        std::vector<Point> points_;
    };
    typedef double PointType;
    Point();
    Point(const PointType &x_in, const Point::PointType &y_in, const PointType &z_in);
    PointType x;
    PointType y;
    PointType z;
};


}


#endif // DLP_SDK_POINTCLOUD_HPP


