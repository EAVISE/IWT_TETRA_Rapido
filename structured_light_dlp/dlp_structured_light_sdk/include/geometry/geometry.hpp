/** \file   geometry.hpp
 *  \brief  Contains definition for the DLP SDK geometry classes
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/capture/capture.hpp>
#include <common/pattern/pattern.hpp>
#include <common/point_cloud.hpp>
#include <common/disparity_map.hpp>
#include <common/module.hpp>
#include <calibration/calibration.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>


#include <vector>
#include <string>

#define GEOMETRY_NO_ORIGIN_SET                      "GEOMETRY_NO_ORIGIN_SET"
#define GEOMETRY_CALIBRATION_NOT_COMPLETE           "GEOMETRY_CALIBRATION_NOT_COMPLETE"
#define GEOMETRY_NULL_POINTER                       "GEOMETRY_NULL_POINTER"
#define GEOMETRY_ORIGIN_RAY_OUT_OF_RANGE            "GEOMETRY_ORIGIN_RAY_OUT_OF_RANGE"
#define GEOMETRY_VIEWPORT_ID_OUT_OF_RANGE           "GEOMETRY_VIEWPORT_ID_OUT_OF_RANGE"
#define GEOMETRY_VIEWPORT_RAY_OUT_OF_RANGE          "GEOMETRY_VIEWPORT_RAY_OUT_OF_RANGE"
#define GEOMETRY_DISPARITY_MAP_RESOLUTION_INVALID   "GEOMETRY_DISPARITY_MAP_RESOLUTION_INVALID"
#define GEOMETRY_SETTINGS_EMPTY                     "GEOMETRY_SETTINGS_EMPTY"
#define GEOMETRY_POINT_CLOUD_EMPTY                  "GEOMETRY_POINT_CLOUD_EMPTY"


/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \class      Geometry
 *  \defgroup   Geometry
 *  \brief      Calculates real world location of points using data from
 *              disparity maps and calibration data
 *
 *  The geometry class calculates points in 3D space (real world) using the
 *  disparity map and calibration data. The geometry class also allows setting the
 *  origin to the projector or the camera.
 *
 */
class Geometry: public dlp::Module{
public:

    /** \class      Settings
     *  \brief      Contains \ref dlp::Parameters::Entry objects needed for Geometry class
     */
    class Settings{
    public:

        /** \class ScaleXYZ
         *  \brief Stores a value to scale the output \ref dlp::Point::Cloud (1 = no scaling)
         */
        class ScaleXYZ: public dlp::Parameters::Entry{
        public:
            ScaleXYZ();
            ScaleXYZ(const double &percent);
            void Set(const double &percent);
            double Get()const;

            std::string GetEntryName()    const;
            std::string GetEntryValue()   const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            double value_;
        };


        /** \class OriginPointDistance
         *  \brief Stores the distance thresholds for the \ref dlp::Point::Cloud output
         *  \note If \ref OriginPointDistance::Maximum and \ref OriginPointDistance::Minimum
         *        are equal no thresholding is applied
         *  \note Distance is calculated from the origin to the point
         */
        class OriginPointDistance{
        public:

            /** \class Maximum
             *  \brief Stores the maximum distance threshold for the \ref dlp::Point::Cloud output
             *  \note  Distance is calculated from the origin to the point
             */
            class Maximum: public dlp::Parameters::Entry{
            public:
                Maximum();
                Maximum(const double &distance);
                void Set(const double &distance);
                double Get()const;

                std::string GetEntryName()    const;
                std::string GetEntryValue()   const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                double value_;
            };


            /** \class Minimum
             *  \brief Stores the minimum distance threshold for the \ref dlp::Point::Cloud output
             *  \note  Distance is calculated from the origin to the point
             */
            class Minimum: public dlp::Parameters::Entry{
            public:
                Minimum();
                Minimum(const double &distance);
                void Set(const double &distance);
                double Get()const;

                std::string GetEntryName()    const;
                std::string GetEntryValue()   const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                double value_;
            };
        };

        class FilterViewpointRays: public dlp::Parameters::Entry{
        public:
            FilterViewpointRays();
            FilterViewpointRays(const bool &enable);
            void Set(const bool &enable);
            bool Get()const;

            std::string GetEntryName()    const;
            std::string GetEntryValue()   const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            bool value_;
        public:
            class MaximumPercentError: public dlp::Parameters::Entry{
            public:
                MaximumPercentError();
                MaximumPercentError(const double &percent);
                void Set(const double &percent);
                double Get()const;

                std::string GetEntryName()    const;
                std::string GetEntryValue()   const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                double value_;
            };
        };
    };


    /** \class ViewPoint
     *  \brief Contains the camera or projector XYZ position in space and its optical rays
     * */
    class ViewPoint{
    public:
        // XYZ position of Object (Single cv::Point3_ object
        cv::Point3d center;

        // Object Rays (List of cv::Point3_ objects)
        cv::Mat ray;
    };

    Geometry();
    ~Geometry();

    ReturnCode Setup(const dlp::Parameters &settings);
    ReturnCode GetSetup(dlp::Parameters *settings) const;

    void Clear();

    ReturnCode SetOriginView(const dlp::Calibration::Data &origin_calib);

    ReturnCode AddView(const dlp::Calibration::Data &viewport_calib,
                                      unsigned int *ret_viewport_id);

    ReturnCode GetNumberOfViews(unsigned int *viewports);

    // Origin_x is the decoded value from the vertical patterns
    // Origin_y is the decoded value from the horizontal patterns
    // Viewport x & y are the camera pixel location
    ReturnCode Find3dLineIntersection(const unsigned int &origin_x, const unsigned int &origin_y,
                                     const unsigned int &viewport_id,
                                     const unsigned int &viewport_x, const unsigned int &viewport_y,
                                     Point *ret_xyz, double *ret_distance);
    void Unsafe_Find3dLineIntersection(const unsigned int &origin_x, const unsigned int &origin_y,
                                       const unsigned int &viewport_id,
                                       const unsigned int &viewport_x, const unsigned int &viewport_y,
                                       Point *ret_xyz, double *ret_distance);



    ReturnCode GeneratePointCloud(const unsigned int &viewport_id,
                                 dlp::DisparityMap &column_disparity,
                                 dlp::DisparityMap &row_disparity,
                                 dlp::Point::Cloud *ret_cloud,
                                 Image *ret_depthmap);

    ReturnCode CalculateFlatness(const dlp::Point::Cloud &cloud, double *flatness);

private:
    Settings::FilterViewpointRays                               filter_viewpoint_rays_;
    Settings::FilterViewpointRays::MaximumPercentError          max_percent_error_;


    Settings::ScaleXYZ                      point_scale_percent_;

    Settings::OriginPointDistance::Maximum  max_distance_;
    Settings::OriginPointDistance::Minimum  min_distance_;

    bool                                    origin_set_;
    ViewPoint                               origin_;
    dlp::Calibration::Data                  origin_calibration_;
    std::vector<dlp::Geometry::ViewPoint>   viewport_;

};


}
