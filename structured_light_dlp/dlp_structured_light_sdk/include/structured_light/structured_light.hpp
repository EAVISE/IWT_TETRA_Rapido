/** \file   structured_light.hpp
 *  \brief  Contains definitions for the DLP SDK structured light base classes
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#ifndef DLP_SDK_STRUCTURED_LIGHT_HPP
#define DLP_SDK_STRUCTURED_LIGHT_HPP

#include <common/returncode.hpp>
#include <common/debug.hpp>
#include <common/other.hpp>
#include <common/image/image.hpp>
#include <common/parameters.hpp>
#include <common/capture/capture.hpp>
#include <common/pattern/pattern.hpp>
#include <common/disparity_map.hpp>
#include <common/module.hpp>
#include <camera/camera.hpp>
#include <dlp_platforms/dlp_platform.hpp>

#define STRUCTURED_LIGHT_NOT_SETUP                                      "STRUCTURED_LIGHT_NOT_SETUP"
#define STRUCTURED_LIGHT_PATTERN_SEQUENCE_NULL                          "STRUCTURED_LIGHT_PATTERN_SEQUENCE_NULL"
#define STRUCTURED_LIGHT_CAPTURE_SEQUENCE_EMPTY                         "STRUCTURED_LIGHT_CAPTURE_SEQUENCE_EMPTY"
#define STRUCTURED_LIGHT_CAPTURE_SEQUENCE_SIZE_INVALID                  "STRUCTURED_LIGHT_CAPTURE_SEQUENCE_SIZE_INVALID"
#define STRUCTURED_LIGHT_CAPTURE_INVALID                                "STRUCTURED_LIGHT_CAPTURE_INVALID"
#define STRUCTURED_LIGHT_PATTERN_SIZE_INVALID                           "STRUCTURED_LIGHT_PATTERN_SIZE_INVALID"
#define STRUCTURED_LIGHT_SETTINGS_PATTERN_ROWS_MISSING                  "STRUCTURED_LIGHT_SETTINGS_PATTERN_ROWS_MISSING"
#define STRUCTURED_LIGHT_SETTINGS_PATTERN_COLUMNS_MISSING               "STRUCTURED_LIGHT_SETTINGS_PATTERN_COLUMNS_MISSING"
#define STRUCTURED_LIGHT_SETTINGS_PATTERN_COLOR_MISSING                 "STRUCTURED_LIGHT_SETTINGS_PATTERN_COLOR_MISSING"
#define STRUCTURED_LIGHT_SETTINGS_PATTERN_ORIENTATION_MISSING           "STRUCTURED_LIGHT_SETTINGS_PATTERN_ORIENTATION_MISSING"
#define STRUCTURED_LIGHT_SETTINGS_IMAGE_ROWS_MISSING                    "STRUCTURED_LIGHT_SETTINGS_IMAGE_ROWS_MISSING"
#define STRUCTURED_LIGHT_SETTINGS_IMAGE_COLUMNS_MISSING                 "STRUCTURED_LIGHT_SETTINGS_IMAGE_COLUMNS_MISSING"
#define STRUCTURED_LIGHT_SETTINGS_SEQUENCE_INCLUDE_INVERTED_MISSING     "STRUCTURED_LIGHT_SETTINGS_SEQUENCE_INCLUDE_INVERTED_MISSING"
#define STRUCTURED_LIGHT_SETTINGS_SEQUENCE_COUNT_MISSING                "STRUCTURED_LIGHT_SETTINGS_SEQUENCE_COUNT_MISSING"
#define STRUCTURED_LIGHT_NULL_POINTER_ARGUMENT                          "STRUCTURED_LIGHT_NULL_POINTER_ARGUMENT"
#define STRUCTURED_LIGHT_DATA_TYPE_INVALID                              "STRUCTURED_LIGHT_DATA_TYPE_INVALID"

/** \brief Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \class Structured Light
 *  \brief Contains base class for 3D structured light pattern generation and decoding classes
 */
class StructuredLight: public dlp::Module{
public:
    /** \defgroup StructuredLight
     *  \brief Contains base class for 3D structured light pattern generation and decoding classes
     *  @{
     */

    /** \class      Settings
     *  \brief      Contains \ref dlp::Parameters::Entry objects needed for \ref dlp::StructuredLight.
     */
    class Settings{
    public:
        /** \class   Sequence
         *  \brief   Settings to generates and analyze the structured light pattern sequences
         */
        class Sequence{
        public:

            /** \class Count
             *  \brief Determines the number of patterns for a structured light sequence
             */
            class Count : public dlp::Parameters::Entry{
            public:
                typedef unsigned int Type;
                Count();
                Count(const unsigned int &count);
                void Set(const unsigned int &count);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;
            };

            /** \class IncludeInverted
             *  \brief Determines if pattern sequence should incude inverted patterns
             *  \note  If inverted patterns are included then the number of patterns generated is effectively doubled
             */
            class IncludeInverted : public dlp::Parameters::Entry{
            public:
                typedef bool Type;
                IncludeInverted();
                IncludeInverted(bool include_inverted);
                void Set(bool include_inverted);
                bool Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                bool value_;
            };

        };

        /** \class Pattern
         *  \brief Contains information pertaining to the patterns generated for structured light
         */
        class Pattern{
        public:
            typedef dlp::Pattern::Settings::Color Color;

            /** \class Size
             *  \brief Contains information about the resolution of the \ref Pattern generated
             *  \note  \ref dlp::StructuredLight::SetDlpPlatform can be used to set these
             *         values automatically
             */
            class Size{
            public:
                /** \class Rows
                 *  \brief %Number of rows a \ref Pattern object contains (pixels).
                 *  \note   Also known as resolution height
                 */
                class Rows : public dlp::Parameters::Entry{
                public:
                    typedef unsigned int Type;

                    Rows();
                    Rows(const unsigned int &rows);
                    void Set(const unsigned int &rows);
                    Type Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;
                };

                /** \class Columns
                 *  \brief %Number of columns a \ref Pattern object contains (pixels).
                 *  \note   Also known as resolution width
                 */
                class Columns : public dlp::Parameters::Entry{
                public:
                    typedef unsigned int Type;
                    Columns();
                    Columns(const unsigned int &columns);
                    void Set(const unsigned int &columns);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;
                };
            };

            /** \class Orientation
             *  \brief Determines if the sequence contains vertical/horizontal patterns
             *         for disparity decoding
             * */
            class Orientation : public dlp::Parameters::Entry{
            public:
                enum class Type{
                    VERTICAL,
                    HORIZONTAL,
                    INVALID
                };
                Orientation();
                Orientation(const Type &orientation);
                void Set(const Type &orientation);
                Type Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                Type value_;
            };
        };
    };

    StructuredLight();
    ~StructuredLight();

    virtual ReturnCode GeneratePatternSequence(Pattern::Sequence *pattern_sequence) = 0;
    virtual ReturnCode DecodeCaptureSequence(Capture::Sequence *capture_sequence,dlp::DisparityMap *disparity_map) = 0;

    ReturnCode SetDlpPlatform( const dlp::DLP_Platform &platform );

    unsigned int GetTotalPatternCount();

protected:
    bool                                is_decoded_;
    bool                                projector_set_;
    unsigned int                        sequence_count_total_;

    dlp::DisparityMap                   disparity_map_;

    Settings::Sequence::Count           sequence_count_;
    Settings::Sequence::IncludeInverted sequence_include_inverted_;
    Settings::Pattern::Size::Rows       pattern_rows_;
    Settings::Pattern::Size::Columns    pattern_columns_;
    Settings::Pattern::Color            pattern_color_;
    Settings::Pattern::Orientation      pattern_orientation_;
};
}
/** @} */

#endif // DLP_SDK_STRUCTURED_LIGHT_HPP
