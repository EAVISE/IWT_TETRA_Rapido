/** \file   gray_code.hpp
 *  \brief  Contains definitions for the DLP SDK gray code classes
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#ifndef DLP_SDK_GRAY_CODE_HPP
#define DLP_SDK_GRAY_CODE_HPP

#include <common/returncode.hpp>
#include <common/debug.hpp>
#include <common/parameters.hpp>
#include <common/capture/capture.hpp>
#include <common/pattern/pattern.hpp>

#include <structured_light/structured_light.hpp>

#define GRAY_CODE_PIXEL_THRESHOLD_MISSING "GRAY_CODE_PIXEL_THRESHOLD_MISSING"

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \class      GrayCode
 *  \ingroup    StructuredLight
 *  \brief      Structured Light subclass used to generate and decode Gray coded
 *              binary patterns
 */
class GrayCode: public dlp::StructuredLight{
public:

    /** \class      Settings
     *  \brief      Contains \ref dlp::Parameters::Entry objects needed for GrayCode class
     */
    class Settings{
    public:
        /** \class Pixel
         *  \brief Settings related to the decoding of pixels for disparity values
         * */
        class Pixel{
        public:
            /** \class Threshold
             *  \brief Store the minimum pixel threshold when validating a pixel
             * */
            class Threshold : public dlp::Parameters::Entry{
                //friend class StructuredLight;
            public:
                typedef unsigned char Type;
                Threshold();
                Threshold(const unsigned char &threshold);
                void Set(const unsigned char &threshold);
                unsigned char Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned char value_;
            };
        };
    };

    GrayCode();
    ~GrayCode();

    ReturnCode Setup(const Parameters &settings);
    ReturnCode GetSetup(Parameters *settings) const;

    ReturnCode GeneratePatternSequence(Pattern::Sequence *pattern_sequence);
    ReturnCode DecodeCaptureSequence(Capture::Sequence *capture_sequence,dlp::DisparityMap *disparity_map);

private:
    Settings::Pixel::Threshold pixel_threshold_;
    unsigned int maximum_patterns_;
    unsigned int maximum_disparity_;
    unsigned int msb_pattern_value_;
    unsigned int resolution_;
    unsigned int offset_;
};
}


#endif // DLP_SDK_GRAY_CODE_HPP
