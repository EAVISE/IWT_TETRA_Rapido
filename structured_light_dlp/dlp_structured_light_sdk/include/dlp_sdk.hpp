/** \file       dlp_sdk.hpp
 *  \brief      Header file that includes all DLP 3D Scanner SDK header files
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

/** \mainpage DLP Pattern Software Development Kit
 *
 * \section intro_sec Introduction
 *
 * The DLP Pattern Software Development Kit is a tool designed to enable faster development with DLP
 * evaluation platforms. The SDK includes pre-built modules that perform specialized tasks using easy
 * to understand methods. The current modules are designed to perform a 3D scanning design using a
 * LightCrafter 4500 EVM as the dynamic structured light pattern projector. Future DLP platform support
 * is planned but not yet realized.
 *
 * \section module_sec Included Modules
 *
 * \subsection LCr4500 LightCrafter 4500 Module
 *
 * Contains easy to use methods for interfacing with a LightCrafter 4500 EVM. Also allows easy generation
 * and display of pattern sequences and firmware creation/upload.
 *
 * \subsection camera Camera Module
 *
 * Contains methods to interface with a Point Grey research camera using FlyCapture 2+ SDK.
 *
 * \subsection structured_light Structured Light Module
 *
 * The structured light module handles the creation of gray code patterns, along with the decoding of
 * captured images containing gray coded information. The module also stores point cloud information
 * and outputs it to a disk.
 *
 * \subsection geometry Geometry Module
 *
 * The geometry module contains mathematical methods to calculate intersections points of lines based on
 * camera/projector geometry. This enables mapping points to real space from captured gray coded images.
 *
 * \subsection calibration Calibration Module
 *
 * The calibration module includes methods to estimate intrinsic and extrinsic parameters of a camera and
 * projector by performing a calibration routine. The parameters of the camera and projector are saved for
 * future use and evaluated by the module to indicate a suspected good or bad calibration.
 */


#ifndef DLP_SDK_HPP
#define DLP_SDK_HPP

// Include SDK headers
#include <common/returncode.hpp>
#include <common/debug.hpp>
#include <common/other.hpp>
#include <common/image/image.hpp>
#include <common/parameters.hpp>
#include <common/capture/capture.hpp>
#include <common/pattern/pattern.hpp>
#include <common/point_cloud.hpp>
#include <common/module.hpp>

#include <camera/camera.hpp>

#include <structured_light/structured_light.hpp>
#include <structured_light/gray_code/gray_code.hpp>

#include <dlp_platforms/dlp_platform.hpp>
#include <dlp_platforms/lightcrafter_4500/lcr4500.hpp>

#include <calibration/calibration.hpp>
#include <geometry/geometry.hpp>



/** \defgroup   Common
 *  \brief      Contains common functions relating to strings, numbers, and time
 */

#endif  //#ifndef DLP_SDK_HPP


