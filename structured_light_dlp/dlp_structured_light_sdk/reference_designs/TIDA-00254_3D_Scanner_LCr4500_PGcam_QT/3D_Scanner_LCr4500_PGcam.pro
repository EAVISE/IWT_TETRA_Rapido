
# Adjust the following lines to match your OpenCV build
OPENCV_INCLUDE_DIR  = "C:\UserData\Programming_Tools\OpenCV-2.3.6.0\opencv\build\include"
OPENCV_LIB_DIR      = "C:\UserData\Programming_Tools\OpenCV-2.3.6.0\opencv_release_no_qt\lib"
OPENCV_BIN_DIR      = "C:\UserData\Programming_Tools\OpenCV-2.3.6.0\opencv_release_no_qt\bin"
OPENCV_VERSION      = 246

# Adjust the following lines to match your FlyCapture2 SDK installation
FLYCAP2_INCLUDE_DIR = "C:\UserData\Programming_Tools\PointGreyResearch\FlyCapture2\include"
FLYCAP2_LIB_C_DIR   = "C:\UserData\Programming_Tools\PointGreyResearch\FlyCapture2\lib\C"
FLYCAP2_BIN_DIR     = "C:\UserData\Programming_Tools\PointGreyResearch\FlyCapture2\bin"

# Adjust the following lines to match your QT installation
MINGW_BIN_DIR     = "C:\UserData\Programming_Tools\QT_5.1.1\5.1.1\mingw48_32\bin"




##########################################################################
######################## DO NOT CHANGE CODE BELOW ########################
##########################################################################

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11

OUT_PWD = $${PWD}/build

CONFIG(debug, debug|release) {
    DESTDIR = $$OUT_PWD/debug
} else {
    DESTDIR = $$OUT_PWD/release
}

INCLUDEPATH += \
    ../../include \
    ../../3rd_party/hidapi-master/hidapi \
    $$OPENCV_INCLUDE_DIR \
    $$FLYCAP2_INCLUDE_DIR


SOURCES += main.cpp \
    ../../src/common/other.cpp \
    ../../src/camera/camera.cpp \
    ../../src/dlp_platforms/dlp_platform.cpp \
    ../../src/structured_light/structured_light.cpp \
    ../../src/dlp_platforms/lightcrafter_4500/lcr4500.cpp \
    ../../src/common/debug.cpp \
    ../../src/structured_light/gray_code/gray_code.cpp \
    ../../src/calibration/calibration_settings.cpp \
    ../../src/geometry/geometry.cpp \
    ../../src/calibration/calibration_data.cpp \
    ../../src/calibration/calibration_camera.cpp \
    ../../src/calibration/calibration_projector.cpp \
    ../../src/structured_light/gray_code/gray_code_settings.cpp \
    ../../src/common/pattern/pattern_settings.cpp \
    ../../src/common/pattern/pattern.cpp \
    ../../src/common/pattern/pattern_sequence.cpp \
    ../../src/dlp_platforms/lightcrafter_4500/lcr4500_settings.cpp \
    ../../src/common/image/image_window.cpp \
    ../../src/common/image/image.cpp \
    ../../src/common/capture/capture_sequence.cpp \
    ../../src/common/capture/capture_settings.cpp \
    ../../src/common/capture/capture.cpp \
    ../../src/common/returncode.cpp \
    ../../src/common/disparity_map.cpp \
    ../../src/common/parameters.cpp \
    ../../src/common/point_cloud.cpp \
    ../../src/common/module.cpp \
    ../../src/geometry/geometry_settings.cpp \
    ../../src/dlp_platforms/dlp_platforms_settings.cpp \
    ../../src/camera/pg_flycap2/pg_flycap2_settings_c.cpp \
    ../../src/camera/pg_flycap2/pg_flycap2_c.cpp \
    ../../src/dlp_platforms/lightcrafter_4500/dlpc350_api.cpp \
    ../../src/dlp_platforms/lightcrafter_4500/dlpc350_usb.cpp \
    ../../src/dlp_platforms/lightcrafter_4500/dlpc350_firmware.cpp \
    ../../src/dlp_platforms/lightcrafter_4500/common.cpp

HEADERS += \
    ../../include/common/point_cloud.hpp \
    ../../include/common/pattern.hpp \
    ../../include/common/other.hpp \
    ../../include/common/image.hpp \
    ../../include/common/debug.hpp \
    ../../include/common/capture.hpp \
    ../../include/camera/camera.hpp \
    ../../include/dlp_platforms/dlp_platform.hpp \
    ../../include/structured_light/structured_light.hpp \
    ../../include/dlp_platforms/lightcrafter_4500/lcr4500.hpp \
    ../../include/dlp_platforms/lightcrafter_4500/lcr4500_defaults.hpp \
    ../../include/calibration/calibration.hpp \
    ../../include/structured_light/gray_code/gray_code.hpp \
    ../../include/geometry/geometry.hpp \
    ../../include/dlp_sdk.hpp \
    ../../include/common/disparity_map.hpp \
    ../../include/common/returncode.hpp \
    ../../include/common/parameters.hpp \
    ../../include/common/module.hpp \
    ../../include/camera/pg_flycap2/pg_flycap2_c.hpp \
    ../../include/common/capture/capture.hpp \
    ../../include/common/image/image.hpp \
    ../../include/common/pattern/pattern.hpp \
    ../../include/dlp_platforms/lightcrafter_4500/dlpc350_api.hpp \
    ../../include/dlp_platforms/lightcrafter_4500/dlpc350_usb.hpp \
    ../../include/dlp_platforms/lightcrafter_4500/dlpc350_firmware.hpp \
    ../../include/dlp_platforms/lightcrafter_4500/common.hpp \
    ../../include/dlp_platforms/lightcrafter_4500/error.hpp \
    ../../include/dlp_platforms/lightcrafter_4500/flashdevice.hpp

LIBS += -L$$OPENCV_LIB_DIR  \
            libopencv_core$$OPENCV_VERSION \
            libopencv_highgui$$OPENCV_VERSION \
            libopencv_imgproc$$OPENCV_VERSION \
            libopencv_features2d$$OPENCV_VERSION \
            libopencv_calib3d$$OPENCV_VERSION \
        -L$$FLYCAP2_LIB_C_DIR \
            libFlyCapture2_C

win32:       CONFIG(release, debug|release): LIBS += -L../../3rd_party/hidapi-master/windows/release/ -lhidapi
else:win32:  CONFIG(debug,   debug|release): LIBS += -L../../3rd_party/hidapi-master/windows/debug/   -lhidapi
macx:        SOURCES += ../../3rd_party/hidapi-master/mac/hid.c
unix: !macx: SOURCES += ../../3rd_party/hidapi-master/linux/hid.c

win32:       LIBS += -lSetupAPI
macx:        LIBS += -framework CoreFoundation -framework IOkit
unix: !macx: LIBS += -lusb-1.0 -ludev

DEPENDPATH += "../../3rd_party/hidapi-master/hidapi"

win32{
    # Required files for the application
    config_dir.commands    += xcopy $$system_path($${PWD}/config)           $$system_path($${DESTDIR}/config)       /s /e /h /i /Y
    output_dir.commands    += xcopy $$system_path($${PWD}/output)           $$system_path($${DESTDIR}/output)       /s /e /h /i /Y
    resources_dir.commands += xcopy $$system_path($${PWD}/../../resources)  $$system_path($${DESTDIR}/resources)    /s /e /h /i /Y

    # Copy OpenCV DLLs
    opencv_core.commands        += xcopy $$system_path($${OPENCV_BIN_DIR}/libopencv_core$${OPENCV_VERSION}.dll)          $$system_path($${DESTDIR})    /Y
    opencv_highgui.commands     += xcopy $$system_path($${OPENCV_BIN_DIR}/libopencv_highgui$${OPENCV_VERSION}.dll)       $$system_path($${DESTDIR})    /Y
    opencv_imgproc.commands     += xcopy $$system_path($${OPENCV_BIN_DIR}/libopencv_imgproc$${OPENCV_VERSION}.dll)       $$system_path($${DESTDIR})    /Y
    opencv_features2d.commands  += xcopy $$system_path($${OPENCV_BIN_DIR}/libopencv_features2d$${OPENCV_VERSION}.dll)    $$system_path($${DESTDIR})    /Y
    opencv_calib3d.commands     += xcopy $$system_path($${OPENCV_BIN_DIR}/libopencv_calib3d$${OPENCV_VERSION}.dll)       $$system_path($${DESTDIR})    /Y
    opencv_flann.commands       += xcopy $$system_path($${OPENCV_BIN_DIR}/libopencv_flann$${OPENCV_VERSION}.dll)         $$system_path($${DESTDIR})    /Y

    # Copy required FlyCap2 DLLs
    flycap2.commands    += xcopy $$system_path($${FLYCAP2_BIN_DIR}/FlyCapture2.dll)   $$system_path($${DESTDIR}) /Y
    flycap2_c.commands  += xcopy $$system_path($${FLYCAP2_BIN_DIR}/FlyCapture2_C.dll) $$system_path($${DESTDIR}) /Y
    libiomp5md.commands += xcopy $$system_path($${FLYCAP2_BIN_DIR}/libiomp5md.dll)    $$system_path($${DESTDIR}) /Y


    # Copy HID DLL
    hid_dll.commands += xcopy $$system_path(../../3rd_party/hidapi-master/windows/Release/hidapi.dll) $$system_path($${DESTDIR}) /Y

    # Copy mingw, QT, and Windows DLLs
    libstdcpp.commands      += xcopy $$system_path($${MINGW_BIN_DIR}/libstdc++-6.dll)       $$system_path($${DESTDIR}) /Y
    libgcc_s_dw2.commands   += xcopy $$system_path($${MINGW_BIN_DIR}/libgcc_s_dw2-1.dll)    $$system_path($${DESTDIR}) /Y
    libwinpthread.commands  += xcopy $$system_path($${MINGW_BIN_DIR}/libwinpthread-1.dll)   $$system_path($${DESTDIR}) /Y

    QMAKE_EXTRA_TARGETS += \
                    opencv_core \
                    opencv_highgui \
                    opencv_imgproc \
                    opencv_features2d \
                    opencv_calib3d \
                    opencv_flann \
                    flycap2  \
                    flycap2_c \
                    libiomp5md \
                    hid_dll \
                    libstdcpp   \
                    libgcc_s_dw2  \
                    libwinpthread

    POST_TARGETDEPS += \
                    opencv_core \
                    opencv_highgui \
                    opencv_imgproc \
                    opencv_features2d \
                    opencv_calib3d \
                    opencv_flann \
                    flycap2  \
                    flycap2_c \
                    libiomp5md \
                    hid_dll \
                    libstdcpp   \
                    libgcc_s_dw2  \
                    libwinpthread
}
macx{
    # Required files for the application
    config_dir.commands    += cp -r $${PWD}/config  $${DESTDIR}/
    output_dir.commands    += cp -r $${PWD}/output  $${DESTDIR}/
    resources_dir.commands += cp -r ../../resources $${DESTDIR}/
}
unix{
    # Required files for the application
    config_dir.commands    += cp -r $${PWD}/config  $${DESTDIR}/
    output_dir.commands    += cp -r $${PWD}/output  $${DESTDIR}/
    resources_dir.commands += cp -r ../../resources $${DESTDIR}/
}

QMAKE_EXTRA_TARGETS += \
    config_dir \
    output_dir \
    resources_dir

POST_TARGETDEPS += \
    config_dir \
    output_dir \
    resources_dir
