/** \file       main.cpp
 *  \brief      TIDA-00254 3D Scanner Application using DLP LightCrafter 4500 and Point Grey Flea3 USB camera
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#include <string>
#include <thread>

#include <dlp_sdk.hpp>
#include <camera/pg_flycap2/pg_flycap2_c.hpp>

dlp::ReturnCode ConnectAndSetupProjector(dlp::DLP_Platform *projector, const std::string &settings_file){
    dlp::ReturnCode ret;
    dlp::Parameters settings;

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<<<<<<<<<<<<<<<<<<<<<< Connect and Setup Projector >>>>>>>>>>>>>>>>>>>>>>" << std::endl;

    // Check that camera is NOT null
    if(!projector){
        std::cout << "Null projector pointer!" << std::endl;
        return ret.AddError("NULL_POINTER");
    }

    // Load the settings file
    std::cout << "Loading projector settings..." << std::endl;
    ret = settings.Load(settings_file);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Failed to load projector settings!" << std::endl;
        return ret;
    }
    else{
        std::cout << "Projector settings loaded" << std::endl;
    }

    // Connect the projector if it hasn't been connected
    std::cout << "Connecting to projector..." << std::endl;
    if(!projector->isConnected())   ret = projector->Connect(0);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Could NOT connect to projector!" << std::endl;
        return ret;
    }
    else{
        std::cout << "Projector connected" << std::endl;
    }

    // Setup the camera
    std::cout << "Setting up projector..." << std::endl;
    ret = projector->Setup(settings);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Could NOT setup projector!" << std::endl;
        return ret;
    }
    else{
        std::cout << "Projector setup complete" << std::endl;
    }

    return ret;
}

dlp::ReturnCode ConnectAndSetupCamera(dlp::Camera *camera, const std::string &settings_file){
    dlp::ReturnCode ret;
    dlp::Parameters settings;

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<<<<<<<<<<<<<<<<<<<<<< Connect and Setup Camera >>>>>>>>>>>>>>>>>>>>>>" << std::endl;

    // Check that camera is NOT null
    if(!camera) return ret.AddError("NULL_POINTER");

    // Load the settings file
    std::cout << "Loading camera settings..." << std::endl;
    ret = settings.Load(settings_file);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Failed to load camera settings!" << std::endl;
        return ret;
    }
    else{
        std::cout << "Camera settings loaded" << std::endl;
    }

    // Connect the camera if it hasn't been connected
    std::cout << "Connecting to camera..." << std::endl;
    if(!camera->isConnected())   ret = camera->Connect(0);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Could NOT connect to camera!" << std::endl;
        return ret;
    }
    else{
        std::cout << "Camera connected" << std::endl;
    }

    // Setup the camera
    std::cout << "Setting up camera..." << std::endl;
    ret = camera->Setup(settings);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Could NOT setup camera!" << std::endl;
        return ret;
    }
    else{
        std::cout << "Camera setup completed" << std::endl;
    }

    return ret;
}

void GenerateCameraCalibrationBoard(dlp::Camera       *camera,
                                    const std::string &camera_calib_settings_file){
    dlp::ReturnCode ret;

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<<<<<<<<<<<<<<<<<<<<<< Generate Camera Calibration Board >>>>>>>>>>>>>>>>>>>>>>" << std::endl;

    // Camera Calibration Variables
    dlp::Calibration::Camera    camera_calib;
    dlp::Parameters             camera_calib_settings;
    dlp::Image                  camera_calib_board_image;

    // Check that camera is NOT null
    if(!camera) return;

    // Load calibration settings
    std::cout << "Loading camera calibration settings..." << std::endl;
    if(camera_calib_settings.Load(camera_calib_settings_file).hasErrors()){

        // Camera calibration settings file did NOT load
        std::cout << "Camera calibration settings did NOT load, using default values" << std::endl;

        camera_calib_settings.Set(dlp::Calibration::Settings::Board::NumberRequired(10));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Color::Foreground(dlp::PixelRGB(255,255,255)));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Color::Background(dlp::PixelRGB(150,150,150)));    // Must be grey!
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows(5));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows::Distance(1));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows::DistanceInPixels(100));  // used to generate checkerboard bmp
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns(9));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns::Distance(1));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns::DistanceInPixels(100));
        camera_calib_settings.Set(dlp::Calibration::Settings::TangentDistortion(dlp::Calibration::Settings::TangentDistortion::Options::NORMAL));
        camera_calib_settings.Set(dlp::Calibration::Settings::SixthOrderDistortion(dlp::Calibration::Settings::SixthOrderDistortion::Options::NORMAL));

        std::cout << "Saving default camera calibration settings..." << std::endl;
        camera_calib_settings.Save(camera_calib_settings_file);
    }

    // Setup camera calibration for current camera
    camera_calib.SetCamera((*camera));

    std::cout << "Setting up camera calibration..." << std::endl;
    ret = camera_calib.Setup(camera_calib_settings);
    if(ret.hasErrors()){
        std::cout << "Camera calibration setup FAILED: " << ret.ToString() << std::endl;
        return;
    }

    // Genererate and save the calibration image
    std::cout << "Generating camera calibration board..." << std::endl;
    camera_calib.GenerateCalibrationBoard(&camera_calib_board_image);

    // Save the calibration board
    std::cout << "Saving calibration board..." <<std::endl;
    camera_calib_board_image.Save("output/calibration_camera/camera_calibration_board.bmp");

    // Wait for user to print calibration board
    double board_height;
    double board_width;

    std::cout << "\nPlease print the camera calibration board and attach to flat surface" <<std::endl;
    std::cout << "The calibration board image is stored in \n../output/calibration_camera/camera_calibration_board.bmp" << std::endl;
    std::cout << "\nEnter 1 after board has been printed and attached to flat surface: ";
    char dummy;
    std::cin >> dummy;

    std::cout << " \nMeasure the calibration board..." << std::endl;
    std::cout << "Once the calibration image has been printed and attached " << std::endl;
    std::cout << "to a flat surface, measure the dimensions of the board" << std::endl;
    std::cout << "\nNOTE: Measure the calibration board in the units desired " << std::endl;
    std::cout << "      for the point cloud! (i.e. mm, in, cm, etc.)\n" << std::endl;

    std::cout << "\nEnter the total height of the calibration pattern: ";
    std::cin >> board_height;
    std::cout << "Enter the total width of the calibration pattern:  ";
    std::cin >> board_width;
    std::cout << std::endl;

    std::cout << "Saving the new feature distances to the calibration settings file...";
    dlp::Calibration::Settings::Board::Features::Rows       calib_board_rows;
    dlp::Calibration::Settings::Board::Features::Columns    calib_board_cols;
    camera_calib_settings.Get(&calib_board_rows);
    camera_calib_settings.Get(&calib_board_cols);
    camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows::Distance(    board_height / (calib_board_rows.Get() + 1)));
    camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns::Distance( board_width  / (calib_board_cols.Get() + 1)));
    camera_calib_settings.Save(camera_calib_settings_file);
}


void PrepareProjectorPatterns( dlp::DLP_Platform    *projector,
                               const std::string    &projector_calib_settings_file,
                               dlp::StructuredLight *structured_light_vertical,
                               const std::string    &structured_light_vertical_settings_file,
                               dlp::StructuredLight *structured_light_horizontal,
                               const std::string    &structured_light_horizontal_settings_file,
                               const bool            previously_prepared,
                               unsigned int         *total_pattern_count){

    dlp::ReturnCode ret;

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<<<<<<<<<<<<<<<<<<<<<< Load DLP LightCrafter 4500 Firmware >>>>>>>>>>>>>>>>>>>>>>" << std::endl;

    // Projector Calibration Variables
    dlp::Calibration::Projector projector_calib;
    dlp::Parameters             projector_calib_settings;

    // Structured Light Sequence
    dlp::Pattern::Sequence calibration_patterns;
    dlp::Pattern::Sequence vertical_patterns;
    dlp::Pattern::Sequence horizontal_patterns;
    dlp::Pattern::Sequence all_patterns;

    // Structured Light Settings
    dlp::Parameters         structured_light_vertical_settings;
    dlp::Parameters         structured_light_horizontal_settings;

    projector_calib.SetDebugEnable(false);


    // Check that projector is NOT null
    if(!projector) return;

    // Check if projector is connected
    if(!projector->isConnected()){
        std::cout << "Projector NOT connected!" << std::endl;
        return;
    }

    // Load projector calibration settings
    std::cout << "Loading projector calibration settings..." << std::endl;
    if(projector_calib_settings.Load(projector_calib_settings_file).hasErrors()){
        // Projector calibration settings file did NOT load
        std::cout << "Projector calibration settings did NOT load, using default values" << std::endl;

        // Setup the projector calibration
        projector_calib_settings.Set(dlp::Calibration::Settings::Board::NumberRequired(10));
        projector_calib_settings.Set(dlp::Calibration::Settings::Model::HorizontalOffsetPercent(0.0));
        projector_calib_settings.Set(dlp::Calibration::Settings::Model::VerticalOffsetPercent(100.0));
        projector_calib_settings.Set(dlp::Calibration::Settings::Board::Color::Foreground(dlp::PixelRGB(255,255,255)));
        projector_calib_settings.Set(dlp::Calibration::Settings::Board::Color::Background(dlp::PixelRGB(0,0,0)));
        projector_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows(9));
        projector_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns(11));
        projector_calib_settings.Set(dlp::Calibration::Settings::TangentDistortion(dlp::Calibration::Settings::TangentDistortion::Options::NORMAL));
        projector_calib_settings.Set(dlp::Calibration::Settings::SixthOrderDistortion(dlp::Calibration::Settings::SixthOrderDistortion::Options::NORMAL));

        std::cout << "Saving default projector calibration settings" << std::endl;
        projector_calib_settings.Save(projector_calib_settings_file);
    }


    // Setup projector calibration for current projector
    projector_calib.SetDlpPlatform((*projector));

    // Add temporary camera resoltuion settings
    projector_calib_settings.Set(dlp::Calibration::Settings::Image::Columns(1000));
    projector_calib_settings.Set(dlp::Calibration::Settings::Image::Rows(1000));


    std::cout << "Setting up projector calibration..." << std::endl;
    ret = projector_calib.Setup(projector_calib_settings);
    if(ret.hasErrors()){
        std::cout << "Projector calibration setup FAILED: " << ret.ToString() << std::endl;
        return;
    }

    // Create the calibration image and convert to monochrome
    std::cout << "Generating projector calibration board..." << std::endl;
    dlp::Image   projector_calibration_board_image;
    projector_calib.GenerateCalibrationBoard(&projector_calibration_board_image);
    projector_calibration_board_image.ConvertToMonochrome();

    // Create calibration pattern
    std::cout << "Generating projector calibration pattern..." << std::endl;
    dlp::Pattern calib_pattern;
    calib_pattern.bitdepth.Set(dlp::Pattern::Settings::Bitdepth::Format::MONO_1BPP);
    calib_pattern.color.Set(dlp::Pattern::Settings::Color::Type::WHITE);
    calib_pattern.type.Set(dlp::Pattern::Settings::Data::Type::IMAGE_DATA);
    calib_pattern.image_data.Create(projector_calibration_board_image);

    // Add the calibration pattern to the calibration pattern sequence
    calibration_patterns.Add(calib_pattern);

    // Setup the strucutred light patterns
    if(!structured_light_vertical)   return;
    if(!structured_light_horizontal) return;

    structured_light_vertical->SetDebugEnable(false);
    structured_light_horizontal->SetDebugEnable(false);

    // Load the vertical structured light settings
    std::cout << "Loading vertical structured light settings..." << std::endl;
    if(structured_light_vertical_settings.Load(structured_light_vertical_settings_file).hasErrors()){
        // Camera calibration settings file did NOT load
        std::cout << "Vertical structured light settings did NOT load, using default values" << std::endl;

        structured_light_vertical_settings.Set(dlp::StructuredLight::Settings::Sequence::Count(9));
        structured_light_vertical_settings.Set(dlp::StructuredLight::Settings::Sequence::IncludeInverted(true));
        structured_light_vertical_settings.Set(dlp::StructuredLight::Settings::Pattern::Color(dlp::Pattern::Settings::Color::Type::WHITE));
        structured_light_vertical_settings.Set(dlp::StructuredLight::Settings::Pattern::Orientation(dlp::StructuredLight::Settings::Pattern::Orientation::Type::VERTICAL));
        structured_light_vertical_settings.Set(dlp::GrayCode::Settings::Pixel::Threshold(10));

        std::cout << "Saving default vertical structured light settings" << std::endl;
        structured_light_vertical_settings.Save(structured_light_vertical_settings_file);
    }

    // Load the horizontal structured light settings
    std::cout << "Loading horizontal structured light settings..." << std::endl;
    if(structured_light_horizontal_settings.Load(structured_light_horizontal_settings_file).hasErrors()){
        // Camera calibration settings file did NOT load
        std::cout << "Vertical structured light settings did NOT load, using default values" << std::endl;

        structured_light_horizontal_settings.Set(dlp::StructuredLight::Settings::Sequence::Count(9));
        structured_light_horizontal_settings.Set(dlp::StructuredLight::Settings::Sequence::IncludeInverted(true));
        structured_light_horizontal_settings.Set(dlp::StructuredLight::Settings::Pattern::Color(dlp::Pattern::Settings::Color::Type::WHITE));
        structured_light_horizontal_settings.Set(dlp::StructuredLight::Settings::Pattern::Orientation(dlp::StructuredLight::Settings::Pattern::Orientation::Type::HORIZONTAL));
        structured_light_horizontal_settings.Set(dlp::GrayCode::Settings::Pixel::Threshold(10));

        std::cout << "Saving default vertical structured light settings" << std::endl;
        structured_light_horizontal_settings.Save(structured_light_horizontal_settings_file);
    }

    structured_light_horizontal->SetDlpPlatform((*projector));
    structured_light_vertical->SetDlpPlatform((*projector));

    std::cout << "Setting up vertical structured light module..." << std::endl;
    structured_light_vertical->Setup(structured_light_vertical_settings);
    std::cout << "Setting up horizontal structured light module..." << std::endl;
    structured_light_horizontal->Setup(structured_light_horizontal_settings);

    // Generate the pattern sequence
    std::cout << "Generating vertical structured light module patterns..." << std::endl;
    structured_light_vertical->GeneratePatternSequence(&vertical_patterns);
    std::cout << "Generating horizontal structured light module patterns..." << std::endl;
    structured_light_horizontal->GeneratePatternSequence(&horizontal_patterns);

    // Combine all of the pattern sequences
    std::cout << "Combining all patterns for projector..." << std::endl;
    all_patterns.Add(calibration_patterns);
    all_patterns.Add(vertical_patterns);
    all_patterns.Add(horizontal_patterns);


    // Force the setting that if it has NOT been prepared previously
    dlp::Parameters force_preparation;
    force_preparation.Set(dlp::DLP_Platform::Settings::PatternSequence::Prepared(previously_prepared));
    projector->Setup(force_preparation);

    // Prepare projector
    projector->SetDebugEnable(!previously_prepared);
    std::cout << "Preparing projector for calibration and strucutred light..." << std::endl;
    ret = projector->PreparePatternSequence(all_patterns);
    projector->SetDebugEnable(false);

    if( ret.hasErrors()){
        std::cout << "Projector prepare sequence FAILED: " << ret.ToString() << std::endl;
        (*total_pattern_count) = 0;
    }

    std::cout << "Projector prepared" << std::endl;
    (*total_pattern_count) = all_patterns.GetCount();
}

void CalibrateCamera(dlp::Camera       *camera,
                     const std::string &camera_calib_settings_file,
                     const std::string &camera_calib_data_file,
                     dlp::DLP_Platform *projector){
    dlp::ReturnCode ret;

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<<<<<<<<<<<<<<<<<<<<<< Calibrating the Camera >>>>>>>>>>>>>>>>>>>>>>" << std::endl;

    // Camera Calibration Variables
    dlp::Calibration::Camera    camera_calib;
    dlp::Calibration::Data      camera_calib_data;
    dlp::Parameters             camera_calib_settings;

    // Check that camera is NOT null
    if(!camera) return;

    // Check if camera is connected
    if(!camera->isConnected()){
        std::cout << "Camera NOT connected!" << std::endl;
        return;
    }

    // Check that projector is NOT null
    if(!projector) return;

    // Check if projector is connected
    if(!projector->isConnected()){
        std::cout << "Projector NOT connected!" << std::endl;
        return;
    }

    // Check for camera calibration data
    if(dlp::File::Exists(camera_calib_data_file)){
        // Ask user is they want to overwrite calibration data or load it
        char recalibrate;
        std::cout << "\nCamera calibration data found. Recalibrate the camera if it has been adjusted or moved. \n" << std::endl;
        std::cout << std::endl << "Recalibrate camera? (0 = no, 1 = yes): ";
        std::cin >> recalibrate;

        if(recalibrate != '1'){
            std::cout << "Returning to main menu" << std::endl;
            return;
        }
    }

    // Load calibration settings
    std::cout << "Loading camera calibration settings..." << std::endl;
    if(camera_calib_settings.Load(camera_calib_settings_file).hasErrors()){
        // Camera calibration settings file did NOT load
        std::cout << "Camera calibration settings did NOT load, using default values" << std::endl;

        camera_calib_settings.Set(dlp::Calibration::Settings::Board::NumberRequired(10));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Color::Foreground(dlp::PixelRGB(255,255,255)));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Color::Background(dlp::PixelRGB(150,150,150)));    // Must be grey!
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows(6));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows::Distance(20));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows::DistanceInPixels(100));  // used to generate checkerboard bmp
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns(9));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns::Distance(20));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns::DistanceInPixels(100));
        camera_calib_settings.Set(dlp::Calibration::Settings::TangentDistortion(dlp::Calibration::Settings::TangentDistortion::Options::NORMAL));
        camera_calib_settings.Set(dlp::Calibration::Settings::SixthOrderDistortion(dlp::Calibration::Settings::SixthOrderDistortion::Options::NORMAL));

        std::cout << "Saving default camera calibration settings" << std::endl;
        camera_calib_settings.Save(camera_calib_settings_file);
    }

    // Setup camera calibration for current camera
    camera_calib.SetCamera((*camera));

    std::cout << "Setting up camera calibration" << std::endl;
    ret = camera_calib.Setup(camera_calib_settings);
    if(ret.hasErrors()){
        std::cout << "Camera calibration setup FAILED: " << ret.ToString() << std::endl;
        return;
    }

    // Project a white pattern to illuminate calibration board
    std::cout << "Projecting white pattern to illuminate calibration board" << std::endl;
    projector->ProjectSolidWhitePattern();


    // Get the number of calibration images needed
    unsigned int boards_success;
    unsigned int boards_required;
    camera_calib.GetCalibrationProgress(&boards_success,&boards_required);

    std::cout << "Calibrating camera..." << std::endl;
    std::cout << "Calibration boards required = " << boards_required << std::endl;
    std::cout << "Calibration boards captured = " << boards_success  << std::endl;


    // Begin capturing images for camera calibration
    dlp::Image          camera_board;
    dlp::Image::Window  camera_view;

    // Get camera resolution
    unsigned int cam_width;
    unsigned int cam_height;

    camera->GetColumns(&cam_width);
    camera->GetRows(&cam_height);

    // Open the camera
    std::cout << "\nCapture the calibration boards at different angles and move the camera "<< std::endl;
    std::cout << "location and viewing angle so that the entire camera view is calibrated..." <<std::endl;
    std::cout << "\nPress SPACE to capture the calibration board position" <<std::endl;
    std::cout << "Press ESC to end calibration" <<std::endl;
    std::cout << "\nNOTES: The calibration board should not be moving during capture!" <<std::endl;
    std::cout << "       Before capturing the first position, set the camera aperture!" <<std::endl;

    std::cout << "\nEnter 1 to start: ";
    char dummy;
    std::cin >> dummy;
    camera_view.Open("Camera Calibration - press SPACE to capture or ESC to exit");

    // Start camera if it hasn't been started
    if(!camera->isStarted()){
        if(camera->Start().hasErrors()){
            std::cout << "Could NOT start camera! \n" << std::endl;
            return;
        }
        else{
            std::cout << "Started camera. \n" << std::endl;
        }
    }


    while(boards_success < boards_required){
        // Wait for the space bar to add the calibration image
        unsigned int return_key = 0;
        while(return_key != ' '){
            camera_board.Clear();
            camera->GetFrame(&camera_board);
            camera_view.Update(camera_board,cam_width/2,cam_height/2);  // Display the capture at half the original size so it fits on the screen
            camera_view.WaitForKey(1,&return_key);
            if(return_key == 27) break;
        }

        if(return_key == 27){
            camera_view.Close();
            std::cout << "Exiting calibration";
            return;
        }

        // Add the calibraiton board image
        bool success = false;
        camera_calib.AddCalibrationBoard(camera_board,&success);

        // Update the status
        camera_calib.GetCalibrationProgress(&boards_success,&boards_required);

        // Display message
        if(success){
            std::cout << "Calibration board successfully added! Captured " << boards_success << " of " << boards_required << std::endl;
            camera_board.Save("output/calibration_camera/camera_only_calibration_capture_" + dlp::Number::ToString(boards_success) + ".bmp");
        }
        else{
            std::cout << "Calibration board NOT found!" << std::endl;
        }
    }

    // Release the image memory
    camera_board.Clear();

    // Calibrate the camera
    double error;
    if(!camera_calib.Calibrate(&error).hasErrors()){
        std::cout << "Camera calibrated" << std::endl;
        std::cout << "\nReprojection error (closer to zero is better) = " << error << std::endl;

        // Get the camera calibration
        camera_calib.GetCalibrationData(&camera_calib_data);

        // Save the camera calibration data
        camera_calib_data.Save(camera_calib_data_file);
        std::cout << "Camera calibration data saved. \n" << std::endl;
    }
    else{
        std::cout << "Camera calibration FAILED \n" << std::endl;
    }

    return;
}

void CalibrateSystem(dlp::Camera       *camera,
                     const std::string &camera_calib_settings_file,
                     const std::string &camera_calib_data_file,
                     dlp::DLP_Platform *projector,
                     const std::string &projector_calib_settings_file,
                     const std::string &projector_calib_data_file){

    dlp::ReturnCode ret;

    // Camera Calibration Variables
    dlp::Calibration::Camera    camera_calib;
    dlp::Calibration::Data      camera_calib_data;
    dlp::Parameters             camera_calib_settings;


    // Projector Calibration Variables
    dlp::Calibration::Projector projector_calib;
    dlp::Calibration::Data      projector_calib_data;
    dlp::Parameters             projector_calib_settings;

    camera_calib.SetDebugEnable(false);
    projector_calib.SetDebugEnable(false);

    // Check that camera is NOT null
    if(!camera) return;

    // Check if camera is connected
    if(!camera->isConnected()){
        std::cout << "Camera NOT connected! \n" << std::endl;
        return;
    }

    // Check that projector is NOT null
    if(!projector) return;

    // Check if projector is connected
    if(!projector->isConnected()){
        std::cout << "Projector NOT connected! \n" << std::endl;
        return;
    }

    // Check for camera calibration data
    bool update_calibration = false;

    if(dlp::File::Exists(camera_calib_data_file)){
        // Ask user is they want to overwrite calibration data or load it
        std::cout << "Camera calibration data found. \n" << std::endl;
        std::cout << std::endl << "Update the camera's intrinsic and distortion data? (0 = no, 1 = yes): ";
        std::cin >> update_calibration;

        // Load calibration data
        camera_calib_data.Load(camera_calib_data_file);
        if(camera_calib.SetCalibrationData(camera_calib_data).hasErrors()){
            std::cout << "Camera calibration data load did NOT succeed, updating all camera calibration parameters. \n" << std::endl;
            update_calibration = true;
        }
    }

    // Load camera calibration settings
    std::cout << "Loading camera calibration settings..." << std::endl;
    if(camera_calib_settings.Load(camera_calib_settings_file).hasErrors()){
        // Camera calibration settings file did NOT load
        std::cout << "Camera calibration settings did NOT load, using default values \n" << std::endl;

        camera_calib_settings.Set(dlp::Calibration::Settings::Board::NumberRequired(10));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Color::Foreground(dlp::PixelRGB(255,255,255)));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Color::Background(dlp::PixelRGB(150,150,150)));    // Must be grey!
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows(6));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows::Distance(21.71));       // measured distance in mm
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows::DistanceInPixels(100));  // used to generate checkerboard bmp
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns(9));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns::Distance(21.55));
        camera_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns::DistanceInPixels(100));
        camera_calib_settings.Set(dlp::Calibration::Settings::TangentDistortion(dlp::Calibration::Settings::TangentDistortion::Options::NORMAL));
        camera_calib_settings.Set(dlp::Calibration::Settings::SixthOrderDistortion(dlp::Calibration::Settings::SixthOrderDistortion::Options::NORMAL));

        std::cout << "Saving default camera calibration settings... \n" << std::endl;
        camera_calib_settings.Save(camera_calib_settings_file);
    }

    // Load projector calibration settings
    std::cout << "Loading projector calibration settings..." << std::endl;
    if(projector_calib_settings.Load(projector_calib_settings_file).hasErrors()){
        // Projector calibration settings file did NOT load
        std::cout << "Projector calibration settings did NOT load, using default values \n" << std::endl;

        // Setup the projector calibration
        projector_calib_settings.Set(dlp::Calibration::Settings::Board::NumberRequired(10));
        projector_calib_settings.Set(dlp::Calibration::Settings::Model::HorizontalOffsetPercent(0.0));
        projector_calib_settings.Set(dlp::Calibration::Settings::Model::VerticalOffsetPercent(100.0));
        projector_calib_settings.Set(dlp::Calibration::Settings::Board::Color::Foreground(dlp::PixelRGB(255,255,255)));
        projector_calib_settings.Set(dlp::Calibration::Settings::Board::Color::Background(dlp::PixelRGB(0,0,0)));
        projector_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Rows(9));
        projector_calib_settings.Set(dlp::Calibration::Settings::Board::Features::Columns(11));
        projector_calib_settings.Set(dlp::Calibration::Settings::TangentDistortion(dlp::Calibration::Settings::TangentDistortion::Options::NORMAL));
        projector_calib_settings.Set(dlp::Calibration::Settings::SixthOrderDistortion(dlp::Calibration::Settings::SixthOrderDistortion::Options::NORMAL));

        std::cout << "Saving default projector calibration settings... \n" << std::endl;
        projector_calib_settings.Save(projector_calib_settings_file);
    }

    // Adjust camera calibration boards with the required number for the projector
    dlp::Calibration::Settings::Board::NumberRequired projector_boards_required;
    projector_calib_settings.Get(&projector_boards_required);
    camera_calib_settings.Set(projector_boards_required);

    // Setup camera calibration for current camera
    camera_calib.SetCamera((*camera));
    projector_calib.SetCamera((*camera));
    projector_calib.SetDlpPlatform((*projector));

    std::cout << "Setting up camera calibration...";
    ret = camera_calib.Setup(camera_calib_settings);
    if(ret.hasErrors()){
        std::cout << "Camera calibration setup FAILED: " << ret.ToString() << std::endl;
        return;
    }

    std::cout << "Setting up projector calibration...";
    ret = projector_calib.Setup(projector_calib_settings);
    if(ret.hasErrors()){
        std::cout << "Projector calibration setup FAILED: " << ret.ToString() << std::endl;
        return;
    }

    // Start camera if it hasn't been started
    if(!camera->isStarted()){
        if(camera->Start().hasErrors()){
            std::cout << "Could NOT start camera! \n" << std::endl;
            return;
        }
    }

    // Project calibration pattern to focus projector
    ret = projector->DisplayPatternInSequence(0,true);
    if(ret.hasErrors()){
        std::cout << ret.ToString() << std::endl;
        return;
    }

    dlp::Time::Sleep::Milliseconds(100);
    char focusing;
    std::cout << "Focus the projector and then enter 1: ";
    std::cin >> focusing;
    std::cout << std::endl;


    // Get the number of calibration images needed
    unsigned int cam_boards_success;
    unsigned int cam_boards_required;
    unsigned int proj_boards_success;
    unsigned int proj_boards_required;

    camera_calib.GetCalibrationProgress(&cam_boards_success,&cam_boards_required);
    projector_calib.GetCalibrationProgress(&proj_boards_success,&proj_boards_required);


    std::cout << "Camera calibration boards required = " << cam_boards_required << std::endl;
    std::cout << "Camera calibration boards captured = " << cam_boards_success  << std::endl;
    std::cout << "Projector calibration boards required = " << proj_boards_required << std::endl;
    std::cout << "Projector calibration boards captured = " << proj_boards_success  << std::endl;

    // Begin capturing images for camera calibration
    dlp::Image          camera_board;
    dlp::Image          projector_board;
    dlp::Image::Window  camera_view;
    unsigned int        return_key = 0;

    // Get camera resolution
    unsigned int cam_width;
    unsigned int cam_height;

    camera->GetColumns(&cam_width);
    camera->GetRows(&cam_height);


    std::cout << "\nCapture the calibration boards at different angles and do NOT move the camera"<< std::endl;
    std::cout << "location or viewing angle after taking the first calibration board capture!" <<std::endl;
    std::cout << "\nPress SPACE to capture the calibration board position" <<std::endl;
    std::cout << "Press ESC to end calibration" <<std::endl;
    std::cout << "\nNOTES: The calibration board should not be moving during capture!" <<std::endl;
    std::cout << "       Before capturing the first position, set the camera position!" <<std::endl;
    std::cout << "       Move the calibration board between captures. " <<std::endl;
    std::cout << "       Ensure that the printed board is always within the illuminated" << std::endl;
    std::cout << "       area and that the illuminated area does NOT fall off of the" << std::endl;
    std::cout << "       flat calibration surface." <<std::endl;

    std::cout << "\nEnter 1 to start: ";
    char dummy;
    std::cin >> dummy;
    camera_view.Open("Camera Calibration - press SPACE to capture or ESC to exit");

    while(proj_boards_success < proj_boards_required){

        // Get the camera board first
        while(cam_boards_success < cam_boards_required){

            // Project white pattern to illuminate camera calibration board
            projector->ProjectSolidWhitePattern();
            dlp::Time::Sleep::Milliseconds(100);

            // Wait for the space bar to add the calibration image
            while(return_key != ' '){
                camera_board.Clear();
                camera->GetFrame(&camera_board);
                camera_view.Update(camera_board, cam_width/2, cam_height/2);
                camera_view.WaitForKey(1,&return_key);
                if(return_key == 27) break;
            }

            if(return_key == 27){
                camera_view.Close();
                std::cout << "Exiting calibration...";
                return;
            }

            // Reset return key
            return_key = 0;

            // Add the calibration board image
            bool success;
            camera_calib.AddCalibrationBoard(camera_board,&success);

            // Update the status
            camera_calib.GetCalibrationProgress(&cam_boards_success,&cam_boards_required);

            // Display message
            if(success){
                std::cout << "Camera calibration board successfully added! Captured " << cam_boards_success << " of " << cam_boards_required << std::endl;
                camera_board.Save("output/calibration_system/system_camera_calibration_capture_" + dlp::Number::ToString(cam_boards_success) + ".bmp");

                // Get the projector calibration capture now
                projector->DisplayPatternInSequence(0,true);
                dlp::Time::Sleep::Milliseconds(250);

                camera->GetFrame(&projector_board);
                camera_view.Update(projector_board,cam_width/2,cam_height/2);
                camera_view.WaitForKey(1,&return_key);

                dlp::Image projected_black;
                projector->ProjectSolidBlackPattern();
                dlp::Time::Sleep::Milliseconds(250);

                camera->GetFrame(&projected_black);
                camera_view.Update(projected_black,cam_width/2,cam_height/2);
                camera_view.WaitForKey(1,&return_key);


                // Add the printed and combination boards
                dlp::Image projected_image;

                projector_calib.RemovePrinted_AddProjectedBoard(camera_board,
                                                                projected_black,
                                                                projector_board,
                                                                &projected_image,
                                                                &success);
                projected_black.Clear();

                // Update the status
                projector_calib.GetCalibrationProgress(&proj_boards_success,&proj_boards_required);

                if(success){
                    std::cout << "Projector calibration board successfully added! Captured " << proj_boards_success << " of " << proj_boards_required << std::endl;
                    projected_image.Save("output/calibration_system/system_projector_calibration_board_" + dlp::Number::ToString(proj_boards_success) +".bmp");
                }
                else{
                    std::cout << "Projector calibration board NOT found... \n" << std::endl;

                    // Remove the most recently added camera calibration board
                    camera_calib.RemoveLastCalibrationBoard();
                    camera_calib.GetCalibrationProgress(&cam_boards_success,&cam_boards_required);
                }
            }
            else{
                std::cout << "Calibration board NOT found... \n" << std::endl;
            }

            dlp::Time::Sleep::Milliseconds(250);
        }
    }

    // Release the image memory
    camera_board.Clear();

    // Close the window
    camera_view.Close();

    // Calibrate the camera
    double error;
    std::cout << "Calibrating camera... \n" << std::endl;
    if(update_calibration)  ret = camera_calib.Calibrate(&error);
    else                    ret = camera_calib.Calibrate(&error,false,false,true);  // Only update the extrinsics

    if( ret.hasErrors()){
        std::cout << "Camera calibration FAILED!" << std::endl;
        return;
    }


    std::cout << "Camera calibration succeeded with reprojection error (closer to zero is better) = " << error << std::endl;

    // Get the camera calibration
    camera_calib.GetCalibrationData(&camera_calib_data);

    // Add camera calibration to projector calibration
    std::cout << "Loading camera calibration into projector calibration..." << std::endl;
    projector_calib.SetCameraCalibration(camera_calib_data);

    std::cout << "Calibrating projector... \n" << std::endl;
    ret = projector_calib.Calibrate(&error);

    if( ret.hasErrors()){
        std::cout << "Projector calibration FAILED \n" << std::endl;
        return;
    }

    std::cout << "Projector calibration succeeded with reprojection error (closer to zero is better) = " << error << std::endl;

    // Get the projector and camera calibration
    projector_calib.GetCalibrationData(&projector_calib_data);

    // Save the calibration data
    std::cout << "Saving camera and projector calibration data..." << std::endl;
    camera_calib_data.Save(camera_calib_data_file);
    projector_calib_data.Save(projector_calib_data_file);

    std::cout << "Calibration data saved" << std::endl;

    return;
}


void ScanObject(dlp::Camera         *camera,
                const std::string   &camera_calib_data_file,
                dlp::DLP_Platform   *projector,
                const std::string   &projector_calib_data_file,
                dlp::GrayCode       *structured_light_vertical,
                dlp::GrayCode       *structured_light_horizontal,
                const unsigned int  &total_pattern_count,
                const std::string   &geometry_settings_file){

    // Check that camera is NOT null
    if(!camera) return;

    // Check if camera is connected
    if(!camera->isConnected()){
        std::cout << "Camera NOT connected! \n" << std::endl;
        return;
    }

    // Check that projector is NOT null
    if(!projector) return;

    // Check if projector is connected
    if(!projector->isConnected()){
        std::cout << "Projector NOT connected! \n" << std::endl;
        return;
    }

    // Check that structured light modules are setup
    if(!structured_light_vertical)   return;
    if(!structured_light_horizontal) return;
    if(!structured_light_vertical->isSetup()){
        std::cout << "Vertical structured light module NOT setup! \n" << std::endl;
        return;
    }
    if(!structured_light_horizontal->isSetup()){
        std::cout << "Horizontal structured light module NOT setup! \n" << std::endl;
        return;
    }

    // Check that calibrations are complete
    dlp::Calibration::Data calibration_data_camera;
    dlp::Calibration::Data calibration_data_projector;


    std::cout << "Loading camera and projector calibration data..." << std::endl;
    calibration_data_camera.Load(camera_calib_data_file);
    calibration_data_projector.Load(projector_calib_data_file);

    if(!calibration_data_camera.isComplete()){
        std::cout << "Camera calibration is NOT complete! \n" << std::endl;
        return;
    }

    if(!calibration_data_camera.isCamera()){
        std::cout << "Camera calibration is NOT from a camera calibration! \n" << std::endl;
        return;
    }

    if(!calibration_data_projector.isComplete()){
        std::cout << "Projector calibration is NOT complete! \n" << std::endl;
        return;
    }

    if(calibration_data_projector.isCamera()){
        std::cout << "Projector calibration is NOT from a projector calibration! \n" << std::endl;
        return;
    }


    // Setup the geometry
    std::cout << "Constructing the camera and projector geometry..." << std::endl;
    unsigned int camera_viewport;
    dlp::Geometry scanner_geometry;
    scanner_geometry.SetDebugEnable(false);
    scanner_geometry.SetOriginView(calibration_data_projector);
    scanner_geometry.AddView(calibration_data_camera,&camera_viewport);

    dlp::Parameters geometry_settings;
    geometry_settings.Load(geometry_settings_file);
    scanner_geometry.Setup(geometry_settings);

    // Project pattern sequence and capture images
    dlp::Capture::Sequence capture_scan;


    // Start timer
    dlp::Time::Chronograph timer;


    // Start camera if it hasn't been started
    if(!camera->isStarted()){
        if(camera->Start().hasErrors()){
            std::cout << "Could NOT start camera! \n" << std::endl;
            return;
        }
    }

    // Wait for user
    unsigned int return_key = 0;
    dlp::Image::Window camera_view;

    // Get camera resolution
    unsigned int cam_width;
    unsigned int cam_height;

    camera->GetColumns(&cam_width);
    camera->GetRows(&cam_height);

    camera_view.Open("Place object to scan in view and then press the space bar...");
    projector->ProjectSolidWhitePattern();
    while(return_key != ' '){
        dlp::Image camera_capture;
        camera_capture.Clear();
        camera->GetFrame(&camera_capture);
        camera_view.Update(camera_capture,cam_width/2,cam_height/2);
        camera_view.WaitForKey(1,&return_key);
        if(return_key == 27) break;
    }
    camera_view.Close();
    projector->ProjectSolidBlackPattern();
    dlp::Time::Sleep::Milliseconds(100);

    // Scan the object
    for(unsigned int iScan = 0; iScan < total_pattern_count*1.25; iScan++){  // Multiplier included to allow ample time for sequence to start
        dlp::Capture capture;
        dlp::Image capture_image;

        if(iScan == 1){
            projector->StartPatternSequence(1,total_pattern_count-1,false);   // Skip the 0 pattern since it is the calibration image
            timer.Reset();
        }
        camera->GetFrame(&capture_image);

        // Add the frame to the sequence
        std::cout << "Captured " + dlp::Number::ToString(iScan) << std::endl;
        capture.image_data = capture_image;
        capture.image_type.Set(dlp::Capture::Settings::Data::Type::IMAGE_DATA);
        capture_scan.Add(capture);
        capture_image.Clear();
        capture.image_data.Clear();
    }

    camera->Stop();
    std::cout << "Capture complete! milliseconds = " << timer.Lap() << std::endl;

    // Find  the first pattern and the seperate the vertical and horizontal patterns
    dlp::Capture::Sequence vertical_scan;
    dlp::Capture::Sequence horizontal_scan;
    bool            first_pattern_found = false;
    unsigned char   previous_ave   = 0;
    unsigned int    capture_offset = 0;

    for(unsigned int iScan = 0; iScan < capture_scan.GetCount(); iScan++){
        dlp::Capture temp;

        capture_scan.Get(iScan,&temp);

        // Calculate the average for the image to determine if the pattern started
        if(!first_pattern_found){
            unsigned char ave;

            // Get the capture image format to determine how to get the mean
            dlp::Image::Format capture_format;
            temp.image_data.GetDataFormat(&capture_format);

            if(capture_format == dlp::Image::Format::RGB_UCHAR){
                dlp::PixelRGB ave_rgb;
                // Get the average for each channel
                temp.image_data.GetMean(&ave_rgb);

                // Average the channels together
                ave = (ave_rgb.r + ave_rgb.g + ave_rgb.b)/3;
            }
            else{
                // Assume that the image is MONO_UCHAR
                temp.image_data.GetMean(&ave);
            }

            if(iScan == 0) previous_ave = ave;

            // If the percent error is greater than 10% the first pattern has been found
            float percent_error = std::abs((previous_ave - ave)/(float)ave);
            if( percent_error > 0.10 ){
                first_pattern_found = true;
                capture_offset = iScan;
            }
        }

        if(first_pattern_found){

            // The first projected pattern has been found
            if(iScan < structured_light_vertical->GetTotalPatternCount() + capture_offset){
                vertical_scan.Add(temp);
                temp.image_data.Save("output/scan_images/scan_capture_"+dlp::Number::ToString(iScan - capture_offset)+".bmp");
            }
            else if(iScan < (structured_light_vertical->GetTotalPatternCount()   +
                             structured_light_horizontal->GetTotalPatternCount() +
                             capture_offset)){
                horizontal_scan.Add(temp);
                temp.image_data.Save("output/scan_images/scan_capture_"+dlp::Number::ToString(iScan - capture_offset)+".bmp");
            }
        }
    }

    // Check that there are enough patterns to decode
    if((structured_light_vertical->GetTotalPatternCount()   != vertical_scan.GetCount()) ||
       (structured_light_horizontal->GetTotalPatternCount() != horizontal_scan.GetCount())){
        std::cout << "NOT enough images. Scans may have been too dark. Please rescan. \n" << std::endl;
        return;
    }

    std::cout << "Vertical structured light images to decode: "<< structured_light_vertical->GetTotalPatternCount() << std::endl;
    std::cout << "Horizontal structured light images to decode: "<< structured_light_horizontal->GetTotalPatternCount() << std::endl;


    dlp::DisparityMap column_disparity;
    dlp::DisparityMap row_disparity;

    timer.Lap();
    std::cout << "Decoding vertical patterns..." << std::endl;
    structured_light_vertical->DecodeCaptureSequence(&vertical_scan,&column_disparity);
    std::cout << "Decode horizontal patterns..." << std::endl;
    structured_light_horizontal->DecodeCaptureSequence(&horizontal_scan,&row_disparity);


    std::cout << "Decoding time = " << timer.Lap() << std::endl;


    dlp::Point::Cloud point_cloud;
    dlp::Image  depth_map;

    std::cout << "Generating point cloud..." << std::endl;
    std::cout << scanner_geometry.GeneratePointCloud(camera_viewport,column_disparity,row_disparity,&point_cloud,&depth_map).ToString() << std::endl;

    std::cout << "Point cloud generation time = " << timer.Lap() << std::endl;
    std::string file_time = dlp::Number::ToString(timer.Reset());

    depth_map.Save("output/scan_data/" + file_time + "_depth_map.bmp");

    dlp::Image::Window depth_window;

    depth_window.Open("Depth map - press space to close window", depth_map, cam_width/2,cam_height/2);

    unsigned int depth_ret = 0;
    while(depth_ret != ' '){
        depth_window.Update(depth_map, cam_width/2,cam_height/2);
        depth_window.WaitForKey(100,&depth_ret);
    }
    depth_window.Close();

    std::cout << "Saving point cloud..." << std::endl;
    point_cloud.SaveXYZ("output/scan_data/" + file_time + "_point_cloud.xyz", ' ');
    std::cout << "Point cloud saved" << std::endl;

    return;
}


int main()
{    
    // System Variables
    dlp::PG_FlyCap2_C   camera;
    dlp::LCr4500        projector;

    // Strucrtured Light modules
    dlp::GrayCode structured_light_vertical;
    dlp::GrayCode structured_light_horizontal;
    unsigned int total_pattern_count = 0;


    camera.SetDebugEnable(false);
    projector.SetDebugEnable(false);

    // Files
    std::string system_settings_file_camera                 = "config/system_settings_camera.txt";
    std::string system_settings_file_projector              = "config/system_settings_projector.txt";
    std::string calibration_settings_file_camera            = "config/calibration_settings_camera.txt";
    std::string calibration_settings_file_projector         = "config/calibration_settings_projector.txt";
    std::string calibration_data_file_camera                = "config/calibration_data_camera.xml";
    std::string calibration_data_file_projector             = "config/calibration_data_projector.xml";
    std::string structured_light_vertical_settings_file     = "config/structured_light_vertical_settings.txt";
    std::string structured_light_horizontal_settings_file   = "config/structured_light_horizontal_settings.txt";
    std::string geometry_settings_file                      = "config/geometry_settings.txt";

    // Connect camera and projector
    ConnectAndSetupProjector(&projector, system_settings_file_projector);
    ConnectAndSetupCamera(   &camera,    system_settings_file_camera);

    // Program menu
    int menu_select = 0;

    do{
        // Print menu
        std::cout << std::endl;
        std::cout << "Texas Instruments DLP Commandline 3D Scanner \n" << std::endl;
        std::cout << "0: Exit " << std::endl;
        std::cout << "1: Generate camera calibration board and enter feature measurements " << std::endl;
        std::cout << "2: Prepare DLP LightCrafter 4500 (once per projector)" << std::endl;
        std::cout << "3: Prepare system for calibration and scanning" << std::endl;
        std::cout << "4: Calibrate camera " << std::endl;
        std::cout << "5: Calibrate system " << std::endl;
        std::cout << "6: Perform scan " << std::endl;
        std::cout << "7: Reconnect camera and projector " << std::endl;
        std::cout  << std::endl << "Select menu item: ";

        // Get menu selection
        std::cin >> menu_select;
        std::cout << std::endl;

        // Execute selection
        switch(menu_select){
        case 0:
            break;
        case 1:
            GenerateCameraCalibrationBoard(&camera, calibration_settings_file_camera);
            break;
        case 2:
            PrepareProjectorPatterns(&projector,
                                      calibration_settings_file_projector,
                                     &structured_light_vertical,
                                      structured_light_vertical_settings_file,
                                     &structured_light_horizontal,
                                      structured_light_horizontal_settings_file,
                                      false,
                                     &total_pattern_count); // Firmware will be uploaded
            break;
        case 3:
            PrepareProjectorPatterns(&projector,
                                      calibration_settings_file_projector,
                                     &structured_light_vertical,
                                      structured_light_vertical_settings_file,
                                     &structured_light_horizontal,
                                      structured_light_horizontal_settings_file,
                                      true,
                                     &total_pattern_count); // Firmware will NOT be uploaded
            break;
        case 4:
            CalibrateCamera(&camera,
                            calibration_settings_file_camera,
                            calibration_data_file_camera,
                            &projector);
            break;
        case 5:
            CalibrateSystem(&camera,
                            calibration_settings_file_camera,
                            calibration_data_file_camera,
                            &projector,
                            calibration_settings_file_projector,
                            calibration_data_file_projector);
            break;
        case 6:
            ScanObject(&camera,
                        calibration_data_file_camera,
                       &projector,
                        calibration_data_file_projector,
                       &structured_light_vertical,
                       &structured_light_horizontal,
                        total_pattern_count,
                        geometry_settings_file);
            break;
        case 7:
            // Disconnect system objects
            camera.Disconnect();
            projector.Disconnect();

            // Reconnect system objects
            ConnectAndSetupProjector(&projector, system_settings_file_projector);
            ConnectAndSetupCamera(   &camera,    system_settings_file_camera);
            break;
        default:
            std::cout << "Invalid menu selection! \n" << std::endl;
        }

        std::cout << std::endl;
        std::cout << std::endl;

    }while(menu_select != 0);

    // Disconnect system objects
    camera.Disconnect();
    projector.Disconnect();

    return 0;
}


