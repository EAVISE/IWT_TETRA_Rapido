Nodige libraries voor structured light DLP:

Opencv 3.0.0
PCL 1.8
Boost : builden met linkervlaggen -lboost_system en -lboost_thread
Flycapture2 SDK 2.8.3.1 : builden met linkervlag -lflycapture
Udev : builden met linkervlag -ludev

Link libraries:
libflycapture.so
libflycapture-c.so

Compiler moet zoeken in directories:
dlp_structured_light_sdk/3rd_party/hidapi-master/hidapi (zit in de repo)
dlp_structured_light_sdk/include (zit in de repo)
/usr/include/flycapture
/usr/include/libusb-1.0

Opmerkingen:
Wanneer je dit programma wil gebruiken, moet de projector worden voorbereidt. D.w.z. dat de patronen die je wil projecteren in het flash geheugen van de projector geladen moet worden. De functie bestaat in dit programma, maar werkt niet altijd even goed. Best dat je de windows versie gebruikt om de correcte patronen op de correcte manier te uploaden. Dit hoeft slechts 1x te gebeuren.
Link: http://www.ti.com/tool/TIDA-00254 , tab software.

Functies:

Zie doc/html/index.html voor de documentatie van de dlp_sdk.

		
