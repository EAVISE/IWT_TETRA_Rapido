Nodige libraries voor Depthsense:

Opencv 3.0.x
PCL 1.8
Boost : builden met vlaggen -lboost_system en -lboost_thread
Depthsense SDK: Download van de softkinetic website en installeer. Bestanden komen in de /opt folder te staan.

Andere vlaggen voor de linker: -lusb (voor de usb communicatie)

Link libraries:
/opt/softkinetic/DepthSenseSDK/lib/lib*.so
/usr/local/lib/libpcl*.so
/usr/lib/libvtk*.so
/lib/x86_64-linux-gnu/libpthread-2.19.so

Compiler moet zoeken in directories:
/usr/local/pcl-1.8  (of waar je het geinstalleerd hebt)
/usr/include/eigen3
/usr/include/vtk-5.8
include (bevat de nodige headers om de depthsense aan de praat te krijgen)

Linker moet zoeken in directories:
/opt/softkinetic/DepthSenseSDK/lib

Voorwaarden: 
	/

Functies:

getd
	Heeft als input de focale lengte, distortie coefficient in de x-richting en de distortie 		coefficient in de y-richting. De distortie-coefs zijn meestal 0.

	Initialiseert de vectoren d, u en v.

getXYZ
	Heeft als input de focale lengte, de phasemap van de depthsense, d, u en v, verkregen uit 		getd.
	Output: cloud. Dit is de puntenwolk.
		D voor elk punt wordt de waarde D berekend. Is D < 0, dan is het een slecht punt.

	Berekend aan de hand van de focale lengte, de phasemap, d, u en v de xyz coordinaten.

onNewDepthSample
	Wordt uitgevoerd elke keer er een nieuwe depthsample is.
	Afhankelijk van de configuratie van de Depthnode, kan je verschillende soorten informatie uit 		NewSampleReceivedData halen.

configureDepthNode
	Configureer de depthnode.

configureNode
	Bepaalt welke soort node er geconfigureerd dient te worden. Momenteel werkt het alleen met 		depthnodes.

onNodeConnected
	Als er een node wordt geconnecteerd, zal deze functie dit signaal verwerken en de verbonden 		node configureren.

onNodeDisconnected
	Kuist de verbinding op wanneer de node disconnect.

onDeviceConnected
	Initialiseert de signalen voor de nodes wanneer het apparaat verbinding maakt.

onDeviceDisconnected
	Kuist de verbinding met het apparaat op nadat het is gedisconnect.



	


