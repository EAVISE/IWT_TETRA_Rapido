#include "depthsense.h""

Context g_context;
DepthNode g_dnode;
uint32_t g_dFrames;

bool g_bDeviceFound;
int16_t tofdata[320*240*100];
int16_t confdata[320*240*100];
int16_t depthdata[320*240*100];

/*boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
int teller = 0;
*/
vector<vector<float> > getXYZ( float fl, Mat P, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, vector<vector<float> > D, vector<vector<float> > d, vector<vector<float> > u, vector<vector<float> > v)
{
	float Dd;
	float int16max	= 32767;				// Max value of 16 bit integer
	float range		= 3;				// Max range (meters) of Softkinetic DepthSense 311
	float C			= range / int16max;

    int i=0;
	for(int c1=0; c1<TOFH;c1++)
	{
		for(int c2=0; c2<TOFW;c2++)
		{
			D[c1][c2]=(float)P.at<int16_t>(c2, c1) * C;	// Convert phase to radial distance (meters)
			if(D[c1][c2] > 0.005) //If the point is saturated, D will be zero or negative. We don't need those points.
			{


                Dd = D[c1][c2] / d[c1][c2];
                cloud->points[i].y = -u[c1][c2] * Dd;
                cloud->points[i].x = v[c1][c2] * Dd;
                cloud->points[i].z = fl * Dd;
                //cout<<cloud.points[i].x<< " "<<cloud.points[i].y<< " "<<cloud.points[i].z<< " "<<endl;
                uint8_t r = 0, g = 255, b = 0;
                uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
                cloud->points[i].rgb = *reinterpret_cast<float*>(&rgb);
            }

			i++;
		}
	}
	return D;
}

int getd( float fl, float k1, float k2, vector<vector<float> > &d, vector<vector<float> > &u, vector<vector<float> > &v )
{
	float c, r, temp, R, RR, RRRR, FF;

	FF = fl*fl;

	for(int c1=0; c1<TOFH;c1++)
	{
		c = (float)c1 - (TOFH/2 - 0.5);		// Vertical   TOF pixel position wrt to center
		for(int c2=0; c2<TOFW;c2++)
		{
			r = (float)c2 - (TOFW/2 - 0.5);	// Horizontal TOF pixel position wrt to center*
			temp = c*c+r*r;
			R = sqrt(temp);
			RR = R * R;
			RRRR = RR * RR;
			v[c1][c2] = c * (1 + k1*RR + k2*RRRR);	// Distortion
			u[c1][c2] = r * (1 + k1*RR + k2*RRRR);
			temp += FF;
			d[(int)c1][(int)c2] = sqrt(temp);
		}
	}
	return 1;
}

/*----------------------------------------------------------------------------*/
// New depth sample event handler
void onNewDepthSample(DepthNode node, DepthNode::NewSampleReceivedData data)
{
	/*if(teller%1== 0)
	{*/
        memcpy ( tofdata+g_dFrames*320*240, data.phaseMap, 320*240*2 );
        memcpy ( confdata+g_dFrames*320*240, data.confidenceMap, 320*240*2 );

        // Create openCV Mat objects to hold TOF image data
        Mat tof   = Mat(TOFH, TOFW, CV_16UC1, tofdata);
        Mat conf  = Mat(TOFH, TOFW, CV_16UC1, confdata);
        /*Mat tof2  = Mat(4*TOFH, 4*TOFW, CV_16UC1);				// Resize for display
        Mat conf2 = Mat(4*TOFH, 4*TOFW, CV_16UC1);

        // Resize TOF data for display

        //resize( tof,  tof2,  tof2.size(), 0, 0, INTER_NEAREST);
        //resize(conf, conf2, conf2.size(), 0, 0, INTER_NEAREST);
        // Display data in opencv window

        imshow("Phase", tof*5);
        waitKey(0);
        imshow("Confidence", conf*10);
        waitKey(0);
*/
        //cout<<g_dFrames<<" "<<conf2<<endl;

        float fl = 300.0;
        float k1 = 0;
        float k2=k1;

        vector<vector<float> > d,u,v, D;
        d.resize(TOFH);
        u.resize(TOFH);
        v.resize(TOFH);
        D.resize(TOFH);

        for(int i=0; i< TOFH; i++)
        {
            d[i].resize(TOFW);
            u[i].resize(TOFW);
            v[i].resize(TOFW);
            D[i].resize(TOFW);
        }

        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
        cloud->resize (TOFH * TOFW);
        cloud->width = TOFW;
        cloud->height = TOFH;
        cloud->is_dense = false;

        int result;

        result = getd(fl, k1, k2, d, u, v);											// Initialize d, u, v. Only need to execute once
        D = getXYZ(fl, tof, cloud, D, d, u, v);							        	// Calculate x, y, z, D from phase map

        /*if(!cloud->points.empty())
        {
            pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
            if(!viewer->updatePointCloud(cloud, rgb))
            {
                cout<<"hier geraken we"<<endl;
                viewer->addPointCloud(cloud, rgb);
                viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1.0);
                viewer->addCoordinateSystem (1.0);
                viewer->initCameraParameters ();
            }
            viewer->spinOnce (1000);
*/
            pcl::PLYWriter plywriter;
            plywriter.write("depthsense.ply", *cloud, false);
  //      }


        g_dFrames++;

        cout<<"frame "<<g_dFrames<<endl;

        if (g_dFrames == 1)
        {
            //system("PAUSE");
            g_context.quit();
        }
    //}
    //teller++;
}

/*----------------------------------------------------------------------------*/
void configureDepthNode()
{
    g_dnode.newSampleReceivedEvent().connect(&onNewDepthSample);

    DepthNode::Configuration config = g_dnode.getConfiguration();

    config.frameFormat = FRAME_FORMAT_QVGA;
    config.framerate = 25;
    config.mode = DepthNode::CAMERA_MODE_CLOSE_MODE;
    config.saturation = true;

    try
    {
        g_context.requestControl(g_dnode,0);

        g_dnode.setConfiguration(config);

		g_dnode.setConfidenceThreshold (100);
		//g_dnode.setEnableAccelerometer (false);
		g_dnode.setEnableConfidenceMap (true);
		g_dnode.setEnableDenoising (true);
		g_dnode.setEnableDepthMap (false);
		g_dnode.setEnableDepthMapFloatingPoint (false);
		g_dnode.setEnablePhaseMap (true);
		g_dnode.setEnableUvMap (false);
		g_dnode.setEnableVertices(false);
		g_dnode.setEnableVerticesFloatingPoint (false);
		//g_dnode.setIlluminationLevel (100);
    }
    catch (ArgumentException& e)
    {
        printf("Argument Exception: %s\n",e.what());
    }
    catch (UnauthorizedAccessException& e)
    {
        printf("Unauthorized Access Exception: %s\n",e.what());
    }
    catch (IOException& e)
    {
        printf("IO Exception: %s\n",e.what());
    }
    catch (InvalidOperationException& e)
    {
        printf("Invalid Operation Exception: %s\n",e.what());
    }
    catch (ConfigurationException& e)
    {
        printf("Configuration Exception: %s\n",e.what());
    }
    catch (StreamingException& e)
    {
        printf("Streaming Exception: %s\n",e.what());
    }
    catch (TimeoutException&)
    {
        printf("TimeoutException\n");
    }

}

/*----------------------------------------------------------------------------*/
void configureNode(Node node)
{
    if ((node.is<DepthNode>())&&(!g_dnode.isSet()))
    {
        g_dnode = node.as<DepthNode>();
        configureDepthNode();
        g_context.registerNode(node);
    }
}

/*----------------------------------------------------------------------------*/
void onNodeConnected(Device device, Device::NodeAddedData data)
{
    configureNode(data.node);
}

/*----------------------------------------------------------------------------*/
void onNodeDisconnected(Device device, Device::NodeRemovedData data)
{
    if (data.node.is<DepthNode>() && (data.node.as<DepthNode>() == g_dnode))
        g_dnode.unset();
    printf("Node disconnected\n");
}

/*----------------------------------------------------------------------------*/
void onDeviceConnected(Context context, Context::DeviceAddedData data)
{
    if (!g_bDeviceFound)
    {
        data.device.nodeAddedEvent().connect(&onNodeConnected);
        data.device.nodeRemovedEvent().connect(&onNodeDisconnected);
        g_bDeviceFound = true;
    }
}

/*----------------------------------------------------------------------------*/
void onDeviceDisconnected(Context context, Context::DeviceRemovedData data)
{
    g_bDeviceFound = false;
    printf("Device disconnected\n");
}
