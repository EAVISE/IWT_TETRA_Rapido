
#ifndef __DEPTHSENSE_INCLUDED__
#define __DEPTHSENSE_INCLUDED__
#ifdef _MSC_VER
#include <windows.h>
#endif

#include <stdio.h>
#include <vector>
#include <exception>

#include <string.h>
#include <iostream>
#include <sys/io.h>

#include <opencv2/opencv.hpp>
#include <pcl/io/ply_io.h>
/*#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
*/
#include <DepthSense.hxx>

// Camera parameters
#define TOFW		320					// Set TOF camera resolution
#define TOFH		240
#define IRWS		16					// Set IR camera resolution
#define IRHS		16
#define UPSAMPLE	10					// Upsampling
#define IRWL		IRWS*UPSAMPLE		// Set IR upsampled camera resolution
#define IRHL		IRHS*UPSAMPLE


using namespace DepthSense;
using namespace std;
using namespace cv;


extern Context g_context;
extern DepthNode g_dnode;
extern uint32_t g_dFrames;

extern bool g_bDeviceFound;
extern int16_t tofdata[320*240*100];
extern int16_t confdata[320*240*100];
extern int16_t depthdata[320*240*100];

vector<vector<float> > getXYZ( float fl, Mat P, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, vector<vector<float> > D, vector<vector<float> > d, vector<vector<float> > u, vector<vector<float> > v);
int getd( float fl, float k1, float k2, vector<vector<float> > &d, vector<vector<float> > &u, vector<vector<float> > &v );
void onNewDepthSample(DepthNode node, DepthNode::NewSampleReceivedData data);
void configureDepthNode();
void configureNode(Node node);
void onNodeConnected(Device device, Device::NodeAddedData data);
void onNodeDisconnected(Device device, Device::NodeRemovedData data);
void onDeviceConnected(Context context, Context::DeviceAddedData data);
void onDeviceDisconnected(Context context, Context::DeviceRemovedData data);

#endif
