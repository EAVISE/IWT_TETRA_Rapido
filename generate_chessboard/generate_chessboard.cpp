#include <iostream>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <cstdlib>

using namespace std;
using namespace cv;
int main(int argc, char **argv)
{
    int blockSize=75;
    int cols = 5;
    int rows = 4;
    if(argc>=2);
        blockSize = atoi(argv[1]);


    if(argc >= 3)
        rows = atoi(argv[2])+1;

    if(argc >= 4)
        cols = atoi(argv[3])+1;

    int imageCols=blockSize*cols;
    int imageRows = blockSize*rows;

    Mat chessBoard(imageRows,imageCols,CV_8UC3,Scalar::all(0));

    unsigned char color=0;

    if(rows%2)
        color=~color;

    for(int i=0;i<imageRows;i=i+blockSize)
    {
        if(cols%2 == 0)
            color=~color;
        for(int j=0;j<imageCols;j=j+blockSize)
        {
            //cout<<i<<" "<<j<<endl;
            Mat ROI=chessBoard(Rect(j,i,blockSize,blockSize));
            //cout<<"color: "<<static_cast<unsigned>(color)<<endl;
            ROI.setTo(Scalar::all(color));
            color=~color;
            //cout<<"blokje gemaakt"<<endl;
        }
    }
    /*imshow("Chess board", chessBoard);
    waitKey(0);*/
    imwrite("chessboard.png", chessBoard);
}
