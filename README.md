Doel:
    Code voor verschillende 3D sensoren. Deze README bevat algemene info. Voor meer info, lees de README die bij elke sensor staat.
Libraries
    Libraries die in elk project worden gebruikt:
    Opencv 3.0.0
    PCL 1.8
    BOOST met linker flags -lboost_system en -lboost_thread
    
    Indien je parallel wilt werken dien je de compilerflag -lopenmp toe te voegen.
    
    Voor de volledige lijst libraries, functies en output van elk project : zie de correcte README