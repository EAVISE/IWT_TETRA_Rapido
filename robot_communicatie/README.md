Nodige libraries voor robot_communicatie:

-Opencv 3.0.0 : 
	in de CMakeLists.txt in de folder src/communicatie erop letten dat de OpenCV include directory juist is.
-PCL 1.8
-OpenMP
-Flycapture2 SDK 2.8.3.1

Voorwaarden:
	In de folder src/communicatie moeten de volgende folders staan: calib_sl, robot_sl0 en scan0.

Werking:
Open 2 terminals.
In beide terminals:
	cd ~/.../IWT_TETRA_RAPIDO/robot_communicatie
	source ./devel/setup.bash

in terminal 1:
	rosrun communicatie visie

in terminal 2: 
	rosrun communicatie robot

in terminal 1:
	volg de prompt en kies uit
	s: 	calibreer camera en projector. Telkens hij "Another?" moet je de calibratieplaat verleggen en "y" ingeven.
		Wanneer je genoeg calibratieposes hebt, geef "n" in.

	r: 	Werkt enkel correct indien je met robot_demo.cpp en alle toebehoren werkt. Beweegt de robot naar een plaats, geeft x,y,z van robot terug en scant dan de scene, op zoek naar een 			calibratiebord (schaakpatroon).
		Wanneer je robot.cpp gebruikt geeft deze functie nonsens terug voor x,y,z. Hierdoor zal je uiteindelijke robot - sensor calibratie niet kloppen.
		Op git staat een correcte transformatiematrix die gebruikt kan worden bij "c".

	c:	Scan de scene en bereken het 3D beeld. Wanneer je met robot.cpp werkt moet je in scanner.cpp aangeven dat je met de camera_scene wil verder gaan.
		Dit doe je in de functie scan() --> setScene(result); NIET set_scene(robot_scene). Als je toch robot_scene gebruikt, zal hij de camera_scene transformeren met de transformatie matrix die 			in stap "r" is uitgerekend. Daar er geen robot is, bevat deze absolute nonsens. Gebruik desnoods de transformatie matrix die op gitlab staat om toch de transformatie uit te voeren.
	
	p:	Gebruikt de point pair feature detector van Wim.
		Om deze correct te laten werken moet in de map ~/.../IWT_TETRA_RAPIDO/robot_communicatie/src/communicatie/model/ een .ply file geplaatst worden van het te zoeken model, MET normalen (kan 			via meshlab berekend worden).
		In de file ~/.../IWT_TETRA_RAPIDO/robot_communicatie/src/communicatie/settings_global.xml moet bij "model_path" het correcte path naar het model opgegeven worden, zodat deze uitgelezen kan 			worden.
		De functie zoekt de beste match van model in de scene en geeft deze terug als object van struct pickpoint.
		De functie schrijft de 5 beste matches weg als .ply, zodat in meshlab kan nagegaan worden waar deze vallen in de scene.


Structuur:
	
in visie.cpp:
	Wordt er een object scanner aangemaakt. Dit is een interface van alle mogelijke scanners.
	Elke scanner heeft zijn eigen subfuncties, die afhangen van de techniek waarmee gescand wordt.

in robot.cpp:
	Dit is een dummy file. Alle code voor de robot aan te sturen moet nog toegevoegd worden door de collega's van Diepenbeek. Deze dient gebruikt te worden indien men geen robot (aansturing) heeft en enkel wil scannen en/of pickpoints zoeken met de point pair feature techniek.
	Wanneer we de demo willen doen (niet aan te raden, daar niet alle packages en bibliotheken op elke pc staan) --> verander in CMakeLists.txt volgende lijn:
	add_executable(robot src/robot.cpp)
	door
	add_executable(robot src/robot_demo.cpp)

