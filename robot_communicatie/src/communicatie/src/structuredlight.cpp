#include "../include/communicatie/visie.hpp"
#include "../include/communicatie/structuredlight.hpp"
#include <bitset>
#include "../include/communicatie/detector.hpp"


int NOP_v;
int NOP_h;
int camera_width;
int camera_height;

/*
 *
 * OUT: decoder object
 *
 * Initiates a decoder
 *
 */
Decoder init_decoder()
{
    Decoder d;

    Mat newmat = Mat::zeros(camera_height, camera_width, CV_32FC1);

    ///initialize Decoder
    d.minimum.push_back(newmat.clone());
    d.minimum.push_back(newmat.clone());
    d.maximum.push_back(newmat.clone());
    d.maximum.push_back(newmat.clone());
    d.Ld.push_back(newmat.clone());
    d.Ld.push_back(newmat.clone());
    d.Lg.push_back(newmat.clone());
    d.Lg.push_back(newmat.clone());
    d.pattern_image.push_back(newmat.clone());
    d.pattern_image.push_back(newmat.clone());

    return d;
}

/*
 *
 * IN: NOP_v: number of vertical patterns
 * IN: NOP_h: number of horizontal patterns
 * IN: projector_width
 * IN: projector_height
 *
 * OUT: Vector of patterns
 *
 * Function generations patterns. The patterns follow a gray code sequence and the smallest patterns changes color every pixel.
 *
 */

vector<Mat> generate_pattern(int NOP_v, int NOP_h, int projector_width, int projector_height)
{
    vector<Mat> result;

    for(int i =0 ; i< NOP_h*2 + NOP_v*2 + 6; i++)
    {
        Mat newmat = Mat::zeros(projector_height, projector_width, CV_8UC1);
        result.push_back(newmat) ;
    }

    ///Generate vertical patterns
    int teller =0;
    for(int k=NOP_v; k>=-1; teller+=2, k--)
    {
        bool change = true;
        bool flag = true;
        for(int i = 0; i < projector_width; i++)
        {
            for(int j =0; j< projector_height; j++)
            {
                ///Fill in the column
                uchar pixel_color=0;
                if(flag)
                    pixel_color = 255;

                result[teller].at<uchar>( j, i ) = pixel_color;
                ///inverse

                if( pixel_color > 0 )
                    pixel_color = ( uchar ) 0;
                else
                    pixel_color = ( uchar ) 255;

                result[teller + 1].at<uchar>( j,i ) = pixel_color;  // inverse
            }

            int macht = pow(2, k);

            if(k>=0)
            {
                if(i%macht == 0 && i != 0)
                {
                    if(change)
                    {
                        flag = !flag;
                        change = false;
                    }
                    else
                        change = true;
                }
            }
            else
            {
                flag = !flag;
            }
        }
    }

    ///Generate Horizontal patterns
    for(int k=NOP_h-1; k>=-1; teller +=2, k--)
    {
        bool change = true;
        bool flag = true;
        for(int i = 0; i < projector_height; i++)
        {
            for(int j =0; j< projector_width; j++)
            {
                ///Fill in the row
                uchar pixel_color=0;
                if(flag)
                    pixel_color = 255;
                result[teller].at<uchar>( i, j ) = pixel_color;

                ///inverse
                if( pixel_color > 0 )
                    pixel_color = ( uchar ) 0;
                else
                    pixel_color = ( uchar ) 255;

                result[teller + 1].at<uchar>( i, j ) = pixel_color;  // inverse
            }
            int macht = pow(2,k);

            if(k>=0)
            {
                if(i%macht == 0 && i != 0)
                {
                    if(change)
                    {
                        flag = !flag;
                        change = false;
                    }
                    else
                        change = true;
                }
            }
            else
            {
                flag = !flag;
            }
        }
    }
    return result;
}

/*
 *
 * When working with a vimba camera, uncomment this function and call it in get sl_images()
 *
 */

/*bool get_vimba(int delay, string path, int serie, int width, int height)
{
    vector<Mat> pattern;
    cout<<"serienummer: "<<serie<<endl;

    int vertical_patterns=0;
    ///Get dimensions
    while(pow(2, vertical_patterns) < width)
    {
        vertical_patterns++;
    }
    NOP_v = vertical_patterns;

    ///Do the same for the horizontal patterns
    int horizontal_patterns=0;
    while(pow(2, horizontal_patterns) < height)
    {
        horizontal_patterns++;
    }
    NOP_h = horizontal_patterns;

    CameraPtr camera ;
    FramePtr frame;
    char * pCameraID = NULL;

    VimbaSystem & system = VimbaSystem :: GetInstance ();
    if ( !VmbErrorSuccess == system.Startup () )
    {
        cout<<"system startup failed"<<endl;
    }
    else
        cout<<"system started"<<endl;

    std::string strCameraID;
    if(NULL == pCameraID)
    {
        CameraPtrVector cameras;
        system.GetCameras(cameras);
        if(cameras.size() <= 0)
        {
            cout<<"no camera found"<<endl;
            return false;
        }
        else
        {
            cameras[0]->GetID(strCameraID);
        }
    }
    else
    {
        strCameraID = pCameraID;
    }

    VmbErrorType res = system.OpenCameraByID(strCameraID.c_str(), VmbAccessModeFull, camera);
    if(VmbErrorSuccess == res)
        cout<<"Camera geopend"<<endl;

    pattern = generate_pattern(NOP_v, NOP_h, width, height);
    cout<<"Pattern generated"<<endl;

    int number_of_patterns = (NOP_h + NOP_v)*2 + 2;

    ///Save patterns
    /*vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION );
    //Kies 0 om geen compressie door te voeren
    compression_params.push_back(0);

    for(int i=0; i<number_of_patterns; i++)
    {
        ostringstream stm ;
        stm << i ;
        try {
            imwrite("patroon" + stm.str()+ ".png", pattern[i], compression_params);
        }
        catch (int runtime_error){
            fprintf(stderr, "Exception converting image to JPPEG format: %s\n");
            return 1;
        }
    }

    for(int i = 0; i<number_of_patterns; i++)
    {
        string Result;
        ostringstream convert;
        convert << i;
        ///Project pattern full screen via projector
        namedWindow( "pattern", CV_WINDOW_NORMAL );
        setWindowProperty("pattern", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
        moveWindow("pattern", 0, 0);
        imshow("pattern", pattern[i]);
        waitKey(delay);

        ///Read from camera
        FeaturePtr pCommandFeature;
        res = camera->GetFeatureByName("GVSPAdjustPacketSize", pCommandFeature );
        if ( VmbErrorSuccess == pCommandFeature->RunCommand() )
        {
            bool bIsCommandDone = false;
            do
            {
                if ( VmbErrorSuccess != pCommandFeature->IsCommandDone( bIsCommandDone ))
                {
                    break;
                }
            }
            while ( false == bIsCommandDone );
        }

        FeaturePtr pFormatFeature;
        // Set pixel format. For the sake of simplicity we only support Mono and BGR in this example.
        res = camera->GetFeatureByName( "PixelFormat", pFormatFeature );
        if ( VmbErrorSuccess == res )
        {
            // Try to set BGR
            res = pFormatFeature->SetValue( VmbPixelFormatRgb8 );
            if ( VmbErrorSuccess != res )
            {
                // Fall back to Mono
                res = pFormatFeature->SetValue( VmbPixelFormatMono8 );
            }

            if ( VmbErrorSuccess == res )
            {
                // Acquire
                res = camera->AcquireSingleImage( frame, 5000 );
            }
        }

        VmbUint32_t nImageSize = 0;
        VmbErrorType err = frame->GetImageSize( nImageSize );

        VmbUint32_t nWidth = 0;
        err = frame->GetWidth( nWidth );

        VmbUint32_t nHeight = 0;
        err = frame->GetHeight( nHeight );

        VmbUchar_t *pImage = NULL;
        err = frame->GetImage(pImage );

        AVTBitmap bitmap;
        bitmap.colorCode = ColorCodeMono8;
        bitmap.bufferSize = nImageSize;
        bitmap.width = nWidth;
        bitmap.height = nHeight;
        AVTCreateBitmap( &bitmap, pImage );

        string pFileName;
        ///Save camera image
        pFileName = path + "/frame" + convert.str()+ ".bmp";

        AVTWriteBitmapToFile( &bitmap, pFileName.c_str() );
    }

    return true;
}*/

/*
 * Necessary function for flycapture library
 */
void PrintError( FlyCapture2::Error error )
{
    error.PrintErrorTrace();
}

/*
 *
 * IN: delay: how long to wait between patterns. Unused.
 * IN: path: location of to be saved images
 * IN: serie: number of the serie of images being taken
 * IN: width: width of the projector
 * IN: height: height of the projector
 *
 * Get an image of every projected image. Only used when scanning with HDMI
 *
 */
bool get_pointgrey(int delay, string path, int serie, int width, int height)
{
    vector<Mat> pattern;
    cout<<"serienummer: "<<serie<<endl;

    int vertical_patterns=0;
    ///Get dimensions
    while(pow(2, vertical_patterns) < width)
    {
        vertical_patterns++;
    }
    NOP_v = vertical_patterns;

    ///Do the same for the horizontal patterns
    int horizontal_patterns=0;
    while(pow(2, horizontal_patterns) < height)
    {
        horizontal_patterns++;
    }
    NOP_h = horizontal_patterns;

    ///Open camera
    FlyCapture2::Error error;

    BusManager busMgr;
    unsigned int numCameras;
    error = busMgr.GetNumOfCameras(&numCameras);
    if (error != PGRERROR_OK)
    {
        cout<<"No cameras found"<<endl;
        return -1;
    }

    if ( numCameras < 1 )
    {
        cout << "Insufficient number of cameras... exiting" << endl;
        //return -1;
    }

    PGRGuid guid;
    error = busMgr.GetCameraFromIndex(0, &guid);
    if (error != PGRERROR_OK)
    {
        cout<<"No index retrieved"<<endl;
        //return -1;
    }

    FlyCapture2::Camera cam;

    // Connect to a camera
    error = cam.Connect(&guid);
    if (error != PGRERROR_OK)
    {
        cout<<"Camera not connected"<<endl;
        //return -1;
    }

    // Get the camera information
    CameraInfo camInfo;
    error = cam.GetCameraInfo(&camInfo);
    if (error != PGRERROR_OK)
    {
        cout<<"No camera info retrieved"<<endl;
        //return -1;
    }
    cout<<"generating pattern"<<endl;
    pattern = generate_pattern(NOP_v, NOP_h, width, height);
    cout<<"Pattern generated"<<endl;

    int number_of_patterns = pattern.size();

    ///Save patterns
    /*vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION );
    //Kies 0 om geen compressie door te voeren
    compression_params.push_back(0);

    for(int i=0; i<number_of_patterns; i++)
    {
        ostringstream stm ;
        stm << i ;
        try {
            imwrite("./patronen_sl/patroon" + stm.str()+ ".png", pattern[i], compression_params);
        }
        catch (int runtime_error){
            fprintf(stderr, "Exception converting image to JPPEG format: %s\n");
            return 1;
        }
    }
    */
    vector<Mat> beelden;

    for(int i = 0; i<number_of_patterns; i++)
    {
        string Result;
        ostringstream convert;
        convert << i;
        ///Project pattern full screen via projector
        namedWindow( "pattern", CV_WINDOW_NORMAL );
        setWindowProperty("pattern", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
        moveWindow("pattern", 0, 0);
        imshow("pattern", pattern[i]);
        waitKey(delay);

        ///Read from camera
        // Start capturing images
        if(i==0)
        {
            error = cam.StartCapture();
            if (error != PGRERROR_OK)
            {
                PrintError(error);
            }
            // Grab images
            Image rawImage;
            error = cam.RetrieveBuffer(&rawImage);
            if (error != PGRERROR_OK)
            {
                cout << "Error grabbing image " << endl;
            }

            // Stop capturing images
            error = cam.StopCapture();
            if (error != PGRERROR_OK)
            {
                PrintError(error);
            }
        }

        error = cam.StartCapture();
        if (error != PGRERROR_OK)
        {
            PrintError(error);
            return -1;
        }

        // Grab images
        Image rawImage;
        error = cam.RetrieveBuffer(&rawImage);
        if (error != PGRERROR_OK)
        {
            cout << "Error grabbing image " << endl;
            continue;
        }


        // Stop capturing images
        error = cam.StopCapture();
        if (error != PGRERROR_OK)
        {
            PrintError(error);
            return -1;
        }

        ///Save camera image

        FlyCapture2::Image cf2Img;
        rawImage.Convert(FlyCapture2::PIXEL_FORMAT_BGR, &cf2Img );
        unsigned int rowBytes = (double)cf2Img.GetReceivedDataSize()/(double)cf2Img.GetRows();
        cv::Mat cvImage = cv::Mat( cf2Img.GetRows(), cf2Img.GetCols(), CV_8UC3, cf2Img.GetData(), rowBytes );

        string plek = path + "/frame" + convert.str()+ ".bmp";
        imwrite(plek, cvImage);
        //tonen(cvImage, "test");
    }

    error = cam.Disconnect();
    if (error != PGRERROR_OK)
    {
        cout<<"Camera not disconnected"<<endl;
        return -1;
    }

    return true;
}

/*
 *
 * Function used to choose between Vimba camera and pointgrey. Unused
 *
 */
bool get_sl_images(int delay, string path, int serie, int width, int height)
{
    /*char antwoord;
    cout<<"Vimba?[y/n] ";
    cin>>antwoord;

    if(antwoord=='y')
        return get_vimba(delay, path, serie, width, height);

    else*/
    return get_pointgrey(delay, path, serie, width, height);


}

/*
 *
 * INOUT: chessboardcorners: Used to save value of every chessboard corner of every serie
 * IN: path: location of images containing the series
 * IN: aantalseries: Number of series
 * IN: width: projector width
 * IN: height: projector height
 * IN: boardw: width of chessboard to be found
 * IN: boardh: height of chessboard to be found
 *
 * OUT: true if succes, false if failure
 *
 * Open the fully lit image of every series and try to find the chessboardcorners.
 * If they are not found, they are excluded from the list
 *
 */

bool findcorners(vector<vector<Point2f> > &chessboardcorners, string path, int aantalseries, int width, int height, int boardw, int boardh)
{
    Mat board;

    Size boardSize(boardw, boardh);

    int vertical_patterns=0;
    ///Get dimensions
    while(pow(2, vertical_patterns) < width)
    {
        vertical_patterns++;
    }
    NOP_v = vertical_patterns;

    ///Do the same for the horizontal patterns
    int horizontal_patterns=0;
    while(pow(2, horizontal_patterns) < height)
    {
        horizontal_patterns++;
    }
    NOP_h = horizontal_patterns;

    vector<int> plek;
    bool found1_1;

    for(int i = 0; i<aantalseries; i++)
    {
        cout<<"finding corners image "<<i<<endl;
        ostringstream conv;
        conv << i;

        string pad = path + conv.str()+"/frame0.bmp";
        board = imread(pad, 0);

        if(board.empty())
        {
            cout<<"No image found"<<i<<endl;
            continue;
        }

        found1_1 = findChessboardCorners(board, boardSize, chessboardcorners[i],CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE /*| CALIB_CB_FAST_CHECK*/);

        if(!found1_1)
        {
            std::cout<<"Corners niet gevonden"<<std::endl;
        	plek.push_back(i);
        }
        else
        {

        	cornerSubPix(board, chessboardcorners[i], Size(11,11), Size(-1,-1), TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 300, 0.001));
        	cvtColor(board, board, CV_GRAY2BGR);
        	drawChessboardCorners( board, boardSize, chessboardcorners[i], found1_1 );
        	//tonen(board, "zijn ze gevonden?");
        }
    }

    if(plek.size() > 0)
    {
        for(int i = 0; i<plek.size(); i++)
            std::cerr << "Checkboard 1_"<<plek[i]<<" corners not found!" << std::endl;
        return false;
    }

    return true;
}

/*
 *
 * Used to determine of a value is ok according to the algorythm
 *
 */
int check_bit(float value1, float value2, float Ld, float Lg, float m)
{
    if (Ld < m)
    {
        return 2;
    }
    if (Ld>Lg)
    {
        if( value1>value2)
            return 1;
        else
            return 0;
    }
    if (value1<=Ld && value2>=Lg)
    {
        return 0;
    }
    if (value1>=Lg && value2<=Ld)
    {
        return 1;
    }

    return 2;
}

/*
 *
 * INOUT: d: Decoder d is used to save the direct and global light components
 * IN: calibration images of one serie
 * IN: dir: direction of the patterns (1 = vertical, 0 = horizontal)
 * IN: b: parameter to determine global or direct light component
 *
 * Calculate the global and direct light component of every pixel
 *
 */
void calculate_light_components(Decoder &d, vector<Mat> beelden, int dir, float b)
{
    int x = beelden[0].cols;
    int y = beelden[0].rows;
    int NOP;
    string richting;
    if(dir)
    {
        NOP = 2*NOP_v-1; //number of highest frequency vertical pattern
        richting="vert";
    }
    else
    {
        NOP = 2*NOP_v + 2*NOP_h-3; //number of highest frequency horizontal pattern
        richting="hor";
    }
    string LD = "ld"+ richting;
    string LG = "lg"+richting;
#pragma omp parallel for
    for(int i =0; i< y; i++)
    {
        for(int j =0; j<x; j++)
        {
            float lpmax=0.0;
            float lpmin=1000.0;
            for(int k = 2; k< 8; k++)
            {
                if(beelden[NOP-k].at<float>(i,j) < lpmin)
                    lpmin = beelden[NOP-k].at<float>(i,j);
            }

            for(int k = 2; k < 8; k++)
            {
                if(beelden[NOP-k].at<float>(i,j) > lpmax)
                    lpmax = beelden[NOP-k].at<float>(i,j);
            }

            float _d =(lpmax - lpmin) / (1 - b);
            float g = (2*(lpmin - b*lpmax) / (1- pow(b,2)));

            d.Ld[dir].at<float>(i,j) = (g>0 ? _d : lpmax);
            d.Lg[dir].at<float>(i,j) = (g>0 ? g : 0);
        }
    }
}

/*
 *
 * INOUT: d: Decoder d is used to save the decoded patterns
 * IN: calibration images of one serie
 * IN: dir: direction of the patterns (1 = vertical, 0 = horizontal)
 * IN: m: parameter to determine pattern code
 * IN: threshold: parameter to determine validity of the pixel
 *
 * Function loops over every pixel of every image in the serie and builds a graycode sequence for every pixel.
 * If a pixel is invalid, it gets the value = 2*(pow(2, NOP_v+5)
 *
 */
void get_pattern_image(Decoder &d, vector<Mat> beelden, int dir, float m, float thresh)
{
    int holder;
    int bit;
    int NOP;
    int start;
    string richting;
    float min_slecht = pow(2, NOP_v+5);

    if(dir)
    {
        start = 0;
        holder = NOP_v;
        NOP = 2*NOP_v+1; //number of highest frequency vertical pattern
        richting = "vert";
    }
    else
    {
        holder = NOP_h;
        NOP = 2*NOP_v+1 + 2*NOP_h+2; //number of highest frequency horizontal pattern
        start = 2*NOP_v+2;
        richting = "hor";
    }

    bit = holder;
#pragma omp parallel for
    for(int i = start; i<=NOP ; i+=2)
    {
        for(int j =0; j< beelden[NOP].rows; j++)
        {
            for(int k =0; k<beelden[NOP].cols; k++)
            {
                float val1 = beelden[i+1].at<float>(j,k);
                float val2 = beelden[i].at<float>(j,k);

                ///save min and max of every pixel in every image pair
                if(val1 < d.minimum[dir].at<float>(j,k) || val2 < d.minimum[dir].at<float>(j,k))
                    d.minimum[dir].at<float>(j,k) = (val1<val2 ? val1 : val2);

                if(val1> d.maximum[dir].at<float>(j,k) || val2> d.maximum[dir].at<float>(j,k))
                    d.maximum[dir].at<float>(j,k) = (val1>val2 ? val1 : val2);
            }
        }
    }

    ///Go over each image pair (img and his inverse) and check for each pixel it's value. Add it to pattern[dir]
    /// If a pixel is in a lighted area, we add 2 raised to the power of (k - the frame number). This way we get a gray code pattern for each pixel in pattern[dir].
    /// Pattern[dir] is of type 32 bit float. If you have more than 32 patterns, change the type of pattern[dir].

    for(int i = start; i<=NOP ; i+=2)
    {

        Mat beeld1 = beelden[i+1];
        Mat beeld2 = beelden[i];
#pragma omp parallel for
        for(int j =0; j< beelden[i+1].rows; j++)
        {
            for(int k =0; k<beelden[i+1].cols; k++)
            {
                float val1 = beeld1.at<float>(j,k);
                float val2 = beeld2.at<float>(j,k);
                float ld = d.Ld[dir].at<float>(j,k);
                float lg = d.Lg[dir].at<float>(j,k);

                ///build pattern

                int p = check_bit(val1, val2, ld, lg, m);
                if(p == 2)
                {
                    d.pattern_image[dir].at<float>(j,k) += (p<<(NOP_v+5)); //p*(pow(2, NOP_v+5));
                }
                else
                {
                    d.pattern_image[dir].at<float>(j,k) +=  (p<<bit); //p*(pow(2, bit));
                }
            }
        }
        bit--;
    }
    ///Convert pattern from gray code to binary
    for(int i=0;i<d.pattern_image[dir].rows;i++)
    {
        for(int j=0; j<d.pattern_image[dir].cols;j++)
        {
            if (d.pattern_image[dir].at<float>(i,j) < min_slecht )
            {   //invalid value: use grey
                int q = static_cast<int>(d.pattern_image[dir].at<float>(i,j));
                int p = q;

                for (unsigned shift = 1; shift < holder; shift <<= 1)
                {
                    p ^= p >> shift;
                }
                d.pattern_image[dir].at<float>(i,j) = p + (d.pattern_image[dir].at<float>(i,j) - q);
                if(dir)
                    d.pattern_image[dir].at<float>(i,j)--;
            }

            if((d.maximum[dir].at<float>(i,j) - d.minimum[dir].at<float>(i,j)) < thresh)
            {
                d.pattern_image[dir].at<float>(i,j) = 2 << (NOP_v+5);// 2*(pow(2, NOP_v+5));
            }
        }
    }
}

/*
 *
 * Colorize pattern. Every code has a unique color. This depends on the direction of the patterns (horizontal of vertical)
 * This function is not necessary, it only exists to visualize results for humans.
 *
 */
void colorize_pattern(Decoder &d, vector<Mat> &image, int dir, int projector_width, int projector_height)
{
    int NOP;
    float max_t = 0;

    int effective_width = projector_width;
    int effective_height = projector_height;
    for(int i = 0; i< d.pattern_image[dir].rows; i++)
    {
        for(int j = 0; j< d.pattern_image[dir].cols; j++)
        {
            if (d.pattern_image[dir].at<float>(i,j) >= (pow(2, NOP_v+5)))
            {
                continue;
            }
            max_t = (d.pattern_image[dir].at<float>(i,j) > max_t ? d.pattern_image[dir].at<float>(i,j) : max_t);
        }
    }

    float n = 4.f;
    float dt = 255.f/n;

    for(int i = 0; i< d.pattern_image[dir].rows; i++)
    {
        for(int j = 0; j< d.pattern_image[dir].cols; j++)
        {
            if (d.pattern_image[dir].at<float>(i,j) >= (pow(2, NOP_v+5)))
            {   //invalid value: use grey
                image[dir].at<Vec3b>(i,j) = Vec3b(128, 128, 128);
                continue;
            }


            //display
            float t = d.pattern_image[dir].at<float>(i,j)*255.f/max_t;
            float c1 = 0.f, c2 = 0.f, c3 = 0.f;
            if (t<=1.f*dt)
            {   //black -> red
                float c = n*(t-0.f*dt);
                c1 = c;     //0-255
                c2 = 0.f;   //0
                c3 = 0.f;   //0
            }
            else if (t<=2.f*dt)
            {   //red -> red,green
                float c = n*(t-1.f*dt);
                c1 = 255.f; //255
                c2 = c;     //0-255
                c3 = 0.f;   //0
            }
            else if (t<=3.f*dt)
            {   //red,green -> green
                float c = n*(t-2.f*dt);
                c1 = 255.f-c;   //255-0
                c2 = 255.f;     //255
                c3 = 0.f;       //0
            }
            else if (t<=4.f*dt)
            {   //green -> blue
                float c = n*(t-3.f*dt);
                c1 = 0.f;       //0
                c2 = 255.f-c;   //255-0
                c3 = c;         //0-255
            }
            image[dir].at<Vec3b>(i,j) = Vec3b(c3, c2,c1);


        }
    }
    if(dir)
        tonen(image[dir], "kleurver");
    else
        tonen(image[dir], "kleurhor");

}

/*
 *
 * INOUT: d: decoder d will be used to save decoded pattern images, global and direct light components
 * IN: draw: If you want to colorize the pattern images, draw must be true
 * IN: path: location of the series
 * IN: b: parameter for the structured light algorithm
 * IN: m: parameter for the structured light algorithm
 * IN: thresh: threshold to separate valid pixels from noise
 * IN: p_w: projector width
 * IN: p_h: projector height
 *
 * Function calls to decode one serie
 *
 */
bool decode(int serienummer, Decoder &d, bool draw, string path, float b, float m, float thresh, int p_w, int p_h)
{
    vector<Mat> beelden ;
    ostringstream serienr;
    serienr << serienummer;
    string pad;
    string einde = ".bmp";
    struct timeval tv1, tv2; struct timezone tz;
    gettimeofday(&tv1, &tz);

    pad = path + serienr.str() + "/frame";
    ///Reading every image in a serie
    for(int i=2; i<=(NOP_v+NOP_h)*2+5; i++)
    {
        ostringstream beeldnr;
        beeldnr << i;
        Mat newmat_i = imread(pad + beeldnr.str() + einde,0);
        if(newmat_i.empty())
            cout<<"no image"<<endl;

        Mat newmat_float;
        newmat_i.convertTo(newmat_float, CV_32FC1);
        beelden.push_back(newmat_float);
    }

    gettimeofday(&tv2, &tz);
    printf( "inlezen duurt %12.4g sec\n", (tv2.tv_sec-tv1.tv_sec) + (tv2.tv_usec-tv1.tv_usec)*1e-6 );

    camera_width = beelden[0].cols;
    camera_height = beelden[0].rows;

    d = init_decoder();

    Mat newmat2 = Mat::zeros(camera_height, camera_width, CV_8UC3);
    vector<Mat> image;
    image.push_back(newmat2.clone());
    image.push_back(newmat2.clone());

    ///Get vertical
    int vertical = 1;
    calculate_light_components(d, beelden, vertical, b);
    get_pattern_image(d, beelden, vertical, m, thresh);


    ///Get Horizontal
    int horizontal = 0;
    calculate_light_components(d, beelden, horizontal, b);
    get_pattern_image(d, beelden, horizontal, m, thresh);

    ///Draw patterns?
    if(draw)
    {
        colorize_pattern(d, image, horizontal, p_w, p_h);
        colorize_pattern(d, image, vertical, p_w, p_h);
    }
    return true;
}

/*
 *
 * INOUT: d: decoder d will be used to save decoded pattern images, global and direct light components
 * IN: draw: If you want to colorize the pattern images, draw must be true
 * IN: path: location of the series
 * IN: b: parameter for the structured light algorithm
 * IN: m: parameter for the structured light algorithm
 * IN: thresh: threshold to separate valid pixels from noise
 * IN: p_w: projector width
 * IN: p_h: projector height
 *
 * OUT: succes or failure
 *
 * Function calls to decode all series
 *
 */

bool decode_all(int aantalseries, vector<Decoder> &dec, bool draw, string path, float b, float m, float thresh, int projector_width, int projector_height)
{
    bool gelukt;
    for(int i=0; i< aantalseries; i++)
    {
        Decoder d;
        cout<<"decode serie "<<i<<" ";
        gelukt = decode(i, d, draw, path, b, m, thresh, projector_width, projector_height);
        cout<<gelukt<<endl;
        if(!gelukt)
            break;

        dec.push_back(d);
        cout<<endl;
    }
    if(!gelukt)
        return false;

    return true;
}

/*
 *
 * IN: aantalseries: number of calibration series
 * IN: dec: The decoders of every serie
 * IN: draw: Unused
 * IN: path: Unused
 * IN: b: parameter for the structured light algorithm
 * IN: m: parameter for the structured light algorithm
 * IN: thresh: threshold to separate valid pixels from noise
 * IN: projector_width: projector width
 * IN: projector_height: projector height
 *
 * OUT: succes or failure
 *
 * Function uses the found chessboardcorners and the decoded patterns to calibrate camera, projector and the camera and the projector as stereo couple
 * It saves all the necessary matrices in camera_projector.xml
 *
 */
bool calibrate_sl(vector<Decoder> dec, vector<vector<Point2f> > corners, int aantalseries, int projector_width, int projector_height, int boardw, int boardh)
{
    clock_t time1 = clock();
    vector<vector<Point2f> > pcorners;
    int window = 10; ///the value for window is halve the size of the desired homography window

    vector<vector<Point3f> > objectpoints;
    vector<cv::Point3f> world_corners;


    for(int h=0; h<boardh; h++)
    {
        for (int w=0; w<boardw; w++)
        {
            world_corners.push_back(cv::Point3f(26.8 * w, 26.8 * h, 0.f));
        }
    }

    for(int k=0; k<aantalseries;k++)
    {
        Decoder d = dec[k];
        vector<Point2f> c = corners[k];
        vector<Point2f> proj;

        for(uint l = 0; l< c.size(); l++) //for each chessboard corner
        {
            vector<Point2f> cam_points;
            vector<Point2f> proj_points;
            if(c[l].x >= window && c[l].y >= window && c[l].x + window < camera_width && c[l].y + window<camera_height)
            {
                for(int x = c[l].x - window; x<=c[l].x + window; x++)  //for each point in the window around the corner
                {
                    for(int y = c[l].y - window; y<=c[l].y + window; y++)
                    {
                        if (d.pattern_image[0].at<float>(y,x) >= (pow(2, NOP_v+5)) || d.pattern_image[1].at<float>(y,x) >= (pow(2, NOP_v+5)))
                        {
                            continue;
                        }
                        else
                        {
                            cam_points.push_back(Point2f(x,y));
                            proj_points.push_back(Point2f(d.pattern_image[1].at<float>(y,x), d.pattern_image[0].at<float>(y,x))); ///pattern point is (vertical pattern, horizontal pattern)
                        }
                    }
                }
                Mat homography = findHomography(cam_points, proj_points, CV_RANSAC, 10 );
                Point3d p = Point3d(c[l].x, c[l].y, 1.0);
                Point3d Q = Point3d(Mat(homography*Mat(p)));
                Point2f q = Point2f(Q.x/Q.z, Q.y/Q.z);
                proj.push_back(q);
            }
            else
            {
                cout<<"chessboard corners are too close to the edge, fatal error"<<endl;
                exit(-1);
            }
        }
        objectpoints.push_back(world_corners);
        pcorners.push_back(proj);
    }

    int cal_flags = 0
            //+ CV_CALIB_FIX_PRINCIPAL_POINT
            //+ cv::CALIB_FIX_K1
            //+ cv::CALIB_FIX_K2
            //+ cv::CALIB_ZERO_TANGENT_DIST
            + cv::CALIB_FIX_K3
            ;
    Size imageSize = dec[0].pattern_image[0].size();
    vector<Mat> cam_rvecs, cam_tvecs;
    Mat cam_mat;
    Mat cam_dist;
    int cam_flags = cal_flags;
    double cam_error = cv::calibrateCamera(objectpoints, corners, imageSize, cam_mat, cam_dist, cam_rvecs, cam_tvecs, cam_flags,
                                           TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5));

    cout<<"camera calibration RMS error: "<<cam_error<<endl;

    ///calibrate the projector
    vector<cv::Mat> proj_rvecs, proj_tvecs;
    Mat proj_mat;
    Mat proj_dist;
    int proj_flags = cal_flags;
    Size projector_size(projector_width, projector_height);
    double proj_error = cv::calibrateCamera(objectpoints, pcorners, projector_size, proj_mat, proj_dist, proj_rvecs, proj_tvecs, proj_flags,
                                            TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5));
    cout<<"projector calibration RMS error: "<<proj_error<<endl;

    ///stereo calibration
    Mat R, T, E, F;
    double stereo_error = cv::stereoCalibrate(objectpoints, corners, pcorners, cam_mat, cam_dist, proj_mat, proj_dist,
                                              imageSize , R, T, E, F//,
                                              //CV_CALIB_FIX_INTRINSIC+
                                              //CV_CALIB_USE_INTRINSIC_GUESS
                                              //CV_CALIB_FIX_ASPECT_RATIO +
                                              //CV_CALIB_ZERO_TANGENT_DIST +
                                              //CV_CALIB_SAME_FOCAL_LENGTH +
                                              //CV_CALIB_RATIONAL_MODEL +
                                              /*CV_CALIB_FIX_K3 + CV_CALIB_FIX_K4 + CV_CALIB_FIX_K5,
                                                                                                                                                                              TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5)*/);
    //cout<<"Stereo RMS error: "<<stereo_error<<endl;
    clock_t time2 = clock();
    cout<<"tijd calib: "<< (float)(time2-time1)/CLOCKS_PER_SEC<<endl;
    ///Save calibration:

    FileStorage fs("./src/communicatie/SL/calib_sl/camera_projector.xml", FileStorage::WRITE);
    fs << "fundamental" << F;
    fs << "rotation" << R;
    fs << "translation" << T;
    fs << "essential" << E;
    fs << "camera_matrix" << cam_mat;
    fs << "projector_matrix" << proj_mat;
    fs << "distortion_camera" << cam_dist;
    fs << "distorion_projector" << proj_dist;
    fs.release();

    cout<<"Stereo RMS error: "<<stereo_error<<endl;

    return true;
}

/*
 *
 * IN: path: location of scan serie
 * IN: aantalseries: Number of series. is normally 1, unless you want to calculate multiple scenes
 * IN: b: parameter for the structured light algorithm
 * IN: m: parameter for the structured light algorithm
 * IN: thresh: threshold to separate valid pixels from noise
 * IN: projector_width: projector width
 * IN: projector_height: projector height
 *
 * OUT: cv::Mat containing the 3D scene. Dim mat: 4xN
 *
 * Function calls for calculating a 3D scene when given the scan images. It decodes and triangulates the points.
 *
 */
Mat calculate3DPoints_all(string path, int aantalseries, float b, float m, float thresh, int projector_width, int projector_height)
{
    int vertical_patterns=0;
    ///Get dimensions
    while(pow(2, vertical_patterns) < projector_width)
    {
        vertical_patterns++;
    }
    NOP_v = vertical_patterns;

    ///Do the same for the horizontal patterns
    int horizontal_patterns=0;
    while(pow(2, horizontal_patterns) < projector_height)
    {
        horizontal_patterns++;
    }
    NOP_h = horizontal_patterns;

    bool draw = false;
    vector<Decoder> dec;
    struct timeval tv1, tv2; struct timezone tz;
    gettimeofday(&tv1, &tz);
    decode_all(aantalseries, dec, draw, path, b, m, thresh, projector_width, projector_height);
    gettimeofday(&tv2, &tz);
    printf( "decode duurt %12.4g sec\n", (tv2.tv_sec-tv1.tv_sec) + (tv2.tv_usec-tv1.tv_usec)*1e-6 );

    ///Get intrinsic and extrinsic camera parameters
    FileStorage fs("src/communicatie/SL/calib_sl/camera_projector.xml", FileStorage::READ);
    Mat cameraMatrix;
    Mat projMatrix;

    Mat rotMat;
    Mat transMat;

    fs["rotation"] >> rotMat;
    if(rotMat.empty())
        cout<<"rotleeg"<<endl;

    fs["translation"] >> transMat;

    if(transMat.empty())
        cout<<"tranleeg"<<endl;

    Mat projmat1 = Mat::eye(Size(4,3), CV_64F);
    for(int i=0; i<projmat1.rows; i++)
    {
        for(int j=0; j<projmat1.cols; j++)
        {
            if(j==i && j<3)
            {
                projmat1.at<double>(i,j)=1;
            }
            else
            {
                projmat1.at<double>(i,j)=0;
            }
        }
    }

    Mat projmat2 = Mat::eye(Size(4,3), CV_64F);
    for(int i=0; i<projmat2.rows; i++)
    {
        for(int j=0; j<projmat2.cols; j++)
        {
            if(j<3)
            {
                projmat2.at<double>(i,j)=rotMat.at<double>(i,j);
            }
            else
            {
                projmat2.at<double>(i,j)=transMat.at<double>(i,0);
            }
        }
    }

    fs["camera_matrix"] >> cameraMatrix;
    fs["projector_matrix"] >> projMatrix;

    if(cameraMatrix.empty())
        cout<<"CameraMatrix leeg"<<endl;
    if(projMatrix.empty())
        cout<<"projMatrix leeg"<<endl;

    Mat result;
    Mat pc;

    //#pragma omp parallel for
    for(int k = 0; k< aantalseries; k++)
    {
        Decoder d = dec[k];
        vector<Point2d> cam_points;
        vector<Point2d> proj_points;
        vector<Paar> combinaties;

        Mat hory = d.pattern_image[0];
        Mat verx = d.pattern_image[1];
        float max_hor = 0;
        float max_ver = 0;

        for(int i = 0; i< hory.rows; i++)
        {
            for(int j = 0; j< hory.cols; j++)
            {
                if (hory.at<float>(i,j) >= (pow(2, NOP_v+5)) || verx.at<float>(i,j) >= (pow(2, NOP_v+5)))
                {
                    continue;
                }
                max_hor = (hory.at<float>(i,j) > max_hor ? hory.at<float>(i,j) : max_hor);
                max_ver = (verx.at<float>(i,j) > max_ver ? verx.at<float>(i,j) : max_ver);
            }
        }

        struct timeval tv4, tv5, tv6; struct timezone tz;
        gettimeofday(&tv4, &tz);

        vector<vector<vector<Point2d> > > punten;///punten[x][y][z]
        punten.resize(max_ver+1);
        for(int i=0; i<punten.size(); i++)
        {
            punten[i].resize(max_hor+1);
            for(int j=0; j< punten[i].size(); j++)
            {
                punten[i][j].reserve(10);
            }
        }

        int tel = 0;
        for(int x = 0; x<camera_width; x++)
        {
            for(int y = 0; y<camera_height; y++)
            {
                if (hory.at<float>(y,x) >= (pow(2, NOP_v+5)) || hory.at<float>(y,x) < 0 || verx.at<float>(y,x) >= (pow(2, NOP_v+5)) || verx.at<float>(y,x) < 0)
                {
                    continue;
                }
                tel++;
                Point2d punt_cam = Point2d(x,y);
                punten[verx.at<float>(y,x)][hory.at<float>(y,x)].push_back(punt_cam);
            }
        }
        //#pragma omp parallel for
        for(int x = 0; x<punten.size(); x++)
        {
            for(int y = 0; y<punten[x].size(); y++)
            {
                Point2d punt, punt2;
                double gemx = 0;
                double gemy = 0;
                if(punten[x][y].size() > 0)
                {
                    for(int z = 0; z < punten[x][y].size(); z++)
                    {
                        gemx += punten[x][y][z].x;
                        gemy += punten[x][y][z].y;
                    }

                    if(y%2==0)
                    {
                        punt = Point2d((gemx/punten[x][y].size()), gemy/punten[x][y].size());
                    }
                    else
                    {
                        punt = Point2d((gemx/punten[x][y].size())+0.5, gemy/punten[x][y].size());
                    }

                    cam_points.push_back(punt);
                    punt2 = Point2d(x,y);
                    proj_points.push_back(punt2);
                }
            }
        }

        gettimeofday(&tv5, &tz);
        printf( "nieuwe ontcijfering duurt %12.4g sec\n", (tv5.tv_sec-tv4.tv_sec) + (tv5.tv_usec-tv4.tv_usec)*1e-6 );

        ///Home made projection:
        Mat P0, P1;

        P0 = cameraMatrix * projmat1;
        P1 = projMatrix * projmat2;

        int number_of_cores= 1; //8
        omp_set_num_threads(number_of_cores);
        vector<vector<Point2d> > cams;
        vector<vector<Point2d> > projs;
        vector<Mat> driepunt;
        int deler = cam_points.size()/number_of_cores;

        int teller = 0;
        int elementteller = 0;
        while(elementteller<cam_points.size())
        {
            vector<Point2d> cam;
            vector<Point2d> proj;
            for(int i= teller*deler; i<(teller + 1)*deler; i++)
            {
                if(i>= cam_points.size())
                    break;

                cam.push_back(cam_points[i]);
                proj.push_back(proj_points[i]);
                elementteller++;
            }
            cams.push_back(cam);
            projs.push_back(proj);
            teller++;
        }

#pragma omp parallel for
        for(int i = 0; i< number_of_cores; i++)
        {
            Mat deel = Mat(1, cams[i].size(), CV_64FC4);
            Mat camsMat = Mat(cams[i]);
            Mat projsMat = Mat(projs[i]);

            triangulatePoints(P0, P1, camsMat, projsMat, deel);
            driepunt.push_back(deel);
        }


        Mat driedpunten;
        hconcat(driepunt, driedpunten);

        gettimeofday(&tv6, &tz);
        printf( "triangulate duurt %12.4g sec\n", (tv6.tv_sec-tv5.tv_sec) + (tv6.tv_usec-tv5.tv_usec)*1e-6 );
        ///Transform into homogeneous points
        double X,Y,Z;
        result = Mat(4, driedpunten.cols,CV_64FC1);

        for(int i=0;i<driedpunten.cols;i++)
        {
            X = driedpunten.at<double>(0,i) / driedpunten.at<double>(3,i);
            Y = driedpunten.at<double>(1,i) / driedpunten.at<double>(3,i);
            Z = driedpunten.at<double>(2,i) / driedpunten.at<double>(3,i);

            result.at<double>(0, i) = X/1000;
            result.at<double>(1, i) = Y/1000;
            result.at<double>(2, i) = Z/1000;
            result.at<double>(3, i) = 1;
        }
    }
    return result;
}

/*
 *
 * INOUT: c: the chessboardcorners of which we want to know the 3D coordinates
 * IN: d: Decoder where we save decoded patterns and global and direct light components
 *
 * OUT: cv::Mat containing 3D coordinates of every chessboard coordinate
 *
 * This function calculates the 3D position of every chessboard corner
 *
 */
Mat calculate3DPoints(vector<Point2f> &c, Decoder d)
{
    vector<Point2d> cam_points;
    vector<Point2d> proj_points;
    for(int l = 0; l< c.size(); l++) //for each chessboard corner
    {
        if (d.pattern_image[0].at<float>(c[l].y,c[l].x) >= (pow(2, NOP_v+5)) || d.pattern_image[1].at<float>(c[l].y,c[l].x) >= (pow(2, NOP_v+5)))
        {
            continue;
        }
        else
        {
        	cam_points.push_back(Point2d(c[l].x,c[l].y));
            proj_points.push_back(Point2d(d.pattern_image[1].at<float>(c[l].y,c[l].x), d.pattern_image[0].at<float>(c[l].y,c[l].x))); ///pattern point is (vertical pattern, horizontal pattern)
        }
    }

    ///Get intrinsic and extrinsic camera parameters
    FileStorage fs("src/communicatie/SL/calib_sl/camera_projector.xml", FileStorage::READ);
    Mat cameraMatrix;
    Mat projMatrix;
    Mat rotMat;
    Mat transMat;

    fs["camera_matrix"] >> cameraMatrix;
    fs["projector_matrix"] >> projMatrix;

    fs["rotation"] >> rotMat;
    if(rotMat.empty())
        cout<<"rotleeg"<<endl;

    fs["translation"] >> transMat;

    if(transMat.empty())
        cout<<"tranleeg"<<endl;

    Mat projmat1 = Mat::eye(Size(4,3), CV_64F);
    for(int i=0; i<projmat1.rows; i++)
    {
        for(int j=0; j<projmat1.cols; j++)
        {
            if(j==i && j<3)
            {
                projmat1.at<double>(i,j)=1;
            }
            else
            {
                projmat1.at<double>(i,j)=0;
            }
        }
    }

    Mat projmat2 = Mat::eye(Size(4,3), CV_64F);
    for(int i=0; i<projmat2.rows; i++)
    {
        for(int j=0; j<projmat2.cols; j++)
        {
            if(j<3)
            {
                projmat2.at<double>(i,j)=rotMat.at<double>(i,j);
            }
            else
            {
                projmat2.at<double>(i,j)=transMat.at<double>(i,0);
            }
        }
    }

    Mat cam, proj;
    cam = Mat(cam_points);
    proj = Mat(proj_points);
    Mat driedpunten = Mat(1, cam_points.size(), CV_64FC4);

    ///Home made projection matrices:
    Mat P0, P1;

    P0 = cameraMatrix * projmat1;
    P1 = projMatrix * projmat2;

    triangulatePoints(P0, P1, cam, proj, driedpunten);
    cout<<"Triangulated "<<cam_points.size()<<" points"<<endl;


    double X,Y,Z;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud_ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
    Mat result = Mat(4, c.size(), CV_64FC1);
    for(uint i=0;i<c.size();i++)
    {
        X = driedpunten.at<double>(0,i) / driedpunten.at<double>(3,i);
        Y = driedpunten.at<double>(1,i) / driedpunten.at<double>(3,i);
        Z = driedpunten.at<double>(2,i) / driedpunten.at<double>(3,i);
        result.at<double>(0,i) = X/1000;
        result.at<double>(1,i) = Y/1000;
        result.at<double>(2,i) = Z/1000;
        result.at<double>(3,i) = 1;

        pcl::PointXYZRGB point;
        point.x = X;
        point.y = Y;
        point.z = Z;
        uint8_t r = 255, g = 0, b = 0;    // Example: Red color
        uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
        point.rgb = *reinterpret_cast<float*>(&rgb);
        point_cloud_ptr -> points.push_back(point);
    }

    return result;
}

/*
 *
 * IN:transformed: transformed coordinates
 * IN:dest: Destination / ground truth
 *
 * This function prints the rms error between 2 sets of coordinates.
 *
 */
void getRmsError(Mat transformed, Mat dest)
{
    Mat tussen;
    transpose(transformed.clone(), tussen);
    double teller=0;
    for(int i=0;i<transformed.rows;i++)
    {
        double verschilx = abs(tussen.at<double>(i,0)) - abs(dest.at<double>(i,0));
        double verschily = abs(tussen.at<double>(i,1)) - abs(dest.at<double>(i,1));
        double verschilz = abs(tussen.at<double>(i,2)) - abs(dest.at<double>(i,2));
        double verschil = sqrt(pow(verschilx,2) + pow(verschily,2) + pow(verschilz,2));
        teller += pow(verschil,2);
    }

    double rms = sqrt(teller/transformed.rows);
    cout<<"rms error transformation = "<<rms<<" mm"<<endl;
}

/*
 *
 * IN: origin: The cv::Mat we want to save
 *
 * This function saves the cv::Mat as a .ply and as a .pcd
 * They all get the name test
 *
 */
void save(Mat origin)
{
    save(origin, "test");
}

/*
 *
 * IN: origin: The cv::Mat we want to save
 * IN: a: Name of the file to which we save the mat.
 *
 * This function saves the cv::Mat as a .ply and as a .pcd
 * They both get the name passed in a
 *
 */
void save(Mat origin, std::string a)
{
    ///Saves a Mat as a pointcloud. Mat has to be ordened as 3xN with N the number of coordinates

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud_ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
    double X, Y, Z;
    for(int i=0;i<origin.cols;i++)
    {
        X = origin.at<double>(0,i);
        Y = origin.at<double>(1,i);
        Z = origin.at<double>(2,i);

        pcl::PointXYZRGB point;
        point.x = X;
        point.y = Y;
        point.z = Z;

        uint8_t r = 0,g = 0,b = 0;
        r = 255, g = 0, b = 0;
        uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
        point.rgb = *reinterpret_cast<float*>(&rgb);
        point_cloud_ptr -> points.push_back(point);
    }

    point_cloud_ptr->width = (int)point_cloud_ptr->points.size();
    point_cloud_ptr->height =1;
    pcl::PLYWriter plywriter;
    plywriter.write(a+".ply", *point_cloud_ptr, false);
    pcl::io::savePCDFileBinary(a+".pcd", *point_cloud_ptr);
}

/*
 *
 * IN: path: location of the serie
 * IN: b: parameter for the structured light algorithm
 * IN: m: parameter for the structured light algorithm
 * IN: thresh: threshold to separate valid pixels from noise
 * IN: projector_width: projector width
 * IN: projector_height: projector height
 * IN: boardw: width of the calibrationboard we are using
 * IN: boardh: height of the calibrationboard we are using
 *
 * This function scans a calibration board, finds the corners, calculates the 3D coordinates of the corners, calculates the average of the corners in 3D and moves this point to match
 * the toolcenterpoint of the robot, so both robot and sensor know the same point in their respective coordinate systems.
 *
 */
cv::Point3d getCenterPoint(string path, float b, float m, float thresh, int projector_width, int projector_height, int boardw, int boardh)
{
    int vertical_patterns=0;
    ///Get dimensions
    while(pow(2, vertical_patterns) < projector_width)
    {
        vertical_patterns++;
    }
    NOP_v = vertical_patterns;

    ///Do the same for the horizontal patterns
    int horizontal_patterns=0;
    while(pow(2, horizontal_patterns) < projector_height)
    {
        horizontal_patterns++;
    }
    NOP_h = horizontal_patterns;


    ///First, we'll use chessboardcorners as unique points to find (also needed for calculation of NOP_v and NOP_h)
    vector <vector<Point2f> > chessboardcorners(1);
    bool gelukt_f = findcorners(chessboardcorners, path, 1, projector_width, projector_height, boardw, boardh);

    ///Calculate the 3D position of the center
    Mat points_sensor;
    bool draw = false;
    Decoder d;
    bool gelukt = decode(0, d, draw, path, b, m, thresh, projector_width, projector_height);
    points_sensor = calculate3DPoints(chessboardcorners[0], d);

    ///Calculate the middlepoint of the calibration board:
    double gemx=0, gemy=0, gemz=0;
    for(int i=0; i< points_sensor.cols; i++)
    {
        gemx += points_sensor.at<double>(0,i);
        gemy += points_sensor.at<double>(1,i);
        gemz += points_sensor.at<double>(2,i);
    }

    cv::Point3d punt = cv::Point3d( gemx/points_sensor.cols, gemy/points_sensor.cols, gemz/points_sensor.cols );

    ///Difference between 2 points, in the same direction as the centerpoint and tool center point (has to be parallell) and the perpendicular direction
    cv::Point3d r1, r2;

    r1.x = points_sensor.at<double>(0,boardw-1) - points_sensor.at<double>(0, 0);
    r1.y = points_sensor.at<double>(1,boardw-1) - points_sensor.at<double>(1, 0);
    r1.z = points_sensor.at<double>(2,boardw-1) - points_sensor.at<double>(2, 0);

    r2.x = points_sensor.at<double>(0,boardw *(boardh-1)) - points_sensor.at<double>(0, 0);
    r2.y = points_sensor.at<double>(1,boardw *(boardh-1)) - points_sensor.at<double>(1, 0);
    r2.z = points_sensor.at<double>(2,boardw *(boardh-1)) - points_sensor.at<double>(2, 0);

    ///Correct point to match robot tool center point.

    //Offset XY vlak: 0.1058m
    //Offset YZ vlak: 0.0141m

    double norm1 = std::sqrt(r1.x*r1.x + r1.y*r1.y + r1.z*r1.z);
    double norm2 = std::sqrt(r2.x*r2.x + r2.y*r2.y + r2.z*r2.z);


    cv::Point3d richting1, richting2;

    richting1.x = r1.x/norm1;
    richting1.y = r1.y/norm1;
	richting1.z = r1.z/norm1;

	richting2.x = r1.y*r2.z - r1.z*r2.y;
	richting2.y = r1.z*r2.x - r1.x*r2.z;
	richting2.z = r1.x*r2.y - r1.y*r2.x;

	cv::Point3d vectorxy, vectoryz;
	vectorxy = richting1 * 0.1058;
	vectoryz = richting2 * 0.0141;

	cv::Point3d translatie;
	translatie = vectorxy + vectoryz;

	punt -= translatie;

    Mat punt_mat = Mat(4, 1, CV_64FC1);
    punt_mat.at<double>(0,0)=punt.x;
    punt_mat.at<double>(0,1)=punt.y;
    punt_mat.at<double>(0,2)=punt.z;
    punt_mat.at<double>(0,3)=1;

    Mat Combo;
    vector<Mat> vector_mat;
    vector_mat.push_back(points_sensor);
    vector_mat.push_back(punt_mat);

    hconcat(vector_mat, Combo);

    save(Combo);
    return punt;
}

/*
 *
 * IN: scene_robot: The scene in the robot coordinate system
 *
 * This function thresholds the robot_scene in x, y and z direction. This way only the region of interest remains.
 *
 */
cv::Mat threshold_scene(cv::Mat scene_robot)
{
    cv::Mat scene = scene_robot.clone();
    ///Convert cv::Mat to Pointcloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr pc(new pcl::PointCloud<pcl::PointXYZ>);

    for(int i=0; i< scene.cols;i++)
    {
        float x,y,z;
        x = scene.at<double>(0, i);
        y = scene.at<double>(1, i);
        z = scene.at<double>(2, i);
        if(x == 0 && y == 0 && z == 0)
            continue;

        pcl::PointXYZ point;
        point.x = x;
        point.y = y;
        point.z = z;

        pc->points.push_back(point);
    }
    ///Rotate pointcloud
    ///Build our own rotation matrix
    Eigen::Matrix4f transform_1 = Eigen::Matrix4f::Identity();
    float theta;
    // Define a rotation matrix (see https://en.wikipedia.org/wiki/Rotation_matrix)
    theta = 7*M_PI/6;
    //rotate around X-Axis
    transform_1 (0,0) = cos (theta);
    transform_1 (0,1) = -sin(theta);
    transform_1 (1,0) = sin (theta);
    transform_1 (1,1) = cos (theta);

    pcl::transformPointCloud (*pc, *pc, transform_1);

    ///Threshhold in x, y and z direction
    pcl::PointCloud<pcl::PointXYZ>::Ptr pc2(new pcl::PointCloud<pcl::PointXYZ>);

    for(int i=0; i<pc->points.size(); i++)
    {
        pcl::PointXYZ point;

        point.x = pc->points[i].data[0];
        point.y = pc->points[i].data[1];
        point.z = pc->points[i].data[2];

        if(point.x < -0.3 || point.x > 0.3 )
            continue;

        if(point.y < 0.23 || point.y > 0.6 )
           continue;

        if(point.z < -0.175 || point.z > 0.02 )
             continue;

        pc2->points.push_back(point);
    }


    /// Rotate pointcloud back
    theta = (2*M_PI) - (7*M_PI/6);
    //rotate around X-Axis
    transform_1 (0,0) = cos (theta);
    transform_1 (0,1) = -sin(theta);
    transform_1 (1,0) = sin (theta);
    transform_1 (1,1) = cos (theta);

    pcl::transformPointCloud (*pc2, *pc2, transform_1);

    /// Convert back to cv::Mat
    scene = cv::Mat(4, pc2->points.size(), CV_64FC1);
    for(int i=0; i<pc2->points.size(); i++)
    {
        scene.at<double>(0, i) = pc2->points[i].data[0];
        scene.at<double>(1, i) = pc2->points[i].data[1];
        scene.at<double>(2, i) = pc2->points[i].data[2]-0.015;
        scene.at<double>(3, i) = 1;
    }

    //save(scene, "threshold");

    return scene;
}
