#include "../include/communicatie/sl_ti.hpp"


/*
 *
 * INOUT: projector: projector object,so we can set it up
 * IN: settingsfile: file with projector settings
 *
 * Code from TI to connect and setup the projector
 *
 */
dlp::ReturnCode ConnectAndSetupProjector(dlp::DLP_Platform *projector, const std::string &settings_file){
    dlp::ReturnCode ret;
    dlp::Parameters settings;

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<<<<<<<<<<<<<<<<<<<<<< Connect and Setup Projector >>>>>>>>>>>>>>>>>>>>>>" << std::endl;

    // Check that camera is NOT null
    if(!projector){
        std::cout << "Null projector pointer!" << std::endl;
        return ret.AddError("NULL_POINTER");
    }

    // Load the settings file
    std::cout << "Loading projector settings..." << settings_file << std::endl;
    ret = settings.Load(settings_file);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Failed to load projector settings!" << std::endl;
        return ret;
    }
    else{
        std::cout << "Projector settings loaded" << std::endl;
    }

    // Connect the projector if it hasn't been connected
    std::cout << "Connecting to projector..." << std::endl;
    if(!projector->isConnected())   ret = projector->Connect(0);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Could NOT connect to projector!" << std::endl;
        std::cout<<ret.ToString()<<std::endl;
        return ret;
    }
    else{
        std::cout << "Projector connected" << std::endl;
    }

    // Setup the camera
    std::cout << "Setting up projector..." << std::endl;
    ret = projector->Setup(settings);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Could NOT setup projector!" << std::endl;
        return ret;
    }
    else{
        std::cout << "Projector setup complete" << std::endl;
    }

    return ret;
}

/*
 *
 * INOUT: camera: camera object,so we can set it up
 * IN: settingsfile: file with camera settings
 *
 * Code from TI to connect and setup the camera
 *
 */

dlp::ReturnCode ConnectAndSetupCamera(dlp::Camera *camera, const std::string &settings_file){
    dlp::ReturnCode ret;
    dlp::Parameters settings;

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<<<<<<<<<<<<<<<<<<<<<< Connect and Setup Camera >>>>>>>>>>>>>>>>>>>>>>" << std::endl;

    // Check that camera is NOT null
    if(!camera) return ret.AddError("NULL_POINTER");

    // Load the settings file
    std::cout << "Loading camera settings..." << std::endl;
    ret = settings.Load(settings_file);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Failed to load camera settings!" << std::endl;
        return ret;
    }
    else{
        std::cout << "Camera settings loaded" << std::endl;
    }

    // Connect the camera if it hasn't been connected
    std::cout << "Connecting to camera..." << std::endl;
    if(!camera->isConnected())   ret = camera->Connect(0);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Could NOT connect to camera!" << std::endl;
        return ret;
    }
    else{
        std::cout << "Camera connected" << std::endl;
    }

    // Setup the camera
    std::cout << "Setting up camera..." << std::endl;
    ret = camera->Setup(settings);

    // Check for error
    if(ret.hasErrors()){
        std::cout << "Could NOT setup camera!" << std::endl;
        std::cout<<ret.ToString()<<std::endl;
        return ret;
    }
    else{
        std::cout << "Camera setup completed" << std::endl;
    }

    return ret;
}

/*
 *
 * INOUT: Projector: Projector, so we can prepare the patterns
 * IN: projector_calib_settings_file: File with projector calibration settings
 * INOUT: structured_light_vertical: Vertical structured light patterns
 * IN: structured_light_vertical_settings_file: File with structured_light_vertical settings
 * INOUT: structured_light_horizontal: Horizontal structured light patterns
 * IN: structured_light_horizontal_settings_file: File with structured_light_horizontal settings
 * INOUT: previously_prepared: is this function been called before?
 * INOUT:total_pattern_count: Total number of patterns prepared
 *
 * This function prepares the projector object to project patterns. If patterns generated here do not match patterns that are on the project (physical object), it will project the wrong patterns.
 *
 */

void PrepareProjectorPatterns( dlp::DLP_Platform    *projector,
                               const std::string    &projector_calib_settings_file,
                               dlp::StructuredLight *structured_light_vertical,
                               const std::string    &structured_light_vertical_settings_file,
                               dlp::StructuredLight *structured_light_horizontal,
                               const std::string    &structured_light_horizontal_settings_file,
                               const bool            previously_prepared,
                               unsigned int         *total_pattern_count){

    dlp::ReturnCode ret;

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<<<<<<<<<<<<<<<<<<<<<< Load DLP LightCrafter 4500 Firmware >>>>>>>>>>>>>>>>>>>>>>" << std::endl;

    // Projector Calibration Variables
    dlp::Calibration::Projector projector_calib;

    // Structured Light Sequence
    dlp::Pattern::Sequence vertical_patterns;
    dlp::Pattern::Sequence horizontal_patterns;
    dlp::Pattern::Sequence all_patterns;

    projector_calib.SetDebugEnable(false);


    // Check that projector is NOT null
    if(!projector) return;

    // Check if projector is connected
    if(!projector->isConnected()){
        std::cout << "Projector NOT connected!" << std::endl;
        return;
    }

    dlp::Parameters         structured_light_vertical_settings;
    dlp::Parameters         structured_light_horizontal_settings;

    /*if(structured_light_vertical_settings.Load(structured_light_vertical_settings_file).hasErrors()){*/
        // Camera calibration settings file did NOT load
        std::cout << "Vertical structured light settings did NOT load, using default values" << std::endl;

        structured_light_vertical_settings.Set(dlp::StructuredLight::Settings::Sequence::Count(12));
        structured_light_vertical_settings.Set(dlp::StructuredLight::Settings::Sequence::IncludeInverted(true));
        structured_light_vertical_settings.Set(dlp::StructuredLight::Settings::Pattern::Color(dlp::Pattern::Settings::Color::Type::WHITE));
        structured_light_vertical_settings.Set(dlp::StructuredLight::Settings::Pattern::Orientation(dlp::StructuredLight::Settings::Pattern::Orientation::Type::VERTICAL));
        structured_light_vertical_settings.Set(dlp::GrayCode::Settings::Pixel::Threshold(10));

        std::cout << "Saving default vertical structured light settings" << std::endl;
        structured_light_vertical_settings.Save(structured_light_vertical_settings_file);
    //}

    // Load the horizontal structured light settings
    std::cout << "Loading horizontal structured light settings..." << std::endl;
    //if(structured_light_horizontal_settings.Load(structured_light_horizontal_settings_file).hasErrors()){
        // Camera calibration settings file did NOT load
        std::cout << "Vertical structured light settings did NOT load, using default values" << std::endl;

        structured_light_horizontal_settings.Set(dlp::StructuredLight::Settings::Sequence::Count(12));
        structured_light_horizontal_settings.Set(dlp::StructuredLight::Settings::Sequence::IncludeInverted(true));
        structured_light_horizontal_settings.Set(dlp::StructuredLight::Settings::Pattern::Color(dlp::Pattern::Settings::Color::Type::WHITE));
        structured_light_horizontal_settings.Set(dlp::StructuredLight::Settings::Pattern::Orientation(dlp::StructuredLight::Settings::Pattern::Orientation::Type::HORIZONTAL));
        structured_light_horizontal_settings.Set(dlp::GrayCode::Settings::Pixel::Threshold(10));

        std::cout << "Saving default vertical structured light settings" << std::endl;
        structured_light_horizontal_settings.Save(structured_light_horizontal_settings_file);
   // }

    structured_light_horizontal->SetDlpPlatform((*projector));
    structured_light_vertical->SetDlpPlatform((*projector));

    std::cout << "Setting up vertical structured light module..." << std::endl;
    structured_light_vertical->Setup(structured_light_vertical_settings);
    std::cout << "Setting up horizontal structured light module..." << std::endl;
    structured_light_horizontal->Setup(structured_light_horizontal_settings);

    // Generate the pattern sequence
    std::cout << "Generating vertical structured light module patterns..." << std::endl;
    structured_light_vertical->GeneratePatternSequence(&vertical_patterns);
    std::cout << "Generating horizontal structured light module patterns..." << std::endl;
    structured_light_horizontal->GeneratePatternSequence(&horizontal_patterns);
std::cout<<"HIER "<<vertical_patterns.GetCount()<<std::endl;
    all_patterns.Add(vertical_patterns);
    all_patterns.Add(horizontal_patterns);

    std::cout << "Preparing projector for calibration and structured light..." << std::endl;
    ret = projector->PreparePatternSequence(all_patterns);

    projector->SetDebugEnable(false);

    if( ret.hasErrors()){
        std::cout << "Projector prepare sequence FAILED: " << ret.ToString() << std::endl;
        (*total_pattern_count) = 0;
    }
    else
    {
        std::cout << "Projector prepared" << std::endl;
        (*total_pattern_count) = all_patterns.GetCount();
    }
}

/*
 *
 * IN: path: location of the to be saved images
 * IN: width: camera width
 * IN: height: camera height
 * INOUT: setup: is everything setup?
 * INOUT: camera: camera object
 * INOUT: projector: projector object:
 * INOUT: structured_light_vertical: vertical structured light patterns
 * INOUT: structured_light_horizontal: horizontal structured light patterns
 * IN: total_pattern_count: total number of patterns to be projected
 *
 * This function scans the scene once. Every scan image is saved at path location.
 *
 */
bool get_ti_images(std::string path, int width, int height, bool &setup, dlp::PG_FlyCap2_C &camera, dlp::LCr4500 &projector,
                   dlp::GrayCode &structured_light_vertical, dlp::GrayCode &structured_light_horizontal, unsigned int &total_pattern_count)
{
    camera.SetDebugEnable(false);
    projector.SetDebugEnable(false);
    std::string system_settings_file_camera                 = "./src/communicatie/config/system_settings_camera.txt";
    std::string system_settings_file_projector              = "./src/communicatie/config/system_settings_projector.txt";
    std::string calibration_settings_file_camera            = "./src/communicatie/config/calibration_settings_camera.txt";
    std::string calibration_settings_file_projector         = "./src/communicatie/config/calibration_settings_projector.txt";
    std::string calibration_data_file_camera                = "./src/communicatie/config/calibration_data_camera.xml";
    std::string calibration_data_file_projector             = "./src/communicatie/config/calibration_data_projector.xml";
    std::string structured_light_vertical_settings_file     = "./src/communicatie/config/structured_light_vertical_settings.txt";
    std::string structured_light_horizontal_settings_file   = "./src/communicatie/config/structured_light_horizontal_settings.txt";
    std::string geometry_settings_file                      = "./src/communicatie/config/geometry_settings.txt";
    if(!setup)
    {
        ConnectAndSetupProjector(&projector, system_settings_file_projector);
        ConnectAndSetupCamera(   &camera,    system_settings_file_camera);
        PrepareProjectorPatterns(&projector,
                                 calibration_settings_file_projector,
                                 &structured_light_vertical,
                                 structured_light_vertical_settings_file,
                                 &structured_light_horizontal,
                                 structured_light_horizontal_settings_file,
                                 true,
                                 &total_pattern_count);
        setup = true;
    }

    dlp::Time::Chronograph timer;
    if(!camera.isStarted()){
        if(camera.Start().hasErrors()){
            std::cout << "Could NOT start camera! \n" << std::endl;
            return false;
        }
    }

    ///Uncomment if you want to see the camera view before each scan.
    //unsigned int return_key = 0;
    //dlp::Image::Window camera_view;
    //camera_view.Open("Place object to scan in view and then press the space bar...");
    //projector.ProjectSolidWhitePattern();
    /*while(return_key != ' '){
        dlp::Image camera_capture;
        camera_capture.Clear();
        camera.GetFrame(&camera_capture);
        camera_view.Update(camera_capture,height, width);
        camera_view.WaitForKey(1,&return_key);
        if(return_key == 27) break;
    }*/
    //sleep(1);
    //camera_view.Close();
    //projector.ProjectSolidBlackPattern();
    dlp::Time::Sleep::Milliseconds(100);
    dlp::Capture::Sequence capture_scan;
    // Scan the object
    for(unsigned int iScan = 0; iScan < total_pattern_count*1.25; iScan++){  // Multiplier included to allow ample time for sequence to start
        dlp::Capture capture;
        dlp::Image capture_image;

        if(iScan == 1){
            projector.StartPatternSequence(0,total_pattern_count-1,false);   // Skip the 0 pattern since it is the calibration image
            timer.Reset();
        }
        camera.GetFrame(&capture_image);

        // Add the frame to the sequence
        capture.image_data = capture_image;
        capture.image_type.Set(dlp::Capture::Settings::Data::Type::IMAGE_DATA);
        capture_scan.Add(capture);
        capture_image.Clear();
        capture.image_data.Clear();
    }

    camera.Stop();
    std::cout << "Capture complete! milliseconds = " << timer.Lap() << std::endl;

    dlp::Capture::Sequence vertical_scan;
    dlp::Capture::Sequence horizontal_scan;
    bool            first_pattern_found = false;
    unsigned char   previous_ave   = 0;
    unsigned int    capture_offset = 0;

    for(unsigned int iScan = 0; iScan < capture_scan.GetCount(); iScan++){
        dlp::Capture temp;

        capture_scan.Get(iScan,&temp);
        // Calculate the average for the image to determine if the pattern started
        if(!first_pattern_found){
            unsigned char ave;

            // Get the capture image format to determine how to get the mean
            dlp::Image::Format capture_format;
            temp.image_data.GetDataFormat(&capture_format);

            if(capture_format == dlp::Image::Format::RGB_UCHAR){
                dlp::PixelRGB ave_rgb;
                // Get the average for each channel
                temp.image_data.GetMean(&ave_rgb);

                // Average the channels together
                ave = (ave_rgb.r + ave_rgb.g + ave_rgb.b)/3;
            }
            else{
                // Assume that the image is MONO_UCHAR
                temp.image_data.GetMean(&ave);
            }

            if(iScan == 0) previous_ave = ave;

            // If the percent error is greater than 10% the first pattern has been found
            float percent_error = std::abs((previous_ave - ave)/(float)ave);
            if( percent_error > 0.10 ){
                first_pattern_found = true;
                capture_offset = iScan;
            }
        }

        if(first_pattern_found){

            // The first projected pattern has been found
            if(iScan < structured_light_vertical.GetTotalPatternCount() + capture_offset){
                vertical_scan.Add(temp);
                temp.image_data.Save(path + "/frame"+dlp::Number::ToString(iScan - capture_offset)+".bmp");
            }
            else if(iScan < (structured_light_vertical.GetTotalPatternCount()   +
                             structured_light_horizontal.GetTotalPatternCount() +
                             capture_offset)){
                horizontal_scan.Add(temp);
                temp.image_data.Save(path + "/frame" +dlp::Number::ToString(iScan - capture_offset)+".bmp");
            }
        }
    }
}
