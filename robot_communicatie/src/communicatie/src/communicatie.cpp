#include "../include/communicatie/visie.hpp"

/*
 * IN: dir: Directory of which we want to know the contents
 * INOUT: files: List of the names of files in the directory
 *
 * Make a list of names of files and folders in any given directory
 *
 */
int getdir (std::string dir, std::vector<std::string> &files)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL)
    {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }

    while ((dirp = readdir(dp)) != NULL)
    {
        files.push_back(std::string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}

/*
 * IN: image: Image to be shown on screen
 * IN: naam: Name of the window in which the image is shown
 *
 * Show an image in a window. Press any key to close the window.
 *
 */
void tonen(cv::Mat image, cv::String naam)              // afbeelding tonen op scherm a.d.h.v. afbeelding en naam venster
{
    cv::namedWindow( naam, cv::WINDOW_NORMAL );
    cv::resizeWindow(naam, 1200,800);
    cv::imshow( naam, image );
    cv::waitKey(0);
}

/*
 * IN: a: Matrix to be printed
 *
 * Print a matrix to the standard output, so you know the contents. Also prints dimensions of the matrix
 *
 */
void printmat(cv::Mat a)
{
    if(a.type() != 6)
    	a.convertTo(a, CV_64FC1);

    cout<<a.rows<<"x"<<a.cols<<endl;

    for(int x = 0; x<a.rows; x++)
    {
        for(int y=0; y< a.cols; y++)
        {
            if(a.at<double>(x,y) < 0.000001 && a.at<double>(x,y) > -0.000001)
                cout<<"0"<<" ";
            else
                cout<<a.at<double>(x,y)<<" ";
        }
        cout<<endl;
    }

}

/*
 * IN: a: Matrix to be printed
 * IN: b: String to be printed before the matrix is printed.
 *
 *  Prints the string first, so you know the difference between multiple printmat() calls.
 *  Print a matrix to the standard output, so you know the contents. Also prints dimensions of the matrix.
 *
 */

void printmat(cv::Mat a, cv::String b)
{
    cout<<b<<endl;
    printmat(a);
}

/*
 * IN: a: Vector of matrices to be printed
 * IN: b: String to be printed before the matrix is printed.
 *
 *  Prints the string first, so you know the difference between multiple printmat() calls.
 *  Prints every matrix in the vector to the standard output, so you know the contents. Also prints dimensions of the matrix.
 *
 */

void printmat(std::vector<cv::Mat> a, cv::String b)
{
    for(int i=0; i< a.size(); i++)
    {
        printmat(a[i], b);
    }
}

