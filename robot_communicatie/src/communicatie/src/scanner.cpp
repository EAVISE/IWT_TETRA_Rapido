#include "../include/communicatie/scanner.hpp"

///General:
/*---------------------------------------------------------------------------------------*/
/*
 *
 * IN: origin: transformation matrix is calculated from here
 * IN: dest: transformation matrix is calculated to here
 *
 * Calculates the transformation matrix between 2 sets of 3D points. It is important to know which is the origin and which is the destination.
 *
 */
cv::Mat calculateTransMat(cv::Mat origin, cv::Mat dest)
{
    cv::Mat transmat = cv::Mat(4,4, CV_64FC1);
    cv::Mat origin_inv;

    ///Find the centroid
    cv::Mat center_origin, center_dest; ///center origin and center dest

    center_origin = cv::Mat::zeros(3,1, CV_64FC1);
    center_dest= cv::Mat::zeros(3,1, CV_64FC1);

    if(origin.cols != dest.cols)
    {
        cerr<<"dest and origin have different sizes"<<endl;
        return cv::Mat();
    }

    for(int i = 0; i< origin.cols; i++)
    {
        center_origin.at<double>(0,0) +=origin.at<double>(0,i);
        center_origin.at<double>(1,0) +=origin.at<double>(1,i);
        center_origin.at<double>(2,0) +=origin.at<double>(2,i);

        center_dest.at<double>(0,0) +=dest.at<double>(0,i);
        center_dest.at<double>(1,0) +=dest.at<double>(1,i);
        center_dest.at<double>(2,0) +=dest.at<double>(2,i);
    }

    center_origin.at<double>(0,0)/=origin.cols;
    center_origin.at<double>(1,0)/=origin.cols;
    center_origin.at<double>(2,0)/=origin.cols;

    center_dest.at<double>(0,0) /= dest.cols;
    center_dest.at<double>(1,0) /= dest.cols;
    center_dest.at<double>(2,0) /= dest.cols;

    ///Recenter Origin and Dest so only the rotation remains and build H.
    cv::Mat H = cv::Mat::zeros(3,3, CV_64FC1);
    cv::Mat a = cv::Mat::zeros(4,1, CV_64FC1);
    cv::Mat b = cv::Mat::zeros(4,1, CV_64FC1);

    for(int i=0; i<origin.cols; i++)
    {
        origin.col(i).copyTo(a);
        dest.col(i).copyTo(b);
        a.resize(3);
        b.resize(3);
        transpose(b.clone(), b);

        a.at<double>(0,0) -= center_origin.at<double>(0,0);
        a.at<double>(1,0) -= center_origin.at<double>(1,0);
        a.at<double>(2,0) -= center_origin.at<double>(2,0);

        b.at<double>(0,0) -= center_dest.at<double>(0,0);
        b.at<double>(1,0) -= center_dest.at<double>(1,0);
        b.at<double>(2,0) -= center_dest.at<double>(2,0);

        H += (a*b);
    }
    cv::Mat w, u, vt;
    cv::SVD::compute(H, w, u, vt);
    cv::Mat rot = u*vt;
    cv::Mat trans = -rot*center_origin + center_dest;

    for(int i=0; i<transmat.rows; i++)
    {
        for(int j=0; j<transmat.cols; j++)
        {
            if(j<3)
            {
                if(i<3)
                    transmat.at<double>(i,j)=rot.at<double>(i,j);
                else
                    transmat.at<double>(i,j)= 0;
            }
            else
            {
                if(i<3)
                    transmat.at<double>(i,j)=trans.at<double>(i,0);
                else
                    transmat.at<double>(i,j)=1;
            }
        }
    }

    cv::FileStorage fs("src/communicatie/SL/transmat.xml", cv::FileStorage::WRITE);
    fs << "transmat" << transmat;

    return transmat;
}

/*
 *
 * IN: robot: sets of 3D points in robot coordinate system
 * IN: sensor: Sets of 3D points in sensor coordinate system
 *
 * Sets up data to be used by calculateTransmat()
 *
 */
bool calibrate_r(std::vector<cv::Point3d> robot, std::vector<cv::Point3d> sensor)
{
    ///Transform the vectors to mats
    cv::Mat robot_mat = cv::Mat(4, sensor.size(), CV_64FC1);
    cv::Mat sensor_mat = cv::Mat(4, sensor.size(), CV_64FC1);

    for(int robx = 0, senx = 0 ; senx< sensor.size(); robx+=2, senx++)
    {
        robot_mat.at<double>(0, senx) = robot[robx].x;
        robot_mat.at<double>(1, senx) = robot[robx].y;
        robot_mat.at<double>(2, senx) = robot[robx].z;
        robot_mat.at<double>(3, senx) = 1;

        sensor_mat.at<double>(0, senx) = sensor[senx].x;
        sensor_mat.at<double>(1, senx) = sensor[senx].y;
        sensor_mat.at<double>(2, senx) = sensor[senx].z;
        sensor_mat.at<double>(3, senx) = 1;
    }

    cv::Mat transMat = calculateTransMat(sensor_mat, robot_mat);
    if(transMat.empty())
        return false;

    return true;
}

/*
 *
 * IN: origin: Mat to be converted.
 * OUT: returns converted mat.
 *
 * Converts Matrix from one coordinate system to another, using a transformation matrix.
 *
 */

cv::Mat convert(Mat origin)
{
    FileStorage fs("src/communicatie/SL/transmat.xml", FileStorage::READ);
    Mat transMat;
    fs["transmat"] >> transMat;

    if(transMat.empty())
        cout<<"transformation Matrix is empty."<<endl;

    transMat.convertTo(transMat, CV_64FC1);

    Mat driedpunten = transMat*origin;

    return driedpunten;
}

///Structured Light Scanner
/*---------------------------------------------------------------------------------------*/
/*
 *
 * Constructor
 *
 * Constructor for object for scanning with HDMI cable
 *
 */
SLScanner::SLScanner(int w, int h, float b_arg, float m_arg, int t, int s, int bw, int bh)
{
    width = w;
    heigth = h;
    b = b_arg;
    m = m_arg;
    thresh = t;
    speed = s;
    boardw = bw;
    boardh = bh;
    count_series();
}

void SLScanner::count_series()
{
    string dir_calib_sl = "src/communicatie/SL/calib_sl/";
    vector<string> files_calib_sl;
    int calib_sl_series=0;

    ///Count the number of structured light calibration series
    getdir(dir_calib_sl, files_calib_sl);
    for(uint i=0; i<files_calib_sl.size(); i++)
    {
        calib_sl_series = i;
    }
    ///Remove ".." and "." directoy from the count
    calib_sl_series-=2;
}

void SLScanner::calibrate()
{
    bool gelukt_f = false;
    bool gelukt_d = false;
    bool gelukt_c = false;
    vector<vector<Point2f> > corners;
    vector<Decoder> dec;

    cout<<"The resolution you gave is: "<<width<<"x"<<heigth<<endl;
    cout<<"Do you wish to make new calibration images? y/n"<<endl;
    char antwoord;
    cin >> antwoord;
    while(antwoord == 'y')
    {
        ostringstream conv;
        conv << calib_sl_series;
        string path = "src/communicatie/calib_sl/serie"+conv.str();
        mkdir(path.c_str(), 0700);
        bool gelukt = get_sl_images(speed, path, calib_sl_series, width, heigth);
        if(gelukt)
            calib_sl_series++;
        else
            cout<<"failed to get serie: "<<calib_sl_series<<endl;
        cout<<"Another?"<<endl;
        cin >> antwoord;
    }

    if(!gelukt_f)
    {
        string path = "src/communicatie/calib_sl/serie";
        vector<vector<Point2f> > chessboardcorners(calib_sl_series);
        gelukt_f = findcorners(chessboardcorners, path, calib_sl_series, width, heigth, boardw, boardh);
        if(gelukt_f)
            corners = chessboardcorners;
    }
    if(!gelukt_d && gelukt_f)
    {
        string path = "src/communicatie/calib_sl/serie";
        bool draw = false;
        gelukt_d = decode_all(calib_sl_series, dec, draw, path, b, m, thresh, width, heigth);
    }

    if(gelukt_f && gelukt_d)
    {
        gelukt_c = calibrate_sl(dec, corners, calib_sl_series, width, heigth, boardw, boardh);
        if(!gelukt_c)
            cout<<"Structured Light setup failed to calibrate."<<endl;
    }
}

cv::Point3d SLScanner::getCameraPoint()
{
    string path = "src/communicatie/SL/robot_sl0";
    cout<<"Do you want to make a new scan? Press y"<<endl;
    char antwoord;
    cin >> antwoord;
    if(antwoord == 'y')
    {
        bool gelukt = get_sl_images(speed, path, 0, width, heigth);
    }
    path="src/communicatie/SL/robot_sl";
    return getCenterPoint(path, b, m, thresh, width, heigth, calrows, calcols);
}

void SLScanner::sensor_calibrate(std::vector<cv::Point3d> robot, std::vector<cv::Point3d> sensor)
{
    ///Calibreer de beide systemen a.d.h.v. de punten.
    bool transmat = calibrate_r(robot, sensor);

    if(transmat)
    {
        std::cout<<"robot_sensor calib gelukt "<<std::endl;
    }
}

void SLScanner::scan()
{
    string path = "src/communicatie/SL/scan0";
    cout<<"Do you want to make a new scan? Press y"<<endl;
    char antwoord;
    cin >> antwoord;
    struct timeval tv1,tv2, tv3 ; struct timezone tz;
    if(antwoord == 'y')
    {
        gettimeofday(&tv3, &tz);
        bool gelukt = get_sl_images(speed, path, 0, width, heigth);
    }
    path="src/communicatie/SL/scan";

    gettimeofday(&tv1, &tz);
    Mat result =  calculate3DPoints_all(path, 1,  b, m, thresh, width, heigth);
    gettimeofday(&tv2, &tz);
    Mat scene_robot = convert(result); //Convert 3d scene to coordinate system of the robot.

    printf( "scanning the scene takes  = %12.4g sec\n", (tv1.tv_sec-tv3.tv_sec) + (tv1.tv_usec-tv3.tv_usec)*1e-6 );
    printf( "everything about the pointcloud takes  = %12.4g sec\n", (tv2.tv_sec-tv1.tv_sec) + (tv2.tv_usec-tv1.tv_usec)*1e-6 );
    printf( "everything about the pointcloud takes + scan  = %12.4g sec\n", (tv2.tv_sec-tv3.tv_sec) + (tv2.tv_usec-tv3.tv_usec)*1e-6 );

    //save(scene_robot);
    save(result);
    //setScene(scene_robot);
    setScene(result);
}

void SLScanner::setScene(cv::Mat a)
{
    scene = a;
}

Mat SLScanner::getScene()
{
    return scene;
}

///TI - Structured Light Scanner
/*---------------------------------------------------------------------------------------*/
/*
 *
 * Constructor
 *
 * Constructor for object for scanning with TI projector
 *
 */
TIScanner::TIScanner(int w, int h, float b_arg, float m_arg, int t, int s, int bw, int bh)
{
    width = w;
    heigth = h;
    b = b_arg;
    m = m_arg;
    thresh = t;
    speed = s;
    boardw = bw;
    boardh = bh;
    calib_ti_series = count_series();

    setup = false;
    dlp::GrayCode slv;
    dlp::GrayCode slh;
    structured_light_vertical = slv;
    structured_light_horizontal = slh;

    camera.SetDebugEnable(false);
    projector.SetDebugEnable(false);
    std::string system_settings_file_camera                 = "./config/system_settings_camera.txt";
    std::string system_settings_file_projector              = "./config/system_settings_projector.txt";
}

/*
 *
 * OUT: number of series
 *
 * Counts how many calibration series have been made
 *
 */
int TIScanner::count_series()
{
    string dir_calib_sl = "src/communicatie/SL/calib_sl/";
    vector<string> files_calib_sl;
    int calib_ti_series=0;

    ///Count the number of structured light calibration series
    getdir(dir_calib_sl, files_calib_sl);
    for(uint i=0; i<files_calib_sl.size(); i++)
    {
        calib_ti_series = i;
    }
    ///Remove ".." and "." directoy from the count
    calib_ti_series-=2;
    return calib_ti_series;
}

/*
 *
 * All function calls to calibrate projector-camera
 *
 */
void TIScanner::calibrate()
{
    bool gelukt_f = false;
    bool gelukt_d = false;
    bool gelukt_c = false;
    vector<vector<Point2f> > corners;
    vector<Decoder> dec;

    cout<<"The resolution you gave is: "<<width<<"x"<<heigth<<endl;
    cout<<"Do you wish to make new calibration images? y/n"<<endl;
    char antwoord;
    cin >> antwoord;
    while(antwoord == 'y')
    {
        ostringstream conv;
        conv << calib_ti_series;
        string path = "src/communicatie/SL/calib_sl/serie"+conv.str();
        mkdir(path.c_str(), 0700);
        bool gelukt = get_ti_images(path, width, heigth, setup, camera, projector,
                                    structured_light_vertical, structured_light_horizontal, total_pattern_count);
        if(gelukt)
            calib_ti_series++;
        else
            cout<<"failed to get serie: "<<calib_ti_series<<endl;
        cout<<"Another?"<<endl;
        cin >> antwoord;
    }

    if(!gelukt_f)
    {
        string path = "src/communicatie/SL/calib_sl/serie";
        vector<vector<Point2f> > chessboardcorners(calib_ti_series);
        gelukt_f = findcorners(chessboardcorners, path, calib_ti_series, width, heigth, boardw, boardh);
        if(gelukt_f)
            corners = chessboardcorners;
    }

    if(!gelukt_d && gelukt_f)
    {
        string path = "src/communicatie/SL/calib_sl/serie";
        bool draw = false;
        gelukt_d = decode_all(calib_ti_series, dec, draw, path, b, m, thresh, width, heigth);
    }

    if(gelukt_f && gelukt_d)
    {
        gelukt_c = calibrate_sl(dec, corners, calib_ti_series, width, heigth, boardw, boardh);
        if(!gelukt_c)
            cout<<"Structured Light setup failed to calibrate."<<endl;
    }
}

/*
 *
 * OUT: Camera point in 3D
 *
 * Gets the camera point in 3D. Used to calculate the transformation matrix between scanner and robot.
 *
 */
cv::Point3d TIScanner::getCameraPoint()
{
    string path = "src/communicatie/SL/robot_sl0";
    bool gelukt = get_ti_images(path, width, heigth, setup, camera, projector,
                                    structured_light_vertical, structured_light_horizontal, total_pattern_count);

    path="src/communicatie/SL/robot_sl";
    return getCenterPoint(path, b, m, thresh, width, heigth, calrows, calcols);
}

/*
 *
 * IN: robot: set of 3D points in robot coordinate system
 * IN: sensor: set of 3D points in sensor coordinate system
 *
 * Function calls to calculate transformation matrix between sensor and robot
 *
 */

void TIScanner::sensor_calibrate(std::vector<cv::Point3d> robot, std::vector<cv::Point3d> sensor)
{
    ///Calibreer de beide systemen a.d.h.v. de punten.
    bool transmat = calibrate_r(robot, sensor);

    cv::Mat sensor_mat = cv::Mat(4, sensor.size(), CV_64FC1);
    cv::Mat robot_mat = sensor_mat.clone();

    if(transmat)
    {
    	for(int i=0; i<sensor.size(); i++)
    	{
    		sensor_mat.at<double>(0, i) = sensor[i].x;
    		sensor_mat.at<double>(1, i) = sensor[i].y;
    		sensor_mat.at<double>(2, i) = sensor[i].z;
    		sensor_mat.at<double>(3, i) = 1;

    		robot_mat.at<double>(0, i) = robot[i].x;
    		robot_mat.at<double>(1, i) = robot[i].y;
    		robot_mat.at<double>(2, i) = robot[i].z;
    		robot_mat.at<double>(3, i) = 1;
    	}

    	cv::Mat transformed = convert(sensor_mat);
    	getRmsError(transformed, robot_mat);
    }
}

/*
 *
 * Function calls to make a new scan, using the TI projector
 *
 */

void TIScanner::scan()
{
    string path = "src/communicatie/SL/scan0";
    cout<<"Do you want to make a new scan? Press y"<<endl;
    char antwoord;
    cin >> antwoord;
    struct timeval tv1,tv2, tv3 ; struct timezone tz;
    if(antwoord == 'y')
    {
        gettimeofday(&tv3, &tz);
        bool gelukt = get_ti_images(path, width, heigth, setup, camera, projector, structured_light_vertical,
                                    structured_light_horizontal,  total_pattern_count);
    }
    path="src/communicatie/SL/scan";

    gettimeofday(&tv1, &tz);
    Mat result =  calculate3DPoints_all(path, 1,  b, m, thresh, width, heigth);
    gettimeofday(&tv2, &tz);
    Mat scene_robot = convert(result); //Convert 3d scene to coordinate system of the robot.
    scene_robot = threshold_scene(scene_robot);

    printf( "scanning the scene takes  = %12.4g sec\n", (tv1.tv_sec-tv3.tv_sec) + (tv1.tv_usec-tv3.tv_usec)*1e-6 );
    printf( "everything about the pointcloud takes  = %12.4g sec\n", (tv2.tv_sec-tv1.tv_sec) + (tv2.tv_usec-tv1.tv_usec)*1e-6 );
    printf( "everything about the pointcloud takes + scan  = %12.4g sec\n", (tv2.tv_sec-tv3.tv_sec) + (tv2.tv_usec-tv3.tv_usec)*1e-6 );

    save(result, "scene camera");
    save(scene_robot, "scene robot");
    setScene(scene_robot);
    //setScene(result);
}

void TIScanner::setScene(cv::Mat a)
{
    scene = a;
}

Mat TIScanner::getScene()
{
    return scene;
}

