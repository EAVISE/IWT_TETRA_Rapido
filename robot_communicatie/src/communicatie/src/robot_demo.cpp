#include "ros/ros.h"
#include "std_msgs/String.h"
#include "communicatie/Point_data.h"
#include "communicatie/Robot_coord.h"
#include <sensor_msgs/PointCloud2.h>
#include <iostream>
#include <sstream>
#include "../include/communicatie/visie.hpp"
#include "hulp_functies.h"
#include "../../mplan/src/convert.h"

#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <ur_msgs/IOStates.h>

#include <vector>
#include <string>
#include <boost/make_shared.hpp>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>

#include "../../mplan/src/poses.h"

//#define SPIN1
#define W84ENTER
//#define W84VAC
//#define W84ENTER2




class Listener_commando
{
private:
    bool move;
    bool finished;
    bool calib;
    bool calib_start;

public:
    Listener_commando();
    void callback(const std_msgs::String::ConstPtr msg);
    bool getMove();
    bool getFinished();
    bool getCalib();
    bool getCalibstart();
    void setMove(bool a);
    void setFinished(bool a);
    void setCalib(bool a);
    void setCalibstart(bool a);
};

Listener_commando::Listener_commando()
{
    move = false;
    finished = false;
    calib = false;
    calib_start = false;
}

void Listener_commando::callback(const std_msgs::String::ConstPtr msg)
{
    //std::cout<<"In de callback robot"<< msg->data<<std::endl;
    if(msg->data == "finished")
        setFinished(true);
    else if(msg->data == "move" || msg->data == "move_calib")
    {
    	setMove(true);
    	if(msg->data == "move_calib")
    	{
    		if (getCalib()==false)
    		{
    			setCalibstart(true);
    		}
    		else
    		{
    			setCalibstart(false);
    		}
    		setCalib(true);
    	}
    	else
    	{
    		setCalib(false);
    		setCalibstart(false);
    	}

    }
}

bool Listener_commando::getMove()
{
    return move;
}

bool Listener_commando::getFinished()
{
    return finished;
}

bool Listener_commando::getCalib()
{
    return calib;
}

bool Listener_commando::getCalibstart()
{
    return calib_start;
}

void Listener_commando::setMove(bool a){
    move = a;
}

void Listener_commando::setFinished(bool a){
    finished = a;
}

void Listener_commando::setCalib(bool a){
    calib = a;
}

void Listener_commando::setCalibstart(bool a){
    calib_start = a;
}

class Listener_robot_data
{
private:
    int id;
    sensor_msgs::PointCloud2 scene;
    cv::Point3d translatie;
    cv::Point3d rotatie;
    std::vector<double> quat;

public:
    Listener_robot_data();
    void callback(const communicatie::Point_data::ConstPtr msg);
    sensor_msgs::PointCloud2  getScene();
    cv::Point3d getTrans();
    cv::Point3d getRot();
    std::vector<double> getQuat();
    int getID();
    void setScene(sensor_msgs::PointCloud2);
    void setTrans(cv::Point3d);
    void setRot(cv::Point3d);
    void setQuat(std::vector<double> q);
    void setID(int);
};

Listener_robot_data::Listener_robot_data()
{
    sensor_msgs::PointCloud2 p;
    scene = p;
    translatie = cv::Point3d(0,0,0);
    rotatie = cv::Point3d(0,0,0);
    id = -1;
}

void Listener_robot_data::callback(const communicatie::Point_data::ConstPtr msg)
{
    setScene(msg->pointcloud);
    cv::Point3d trans = cv::Point3d(msg->x, msg->y, msg->z);
    setTrans(trans);
    cv::Point3d rot = cv::Point3d(msg->rx, msg->ry, msg->rz);
    setRot(rot);
    std::vector<double> q;
    q.push_back(msg->q1);
    q.push_back(msg->q2);
    q.push_back(msg->q3);
    q.push_back(msg->q4);
    setQuat(q);
    setID(msg->point_id);
}

sensor_msgs::PointCloud2  Listener_robot_data::getScene()
{
    return scene;
}

cv::Point3d Listener_robot_data::getTrans()
{
    return translatie;
}

cv::Point3d Listener_robot_data::getRot()
{
    return rotatie;
}

std::vector<double> Listener_robot_data::getQuat()
{
	return quat;
}

int Listener_robot_data::getID()
{
    return id;
}

void Listener_robot_data::setScene(sensor_msgs::PointCloud2 a)
{
    scene = a;
}

void Listener_robot_data::setTrans(cv::Point3d a)
{
    translatie = a;
}

void Listener_robot_data::setRot(cv::Point3d a)
{
    rotatie = a;
}
void Listener_robot_data::setQuat(std::vector<double> q)
{
	quat = q;
}
void Listener_robot_data::setID(int a)
{
    id = a;
}


class vac
{
private:
    bool in_state;

public:
    vac();
    void callback(const ur_msgs::IOStates& msg);
    bool getState();
  //  void setState(bool a);
};

vac::vac()
{
    in_state = false;
}

void vac::callback(const ur_msgs::IOStates& msg)
{
	//ur_msgs::Digital dig_in;
	//dig_in = msg.digital_in_states[7];
	//in_state = dig_in.state;
	in_state = msg.digital_in_states[7].state;
	//ROS_INFO("dig_in.pin %i",dig_in.pin);
	/*
	bool st;
	st = dig_in.state;
	if(st)
	{
		ROS_INFO("Digital_input_7 = HIGH");
	}
	else
	{
		ROS_INFO("Digital_input_7 = LOW");
	}
	*/
}

bool vac::getState()
{
    return in_state;
}



void gripper(bool active)
{
	int tmp;
	if(active)
	{
		// set_io
		// rosservice "/set_io" 		  for ur_driver 	   (older version)
		//	          "/ur_driver/set_io" for ur_modern_driver (newer version)
		tmp = system("rosservice call /set_io 1 1 1");  // Int8 fun,  int8 pin , float32 state
		// fun = 1 = set digital Output
	}
	else
	{
		tmp = system("rosservice call /set_io 1 1 0");
	}

}

//void Io_Callback(const std_msgs::String::ConstPtr& msg)
//#include <ur_msgs/IOStates.h>
/*
void Io_Callback(const ur_msgs::IOStates& msg)
  {
    //ROS_INFO("I heard: [%s]", msg->data.c_str());
	ROS_INFO("I heard: ----------------------");
	ur_msgs::Digital dig_in;
	dig_in = msg.digital_in_states[7];
	ROS_INFO("dig_in.pin %i",dig_in.pin);
	//dig_in.pin = 7;
	bool st;
	st = dig_in.state;
	if(st)
	{
		ROS_INFO("HIGH");
	}
	else
	{
		ROS_INFO("LOW");

	}


    //std::cin.get();

  }
*/
int main(int argc, char **argv)
{
    ros::init(argc,argv,"robot_controller");
    ros::NodeHandle listener_node, talker_node, data_listener_node, data_talker_node;
    ros::NodeHandle nh;
    ros::NodeHandle nh_ur;


#ifdef SPIN1

#else
    ros::AsyncSpinner spinner(1); // One Thread
    spinner.start();
#endif

    ros::Publisher robot_pub = talker_node.advertise<std_msgs::String>("robot_commando", 1000);
    ros::Publisher robot_data_pub = data_talker_node.advertise<communicatie::Robot_coord>("robot_data", 1000);

    Listener_commando listener;
    ros::Subscriber robot_sub = listener_node.subscribe("visie_commando", 1000, &Listener_commando::callback, &listener);

   // ros::Subscriber ur_sub = nh_ur.subscribe("io_states",1000,Io_Callback);
    vac vac_obj;
    ros::Subscriber ur_sub = nh_ur.subscribe("io_states", 1000, &vac::callback, &vac_obj);

    ros::Publisher pointcloud2_pub = nh.advertise<sensor_msgs::PointCloud2>("pointcloud2_publ",2);

		 //param nh The ros::NodeHandle to use to subscribe.
		   //* \param topic The topic to subscribe to.
		   //* \param queue_size The subscription queue size
		   //* \param transport_hints The transport hints to pass along
		   //* \param callback_queue The callback queue to pass along
		   //*/
		  //void subscribe(ros::NodeHandle& nh,
		//		  const std::string& topic,
		//		  uint32_t queue_size,
		//		  const ros::TransportHints& transport_hints = ros::TransportHints(),
		//		  ros::CallbackQueueInterface* callback_queue = 0)

    		//<ur_msgs::IOStates>


    	Listener_robot_data data_listener;
    ros::Subscriber robot_data = data_listener_node.subscribe("visie_data", 1000, &Listener_robot_data::callback, &data_listener);

    moveit::planning_interface::MoveGroup group("manipulator");
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    moveit::planning_interface::MoveGroup::Plan my_plan;

    //robot_state::RobotState current_state;

    double speedfact;
    speedfact = 0.3;
    group.setMaxVelocityScalingFactor(speedfact);
    group.setNumPlanningAttempts(20);

    ros::Rate poll_rate(100);
    while(robot_pub.getNumSubscribers() == 0)
        poll_rate.sleep();

    std_msgs::String msg_start;
    std::stringstream ss_start;
    ss_start << "start";
    msg_start.data = ss_start.str();

    std_msgs::String msg_moved;
    std::stringstream ss_moved;
    ss_moved << "moved";
    msg_moved.data = ss_moved.str();

    int teller=0;
    int cnt_calpos=0; //Counter Calibration Pose
    bool success = false;

    struct timeval tv1, tv2; struct timezone tz;

    ROS_INFO("Get Launch parameters :");
    // Parameters
    std::string file_name , dir_name ;
    std::string fpath, file_nr;
    nh.getParam("DirName",dir_name);
    ROS_INFO(" DirName: %s",dir_name.c_str());
    nh.getParam("FileName",file_name);
    ROS_INFO(" FileName: %s",file_name.c_str());

    double StdGoalOrientationTolerance = 0.001; // ~0.06 deg
    double StdGoalPositionTolerance = 0.0001;   // 0.1mm - This is be the radius of a sphere where the end-effector must reach.
    double StdGoalJointTolerance = 0.0001;

    double PickGoalOrientationTolerance = 0.02; // ~1.2 deg
    double PickGoalPositionTolerance = 0.00025;
    double PickGoalJointTolerance = 0.0001;

    double DropGoalOrientationTolerance = 0.02;
    double DropGoalPositionTolerance = 0.00025;
    double DropGoalJointTolerance = 0.0001;

	double BigGoalOrientationTolerance = 0.2; // ~11.5deg
	double BigGoalPositionTolerance = 0.02; // 2cm
	double BigGoalJointTolerance = 0.02;  //


			if(1)
{
	double OldGoalOrientationTolerance;
	double OldGoalPositionTolerance;
	double OldGoalJointTolerance;

	OldGoalOrientationTolerance = group.getGoalOrientationTolerance();
	OldGoalPositionTolerance = group.getGoalPositionTolerance();
	OldGoalJointTolerance = group.getGoalJointTolerance();

	std::cout << "OldGoalOrientationTolerance = " << OldGoalOrientationTolerance << std::endl;
	std::cout << "OldGoalPositionTolerance = " << OldGoalPositionTolerance << std::endl;
	std::cout << "OldGoalJointTolerance = " << OldGoalJointTolerance << std::endl;

	// standard values:
	// OldGoalOrientationTolerance = 0.001 -> ~0.06 deg
	// This is the tolerance for roll, pitch and yaw, in radians.
	//
	// OldGoalPositionTolerance = 0.0001 -> 0.1mm
	// This is be the radius of a sphere where the end-effector must reach.
	//
	// OldGoalJointTolerance = 0.0001
	//
}

//	int Number_Of_Positions = 20;
//	nh.getParam("NumberOfPositions",Number_Of_Positions);
//	ROS_INFO(" aantal calibratie posities : %i ",Number_Of_Positions);

	std::vector<int> nr_list;
	nh.getParam("nr_list", nr_list);
	ROS_INFO(" # numbers in list : %i ",nr_list.size());

	 for(unsigned i=0; i < nr_list.size(); i++)
	 {
	  		ROS_INFO("nr_list[%d]=%d",i,nr_list[i]);
/*
	  		std::ostringstream convert;
	  		convert << nr_list[i];
			file_nr = convert.str();
	  		ROS_INFO("--- string : %s",file_nr.c_str());
*/
	 }

	if(1)
	{
		// We can print the name of the reference frame for this robot.
		ROS_INFO("Reference frame: %s", group.getPlanningFrame().c_str());
		// We can also print the name of the end-effector link for this group.
		ROS_INFO("Reference link : %s", group.getEndEffectorLink().c_str());
		//std::cout << "Press Enter to continue..." << std::endl;
		//std::cin.get();
		//group.setEndEffector()
		//group.setEndEffectorLink("ee_link2");
		//group.setEndEffectorLink("ee_link");
		//group.setPoseTarget()  //eelink meegeven
		//ROS_INFO("Reference frame: %s", group.getEndEffectorLink().c_str());
        //				std::cout << "Press Enter to continue..." << std::endl;
		// std::cin.get();
	}

#define CHECK_ROBOT_LIMITS
#ifdef CHECK_ROBOT_LIMITS
	std::vector<double> JointValues = group.getCurrentJointValues();

	int length = 6;
	bool range_ok = false;
	for (int i=0; i<=length; i++)
	{
		if(JointValues[i]<=-3.14 or JointValues[i]>=3.14)
		{
			range_ok=false;
			break;
		}
		else
		{
			range_ok = true;
		}
	}
	disp_JV(JointValues);
	if(range_ok)
	{
		std::cout << "Robot joint values between boundaries (limited range)" << std::endl;
	}
	else
	{
		std::cout << "WARNING!!!  The Current robot joint values are OUT OF (the limited) RANGE [-pi,pi]" << std::endl;
		std::cout << "            Are you Sure to CONTINUE? (Press Enter)" << std::endl;
		std::cin.get();
	}
#endif



//#define SAVEPOS
#ifdef SAVEPOS
            	std::vector<double> JointValues;
            	std::string fpath0,fpath1,fpath2;
				geometry_msgs::Pose gpose;

            	// save jointvalues
            	geometry_msgs::PoseStamped robotpose = group.getCurrentPose(); // Get the pose for the end-effector \e end_effector_link.
            	std::vector<double> JointVals = group.getCurrentJointValues();

            	fpath0 = "/home/rapido/catkin_ws_ur_oldfw/calib/fixed_poses/pose_JV_outofcam_03.txt";
            	write_JointValues(fpath0, JointVals);

            	fpath1 ="/home/rapido/catkin_ws_ur_oldfw/calib/fixed_poses/pose_qat_outofcam_03.txt";
            	fpath2 = "/home/rapido/catkin_ws_ur_oldfw/calib/fixed_poses/pose_rod_outofcam_03.txt";

            	write_pose(fpath1, robotpose, PT_QUAT);
            	convert_pose2UR(robotpose,gpose);
            	write_pose(fpath2, gpose, PT_RODR);
#endif

	bool add_frame = true;
	if (add_frame)
	{
		// define the pose
		geometry_msgs::Pose pose_alu_frame;
		EulerDeg2Quat(0,0,-0.005 ,90,0,135,pose_alu_frame);
		std::string bestand = "file:///home/rapido/catkin_ws_ur_oldfw/src/mplan/src/frame_en_zijkant.obj";
		add_object_CAD(bestand, pose_alu_frame, "alu_frame", group, planning_scene_interface);
	}

	bool add_table = true;
		if (add_table)
		{
			/* Define a box to add to the world. */
			shape_msgs::SolidPrimitive primitive1;

			primitive1.type = primitive1.BOX;
			primitive1.dimensions.resize(3);
			primitive1.dimensions[0] = 1.60;
			primitive1.dimensions[1] = 0.02;
			primitive1.dimensions[2] = 1.32;

			/* A pose for the box (specified relative to frame_id) */
			geometry_msgs::Pose box_pose1;

			box_pose1.orientation.x = 0.653281;
			box_pose1.orientation.y = -0.270598;
			box_pose1.orientation.z = -0.270598;
			box_pose1.orientation.w = 0.653281;

			//	box_pose1.position.x = -0.1128;
			//	box_pose1.position.y = -0.1128;
			box_pose1.position.x = -0.1;
			box_pose1.position.y = -0.1;
			box_pose1.position.z = -(0.16 + 0.02);//0.877;

			add_object_primitive(primitive1, box_pose1, "table_zone", group, planning_scene_interface);
		}


	bool camera_zone = true;
	if (camera_zone)
   	{
   		/* Define a box to add to the world. */
   		shape_msgs::SolidPrimitive primitive1;

   		primitive1.type = primitive1.BOX;
   		primitive1.dimensions.resize(3);
   		primitive1.dimensions[0] = 0.32;
   		primitive1.dimensions[1] = 0.15;
   		primitive1.dimensions[2] = 0.60;

   		/* A pose for the box (specified relative to frame_id) */
   		geometry_msgs::Pose box_pose1;

   		box_pose1.orientation.x = 0.653281;
   		box_pose1.orientation.y = -0.270598;
   		box_pose1.orientation.z = -0.270598;
   		box_pose1.orientation.w = 0.653281;

   		//	box_pose1.position.x = -0.1128;
   		//	box_pose1.position.y = -0.1128;
   		box_pose1.position.x = -0.33;
   		box_pose1.position.y = -0.33;
   		box_pose1.position.z = 0.857;//0.877;

   		add_object_primitive(primitive1, box_pose1, "camera_zone", group, planning_scene_interface);
   	}

	bool add_bin = true;
	if (add_bin)
	{
		moveit_msgs::CollisionObject collision_obj_bin1;

		// import CAD file and rotate object

		geometry_msgs::Pose pose_bin1;
		//EulerDeg2Quat(0,0,0,90,0,-45,pose_alu_frame);

		float x,y,z, afstand = 0.74;
		// x = (afstand * 0.7071) - (0.297/2);
		x = (afstand * 0.7071) - (0.397/2);
		x = -x;
		y = x;
		z = -0.17;

		EulerDeg2Quat(x,y,z,90,0, 45,pose_bin1);

		std::string bestand = "file:///home/rapido/catkin_ws_ur_oldfw/src/mplan/src/box_40x30x17.obj" ;
		//  bestand = "file:///home/gmoonen/catkin_ws_ur_oldfw/src/mplan/src/box_40x30x17.obj";
		add_object_CAD(bestand, pose_bin1, "bin1", group, planning_scene_interface);
	}


	bool add_caltab = false;
   	if (add_caltab)
   	{
   		// define the pose
   		geometry_msgs::Pose pose_caltab;
   		// GET curren pose robot
   		// add at current pose
   		// attach
   		// position robot in home (ee_link)
   		// -0.81727; -0.19137; -0.005491
   		//0.70711; -3.2757e-05; -3.2759e-05; 0.70711
   		//EulerDeg2Quat(0,0,-0.005 ,90,0,135,pose_caltab);
   		set_pose(pose_caltab, -0.81727, -0.19137, -0.005491, 0.70711, -0.00003 , -0.00003, 0.70711);
   		//std::string bestand = "file:///home/rapido/catkin_ws_ur_oldfw/calib/CAD/calibratieplaat_A5.obj";
   		//add_object_CAD(bestand, pose_caltab, "caltab", group, planning_scene_interface);

   		shape_msgs::SolidPrimitive primitive1;

   		primitive1.type = primitive1.BOX;
   		primitive1.dimensions.resize(3);
   		primitive1.dimensions[0] = 0.22;
   		primitive1.dimensions[1] = 0.16;
   		primitive1.dimensions[2] = 0.02;

   		/* A pose for the box (specified relative to frame_id) */
   		geometry_msgs::Pose caltab_pose1;

   		caltab_pose1 = pose_caltab;
//   		caltab_pose1.orientation.x = ;
//   		caltab_pose1.orientation.z = -0.270598;
//   		caltab_pose1.orientation.w = 0.653281;
//
//
//   		caltab_pose1.position.x = -0.33;
//   		caltab_pose1.position.y = -0.33;
//   		caltab_pose1.position.z = 0.857;//0.877;

   		add_object_primitive(primitive1, caltab_pose1, "caltab", group, planning_scene_interface);
   	}
   	bool add_point_cloud=false; //from file
   	if(add_point_cloud)
   	{
    	std::cout<<"Adding pointcloud"<<std::endl;

		geometry_msgs::Pose pose_pcl;
		set_pose(pose_pcl,0,0,0,0,0,0,1);

		//EulerDeg2Quat(0,0,0.5 ,90,0,135,pose_obj);
		std::string bestand = "file:////home/rapido/catkin_ws_ur_oldfw/scene robot.ply";
		add_object_CAD(bestand, pose_pcl, "points", group, planning_scene_interface);
   	}

//#define TEST_PLAN
#ifdef TEST_PLAN


   	bool add_obj_test = true;
   		if (add_obj_test)
   			{
   				// define the pose
   				geometry_msgs::Pose pose_obj;
   				//EulerDeg2Quat(0,0,0.5 ,90,0,135,pose_obj);
   				EulerDeg2Quat(-0.35,-0.35,-0.05 ,0,0,0,pose_obj);
   				//std::string bestand = "file:///home/rapido/catkin_ws_ur_oldfw/src/mplan/src/frame_en_zijkant.obj";
   				std::string bestand = "file:///home/rapido/catkin_ws_ur_oldfw/src/communicatie/model/sphere.ply";
   				add_object_CAD(bestand, pose_obj, "pick_obj", group, planning_scene_interface);
   			}

   		std::cout<<"...attach obj to robot" <<std::endl;
   		group.attachObject("pick_obj");
   		//group.getEndEffector();
   		//group.getEndEffectorLink();

   	  // See ompl_planning.yaml for a complete list
   	  //
//   		manipulator:
//   		  planner_configs:
//   		    - SBLkConfigDefault
//   		    - ESTkConfigDefault
//   		    - LBKPIECEkConfigDefault
//   		    - BKPIECEkConfigDefault
//   		    - KPIECEkConfigDefault
//   		    - RRTkConfigDefault
//   		    - RRTConnectkConfigDefault
//   		    - RRTstarkConfigDefault
//   		    - TRRTkConfigDefault
//   		    - PRMkConfigDefault
//   		    - PRMstarkConfigDefault

   		std::cout<<"...group.setPlannerId(RRTkConfigDefault);..." <<std::endl;
		group.setPlannerId("RRTkConfigDefault");
		std::cout << "Press Enter to continue+++++++++++++++++++++++++++++" << std::endl;
		std::cin.get();
	//	std::cout<<"...group.setPlannerId(RRTConnectkConfigDefault);..." <<std::endl;
	//	group.setPlannerId("RRTConnectkConfigDefault");


   	 if(1)
   	   {
   		   std::vector<std::string> obj = planning_scene_interface.getKnownObjectNames();
   		   if (obj.size() > 0)
   		   {
   			   std::cout << std::endl << "-- KNOWN COLLISION OBJECTS --" << std::endl;
   			   for (int i = 0; i < obj.size(); ++i)
   				   std::cout << obj[i] << std::endl;
   		   }
   		   else
   		   {
   			   ROS_INFO("NONE KnownObjects...");
   		   }
   	   }


 	//read_pose(fpath, fpose, PT_QUAT);
       	   std::string fpath_fixed = "/home/rapido/catkin_ws_ur_oldfw/calib/fixed_poses/pose_qat_outofcam_01.txt";
        	geometry_msgs::Pose pose_outofcam;
         	read_pose(fpath_fixed, pose_outofcam, PT_QUAT);

         	bool succs;
        	    succs = plan2pos(pose_outofcam,group,my_plan);
        	    // geeft bool terug als pad gevonden is
        	    // (als optie om niet std uit te voeren ,kan het zijn dat het niet is uitgevoerd)

        	    // wat doen als geen pad gevonden?
        	    if (!succs)
        	    {
        	    	std::cout << "Robot NIET verplaats, GEEN PAD gevonden?" << std::endl;
        	    	std::cout << "Press Enter to continue...(Check plan first)" << std::endl;
        	    	std::cin.get();
        	    }

         //}

        	    std::cout<<"Robot: Moving robot"<<std::endl;
        	    ///De robot laat aan de visie weten dat hij verplaatst is
        	    robot_pub.publish(msg_moved);


#endif


    bool vlag = false;
    while(!vlag)
    {

        while(!listener.getFinished())
        {
            ///De robot laat de visiekant weten dat we kunnen starten.
        	std::cout<<"Robot->Vision: Start vision"<<std::endl;
        	robot_pub.publish(msg_start);
#ifdef SPIN1
        	ros::spinOnce();
#else
#endif
        	///Robot wacht op commando op te bewegen (uit het zichtveld van de camera)
        	std::cout<<"...wacht op vision (get move,calib)"<<std::endl;

        	while(!listener.getMove())
        	{
        		sleep(0.001);
        		//std::cout<<".";
        		if(listener.getFinished())
        		{
                	std::cout<<"...break (getFinished)"<<std::endl;
        			break;
        		}

#ifdef SPIN1
        		ros::spinOnce();
#else
#endif
        	}
        	if(listener.getFinished())
        	{
            	std::cout<<"...break (getFinished2)"<<std::endl;
        		break;
        	}

            listener.setMove(false);
            if(listener.getCalib())
            {
              	/// Visie laat weten dat we robot-visie gaan calibreren.
            	std::cout<<"...(listener.getCalib()=true)"<<std::endl;

            	if(listener.getCalibstart())
            	{
                	std::cout<<"...(listener.getCalibstart()=true)"<<std::endl;

            		cnt_calpos=0;  //Counter Calibration Pose , eerste keer calib op nul zetten

            		group.setGoalOrientationTolerance(StdGoalOrientationTolerance);
            		group.setGoalPositionTolerance(StdGoalPositionTolerance);
            		group.setGoalJointTolerance(StdGoalJointTolerance);

            		// calibratie plaat toevoegen en aan robot bevestigen (om ermee te plannen)
            		// calibratieplaat inlezen en toevoegen
            		// ad_object_CAD()...
            		// ~/catkin_ws_ur_oldfw/calib/CAD/calibratieplaat_test.obj
            		// calibratieplaat aan robot bevestigen (attach)
            		// group.attachObject(collision_object.id);

            			//planning_scene_interface.removeCollisionObjects(collision_object_ids);
				std::vector<std::string> collision_object_ids;
				collision_object_ids.push_back("bin1");
				planning_scene_interface.removeCollisionObjects(collision_object_ids);
				std::cout<<"BIN removed from scene" <<std::endl;

            	}

                /// Verplaats de robot(met calibratieplaatje) naar een plek binnen het gezichtsveld van de camera
            	// Deze posties worden (een voor een , per iteratie) uitgelezen uit bestanden

            	// bestand / calibratie poses uitlezen
            	 geometry_msgs::Pose fpose,robotpose;

            	 std::ostringstream convert;
            	 convert << nr_list[cnt_calpos];
            	 file_nr = convert.str();
            	 //            		  		ROS_INFO("--- string : %s",file_nr.c_str());

            	 //std::string fpath = "/home/rapido/catkin_ws_ur_oldfw/src/mplan/src/poses/start_pose_write.txt";
            	 //fpath = dir_name + file_name + "1" +".txt";
            	 fpath = dir_name + file_name + file_nr +".txt";

            	 ROS_INFO(" fpath: %s",fpath.c_str());

//            	 read_pose(fpath,startpose);
            	 read_pose(fpath, fpose, PT_QUAT);

//            	 fpath = dir_name + file_name + "_JV_" + file_nr +".txt";
            	 fpath = dir_name + file_name + "JV_" + file_nr +".txt";
            	 std::vector<double> JointValues;
            	 read_JointValues(fpath,JointValues);

              	 convert_pose2UR(fpose,robotpose);
              	 disp_pose(robotpose);

              	 //robot bewegen/plannen naar calibratie_positie
              	 bool succes;
              	 //succes = plan2pos(fpose,group,my_plan);
              	 succes = plan2pos(JointValues,group,my_plan);
              	 if(0)
              	 {
              		 // save jointvalues
              		ROS_INFO("getCurrentJointValues (nr %i)", cnt_calpos);
              	    //geometry_msgs::PoseStamped robotpos = group.getCurrentPose();
              	    std::vector<double> JointVals = group.getCurrentJointValues();
               	    fpath = dir_name + file_name + "_JV_" + file_nr +".txt";
              	    write_JointValues(fpath, JointVals);

              	 }

                /// Robot coordinaten terugsturen naar Visie
                communicatie::Robot_coord robot_points;

                robot_points.x=fpose.position.x;
                robot_points.y=fpose.position.y;
                robot_points.z=fpose.position.z;

                //convert_pose2UR(fpose,robotpose); //als eerder als uitgevoerd (om op 't scherm te tonen), mag dit in commentaar.
                robot_points.rx=robotpose.orientation.x;
                robot_points.ry=robotpose.orientation.y;
                robot_points.rz=robotpose.orientation.z;

                //robot_points.seq_number = 1;
                cnt_calpos++;						  //verhogen eigenlijk al iets te vroeg, maar
                robot_points.seq_number = cnt_calpos; //seq_number begint bij 1 (niet bij 0) , maar ook goed voor nr_list.size
                robot_data_pub.publish(robot_points);

                if(cnt_calpos==nr_list.size())
                {
                	// laatste positie
                	// cnt_calpos=0; // terug op nul zetten voor volgende keer?
                	listener.setCalib(false);

                	// calibratieplaat van robot verwijderen
                }
            }
            else //"move"
            {    /// Visie laat weten dat het een scan van de bak/scene wil nemen
            	std::cout<<"Visie laat weten dat het een scan wil nemen"<<std::endl;
#ifdef W84ENTER
            	std::cout<<"Druk enter om de robot opzij te plaatsen..."<<std::endl;
            	std::cin.get();
#else
            	std::cout<<"De robot gaat opzij bewegen..."<<std::endl;
#endif

            	/// Code aanroepen om de robot te verplaatsen naar een plek buiten het gezichtsveld van de camera.

            	//read_pose(fpath, fpose, PT_QUAT);
            	bool succs;
            	std::string fpath_fixed;
            	if(0) // plan to pose
            	{
            		fpath_fixed = "/home/rapido/catkin_ws_ur_oldfw/calib/fixed_poses/pose_qat_outofcam_01.txt";
            		geometry_msgs::Pose pose_outofcam;
            		read_pose(fpath_fixed, pose_outofcam, PT_QUAT);
            		succs = plan2pos(pose_outofcam,group,my_plan);
            	}
            	else // plan to JointValues
            	{
            		//fpath = dir_name + file_name + "_JV_" + file_nr +".txt";
            		fpath_fixed = "/home/rapido/catkin_ws_ur_oldfw/calib/fixed_poses/pose_JV_outofcam_03.txt";
            		std::vector<double> JointValues;
            		read_JointValues(fpath_fixed,JointValues);
            		// group.setGoalOrientationTolerance(StdGoalOrientationTolerance);
            		// group.setGoalPositionTolerance(StdGoalPositionTolerance);
            		group.setGoalJointTolerance(BigGoalJointTolerance);

            		succs = plan2pos(JointValues,group,my_plan);
            	}

            	// geeft bool terug als pad gevonden is
            	// (als optie om niet std uit te voeren ,kan het zijn dat het niet is uitgevoerd)

            	//           	    std::string fpath_fixed2 = "/home/rapido/catkin_ws_ur_oldfw/calib/fixed_poses/pose_qat_outofcam_JV_01.txt";

            	// std::vector<double> JointValues;

            //	read_JointValues(fpath_fixed2,JointValues);

            	//save robot poses (quat, rod, JointValues)






            	//?   }

            	// wat doen als geen pad gevonden?
            	if (!succs)
            	{
            		std::cout << "Robot NIET verplaats, GEEN PAD gevonden?" << std::endl;
            		std::cout << "Press Enter to continue...(Check plan first)" << std::endl;
            		std::cin.get();
            	}

            	//}

           	    std::cout<<"Robot: Moving robot"<<std::endl;
           	    ///De robot laat aan de visie weten dat hij verplaatst is
           	    robot_pub.publish(msg_moved);
           	    ros::spinOnce();
            }
        }
        // Visie heeft laten weten dat visie klaar is met beelden nemen,
        // en ons dadelijk ook data gaat doorsturen (pickpose(s) en puntenwolk)
        //												en object? gaan ervan uit dat we weten wat we picken (CAD)

        std::cout<<"Robot: Vision finished received, iteration "<<teller<<std::endl;
        std::cout<<data_listener.getID()<<std::endl;

        // robot al boven bak bewegen
        std::string fpath_fixed = "/home/rapido/catkin_ws_ur_oldfw/calib/fixed_poses/pose_qat_bovenbak_01.txt";
  	    geometry_msgs::Pose pose_ready2pick;
        read_pose(fpath_fixed, pose_ready2pick, PT_QUAT);

    	group.setGoalOrientationTolerance(BigGoalOrientationTolerance);
        group.setGoalPositionTolerance(BigGoalPositionTolerance);
        group.setGoalJointTolerance(BigGoalJointTolerance);

        bool succs;
        succs = plan2pos(pose_ready2pick, group, my_plan);
        // wat doen als geen pad gevonden? -> volgende pose/ object
        if (!succs)
        {
        	std::cout << "Robot NOT moved (No good plan found)" << std::endl;
        	std::cout << "Press Enter to continue...(Check plan first)" << std::endl;
        	std::cin.get();
        }

        // gripper aanzetten
        std::cout<<"Activate gripper" <<std::endl;
        gripper(true);


        while(data_listener.getID() < 0)
        {
        	//std::cout<<data_listener.getID()<<std::endl;
        	ros::spinOnce();
        }
        ///de doorgestuurde info zit in het data_listener object. Via de getters kan je er nu aan.
        if(data_listener.getScene().height != 0)
            std::cout<<"het werkt!"<< data_listener.getScene().height<<std::endl;

		/// Code voor het picken van een object. ---------------------------------

		// data_listener.getRot();
        // data_listener.getTrans();
        cv::Point3d rot;
        cv::Point3d transl;
        std::vector<double> quatern;

        rot = data_listener.getRot();
        transl = data_listener.getTrans();
        quatern = data_listener.getQuat();


//        geometry_msgs::Pose posein,poseout;
//        set_pose(posein,0,0,0,rot.x,rot.y,rot.z,-99);
//        convert_pose2quat(posein,poseout);



        std::cout<<"Ontvangen punt... "<<std::endl; //(negeer laatste waarde)
        geometry_msgs::Pose pp; //pickpose
        // setpose (x,y,z,rx,ry,rz,rw);
        // (nog omvormen nr quat);
        // set_pose(pp,transl.x,transl.y,transl.z,-rot.x,-rot.z,-rot.y,0);

        set_pose(pp,transl.x, transl.y, transl.z, float (quatern[0]), float (quatern[1]), float(quatern[2]), float(quatern[3]) );
        // set_pose(pp, 0.30, -0.23, 0.4, float (quatern[0]), float (quatern[1]), float(quatern[2]), float(quatern[3]) );
        /// set_pose(pp, 0.30, -0.23, 0.4, poseout.orientation.x ,poseout.orientation.y, poseout.orientation.z , poseout.orientation.w);

        disp_pose(pp);


        // std::cout<< transl.x << transl.y << transl.z << rot.x << rot.y <<rot.z <<std::endl;
        bool check_valid = true;
        bool position = true;
        if(check_valid)
        {
        	 //simple check
        	 double x_min, y_min, z_min, x_max, y_max ,z_max;
        	 double x,y,z;
        	 double xo,yo;
/*
        	 x_min = -0.60;
        	 x_max = -0.25;
        	 y_min = -0.60;
        	 y_max = -0.25;
        	 z_min = -0.14;
			 z_max =  0.06;
 */
        	 x_min = -0.2;
        	 x_max =  0.2;
        	 y_min = -0.60;
        	 y_max = -0.30;
        	 z_min = -0.14;
        	 z_max =  0.06;

        	 x = pp.position.x;
        	 y = pp.position.y;
        	 z = pp.position.z;

			 // x' = x cos a -y sin a
			 // y' = x sin a + y cos a
			 // a= 45°

			 xo = x * 0.707 - y * 0.707;
			 yo = x * 0.707 + y * 0.707;

			 x=xo;
			 y=yo;

    	     std::cout<<" x' " << x << " y' " << y << std::endl; //(negeer laatste waarde)


        	 if(x < x_min or x > x_max or
        			 y < y_min or y > y_max or
        			 z < z_min or z > z_max)
        	 {
        		 position = false;
        	     std::cout<<" pickpoint out of BINrange... "<<std::endl; //(negeer laatste waarde)
        	     std::cin.get();
        	 }
         }



#ifdef W84ENTER
        std::cout<<"Press Enter..."<<std::endl;
        std::cin.get();
#endif

		// data_listener.getTrans();
        sensor_msgs::PointCloud2 Scene =  data_listener.getScene();
        Scene.header.frame_id = "base_link";
        pointcloud2_pub.publish(Scene);
        ros::spinOnce();

        geometry_msgs::Pose pose_2pick,pose_object,pose_scene, pose_end;

#define USE_REAL_QUAT
#ifdef USE_REAL_QUAT
        pose_2pick=pp;
        pose_2pick.position.z=transl.z - 0.005;
#else
        //read quat from file
        fpath_fixed = "/home/rapido/catkin_ws_ur_oldfw/calib/fixed_poses/pose_qat_bovenbak_01.txt";
        read_pose(fpath_fixed, pose_2pick, PT_QUAT);

        // pose_2pick.position.x=pose_2pick.position.x-0.1;
        // pose_2pick.position.y=pose_2pick.position.y-0.1;
        // pose_2pick.position.z=pose_2pick.position.z-0.05;

        // coördinaten (translatie) overschrijven met die van het echte gevonden punt.
        pose_2pick.position.x=transl.x;
        pose_2pick.position.y=transl.y;
        pose_2pick.position.z=transl.z - 0.003;
        // orientatie voorlopig nog van uitgelezen pose (+/- vertikaal naar beneden)
#endif

        // plan 2 pick position
        // group.setEndEffectorLink("ee_link2");
        //ROS_INFO("Reference link : %s", group.getEndEffectorLink().c_str());
        // bool succs;
        group.setGoalOrientationTolerance(PickGoalOrientationTolerance);
        group.setGoalPositionTolerance(PickGoalPositionTolerance);
        group.setGoalJointTolerance(PickGoalJointTolerance);

        succs = plan2pos(pose_2pick, group, my_plan);
        //ROS_INFO("Reference link : %s", group.getEndEffectorLink().c_str());

        // wat doen als geen pad gevonden? -> volgende pose / object
        if (!succs)
        {
        	std::cout << "Robot NOT moved (No good plan found)" << std::endl;
        	std::cout << "Press Enter to continue...(Check plan first)" << std::endl;
        	std::cin.get();
        }

        if(1) // plan terug enkel mm omhoog
        {
           	std::cout << "Plan terug enkel mm omhoog" << std::endl;
            pose_2pick.position.z=transl.z + 0.005; //5mm hoger dan gevonden object
            succs = plan2pos(pose_2pick, group, my_plan);
        }


        // add object to pick
        bool add_obj2pick = true;
        if (add_obj2pick)
        {
#ifdef W84ENTER
        	std::cout << "Press Enter to add obj2pick..." << std::endl;
        	std::cin.get();
#else
        	std::cout << "add obj2pick..." << std::endl;
#endif

        	//std::string bestand = "file:///home/rapido/catkin_ws_ur_oldfw/src/mplan/src/frame_en_zijkant.obj";
        	std::string bestand = "file:///home/rapido/catkin_ws_ur_oldfw/src/communicatie/model/sphere.ply";

        	if(0)
        	{
        		// define the pose
        		geometry_msgs::Pose pose_obj;
        		//EulerDeg2Quat(0,0,0.5 ,90,0,135,pose_obj);
        		EulerDeg2Quat(-0.3,-0.3,-0.10 ,0,0,0,pose_obj);
        		add_object_CAD(bestand, pose_obj, "pick_obj", group, planning_scene_interface);
        	}
        	else
        	{
                pose_2pick.position.z = transl.z; // terug op originele hoogte leggen , of hoger?
        		// nog geen onderscheid tussen object_pose en pick_pose
        		pose_object = pose_2pick;
        		//disp_pose(pose_object);

        		double dx,dy,dz;
        		double r; // radius ball (the axes are in the center of the ball)
        		r = 0.04;
        		dx = -(rot.x * r);
        		dy = -(rot.y * r);
        		dz = -(rot.z * r);
        		pose_object.position.x = pose_object.position.x + dx;
        		pose_object.position.y = pose_object.position.y + dy;
        		pose_object.position.z = pose_object.position.z + dz;

        		//pose_object.position.z = pose_2pick.position.z - 0.04; // min halve diameter object

        		pose_object.position.z = pose_object.position.z + 0.001; // 1mm hoger leggen
        		disp_pose(pose_object);
        		//std::cin.get();
        		add_object_CAD(bestand, pose_object, "pick_obj", group, planning_scene_interface);
        	}
        }

        // attach object to robot
        std::cout<<"...attach obj to robot" <<std::endl;
        group.attachObject("pick_obj");

//        // gripper aanzetten
//        std::cout<<"Activate gripper" <<std::endl;
//        gripper(true);
        // al eerder aangezet...(voor dat we ernaar toe bewegen)

        // wachten op vaccum
#ifdef W84VAC
        std::cout << "Wait for vacuum..." << std::endl;



        while(!vac_obj.getState())
        {
			ros::spinOnce();
			//break;  // for simulation without robot
        }
        std::cout << "Vacuum OK" << std::endl;
#endif
        // scene pose?
        // std::cout << "TODO: Add pointcloud to planningscene..." << std::endl;

        // succs = plan2pos(pose_2pick, group, my_plan);
        // wat doen als geen pad gevonden? -> volgende pose/ object

#ifdef W84ENTER
        std::cout << "Press Enter to continue..." << std::endl;
        std::cin.get();
#endif

#ifdef TEST_NAAST

			std::cout << "Beweeg naast bak (test of ie door de bak gaat)..." << std::endl;

			//test beweeg weg naast bak (om te kijken of ie er door gaat)

			pose_2pick.position.x = pose_2pick.position.x - 0.15;
			pose_2pick.position.y = pose_2pick.position.y + 0.30;
			//pose_2pick.position.z=pose_2pick.position.z-0.15;


		      if(1) //constraints opleggen om geen gekke beweging te krijgen?
		        {
		        	// eerst zorgen dat we de orientatie van de startpositie kennen?
		        	// om er voor te zorgen dat deze voldoet aan de constraints?)


		        	//group.getCurrentState()
		        	//group.getCurrentPose()

		        	// Planning with Path Constraints
		        	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		        	//
		        	// Path constraints can easily be specified for a link on the robot.
		        	// Let's specify a path constraint and a pose goal for our group.
		        	// First define the path constraint.
		        	//
		        	// OrientationConstraint
		        	// PositionConstraints
		        	// VisibilityConstraints
		        	// JointConstraint

		        	moveit_msgs::OrientationConstraint ocm;
		        	ocm.link_name = "ee_link";
		        	ocm.header.frame_id = "base_link";
//		        	ocm.orientation.w = 1.0;
		        	ocm.orientation.w = pose_2pick.orientation.w;
		        	ocm.orientation.x = pose_2pick.orientation.x;
		        	ocm.orientation.y = pose_2pick.orientation.y;
		        	ocm.orientation.z = pose_2pick.orientation.z;

		        	// tolerance is optional
		        	float tol = 0.1;
		        	tol = 1;
		        	ocm.absolute_x_axis_tolerance = tol;
		        	ocm.absolute_y_axis_tolerance = tol;
		        	ocm.absolute_z_axis_tolerance = tol;

		        	// A weighting factor for this constraint
		        	//   (denotes relative importance to other constraints.
		        	//   Closer to zero means less important
		        	// bij gebruik van meerdere constraints...
		        	ocm.weight = 1.0;


		        	// Now, set it as the path constraint for the group.
		        	moveit_msgs::Constraints test_constraints;
		        	test_constraints.orientation_constraints.push_back(ocm);
		        	group.setPathConstraints(test_constraints);
		        }



			// bool succs;
			succs = plan2pos(pose_2pick, group, my_plan);
			// wat doen als geen pad gevonden? -> volgende pose/ object
			if (!succs)
			{
				std::cout << "Robot NOT moved (No good plan found)" << std::endl;
				std::cout << "Press Enter to continue...(Check plan first)"	<< std::endl;
				std::cin.get();
			}

#endif

		if(1)
		{
			std::cout << " plan (back) above box..." << std::endl;
			bool succs;
			pose_ready2pick.position.z = pose_ready2pick.position.z +0.05;

			// orientatie en positie mogen veel afwijken
			group.setGoalOrientationTolerance(BigGoalOrientationTolerance);
		    group.setGoalPositionTolerance(BigGoalPositionTolerance);
		    group.setGoalJointTolerance(BigGoalJointTolerance);

			succs = plan2pos(pose_ready2pick, group, my_plan);
			// wat doen als geen pad gevonden? -> volgende pose/ object
			if (!succs)
			{
				std::cout << "Robot NOT moved (No good plan found)" << std::endl;
				std::cout << "Press Enter to continue...(Check plan first)" << std::endl;
				std::cin.get();
			}
		}

		if(1)
		{
			fpath_fixed = "/home/rapido/catkin_ws_ur_oldfw/calib/fixed_poses/pose_qat_afleg_01.txt";
	        read_pose(fpath_fixed, pose_end, PT_QUAT);

	        std::cout << "Go to drop position..." << std::endl;
	        group.setGoalOrientationTolerance(DropGoalOrientationTolerance);
	        group.setGoalPositionTolerance(DropGoalPositionTolerance);
	        group.setGoalJointTolerance(DropGoalJointTolerance);
	        bool succs;
	        succs = plan2pos(pose_end, group, my_plan);
	        // wat doen als geen pad gevonden? -> volgende pose/ object
	        if (!succs)
			{
				std::cout << "Robot NOT moved (No good plan found)" << std::endl;
				std::cout << "Press Enter to continue...(Check plan first)" << std::endl;
				std::cin.get();
			}
		}

		/// detach object
#ifdef W84ENTER
		std::cout << "Press Enter to Detach object..." << std::endl;
		std::cin.get();
#else
		std::cout << "Detach object..." << std::endl;
#endif
		group.detachObject("pick_obj");

		/// deactivate gripper
		std::cout<<"Deactivate gripper" <<std::endl;
		gripper(false);

		/// remove object from scene
#ifdef W84ENTER
		std::cout << "Press Enter to remove object from scene..." << std::endl;
		std::cin.get();
#else
		std::cout << "Removing Object from Scene..." << std::endl;
#endif
		//		 group.
		std::vector<std::string> collision_object_ids;
		collision_object_ids.push_back("pick_obj");
		planning_scene_interface.removeCollisionObjects(collision_object_ids);
		std::cout<<"picking object removed from scene" <<std::endl;

		listener.setFinished(false);
		///ID terug op -1 zetten zodat er de volgende keer weer gewacht wordt tot er nieuwe data in data_listener zit.
		data_listener.setID(-1);
		teller++;
		if(teller>5)
		{
			/// Met deze laatste "start" laat de robot aan de visie weten dat het picken gelukt is, en er nog een pose mag doorgegeven worden"
			std::cout<<"Robot: Start vision"<<std::endl;
			robot_pub.publish(msg_start);
			ros::spinOnce();
			break;
		}
    }
}

