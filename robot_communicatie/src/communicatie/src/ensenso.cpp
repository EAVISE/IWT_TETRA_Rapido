#include "../include/communicatie/ensenso.hpp"
#include "../include/communicatie/calib_en/nxLib.h"
#include <ueye.h>

void ensensoExceptionHandling (const NxLibException &ex,
                               std::string func_nam)
{
    PCL_ERROR ("%s: NxLib error %s (%d) occurred while accessing item %s.\n", func_nam.c_str (), ex.getErrorText ().c_str (), ex.getErrorCode (),
               ex.getItemPath ().c_str ());
    if (ex.getErrorCode () == NxLibExecutionFailed)
    {
        NxLibCommand cmd ("");
        PCL_WARN ("\n%s\n", cmd.result ().asJson (true, 4, false).c_str ());
    }
}

/*
 * INOUT: cloud: save registered pointcloud in cloud.
 *
 * This function grabs a pointcloud from the ensenso camera
 *
 */
void get_en_image(cv::Mat &cloud)
{
    char flag = 'g';
    int i = 0;
    while(flag != 'q')
    {
        ostringstream conv;
        conv << i;
        cout<<"Capturing new calibration image from the ensenso stereo vision camera."<<endl;
        ///Read the Ensenso stereo cameras:
        try {
            // Initialize NxLib and enumerate cameras
            nxLibInitialize(true);

            // Reference to the first camera in the node BySerialNo
            NxLibItem root;
            NxLibItem camera = root[itmCameras][itmBySerialNo][0];

            // Open the Ensenso
            NxLibCommand open(cmdOpen);
            open.parameters()[itmCameras] = camera[itmSerialNumber].asString();
            open.execute();

            // Capture an image
            NxLibCommand (cmdCapture).execute();

            // Stereo matching task
            NxLibCommand (cmdComputeDisparityMap).execute ();

            // Convert disparity map into XYZ data for each pixel
            NxLibCommand (cmdComputePointMap).execute ();

            // Get info about the computed point map and copy it into a std::vector
            double timestamp;
            std::vector<float> image;
            std::vector<float> pointMap;
            int width, width_i, height, height_i, channels, channels_i, bytesperelement, bytesperelement_i;
            camera[itmImages][itmRaw][itmLeft].getBinaryDataInfo (&width_i, &height_i, &channels_i, &bytesperelement_i, 0, &timestamp);  // Get raw image timestamp
            camera[itmImages][itmRaw][itmLeft].getBinaryData (image, 0);  // Get raw image timestamp

            camera[itmImages][itmPointMap].getBinaryDataInfo (&width, &height, &channels, &bytesperelement, 0, 0);
            camera[itmImages][itmPointMap].getBinaryData (pointMap, 0);

            /*std::cout<<"width_i "<<width_i<<" height_i "<<height_i<<" channels_i "<<channels_i<<" bytes_i "<<bytesperelement_i<<std::endl;
            std::cout<<"width "<<width<<" height "<<height<<" channels "<<channels<<" bytes "<<bytesperelement<<std::endl;
            for(int j = 0, k = 0; j< pointMap.size(); j += 3 , k += 2)
            {
                std::cout<<"Image_x: "<<image[k]<<" image_y "<<image[k+1]<<std::endl;
                std::cout<<"pointmap_x: "<<pointMap[j]<<" pointmap_y: "<<pointMap[j+1]<<" pointmap_z "<<pointMap[j+2]<<std::endl;
            }*/

            /*// Copy point cloud and convert in meters
            //cloud.header.stamp = getPCLStamp (timestamp);
            cloud.resize (height * width);
            cloud.width = width;
            cloud.height = height;
            cloud.is_dense = false;

            // Copy data in point cloud (and convert milimeters in meters)
            for (size_t i = 0; i < pointMap.size (); i += 3)
            {
              cloud.points[i / 3].x = pointMap[i] / 1000.0;
              cloud.points[i / 3].y = pointMap[i + 1] / 1000.0;
              cloud.points[i / 3].z = pointMap[i + 2] / 1000.0;
            }*/

            cloud = cv::Mat(4, pointMap.size()/3, CV_32FC1);

            for (size_t i = 0; i < pointMap.size (); i += 3)
            {
                cloud.at<float>(0, i/3) = pointMap[i] / 1000.0;
                cloud.at<float>(1, i/3) = pointMap[i + 1] / 1000.0;
                cloud.at<float>(2, i/3) = pointMap[i + 2] / 1000.0;
                cloud.at<float>(3, i/3) = 1;
            }

            NxLibCommand (cmdRectifyImages).execute();

            // Save images
            NxLibCommand saveImage(cmdSaveImage);
            //   raw left
            saveImage.parameters()[itmNode] = camera[itmImages][itmRaw][itmLeft].path;
            saveImage.parameters()[itmFilename] = "calib_en/raw_left" + conv.str()+".png";
            saveImage.execute();
            //   raw right
            /*saveImage.parameters()[itmNode] = camera[itmImages][itmRaw][itmRight].path;
            saveImage.parameters()[itmFilename] = "calib_en/raw_right.png";
            saveImage.execute();
            //   rectified left
            saveImage.parameters()[itmNode] = camera[itmImages][itmRectified][itmLeft].path;
            saveImage.parameters()[itmFilename] = "calib_en/rectified_left.png";
            saveImage.execute();
            //   rectified right
            saveImage.parameters()[itmNode] = camera[itmImages][itmRectified][itmRight].path;
            saveImage.parameters()[itmFilename] = "calib_en/rectified_right.png";
            saveImage.execute();*/
        } catch (NxLibException& e) { // Display NxLib API exceptions, if any
            printf("An NxLib API error with code %d (%s) occurred while accessing item %s.\n", e.getErrorCode(), e.getErrorText().c_str(), e.getItemPath().c_str());
            if (e.getErrorCode() == NxLibExecutionFailed) printf("/Execute:\n%s\n", NxLibItem(itmExecute).asJson(true).c_str());
        }
        /*catch (NxLibException &ex)
        {
            ensensoExceptionHandling (ex, "grabSingleCloud");
        }*/
        catch (...) { // Display other exceptions
            printf("Something, somewhere went terribly wrong!\n");
        }

        cout<<"To quit capturing calibration images, choose q. Else, choose any other letter."<<endl;
        cin >> flag;
        i++;
    }
}

/*
 *
 * IN: i: number of the calibration image being taken.
 *
 * Saves the image of the left camera in RGB. This can be used to calibrate the ensenso with a robot.
 *
 */
void getCalibRGBImage(int i)
{
    ostringstream conv;
    conv << i;

    cout<<"Capturing new calibration image from the ensenso RGB camera."<<endl;

    ///Read the IDS RGB Camera attached to the Ensenso stereo camera
    HIDS hCam = 0;
    printf("Success-Code: %d\n",IS_SUCCESS);
    //Kamera öffnen
    INT nRet = is_InitCamera (&hCam, NULL);
    printf("Status Init %d\n",nRet);

    //Pixel-Clock setzen
    UINT nPixelClockDefault = 9;
    nRet = is_PixelClock(hCam, IS_PIXELCLOCK_CMD_SET,
                         (void*)&nPixelClockDefault,
                         sizeof(nPixelClockDefault));

    printf("Status is_PixelClock %d\n",nRet);

    //Farbmodus der Kamera setzen
    //INT colorMode = IS_CM_CBYCRY_PACKED;
    INT colorMode = IS_CM_BGR8_PACKED;

    nRet = is_SetColorMode(hCam,colorMode);
    printf("Status SetColorMode %d\n",nRet);

    UINT formatID = 4;
    //Bildgröße einstellen -> 2592x1944
    nRet = is_ImageFormat(hCam, IMGFRMT_CMD_SET_FORMAT, &formatID, 4);
    printf("Status ImageFormat %d\n",nRet);

    //Speicher für Bild alloziieren
    char* pMem = NULL;
    int memID = 0;
    nRet = is_AllocImageMem(hCam, 1280, 1024, 24, &pMem, &memID);
    printf("Status AllocImage %d\n",nRet);

    //diesen Speicher aktiv setzen
    nRet = is_SetImageMem(hCam, pMem, memID);
    printf("Status SetImageMem %d\n",nRet);

    //Bilder im Kameraspeicher belassen
    INT displayMode = IS_SET_DM_DIB;
    nRet = is_SetDisplayMode (hCam, displayMode);
    printf("Status displayMode %d\n",nRet);

    //Bild aufnehmen
    nRet = is_FreezeVideo(hCam, IS_WAIT);
    printf("Status is_FreezeVideo %d\n",nRet);

    //Bild aus dem Speicher auslesen und als Datei speichern
    string path = "./EN/snap_BGR"+conv.str()+".png";
    std::wstring widepath;
    for(int j = 0; j < path.length(); ++j)
        widepath += wchar_t (path[j] );

    IMAGE_FILE_PARAMS ImageFileParams;
    ImageFileParams.pwchFileName = &widepath[0];
    ImageFileParams.pnImageID = NULL;
    ImageFileParams.ppcImageMem = NULL;
    ImageFileParams.nQuality = 0;
    ImageFileParams.nFileType = IS_IMG_PNG;

    nRet = is_ImageFile(hCam, IS_IMAGE_FILE_CMD_SAVE, (void*) &ImageFileParams, sizeof(ImageFileParams));
    printf("Status is_ImageFile %d\n",nRet);

    //Kamera wieder freigeben
    is_ExitCamera(hCam);
}

/*
 *
 * INOUT: cloud : cloud being grabbed from ensenso.
 * INOUT: image: Image of the left camera that belongs to the cloud.
 *
 * Saves the image of the left camera in RGB and the pointcloud. This can be used to calibrate the ensenso with a robot.
 *
 */

void getCalibImage(cv::Mat &cloud, cv::Mat &image)
{
    NxLibItem root; // References the tree's root item at path "/"

    //replace "1234" with your camera's serial number:
    NxLibItem camera = root[itmCameras][itmBySerialNo]["1234"]; // References the camera's item at path "/Cameras/BySerialNo/1234"

    // Call the Open command to open the camera
    NxLibCommand open(cmdOpen); // Note: the variable name 'open' does not matter; the constant 'cmdOpen'
    // specifies the name of the command to be executed when '.execute()' is called
    // on the object.
    // Set parameters for the command
    open.parameters()[itmCameras] = "1234"; // Insert the serial number of the camera to be opened into the Cameras parameter of Open
    open.execute();

    // Capture a single image, compute the x,y,z coordinates of all pixels and find their average Z value

    // Execute the Capture command with default parameters
    NxLibCommand capture(cmdCapture);
    capture.execute();

    // Now we can compute the disparity map and point cloud. Computing the disparity map is the computationally most demanding
    // part. All other computations (such as the generation of the x,y,z data via ComputePointMap) are based the computed
    // DisparityMap. It is thus necessary to call ComputeDisparityMap first before calling ComputePointMap.

    NxLibCommand computeDisparity(cmdComputeDisparityMap);
    computeDisparity.execute();

    NxLibCommand computePoints(cmdComputePointMap);
    computePoints.execute();

    int width, height, channels, bytesPerElement;
    double timestamp;

    // query information about the binary data
    camera[itmImages][itmPointMap].getBinaryDataInfo(&width, &height, &channels, &bytesPerElement, 0, &timestamp);
    assert(width > 0 && height > 0 && channels == 3);   // the binary data format is fixed, but let's be safe and do some basic checks
    assert(isFloat && bytesPerElement == sizeof(float));


    std::vector<float> pointMap; // This is where the point cloud data will be copied to
    camera[itmImages][itmPointMap].getBinaryData(pointMap, 0); // points will now contain x, y and z coordinates for each image pixel

    std::vector<uchar> imageMap;
    int  width_i,  height_i, channels_i, bytesperelement_i;
    camera[itmImages][itmRaw][itmLeft].getBinaryDataInfo (&width_i, &height_i, &channels_i, &bytesperelement_i, 0, &timestamp);  // Get raw image timestamp
    camera[itmImages][itmRaw][itmLeft].getBinaryData (imageMap, 0);  // Get raw image timestamp

    ///Convert pointMap to a cv::Mat
    //cloud = cv::Mat(4, pointMap.size()/3, CV_32FC1);

    cloud = cv::Mat(width,height, CV_32FC4);

    int cols = 0, rows = 0;
    for (size_t i = 0; i < pointMap.size (); i += 3)
    {
        cloud.at<cv::Vec4f>(rows, cols)[0] = pointMap[i] / 1000.0;
        cloud.at<cv::Vec4f>(rows, cols)[1] = pointMap[i + 1] / 1000.0;
        cloud.at<cv::Vec4f>(rows, cols)[2] = pointMap[i + 2] / 1000.0;
        cloud.at<cv::Vec4f>(rows, cols)[3] = 1;
        cols++;
        if(cols >= width)
        {
            cols = 0;
            rows++;
        }
        if(rows >= height)
            std::cout<<"More points than there are in the cloud."<<std::endl;
    }

    ///Convert image to cv::Mat:
    image = cv::Mat(height_i, width_i, CV_8UC1);
    cols = 0;
    rows = 0;
    for( size_t i =0; i< imageMap.size()/2; i++)
    {
        image.at<uchar>(rows, cols) = imageMap[i];
        cols++;
        if(cols >= width_i)
        {
            cols = 0;
            rows++;
        }

        if(rows >= height_i)
            std::cout<<"More points than there are in the image."<<std::endl;
    }
}

/*
 *
 * IN: Cloud: Pointcloud of the scene
 * IN: Image: image of the scene
 * IN: boardw: Width of the chessboard to be found
 * IN: boardh: height of the chessboard to be found.
 *
 * OUT: centerpoint of the chessboard in 3D
 *
 * This function uses the image to find chessboard corners. After this, the chessboardcorners are used to find the corresponding points in the pointcloud.
 * The average of these points is the center of the chessboard.
 *
 */

cv::Point3d getCenterPointEN(cv::Mat cloud, cv::Mat image, int boardw, int boardh)
{
    cv::namedWindow("test");
    cv::imshow("test", image);
    cv::waitKey(0);

    ///Find chessboardcorners

    cv::Size boardSize(boardw, boardh);
    std::vector<cv::Point2f> chessboardcorners;

    bool found1_1 = cv::findChessboardCorners(image, boardSize, chessboardcorners,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE /*| CALIB_CB_FAST_CHECK*/);

    if(!found1_1)
    {
        std::cerr << "Checkboard corners not found!" << std::endl;
        return cv::Point3d();
    }

    cornerSubPix(image, chessboardcorners, cv::Size(11,11), cv::Size(-1,-1), cv::TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 300, 0.001));
    cvtColor(image, image, CV_GRAY2BGR);
    drawChessboardCorners( image, boardSize, chessboardcorners, found1_1 );
    cv::namedWindow("zijn ze gevonden");
    cv::imshow("zijn ze gevonden", image);
    cv::waitKey(0);

    ///Find 3d coordinates of chessboardcorner:
    cv::Point3d punt;
    for(size_t i = 0; i< chessboardcorners.size(); i++)
    {
        punt.x += cloud.at<cv::Vec4f>(chessboardcorners[i].y, chessboardcorners[i].x)[0];
        punt.y += cloud.at<cv::Vec4f>(chessboardcorners[i].y, chessboardcorners[i].x)[1];
        punt.z += cloud.at<cv::Vec4f>(chessboardcorners[i].y, chessboardcorners[i].x)[2];
    }

    return cv::Point3d( punt.x/chessboardcorners.size(), punt.y/chessboardcorners.size(), punt.z/chessboardcorners.size() );
}
