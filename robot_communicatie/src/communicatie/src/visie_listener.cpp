#include "../include/communicatie/visie.hpp"
Listener::Listener()
{
    moved = false;
    start = false;
}

void Listener::callback(const std_msgs::String::ConstPtr msg)
{
    if(msg->data == "start")
        setStart(true);
    else if(msg->data == "moved")
        setMoved(true);
}

bool Listener::getMoved()
{
    return moved;
}

bool Listener::getStart()
{
    return start;
}

void Listener::setMoved(bool a){
    moved = a;
}

void Listener::setStart(bool a){
    start = a;
}

Listener_data::Listener_data()
{
    translatie = cv::Point3d(0,0,0);
    rotatie = cv::Point3d(0,0,0);
    id = -1;
}

void Listener_data::callback(const communicatie::Robot_coord::ConstPtr msg)
{
    cv::Point3d trans = cv::Point3d(msg->x, msg->y, msg->z);
    setTrans(trans);
    cv::Point3d rot = cv::Point3d(msg->rx, msg->ry, msg->rz);
    setRot(rot);
    setID(msg->seq_number);
}

cv::Point3d Listener_data::getTrans()
{
    return translatie;
}

cv::Point3d Listener_data::getRot()
{
    return rotatie;
}

int Listener_data::getID()
{
    return id;
}

void Listener_data::setTrans(cv::Point3d a)
{
    translatie = a;
}

void Listener_data::setRot(cv::Point3d a)
{
    rotatie = a;
}

void Listener_data::setID(int a)
{
    id = a;
}
