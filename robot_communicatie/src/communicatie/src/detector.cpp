#include "../include/communicatie/detector.hpp"
#include "../include/communicatie/structuredlight.hpp"

/*
 * IN: Parameters to finetune the PPFDetector class
 * Meaning of these values can be found in a paper by Wim Abbeloos.
 *
 * Constructor of the PPFDetector class
 *
 */

PPFDetector::PPFDetector(double RDS, double RSS, double NA,  double PT, double RT, double UWC, double RSD, double RSSS)
{
    RelativeDistanceStep = RDS;
    RelativeSamplingStep = RSS;
    NumAngles = NA;
    positionThreshold = PT;
    rotationThreshold = RT;
    useWeightedClustering = UWC;
    relativeSceneDistance = RSD;
    relativeSceneSampleStep =  RSSS;
    detector = new cv::ppf_match_3d::PPF3DDetector(RSS, RDS, NA);
}

/*
 *
 * IN: path: Path to the model that needs to be trained
 *
 * Trains a model for the PPFDetector object. Needs to be called before you can search for a pickpoint
 *
 */

void PPFDetector::trainModel(std::string path)
{
    cv::Mat pc = cv::ppf_match_3d::loadPLYSimple(path.c_str(), 1);

    int64 tick1 = cv::getTickCount();
    detector->trainModel(pc);
    int64 tick2 = cv::getTickCount();
    cout << endl << "Training Elapsed Time " << (tick2-tick1)/cv::getTickFrequency() << " sec" << endl;

}

/*
 *
 * IN: pc: pc is the scene in which you want to find a previously trained model.
 * OUT: Pickpoint: Struct with x,y,z coordinates, normals, quaternion and model in scene of the found model.
 *
 * Find the model in the scene. This function returns the x,y,z coordinates, x,y,z of the normal, the quaternion and the transformed model(where the model is in the scene).
 *
 */
Pickpoint PPFDetector::getPickPoint(cv::Mat pc)
{
    Pickpoint puntje;
    pc.convertTo(pc, CV_32FC1);
	detector->setSearchParams(positionThreshold, rotationThreshold, useWeightedClustering);
    vector<cv::ppf_match_3d::Pose3DPtr> results;
    int64 tick1 = cv::getTickCount();

    detector->match(pc, results, relativeSceneSampleStep, relativeSceneDistance);
    int64 tick2 = cv::getTickCount();
    cout << endl << "PPF Elapsed Time " << (tick2-tick1)/cv::getTickFrequency() << " sec" << endl;

    size_t results_size = results.size();
    cout << "Number of matching poses: " << results_size << endl;
    if (results_size == 0) {
        cout << endl << "No matching poses found. Exiting." << endl;
        exit(0);
    }

    std::string path = "src/communicatie/model/sphere.ply";
    cv::Mat pc_model = cv::ppf_match_3d::loadPLYSimple(path.c_str(), 1);
    cv::Mat pc_pickpoints = pc_model.clone();
    ///Calculate the position of the model for the 5 best poses
    cv::Mat model_transformed;
    for (size_t i=0; i<results_size; i++)
    {
            cv::ppf_match_3d::Pose3DPtr result = results[i];
            if(i<5)
            {

            	cout << "Pose Result " << i << " saved, score: " << result->numVotes<< endl;
                cv::Mat pct = cv::ppf_match_3d::transformPCPose(pc_model, result->pose);

                std::stringstream filename;
                filename << "model_pose" << i  << ".ply";
                cv::ppf_match_3d::writePLY( pct, filename.str().c_str() );
                if(i==0)
                    model_transformed = pct;
            }
    }

    ///Pickpoint berekenen: XYZ
    float grootste_z = -100;
    pcl::PointNormal point;
    cv::Mat save_mat = cv::Mat(1, 6, CV_32FC1);

    ///Puntje ergens op de zijkant als pickpoint geven:
    /*for(int i = 0; i < model_transformed.rows ; i++)
        {
        	if(grootste_z < model_transformed.at<float>(i, 2))
        	{
                grootste_z = model_transformed.at<float>(i, 2);
        	}
        }

    for(int i = 0; i < model_transformed.rows ; i++)
    {
    	if(grootste_z-0.005 > model_transformed.at<float>(i, 2) && grootste_z-0.010 < model_transformed.at<float>(i, 2))
    	{
    		point.data[0] = model_transformed.at<float>(i, 0);
    		point.data[1] = model_transformed.at<float>(i, 1);
    		point.data[2] = model_transformed.at<float>(i, 2);

    		point.normal[0] = model_transformed.at<float>(i, 3);
    		point.normal[1] = model_transformed.at<float>(i, 4);
    		point.normal[2] = model_transformed.at<float>(i, 5);

           break;
    	}
    }*/


    ///Hoogste punt als pickpoint geven:
    for(int i = 0; i < model_transformed.rows ; i++)
    {
    	if(grootste_z < model_transformed.at<float>(i, 2))
    	{
    		point.data[0] = model_transformed.at<float>(i, 0);
    		point.data[1] = model_transformed.at<float>(i, 1);
            point.data[2] = model_transformed.at<float>(i, 2);

            point.normal[0] = model_transformed.at<float>(i, 3);
            point.normal[1] = model_transformed.at<float>(i, 4);
            point.normal[2] = model_transformed.at<float>(i, 5);

            grootste_z = model_transformed.at<float>(i, 2);
    	}
    }


    ///pickpoint bepalen aan de hand van een index:
    /*int index = 236;
	point.data[0] = model_transformed.at<float>(index, 0);
	point.data[1] = model_transformed.at<float>(index, 1);
	point.data[2] = model_transformed.at<float>(index, 2);

	point.normal[0] = model_transformed.at<float>(index, 3);
    point.normal[1] = model_transformed.at<float>(index, 4);
    point.normal[2] = model_transformed.at<float>(index, 5);*/

    ///Pickpoint opslaan als .PLY
    save_mat.at<float>(0, 0) = point.data[0];
    save_mat.at<float>(0, 1) = point.data[1];
    save_mat.at<float>(0, 2) = point.data[2];
    save_mat.at<float>(0, 3) = point.normal[0];
    save_mat.at<float>(0, 4) = point.normal[1];
    save_mat.at<float>(0, 5) = point.normal[2];

    std::stringstream filename;
    filename << "pickpoint_pose" << 0  << ".ply";
    cv::ppf_match_3d::writePLY( save_mat, filename.str().c_str() );

    ///pickpoint opslaan
    puntje.point = point;

    ///Bereken Pickpoint: Quaternion
    Eigen::Vector3f origin(0,0,1);
    Eigen::Vector3f destination(-point.normal[0], -point.normal[1], -point.normal[2]);
    Eigen::Quaternionf q;
    q.setFromTwoVectors(origin, destination);

    ///Quaternion opslaan
    puntje.quaternion.push_back(q.x());
    puntje.quaternion.push_back(q.y());
    puntje.quaternion.push_back(q.z());
    puntje.quaternion.push_back(q.w());

    ///Bereken Pickpoint: Model toevoegen als cv::Mat
    puntje.model_pc = model_transformed;

    return puntje;
}
