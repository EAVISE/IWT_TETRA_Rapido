#include "../include/communicatie/visie.hpp"
#include "../include/communicatie/scanner.hpp"
#include "../include/communicatie/detector.hpp"

/*
 *
 * IN: pc: Pointcloud without normals
 * OUT: cv::Mat with normals
 *
 * This function calculates the normals of the scene. pc has to be off size 3xN (N= number of points)
 *
 */
cv::Mat sceneWithNormals(cv::Mat pc)
{
    pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr pcxyz(new pcl::PointCloud<pcl::PointXYZ>);
    for(int i=0; i< pc.cols;i++)
    {
        float x,y,z;
        x = pc.at<double>(0, i);
        y = pc.at<double>(1, i);
        z = pc.at<double>(2, i);
        if(x == 0 && y == 0 && z == 0)
        	continue;

        pcl::PointXYZ point;
        point.x = x;
        point.y = y;
        point.z = z;

        pcxyz->points.push_back(point);
    }


    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;

    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
    ne.setSearchMethod (tree);
    ne.setInputCloud(pcxyz);
    ne.setViewPoint(0.0, 0.0, 1.0);
    ///Verminder om sneller te laten gaan, maar minder normalen te berekenen.
    ne.setRadiusSearch(0.005);

    ne.compute(*normals);

    pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
    pcl::concatenateFields (*pcxyz, *normals, *cloud_with_normals);

    cloud_with_normals->width = (int)cloud_with_normals->points.size();
    cloud_with_normals->height =1;
    pcl::PLYWriter plywriter;
    plywriter.write("scene_robot_normals.ply", *cloud_with_normals, false);

    pc = cv::Mat(pcxyz->points.size(), 6, CV_64FC1);
    for(int i=0; i<cloud_with_normals->points.size(); i++)
    {

    	if(cloud_with_normals->points[i].data[0] == 0 && cloud_with_normals->points[i].data[1] == 0 && cloud_with_normals->points[i].data[2] == 0)
    	{
    		continue;
    	}
    	pc.at<double>(i, 0) = cloud_with_normals->points[i].data[0];
        pc.at<double>(i, 1) = cloud_with_normals->points[i].data[1];
        pc.at<double>(i, 2) = cloud_with_normals->points[i].data[2];

        pc.at<double>(i, 3) = cloud_with_normals->points[i].normal[0];
        pc.at<double>(i, 4) = cloud_with_normals->points[i].normal[1];
        pc.at<double>(i, 5) = cloud_with_normals->points[i].normal[2];
    }

    return pc;
}

/*
 *
 * IN: toconvert: Scene to convert to be compatible with rosmsgs
 * OUT: Scene as a rosmsg
 *
 * Converts scene from cv::Mat to rosmsg
 *
 */

sensor_msgs::PointCloud2 convertToROSPCL(cv::Mat toconvert)
{
    pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);

    float X, Y, Z;
    for(int i=0;i<toconvert.rows;i++)
    {
        pcl::PointNormal point;

        point.data[0] = toconvert.at<double>(i ,0);
        point.data[1] = toconvert.at<double>(i, 1);
        point.data[2] = toconvert.at<double>(i, 2);

        point.normal[0] = toconvert.at<double>(i, 3);
        point.normal[1] = toconvert.at<double>(i, 4);
        point.normal[2] = toconvert.at<double>(i, 5);

        cloud_with_normals->points.push_back(point);
    }

    sensor_msgs::PointCloud2 cloud_msg;
    pcl::toROSMsg(*cloud_with_normals, cloud_msg);
    return cloud_msg;
}

/*
 *
 * IN: sig: signal
 *
 * Function to shutdown node when you press CTRL-C
 *
 */

void mySigintHandler(int sig)
{
    std::cout<<"Shutting down vision node"<<std::endl;
    ros::shutdown();
    exit(-1);
}

/*
 *
 * Main function. Calls all the functions in the right order to scan a scene, calculate the 3D points and find the pickpoints. Also used to calibrate camera-projector and robot-sensor
 *
 */

int main(int argc, char **argv)
{
    ///Inlezen van parameters:
    cv::FileStorage fs1("src/communicatie/settings_global.xml", cv::FileStorage::READ); //Kan uitgebreid worden naar detector.
    std::string type;
    std::string model_path;

    fs1["scanner"] >> type;
    fs1["model_path"] >> model_path;

    ///Correcte scanner aanmaken
    Scanner *scanner;

    if(type == "SL")
    {
        cv::FileStorage fs("src/communicatie/SL/settings_sl.xml", cv::FileStorage::READ);
        int width, height, threshold, speed, chessboard_width, chessboard_height;
        float b, m;
        fs["width_projector"] >> width;
        fs["height_projector"] >> height;
        fs["b"] >> b;
        fs["m"] >> m;
        fs["threshold"] >> threshold;
        fs["speed_ms"] >> speed;
        fs["width_chessboard"] >> chessboard_width;
        fs["height_chessboard"] >> chessboard_height;

        fs.release();

        scanner = new SLScanner(width, height, b, m, threshold, speed, chessboard_width, chessboard_height);

        fs1["calrows"] >> scanner->calrows;
        fs1["calcols"] >> scanner->calcols;
    }
    else if(type == "TI")
    {
        cv::FileStorage fs("src/communicatie/SL/settings_sl.xml", cv::FileStorage::READ);
        int width, height, threshold, speed, chessboard_width, chessboard_height;
        float b, m;
        fs["width_projector"] >> width;
        fs["height_projector"] >> height;
        fs["b"] >> b;
        fs["m"] >> m;
        fs["threshold"] >> threshold;
        fs["speed_ms"] >> speed;
        fs["width_chessboard"] >> chessboard_width;
        fs["height_chessboard"] >> chessboard_height;

        fs.release();

        scanner = new TIScanner(width, height, b, m, threshold, speed, chessboard_width, chessboard_height);

        fs1["calrows"] >> scanner->calrows;
        fs1["calcols"] >> scanner->calcols;
    }
    else
        std::cout<<"No scannertype selected"<<std::endl;

    fs1.release();

    ///Detector maken

    Detector *detector = new PPFDetector(0.05, 0.05, 30.0, 0.02, 20.0, 1.0, 0.03, 1.0);


    ros::init(argc,argv,"visie_controller");
    ros::NodeHandle listener_node, talker_node, talker_data, listener_data;

    ///Gebruikt om te stoppen met ctrl-C
    signal(SIGINT, mySigintHandler);

    ///Listener maken voor de commando's die van de robot komen
    Listener listener;
    ros::Subscriber visie_sub = listener_node.subscribe("robot_commando", 1000, &Listener::callback, &listener);

    ///Listener maken voor de data die van de robot komt
    Listener_data data_listener;
    ros::Subscriber visie_data_sub = listener_data.subscribe("robot_data", 1000, &Listener_data::callback, &data_listener);

    ///Publishers om commado's en data naar de robot te sturen
    ros::Publisher visie_pub = talker_node.advertise<std_msgs::String>("visie_commando", 1000);
    ros::Publisher visie_data_pub = talker_data.advertise<communicatie::Point_data>("visie_data", 1000);

    ///De messages aanmaken die de visie kan versturen naar de robot
    std_msgs::String msg_finished;
    std::stringstream ss_finished;
    ss_finished << "finished";
    msg_finished.data = ss_finished.str();

    std_msgs::String msg_move;
    std::stringstream ss_move;
    ss_move << "move";
    msg_move.data = ss_move.str();

    std_msgs::String msg_move_calib;
    std::stringstream ss_move_calib;
    ss_move_calib << "move_calib";
    msg_move_calib.data = ss_move_calib.str();

    ///Train model. Als er een manier is om het te saven, lees het dan hier uit.
    detector->trainModel(model_path.c_str());


   ///Wachten tot er minstens 1 subscriber is voor we verder gaan
    ros::Rate poll_rate(100);
    while(visie_pub.getNumSubscribers() == 0)
        poll_rate.sleep();
    bool vlag = false;
    while(!vlag)
    {
        if(visie_pub.getNumSubscribers() == 0)
        {
            std::cout<<"connection with robot lost, shutting down"<<std::endl;
            ros::shutdown();
            return -1;
        }
        else
        {
            /// Wait for robot to give start sign
            while(!listener.getStart())
                ros::spinOnce();

            listener.setStart(false);

            std::cout<<"Options:\n"
                       "s: Calibrate the sensor\n"
                       "r: Calibrate the robot and the sensor\n"
                       "c: Calculate the 3D scene\n"
                       "p: Find the best picking poses(Druk p om een Point_data message te versturen, zoals ook zou gebeuren in de realiteit\n"
                       "q: Quit"<<std::endl;

            char antwoord;
            std::cin >> antwoord;

            if(antwoord == 's')
            {
                ///visie vraagt robot om uit het gezichtsveld te bewegen
                visie_pub.publish(msg_move);
                ros::spinOnce();
                while(!listener.getMoved())
                    ros::spinOnce();

                listener.setMoved(false);
                /// Robot heeft aan de visie laten weten dat hij verplaatst is.
                /// Er kan gecalibreerd worden
                scanner->calibrate();
                std::cout<<"Calibrated"<<std::endl;
            }
            else if(antwoord == 'r')
            {
                ///Get points
                ///We gaan 20 beelden nemen en 20 robot posities opvragen voor de calibratie uit te voeren.
                int number_of_calib = 6;
                std::vector<cv::Point3d> robot_calib_points;
                std::vector<cv::Point3d> camera_calib_points;
                for(int i = 0; i< number_of_calib; i++)
                {
                    //Tell robot to move to calibrate position
                    visie_pub.publish(msg_move_calib);
                    ros::spinOnce();

                    while(data_listener.getID() < 0)
                        ros::spinOnce();
                    ///Datalistener ID terug op -1 zodat we de volgende keer het zien wanneer er een nieuw coordinaat wordt doorgestuurd.
                    ///Ask robot position
                    cv::Point3d trans = data_listener.getTrans();
                    robot_calib_points.push_back(trans);
                    std::cout<<trans<<std::endl;
                    cv::Point3d rot = data_listener.getRot();
                    robot_calib_points.push_back(rot);

                    std::cout<<" robot coordinate "<<data_listener.getID()<<" received"<<std::endl;
                    ///Take image and get center of board
                    cv::Point3d camera_point = scanner->getCameraPoint();

                    camera_calib_points.push_back(camera_point);

                    data_listener.setID(-1);
                }
                ///calibrate
                scanner->sensor_calibrate(robot_calib_points, camera_calib_points);
            }
            else if(antwoord == 'c')
            {
                int64 tick1 = cv::getTickCount();

                ///Tell robot to move out of the way
                visie_pub.publish(msg_move);
                ros::spinOnce();
                while(!listener.getMoved())
                    ros::spinOnce();

                listener.setMoved(false);

                std::cout<<"3Dscene: robot moved"<<std::endl;
                /// Visie scant de scene
                /// Uitbreiding: Visie stuurt dat scan voltooid is en robot kan zich al boven de scene positioneren

                scanner->scan();

                int64 tick2 = cv::getTickCount();
                cout << endl << "scan berekend: " << (tick2-tick1)/cv::getTickFrequency() << " sec" << endl;

                ///Calculate normals of the scene and set it
                scanner->setScene(sceneWithNormals(scanner->getScene()));

                int64 tick3 = cv::getTickCount();
                cout << endl << "normalen berekend: " << (tick3-tick2)/cv::getTickFrequency() << " sec" << endl;

            }
            else if(antwoord == 'p')
            {
                int64 tick1 = cv::getTickCount();

                cv::Mat resultaat = scanner->getScene().clone();
                if(!resultaat.empty())
                {
                    Pickpoint point = detector->getPickPoint(resultaat);

                    /// Laat de robot weten dat het voor de visie klaar is en dat we wachten op een nieuw start commando.
                    visie_pub.publish(msg_finished);
                    ros::spinOnce();

                    /// Maak message met pick poses en pointcloud
                    communicatie::Point_data scene_info;
                    ///ID mag niet kleiner dan 0 zijn. De robotkant checked hierop.
                    scene_info.point_id = 1;

                    scene_info.pointcloud = convertToROSPCL(scanner->getScene().clone());
                    scene_info.x = point.point.data[0];
                    scene_info.y = point.point.data[1];
                    scene_info.z = point.point.data[2];
                    scene_info.rx = point.point.normal[0];
                    scene_info.ry = point.point.normal[1];
                    scene_info.rz = point.point.normal[2];
                    scene_info.q1 = point.quaternion[0];
                    scene_info.q2 = point.quaternion[1];
                    scene_info.q3 = point.quaternion[2];
                    scene_info.q4 = point.quaternion[3];
                    scene_info.model = convertToROSPCL(point.model_pc.clone());

                    /// Send message to Robot
                    visie_data_pub.publish(scene_info);
                    ros::spinOnce();
                }
                else
                {
                    std::cout<<"scene is leeg"<<std::endl;
                    listener.setStart(true);
                }

                int64 tick2 = cv::getTickCount();
                cout << endl << "Pickpose berekend: " << (tick2-tick1)/cv::getTickFrequency() << " sec" << endl;

                continue;
            }
            else if(antwoord == 'q')
            {
                vlag = true;
            }
            else
            {
                listener.setStart(true);
            }
        }
    }
    return 0;
}
