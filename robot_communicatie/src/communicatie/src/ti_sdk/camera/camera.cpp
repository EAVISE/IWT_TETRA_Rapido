#include <iostream>

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/other.hpp>
#include <common/parameters.hpp>
#include <camera/camera.hpp>

namespace dlp{

	Camera::Settings::Trigger::Trigger(){
        this->value_ = Type::SOFTWARE_SLAVE;
	}

    Camera::Settings::Trigger::Trigger(const Type &Trigger){
		this->value_ = Trigger;
	}

    void Camera::Settings::Trigger::Set(const Type &Trigger){
		this->value_ = Trigger;
	}

    Camera::Settings::Trigger::Type Camera::Settings::Trigger::Get() const{
		return this->value_;
	}

	std::string Camera::Settings::Trigger::GetEntryName() const{
		return "CAMERA_SETTINGS_TRIGGER_TYPE";
	}

	std::string Camera::Settings::Trigger::GetEntryDefault() const{
		return "SOFTWARE_SLAVE";
	}

	std::string Camera::Settings::Trigger::GetEntryValue() const{
		switch (this->value_){
        case Type::SOFTWARE_SLAVE: return "SOFTWARE_SLAVE";
			break;
        case Type::HARDWARE_SLAVE: return "HARDWARE_SLAVE";
			break;
        case Type::HARDWARE_MASTER: return "HARDWARE_MASTER";
			break;
        case Type::INVALID: return "INVALID";
			break;
		default:				return "SOFTWARE_SLAVE";
		}
	}

	void Camera::Settings::Trigger::SetEntryValue(std::string value){
		if (value.compare("SOFTWARE_SLAVE") == 0){
            this->value_ = Type::SOFTWARE_SLAVE;
		}
		else if (value.compare("HARDWARE_SLAVE") == 0){
            this->value_ = Type::HARDWARE_SLAVE;
		}
        else if (value.compare("HARDWARE_MASTER") == 0){
            this->value_ = Type::HARDWARE_MASTER;
		}
		else
            this->value_ = Type::INVALID;
	}




}


