/** \file   pg_flycap2_c.cpp
 *  \brief  Contains methods for pg_flycap2 class
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */


#include <common/returncode.hpp>
#include <common/debug.hpp>
#include <common/image/image.hpp>
#include <camera/camera.hpp>
#include <camera/pg_flycap2/pg_flycap2_c.hpp>

#include <flycapture/C/FlyCapture2_C.h>

#include <iostream>

/** \brief Contains all DLP SDK classes, functions, etc. */
namespace dlp{

PG_FlyCap2_C::PG_FlyCap2_C(){
    this->debug_.SetName("PG_FLYCAP2_DEBUG: ");
    this->is_connected_ = false;
    this->is_setup_     = false;
    this->is_started_   = false;
}

/** \brief      Detect number of Point grey Cameras Connected to the system
 *  \param[out] num_cameras  Pointer to return the number of cameras connected in the system
 *  \retval     PG_FLYCAP_C_NO_CAMERAS_DETECTED    No Point Grey Cameras detected in the system
 */
ReturnCode PG_FlyCap2_C::DetectNumOfCameras(unsigned int *num_cameras)
{
    ReturnCode ret;

    // Create an instance/context for PointGrey Camera and returns a pointer to the context.
    // Needs to be called before any calls that use camera context can be made.
    if(fc2CreateContext(&this->context_) != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera Context Creation Failed");
        return ret.AddError(PG_FLYCAP_C_NO_CONTEXT_CREATED);
    }

    // Returns the number of PointGrey cameras attached to the PC
    if(fc2GetNumOfCameras(this->context_,num_cameras) != FC2_ERROR_OK)
    {
        this->debug_.Msg("No Point Grey cameras detected");
        return ret.AddError(PG_FLYCAP_C_NO_CAMERAS_DETECTED);
    }

    return ret;
}


/**
 * \brief   Connects a camera object to the camera specified by the Camera Index
 * \param[in] cam_id    Used to find the GUID which is a unique identifier of the camera connected to the system
 * \retval  PG_FLYCAP_C_INVALID_CAMERA_ID    Camera ID sent is invalid
 */
ReturnCode PG_FlyCap2_C::Connect(int cam_id)
{

    ReturnCode ret;

    unsigned int number_cameras;
    this->DetectNumOfCameras(&number_cameras);

    // If GUID is set to NULL or ommited then the connection is made to the first camera connected to the PC (index = 0)
    fc2PGRGuid guid;

    // It returns the Point Grey Guid for a camera on the PC which uniquely identifies the camera specified by index and is used
    // to identify the camera during a fc2Connect() API call
    if (fc2GetCameraFromIndex(this->context_, cam_id, &guid) != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera ID is Invalid");
        return ret.AddError(PG_FLYCAP_C_INVALID_CAMERA_ID);
    }

    // Connect the camera object to the camera specified by GUID
    if (fc2Connect(this->context_, &guid) != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera NOT Connected");
        return ret.AddError(CAMERA_NOT_CONNECTED);
    }

    // Set camera connected flag and save camera id
    this->is_connected_ = true;
    this->cam_id_ = cam_id;


    // Retrieves information from the camera such as serial number, model name and
    // other camera information.
    if ((fc2GetCameraInfo(this->context_,&this->camInfo_) != FC2_ERROR_OK))
    {
        this->debug_.Msg("Failed to get Camera Information");
    }
    else
    {
        this->debug_.Msg(0, "**** CAMERA INFORMATION ******");
        this->debug_.Msg(0, "Serial Number      = " + dlp::Number::ToString(this->camInfo_.serialNumber));
        this->debug_.Msg(0, "Camera Model       = " + dlp::Number::ToString(this->camInfo_.modelName));
        this->debug_.Msg(0, "Camera Vendor      = " + dlp::Number::ToString(this->camInfo_.vendorName));
        this->debug_.Msg(0, "Camera Sensor      = " + dlp::Number::ToString(this->camInfo_.sensorInfo));
        this->debug_.Msg(0, "Camera Resolution  = " + dlp::Number::ToString(this->camInfo_.sensorResolution));
    }
    return ret;
}


/** \brief   Configures the Camera for the parameters in the parameters file
 *           It sets up the following things:
 *           Row - height Of the image
 *           Columns - Width of the Image
 *           OffsetX - Horizontal Image Offset
 *           OffsetY - Vertical Image Offset
 *           Pixel Format - Pixel Format of the image ( 8 bit MONO/ RGB8 R=G=G=8 bits/ RAW8 which is 8 bits of raw sensor output data)
 *           Video Mode - 0/4/8
 *           Shutter Speed
 *           GPIO_2 output strobe settings
 * \param[in] settings    \ref dlp::Parameters object to retrieve entries
 * \retval    CAMERA_NOT_CONNECTED            Camera is NOT Connected
 * \retval    CAMERA_NOT_SETUP                Camera is NOT configured/setup
 */
ReturnCode PG_FlyCap2_C::Setup(const dlp::Parameters &settings)
{
    ReturnCode ret;


    // Check if the camera is connected before you begin setting it up
    if (!(this->is_connected_))
    {
        this->debug_.Msg("Camera is NOT Connected");
        return ret.AddError(CAMERA_NOT_CONNECTED);
    }

    // Retrieve the camera settings
    settings.Get(&this->rows_);
    settings.Get(&this->columns_);
    settings.Get(&this->offsetX_);
    settings.Get(&this->offsetY_);
    settings.Get(&this->shutter_speed_);
    settings.Get(&this->exposure_);
    settings.Get(&this->frame_rate_);
    settings.Get(&this->video_mode_);
    settings.Get(&this->pixel_format_);
    settings.Get(&this->image_format_);
    settings.Get(&this->gain_);

    // Set the video mode and convert DLP SDK format to FlyCapture2 format
    this->init_settings_.mode = FC2_MODE_0;
    /*switch(this->video_mode_.Get())
    {
    case Settings::VideoMode::Options::MODE0_FULL_RESOLUTION:
        this->init_settings_.mode = FC2_MODE_0;
        break;
    case Settings::VideoMode::Options::MODE4_HALF_RESOLUTION:
        this->init_settings_.mode = FC2_MODE_4;
        break;
    default:
        ret.AddError(CAMERA_RESOLUTION_INVALID);
    }*/

    // Get the camera offset
    this->init_settings_.offsetX = this->offsetX_.Get();
    this->init_settings_.offsetY = this->offsetY_.Get();

    // Get the camera resolution
    this->init_settings_.width   = this->columns_.Get();
    this->init_settings_.height  = this->rows_.Get();


    // Get the camera's pixel format
    switch(this->pixel_format_.Get()){
    case Settings::PixelFormat::Options::PIXEL_FORMAT_RAW8:
        this->init_settings_.pixelFormat = FC2_PIXEL_FORMAT_RAW8;
        break;
    case Settings::PixelFormat::Options::PIXEL_FORMAT_MONO8:
        this->init_settings_.pixelFormat = FC2_PIXEL_FORMAT_MONO8;
        break;
    case Settings::PixelFormat::Options::PIXEL_FORMAT_RGB8:
        this->init_settings_.pixelFormat = FC2_PIXEL_FORMAT_RGB8;
        break;
    default:
        this->init_settings_.pixelFormat = FC2_PIXEL_FORMAT_MONO8;
    }

    // Setup the camera's format settings (resolution, pixel format, offset, etc.)
    // and set the maximum data packet size to 100%
    this->error_ = fc2SetFormat7Configuration(this->context_, &init_settings_, (float)100);
    if(this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera Format7 Configuration Failed");
        return ret.AddError(CAMERA_NOT_SETUP);
    }


    // Set the camera exposure
    this->init_property_.type           = FC2_AUTO_EXPOSURE;
    this->init_property_.absControl     = true;
    this->init_property_.onePush        = false;
    this->init_property_.onOff          = true;
    this->init_property_.autoManualMode = false;
    this->init_property_.absValue       = this->exposure_.Get();

    this->error_ = fc2SetProperty(this->context_, &this->init_property_);
    if(this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera Set Exposure Failed");
        return ret.AddError(CAMERA_NOT_SETUP);
    }

    // Set the camera shutter speed
    this->init_property_.type           = FC2_SHUTTER;
    this->init_property_.absControl     = true;
    this->init_property_.onePush        = false;
    this->init_property_.onOff          = true;
    this->init_property_.autoManualMode = false;
    this->init_property_.absValue       = this->shutter_speed_.Get();

    this->error_ = fc2SetProperty(this->context_, &init_property_);
    if(this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera Set Shutter Failed");
        return ret.AddError(CAMERA_NOT_SETUP);
    }

    // Set the frame rate
    this->init_property_.type           = FC2_FRAME_RATE;
    this->init_property_.absControl     = true;
    this->init_property_.onePush        = false;
    this->init_property_.onOff          = true;
    this->init_property_.autoManualMode = false;
    this->init_property_.absValue       = this->frame_rate_.Get();

    this->error_ = fc2SetProperty(this->context_, &this->init_property_);
    if (this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera Set Frame Rate Failed");
        return ret.AddError(CAMERA_NOT_SETUP);
    }

    // Set the camera gain
    this->init_property_.type           = FC2_GAIN;
    this->init_property_.absControl     = true;
    this->init_property_.onePush        = false;
    this->init_property_.onOff          = true;
    this->init_property_.autoManualMode = false;
    this->init_property_.absValue       = this->gain_.Get();

    this->error_ = fc2SetProperty(this->context_, &this->init_property_);
    if (this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera Set Gain Failed");
        return ret.AddError(CAMERA_NOT_SETUP);
    }


    //Call Set Sharpness
    this->init_property_.type = FC2_SHARPNESS;
    this->init_property_.absControl = false;
    this->init_property_.onePush = false;
    this->init_property_.onOff = true;
    this->init_property_.autoManualMode = false;
    this->init_property_.valueA = sharpness_.Get();  //set it based on settings

    this->error_ = fc2SetProperty(this->context_, &this->init_property_);
    if (this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera Set Sharpness Failed");
        return ret.AddError(CAMERA_NOT_SETUP);
    }



    // Set Strobe controls
    settings.Get(&this->strobe_source_);
    settings.Get(&this->strobe_polarity_);
    settings.Get(&this->strobe_enable_);

    this->strobe_settings_.source = 2;
    /*switch(this->strobe_source_.Get()){
    case Settings::StrobeSource::Options::GPIO_1:
        this->strobe_settings_.source = 1;
        break;
    case Settings::StrobeSource::Options::GPIO_2:
        this->strobe_settings_.source = 2;
        break;
    case Settings::StrobeSource::Options::GPIO_3:
        this->strobe_settings_.source = 3;
        break;
    default:
        ret.AddError(PG_FLYCAP_C_STROBE_INVALID);
    }*/
    this->strobe_settings_.polarity = 1;
    /*switch(this->strobe_polarity_.Get()){
    case Settings::StrobePolarity::Options::LOW:
        this->strobe_settings_.polarity  = 0;
        break;
    case Settings::StrobePolarity::Options::HIGH:
        this->strobe_settings_.polarity = 1;
        break;
    default:
        ret.AddError(PG_FLYCAP_C_STROBE_POLARITY_INVALID);
    }*/

    if (ret.hasErrors())
    {
        this->debug_.Msg("Setting Strobe Polarity to Default");
        return ret;
    }

    this->strobe_settings_.onOff = this->strobe_enable_.Get();
    this->strobe_settings_.delay    = 0.0;
    this->strobe_settings_.duration = 0.0;

    this->error_ = fc2SetStrobe(this->context_, &this->strobe_settings_);
    if (this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera Set Output Strobe/Trigger Failed");
        return ret.AddError(CAMERA_NOT_SETUP);
    }

    // Set Input Trigger on camera if its present
    if (settings.Contains(this->trigger_type_))
    {
        // Get the trigger settings
        settings.Get(&this->trigger_type_);
        settings.Get(&this->trigger_enable_);

        // Convert DLP SDK trigger to FlyCapture2 format
        switch(this->trigger_type_.Get()){
        case Camera::Settings::Trigger::Type::SOFTWARE_SLAVE:
            this->init_trigger_settings_.source = 7;
            break;
        case Camera::Settings::Trigger::Type::HARDWARE_SLAVE:
            this->init_trigger_settings_.source = 0;
            break;
        default:
            return ret.AddError(CAMERA_TRIGGER_INVALID);
        }

        // Initialize all other parameters of Trigger Settings
        this->init_trigger_settings_.parameter  = 0;
        this->init_trigger_settings_.onOff      = this->trigger_enable_.Get();
        this->init_trigger_settings_.mode       = 0;   // Refer to different trigger modes in the document

        this->error_ = fc2SetTriggerMode(this->context_,&this->init_trigger_settings_);
        if (this->error_ != FC2_ERROR_OK)
        {
            this->debug_.Msg("Failed to set the trigger mode for camera");
            return ret.AddError(CAMERA_NOT_SETUP);
        }

        // If the trigger was turned on poll the camera until it is
        // ready to accept triggers
        if (this->init_trigger_settings_.onOff && (this->init_trigger_settings_.source == 0))    // Hardware trigger is enabled
        {
            // Poll to ensure camera is ready
            const unsigned int softwareTrigger = 0x62C;
            unsigned int regVal = 0;

            this->debug_.Msg("Hardware Trigger Mode Enabled\n Waiting for Hardware Triggers.........");
            do
            {
                this->error_ = fc2ReadRegister(this->context_,softwareTrigger, &regVal);
                if (this->error_ != FC2_ERROR_OK)
                {
                    this->debug_.Msg(1, "Error Polling for Trigger Ready");
                    return ret.AddError(CAMERA_NOT_SETUP);
                }

                // This bit gets cleared as soon as Trigger is enabled and
                // it is finishes processing exisiting frame
            } while ((regVal >> 31) != 0);
        }
    }

    // Mark setup flag as true
    this->is_setup_ = true;

    return ret;
}

/**
 * \brief Returns the camera configuration settings
 * \param[out] settings         Pointer to \ref dlp::Parameters object to store settings in
 * \retval  CAMERA_NOT_SETUP    Camera has NOT been set up/configured
 */
ReturnCode PG_FlyCap2_C::GetSetup(Parameters *settings)const{
    ReturnCode ret;

    // Check that pointer is NOT null
    if(!settings)
        return ret.AddError(PG_FLYCAP_C_NULL_POINTER);

    if(!this->isSetup())
        return ret.AddError(CAMERA_NOT_SETUP);

    // Clear the parameters list
    settings->Clear();

    // Save the settings
    settings->Set(this->rows_);
    settings->Set(this->columns_);
    settings->Set(this->offsetX_);
    settings->Set(this->offsetY_);
    settings->Set(this->shutter_speed_);
    settings->Set(this->exposure_);
    settings->Set(this->video_mode_);
    settings->Set(this->pixel_format_);
    settings->Set(this->image_format_);
    settings->Set(this->gain_);
    settings->Set(this->strobe_polarity_);
    settings->Set(this->strobe_source_);
    settings->Set(this->strobe_enable_);

    return ret;
}


/**
 * \brief Disconnects the camera object from the camera
 * \retval CAMERA_NOT_DISCONNECTED      Camera could NOT be disconnected
 */
ReturnCode PG_FlyCap2_C::Disconnect()
{
    ReturnCode ret;

    // Disconnects the fc2Context from the camera
    this->error_ = fc2Disconnect(this->context_);
    if(this->error_ != FC2_ERROR_OK)
    {
        return ret.AddError(CAMERA_NOT_DISCONNECTED);
    }

    // Mark flag that the camera has been disconnected
    this->is_connected_ = false;

    return ret;
}

/** \brief      Starts the Isochronous image capture and uses the currently set or most recently set Video mode of the camera
 *  \retval     CAMERA_NOT_CONNECTED        Camera is not connected
 *  \retval     CAMERA_NOT_STARTED          Camera cannot be started
 */
ReturnCode PG_FlyCap2_C::Start()
{
    ReturnCode ret;
    if (!(this->is_connected_  && this->is_setup_))
    {
        this->debug_.Msg("Connect and Set up the camera before starting it");
        return ret.AddError(CAMERA_NOT_CONNECTED);
    }


    // Start the isochronous camera capture using the current video mode or the
    // most recently set video mode of the camera
    this->error_ = fc2StartCapture(this->context_);
    if (this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Failed to Start the camera");
        return ret.AddError(CAMERA_NOT_STARTED);
    }

    // Mark flag that camera has started
    this->is_started_ = true;

    return ret;
}


/** \brief  Stops the image captures
 *  \retval CAMERA_NOT_STOPPED      Camera cannot be stopped
 */
ReturnCode PG_FlyCap2_C::Stop()
{
    ReturnCode ret;

    // Stops the isochronous camera capture
    this->error_ = fc2StopCapture(this->context_);
    if(this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Failed to Stop the Camera");
        return ret.AddError(CAMERA_NOT_STOPPED);
    }

    // Mark flag that camera has started
    this->is_started_ = false;

    return ret;
}



/** * \brief        This function is actually used to grab the captured frames from the camera's internal buffer by making a blocking call to RetrieveFrameBuffer()
 *                  If the Grab mode is NOT specified, then the default behaviour is to requeue images for DMA if they have NOT been retrieved by the time the next image transfer completes
 *                  If BUFFER_FRAMES is specified, the next image in the sequence will be retrieved. Note that for the BUFFER_FRAMES case, if retrieval does NOT keep up with the DMA process, images will be lost. The default behavior is to perform DROP_FRAMES image retrieval.
 *  \param[out]     ret_frame   Pointer to \ref dlp::Image object that holds the captured frame
 *  \retval         CAMERA_NOT_CONNECTED        Camera is not connected
 *  \retval         CAMERA_NOT_STARTED          Camera is not started
 *  \retval         CAMERA_FRAME_GRAB_FAILED    Camera frame NOT grabbed
 */
ReturnCode PG_FlyCap2_C::GetFrame(Image *ret_frame)
{
    ReturnCode ret;

    fc2Image pg_image;

    if (!(this->is_connected_ && this->is_started_))
    {
        this->debug_.Msg("Connect and start the camera before capturing a frame");
        return ret.AddError(CAMERA_NOT_CONNECTED);
    }

    // Creates an fc2Image structure
    this->error_ = fc2CreateImage(&pg_image);

    if (this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera Capture Frame Failed");
        return ret.AddError(CAMERA_FRAME_GRAB_FAILED);
    }

    // Retrieves the next image object containing the next image
    this->error_ = fc2RetrieveBuffer(this->context_,&pg_image);

    if (this->error_ != FC2_ERROR_OK)
    {
        this->debug_.Msg("Camera Capture Frame Failed");
        return ret.AddError(CAMERA_FRAME_GRAB_FAILED);
    }

    // Coverts the current image buffer to the specified output format and stores the result
    // into the specified image
    fc2Image pg_image_mono;
        this->error_ = fc2CreateImage(&pg_image_mono);
        this->error_ = fc2ConvertImageTo(FC2_PIXEL_FORMAT_MONO8, &pg_image, &pg_image_mono);
        ret_frame->Clear();
        ret_frame->Create(pg_image_mono.cols, pg_image_mono.rows, Image::Format::MONO_UCHAR, pg_image_mono.pData).hasErrors();
        error_ = fc2DestroyImage(&pg_image_mono);

    /*if(this->image_format_.Get() == Settings::ImageFormat::Options::IMAGE_FORMAT_MONO8){
        fc2Image pg_image_mono;
        this->error_ = fc2CreateImage(&pg_image_mono);
        this->error_ = fc2ConvertImageTo(FC2_PIXEL_FORMAT_MONO8, &pg_image, &pg_image_mono);
        ret_frame->Clear();
        ret_frame->Create(pg_image_mono.cols, pg_image_mono.rows, Image::Format::MONO_UCHAR, pg_image_mono.pData).hasErrors();
        error_ = fc2DestroyImage(&pg_image_mono);
    }
    else if(this->image_format_.Get() == Settings::ImageFormat::Options::IMAGE_FORMAT_RGB8){
        fc2Image pg_image_rgb;
        this->error_ = fc2CreateImage(&pg_image_rgb);
        this->error_ = fc2ConvertImageTo(FC2_PIXEL_FORMAT_BGR, &pg_image, &pg_image_rgb);
        ret_frame->Clear();
        ret_frame->Create(pg_image_rgb.cols, pg_image_rgb.rows, Image::Format::RGB_UCHAR, pg_image_rgb.pData).hasErrors();
        error_ = fc2DestroyImage(&pg_image_rgb);
    }
    else{
        this->debug_.Msg("Camera Capture Frame Failed");
        return ret.AddError(CAMERA_FRAME_GRAB_FAILED);
    }*/


    // Destroys the fc2Image structure
    error_ = fc2DestroyImage(&pg_image);

    return ret;
}


/**
 * \brief           Return a capture sequence with specified number of image \ref dlp::Image captures
 * \param[in]       arg_number_captures  Number of captures to be added in the sequence
 * \param[out]      ret_capture_sequence Pointer to a \ref dlp::Capture::Sequence object that holds the capture sequence
 * \retval          CAMERA_FRAME_GRAB_FAILED    Camera frame NOT grabbed
 */
ReturnCode PG_FlyCap2_C::GetCaptureSequence(const unsigned int &arg_number_captures, Capture::Sequence* ret_capture_sequence)
{
    ReturnCode ret;
    Capture pg_capture;
    Image pg_image;


    pg_capture.image_type.Set(Capture::Settings::Data::Type::IMAGE_DATA);

    // Grab arg_num_Captures number of frames from the camera
    for (unsigned int i = 0; i<arg_number_captures; i++)
    {
        // Grab a frame from the camera
        ret = this->GetFrame(&pg_image);

        if (ret.hasErrors())
        {
            this->debug_.Msg("Camera Capture Error");
            return ret;
        }

        pg_capture.image_data = pg_image;
        if (ret_capture_sequence->Add(pg_capture).hasErrors())
        {
            this->debug_.Msg("Camera Captures cannot be added to the Capture Sequence");
            ret.AddError(CAMERA_FRAME_GRAB_FAILED);
            return ret;
        }

        pg_capture.image_data.Clear();
    }

    return ret;
}

/** \brief Returns true if camera is connected via USB */
bool PG_FlyCap2_C::isConnected() const{
    return this->is_connected_;
}

/** \brief Returns true if camera frame captures have been started
 *  \note Frames may or may NOT be captured by the PC
 */
bool PG_FlyCap2_C::isStarted() const{
    return this->is_started_;
}

/** \brief          Returns ID of the camera connected to the system
 *  \param[out]     ret_id  Pointer to retrieve the camera ID
 *  \retval         CAMERA_NOT_CONNECTED    Camera is NOT Connected
 */
ReturnCode PG_FlyCap2_C::GetID(unsigned int* ret_id) const{
    ReturnCode ret;
    if (!this->isConnected())
    {
        this->debug_.Msg("No Camera is connected");
        ret.AddError(CAMERA_NOT_CONNECTED);
    }
    else{
        *ret_id = this->cam_id_;
    }

    return ret;
}

/** \brief          Returns number of active rows set on the camera sensor
 */
ReturnCode PG_FlyCap2_C::GetRows(unsigned int* ret_rows) const{
    ReturnCode ret;

    (*ret_rows) = this->rows_.Get();

    return ret;
}

/** \brief          Returns number of active columns set on the camera sensor
 */
ReturnCode PG_FlyCap2_C::GetColumns(unsigned int* ret_columns) const {
    ReturnCode ret;

    (*ret_columns) = this->columns_.Get();

    return ret;
}

/** \brief          Returns frame rate set on the camera sensor
 */
ReturnCode PG_FlyCap2_C::GetFrameRate(float* ret_framerate) const {
    ReturnCode ret;

    (*ret_framerate) = this->frame_rate_.Get();

    return ret;
}

/** \brief          Returns exposure value set on the camera sensor
 */
ReturnCode PG_FlyCap2_C::GetExposure(float* ret_exposure) const {
    ReturnCode ret;

    (*ret_exposure) = this->shutter_speed_.Get();

    return ret;
}

/** \brief          Returns type of input trigger set on the camera sensor
 */
ReturnCode PG_FlyCap2_C::GetTrigger(Camera::Settings::Trigger::Type *ret_trigger) const {
    ReturnCode ret;

    (*ret_trigger) = this->trigger_type_.Get();

    return ret;
}
}
