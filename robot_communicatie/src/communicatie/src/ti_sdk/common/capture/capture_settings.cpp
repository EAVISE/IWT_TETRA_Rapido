/** \file   capture_settings.cpp
 *  \brief  Contains methods to change settings for capture data
 */

#include <string>
#include <vector>
#include <iostream>

#include <common/debug.hpp>
#include <common/image/image.hpp>
#include <common/other.hpp>
#include <common/parameters.hpp>
#include <common/capture/capture.hpp>

namespace dlp{

/** \brief Default Constructor, assignes value type as TYPE::INVALID*/
Capture::Settings::Data::Data(){
    this->value_ = Type::INVALID;
}

/** \brief Constructor, Assigns \ref dlp::Capture::Settings::Data::Type as the specified type paramater*/
Capture::Settings::Data::Data(const Type &type){
    this->value_ = type;
}

/** \brief Assigns the \ref dlp::Capture::Settings::Data::Type as the specified type paramater*/
void Capture::Settings::Data::Set(const Type &type){
    this->value_ = type;
}

/** \brief returns the \ref dlp::Capture::Settings::Data::Type of capture*/
Capture::Settings::Data::Type Capture::Settings::Data::Get() const{
    return this->value_;
}

/** \brief returns "CAPTURE_SETTINGS_DATA_TYPE"*/
std::string Capture::Settings::Data::GetEntryName() const{
    return "CAPTURE_SETTINGS_DATA_TYPE";
}

/** \brief Returns a string of the type of file*/
std::string Capture::Settings::Data::GetEntryValue() const{
    switch(this->value_){
    case Type::IMAGE_DATA:  return "IMAGE_DATA";
    case Type::IMAGE_FILE:  return "IMAGE_FILE";
    case Type::INVALID:
    default:
        return "INVALID";
    }
}

/** \brief Returns "INVALID"*/
std::string Capture::Settings::Data::GetEntryDefault() const{
    return "INVALID";
}

/** \brief Sets the data type based on string input parameter*/
void Capture::Settings::Data::SetEntryValue(std::string value){
    if(value.compare("IMAGE_DATA") == 0){
        this->value_ = Type::IMAGE_DATA;
    }
    else if(value.compare("IMAGE_FILE") == 0){
        this->value_ = Type::IMAGE_FILE;
    }
    else{
        this->value_ = Type::INVALID;
    }
}

}

