/**
 * \file    capture.cpp
 * \ingroup Common
 * \brief   Contains dlp::Capture methods
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#include <string>
#include <vector>
#include <iostream>

#include <common/debug.hpp>
#include <common/image/image.hpp>
#include <common/other.hpp>
#include <common/capture/capture.hpp>

namespace dlp{

/** \brief Constructs empty object */
Capture::Capture(){
    this->camera_id   = 0;
    this->pattern_id  = 0;
    this->image_type.Set(Settings::Data::Type::INVALID);
    this->image_file  = "";
    this->image_data.Clear();
}

/** \brief Destructs object and deallocates memory */
Capture::~Capture(){
    this->image_data.Clear();
}

}

