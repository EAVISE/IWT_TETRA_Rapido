/** \file   calibration_settings.cpp
 *  \brief  Contains calibration objects and methods for changing and storing calibration setting entries.
 */

#include <common/debug.hpp>
#include <common/parameters.hpp>
#include <calibration/calibration.hpp>


#include <vector>
#include <string>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief Default value 0*/
Calibration::Settings::Model::Rows::Rows(){
    this->value_ = 0;
}

/** \brief take row count parameter
 * \param[in] rows number of rows*/
Calibration::Settings::Model::Rows::Rows(const unsigned int &rows){
    this->value_ = rows;
}

/** \brief row count set method
 * \param[in] rows number of rows*/
void Calibration::Settings::Model::Rows::Set(const unsigned int &rows){
    this->value_ = rows;
}

/** \brief row count get method*/
unsigned int Calibration::Settings::Model::Rows::Get() const{
    return this->value_;
}

/**
 * \brief Calibration::Settings::Model::Rows::ParameterGetName
 * \retval CALIBRATION_SETTINGS_MODEL_ROWS
 */
std::string Calibration::Settings::Model::Rows::GetEntryName() const{
    return "CALIBRATION_SETTINGS_MODEL_ROWS";
}

/** \brief Returns entry value as string*/
std::string Calibration::Settings::Model::Rows::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Returns default value of "0"*/
std::string Calibration::Settings::Model::Rows::GetEntryDefault() const{
    return "0";
}

/** \brief Set entry value
 * \param[in] value String to set number of rows to after conversion to integer*/
void Calibration::Settings::Model::Rows::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}

/** \brief Construct column with default of zero*/
Calibration::Settings::Model::Columns::Columns(){
    this->value_ = 0;
}

/** \brief construct Column with explicit number of columns
 * \param[in] columns number of columns to set*/
Calibration::Settings::Model::Columns::Columns(const unsigned int &columns){
    this->value_ = columns;
}

/** \brief set method for columns
 * \param[in] columns number of columns to set*/
void Calibration::Settings::Model::Columns::Set(const unsigned int &columns){
    this->value_ = columns;
}

/** \brief get method for columns*/
unsigned int Calibration::Settings::Model::Columns::Get() const{
    return this->value_;
}

/** \brief Get method for column entry name "CALIBRATION_SETTINGS_MODEL_COLUMNS"
 *  \retval CALIBRATION_SETTINGS_MODEL_COLUMNS
 */
std::string Calibration::Settings::Model::Columns::GetEntryName() const{
    return "CALIBRATION_SETTINGS_MODEL_COLUMNS";
}

/** \brief Get entry value for number of columns as string*/
std::string Calibration::Settings::Model::Columns::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Get entry default "0" as string*/
std::string Calibration::Settings::Model::Columns::GetEntryDefault() const{
    return "0";
}


/** \brief Set entry value from string
 * \param[in] value String of a number to be converted to set value to*/
void Calibration::Settings::Model::Columns::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}






/** \brief Effective pixel size default constructor = 0*/
Calibration::Settings::Model::EffectivePixelSize::EffectivePixelSize(){
    this->value_ = 0;
}

/** \brief Effective pixel size explicit constructor
 * \param[in] micrometers float from 0.0 to 1 representing the vertical offset percent*/
Calibration::Settings::Model::EffectivePixelSize::EffectivePixelSize(const float &micrometers){
    this->value_ = micrometers;
}

/** \brief set value of EffectivePixelSize
 * \param[in] micrometers float from 0.0 to 1 representing the vertical offset percent*/
void Calibration::Settings::Model::EffectivePixelSize::Set(const float &micrometers){
    this->value_ = micrometers;
}

/** \brief get method for EffectivePixelSize value*/
float Calibration::Settings::Model::EffectivePixelSize::Get() const{
    return this->value_;
}

/** \brief returns "CALIBRATION_SETTINGS_MODEL_EFFECTIVE_PIXEL_SIZE"
  * \retval CALIBRATION_SETTINGS_MODEL_EFFECTIVE_PIXEL_SIZE
  */
std::string Calibration::Settings::Model::EffectivePixelSize::GetEntryName() const{
    return "CALIBRATION_SETTINGS_MODEL_EFFECTIVE_PIXEL_SIZE";
}

/** \brief get value as string*/
std::string Calibration::Settings::Model::EffectivePixelSize::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief get default 0.0 as string*/
std::string Calibration::Settings::Model::EffectivePixelSize::GetEntryDefault() const{
    return dlp::Number::ToString(0.0);
}

/** \brief set value from string value
 * \param[in] value string of a number (float) to be set*/
void Calibration::Settings::Model::EffectivePixelSize::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<float>(value);
}



/** \brief Effective pixel size default constructor = 0*/
Calibration::Settings::Model::EstimatedFocalLength::EstimatedFocalLength(){
    this->value_ = 0;
}

/** \brief Effective pixel size explicit constructor
 * \param[in] offset float from 0.0 to 1 representing the vertical offset percent*/
Calibration::Settings::Model::EstimatedFocalLength::EstimatedFocalLength(const float &millimeters){
    this->value_ = millimeters;
}

/** \brief set value of EstimatedFocalLength percent
 * \param[in] offset float from 0.0 to 1 representing the vertical offset percent*/
void Calibration::Settings::Model::EstimatedFocalLength::Set(const float &millimeters){
    this->value_ = millimeters;
}

/** \brief get method for EstimatedFocalLength value*/
float Calibration::Settings::Model::EstimatedFocalLength::Get() const{
    return this->value_;
}

/** \brief returns "CALIBRATION_SETTINGS_MODEL_EFFECTIVE_PIXEL_SIZE"
  * \retval CALIBRATION_SETTINGS_MODEL_ESTIMATED_FOCAL_LENGTH
  */
std::string Calibration::Settings::Model::EstimatedFocalLength::GetEntryName() const{
    return "CALIBRATION_SETTINGS_MODEL_ESTIMATED_FOCAL_LENGTH";
}

/** \brief get value as string*/
std::string Calibration::Settings::Model::EstimatedFocalLength::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief get default 0.0 as string*/
std::string Calibration::Settings::Model::EstimatedFocalLength::GetEntryDefault() const{
    return dlp::Number::ToString(0.0);
}

/** \brief set value from string value
 * \param[in] value string of a number (float) to be set*/
void Calibration::Settings::Model::EstimatedFocalLength::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<float>(value);
}



















/** \brief Vertical Offset percent default constructor = 0*/
Calibration::Settings::Model::VerticalOffsetPercent::VerticalOffsetPercent(){
    this->value_ = 0;
}

/** \brief Vertical offset percent explicit constructor
 * \param[in] offset float from 0.0 to 1 representing the vertical offset percent*/
Calibration::Settings::Model::VerticalOffsetPercent::VerticalOffsetPercent(const float &offset){
    this->value_ = offset;
}

/** \brief set value of VerticalOffset percent
 * \param[in] offset float from 0.0 to 1 representing the vertical offset percent*/
void Calibration::Settings::Model::VerticalOffsetPercent::Set(const float &offset){
    this->value_ = offset;
}

/** \brief get method for vertical offset percent value*/
float Calibration::Settings::Model::VerticalOffsetPercent::Get() const{
    return this->value_;
}

/** \brief returns "CALIBRATION_SETTINGS_MODEL_VERTICAL_OFFSET_PERCENT"
  * \retval CALIBRATION_SETTINGS_MODEL_VERTICAL_OFFSET_PERCENT
  */
std::string Calibration::Settings::Model::VerticalOffsetPercent::GetEntryName() const{
    return "CALIBRATION_SETTINGS_MODEL_VERTICAL_OFFSET_PERCENT";
}

/** \brief get value as string*/
std::string Calibration::Settings::Model::VerticalOffsetPercent::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief get default 0.0 as string*/
std::string Calibration::Settings::Model::VerticalOffsetPercent::GetEntryDefault() const{
    return dlp::Number::ToString(0.0);
}

/** \brief set percent from string value
 * \param[in] value string of a number (float) to be set*/
void Calibration::Settings::Model::VerticalOffsetPercent::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<float>(value);
}

/** \brief Default constructor sets horizontal offset percent to 0*/
Calibration::Settings::Model::HorizontalOffsetPercent::HorizontalOffsetPercent(){
    this->value_ = 0;
}

/** \brief Constructor, sets horizontal offset percent
 * \param[in] offset a float from 0 to 1 indicating offset*/
Calibration::Settings::Model::HorizontalOffsetPercent::HorizontalOffsetPercent(const float &offset){
    this->value_ = offset;
}

/** \brief set horizontal offset
 * \param[in] offset a float from 0 to 1 indicating offset*/
void Calibration::Settings::Model::HorizontalOffsetPercent::Set(const float &offset){
    this->value_ = offset;
}

/** \brief returns horizontal offset*/
float Calibration::Settings::Model::HorizontalOffsetPercent::Get() const{
    return this->value_;
}

/** \brief gets the entry name as a string
 *  \retval CALIBRATION_SETTINGS_MODEL_HORIZONTAL_OFFSET_PERCENT
 */
std::string Calibration::Settings::Model::HorizontalOffsetPercent::GetEntryName() const{
    return "CALIBRATION_SETTINGS_MODEL_HORIZONTAL_OFFSET_PERCENT";
}

/** \brief gets the entry value as a string*/
std::string Calibration::Settings::Model::HorizontalOffsetPercent::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief gets default value "0.0"*/
std::string Calibration::Settings::Model::HorizontalOffsetPercent::GetEntryDefault() const{
    return dlp::Number::ToString(0.0);
}

/** \brief sets the entry value from a string
 * \param[in] value value from 0.0 to 1 indicating horizontal offset percent to set*/
void Calibration::Settings::Model::HorizontalOffsetPercent::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<float>(value);
}

/** \brief default constructor sets rows to 0*/
Calibration::Settings::Image::Rows::Rows(){
    this->value_ = 0;
}

/** \brief Constructor, sets number of rows
 * \param[in] rows number of rows*/
Calibration::Settings::Image::Rows::Rows(const unsigned int &rows){
    this->value_ = rows;
}

/** \brief set number of rows
 * \param[in] rows number of rows to set*/
void Calibration::Settings::Image::Rows::Set(const unsigned int &rows){
    this->value_ = rows;
}

/** \brief get the number of rows*/
unsigned int Calibration::Settings::Image::Rows::Get() const{
    return this->value_;
}

/** \brief return the netry name a string
 *  \retval CALIBRATION_SETTINGS_IMAGE_ROWS
 */
std::string Calibration::Settings::Image::Rows::GetEntryName() const{
    return "CALIBRATION_SETTINGS_IMAGE_ROWS";
}

/** \brief return the entry value as a string*/
std::string Calibration::Settings::Image::Rows::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief return entry default value of "0"*/
std::string Calibration::Settings::Image::Rows::GetEntryDefault() const{
    return "0";
}

/** \brief set entry value from a string
 * \param[in] value string value of how many rows to set*/
void Calibration::Settings::Image::Rows::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}



/** \brief Constructor default 0*/
Calibration::Settings::Image::Columns::Columns(){
    this->value_ = 0;
}

/** \brief Constructor with input parameter
 * \param[in] columns number of columns*/
Calibration::Settings::Image::Columns::Columns(const unsigned int &columns){
    this->value_ = columns;
}

/** \brief set number of columns
 * \param[in] columns number of columns to set*/
void Calibration::Settings::Image::Columns::Set(const unsigned int &columns){
    this->value_ = columns;
}

/** \brief return number of columns*/
unsigned int Calibration::Settings::Image::Columns::Get() const{
    return this->value_;
}

/** \brief return entry name
 *  \retval CALIBRATION_SETTINGS_IMAGE_COLUMNS
 */
std::string Calibration::Settings::Image::Columns::GetEntryName() const{
    return "CALIBRATION_SETTINGS_IMAGE_COLUMNS";
}

/** \brief return entry value*/
std::string Calibration::Settings::Image::Columns::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief return entry default of "0"*/
std::string Calibration::Settings::Image::Columns::GetEntryDefault() const{
    return "0";
}

/** \brief set entry value based on string
 * \param[in] value string input to set number of columns*/
void Calibration::Settings::Image::Columns::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}

/** \brief default constructor, sets to 0*/
Calibration::Settings::Board::Features::Rows::Rows(){
    this->value_ = 0;
}

/** \brief constructor, takes number of rows as parameter
 * \param[in] rows number of rows to construct with*/
Calibration::Settings::Board::Features::Rows::Rows(const unsigned int &rows){
    this->value_ = rows;
}

/** \brief set number of rows
 * \param[in] rows number of rows to set*/
void Calibration::Settings::Board::Features::Rows::Set(const unsigned int &rows){
    this->value_ = rows;
}

/** \brief get number of rows*/
unsigned int Calibration::Settings::Board::Features::Rows::Get() const{
    return this->value_;
}

/** \brief return entry name
 *  \retval CALIBRATION_SETTINGS_BOARD_FEATURE_ROWS
 */
std::string Calibration::Settings::Board::Features::Rows::GetEntryName() const{
    return "CALIBRATION_SETTINGS_BOARD_FEATURE_ROWS";
}

/** \brief return entry value as string*/
std::string Calibration::Settings::Board::Features::Rows::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief return default value of "0"*/
std::string Calibration::Settings::Board::Features::Rows::GetEntryDefault() const{
    return "0";
}

/** \brief set number of rows
 * \param[in] value number of rows to set*/
void Calibration::Settings::Board::Features::Rows::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}

/** \brief Distance default constructor 0*/
Calibration::Settings::Board::Features::Rows::Distance::Distance(){
    this->value_ = 0;
}

/** \brief Distance constructor
 * \param[in] distance distance to start with by default (double)*/
Calibration::Settings::Board::Features::Rows::Distance::Distance(const double &distance){
    this->value_ = distance;
}

/** \brief set the distance
 * \param[in] distance distance to set*/
void Calibration::Settings::Board::Features::Rows::Distance::Set(const double &distance){
    this->value_ = distance;
}

/** \brief get the distance*/
double Calibration::Settings::Board::Features::Rows::Distance::Get() const{
    return this->value_;
}

/** \brief return entry name
 *  \retval CALIBRATION_SETTINGS_BOARD_FEATURE_ROWS_DISTANCE
 */
std::string Calibration::Settings::Board::Features::Rows::Distance::GetEntryName() const{
    return "CALIBRATION_SETTINGS_BOARD_FEATURE_ROWS_DISTANCE";
}

/** \brief return entry value*/
std::string Calibration::Settings::Board::Features::Rows::Distance::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief return default value "0"*/
std::string Calibration::Settings::Board::Features::Rows::Distance::GetEntryDefault() const{
    return "0";
}

/** \brief set entry value from string input
 * \param[in] value value to set from string*/
void Calibration::Settings::Board::Features::Rows::Distance::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<double>(value);
}

/** \brief default constructor sets value to 0*/
Calibration::Settings::Board::Features::Rows::DistanceInPixels::DistanceInPixels(){
    this->value_ = 0;
}

/** \brief constructor with number of pixels as parameter
 * \param[in] pixels number of pixels to set*/
Calibration::Settings::Board::Features::Rows::DistanceInPixels::DistanceInPixels(const unsigned int &pixels){
    this->value_ = pixels;
}

/** \brief set number of pixels as parameter
 * \param[in] pixels number of pixels to set*/
void Calibration::Settings::Board::Features::Rows::DistanceInPixels::Set(const unsigned int &pixels){
    this->value_ = pixels;
}

/** \brief returns number of pixels*/
unsigned int Calibration::Settings::Board::Features::Rows::DistanceInPixels::Get() const{
    return this->value_;
}

/** \brief return entry name
 *  \retval CALIBRATION_SETTINGS_BOARD_FEATURE_ROW_DISTANCE_IN_PIXELS
 */
std::string Calibration::Settings::Board::Features::Rows::DistanceInPixels::GetEntryName() const{
    return "CALIBRATION_SETTINGS_BOARD_FEATURE_ROW_DISTANCE_IN_PIXELS";
}

/** \brief return entry value as string*/
std::string Calibration::Settings::Board::Features::Rows::DistanceInPixels::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief return entry default "0"*/
std::string Calibration::Settings::Board::Features::Rows::DistanceInPixels::GetEntryDefault() const{
    return "0";
}

/** \brief set entry value from string
 * \param[in] value value to set entry*/
void Calibration::Settings::Board::Features::Rows::DistanceInPixels::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}


Calibration::Settings::Board::Features::Columns::Columns(){
    this->value_ = 0;
}

Calibration::Settings::Board::Features::Columns::Columns(const unsigned int &columns){
    this->value_ = columns;
}

void Calibration::Settings::Board::Features::Columns::Set(const unsigned int &columns){
    this->value_ = columns;
}

unsigned int Calibration::Settings::Board::Features::Columns::Get() const{
    return this->value_;
}

/** \brief Gets entry name for column features
 *  \retval CALIBRATION_SETTINGS_BOARD_FEATURE_COLUMNS
 */
std::string Calibration::Settings::Board::Features::Columns::GetEntryName() const{
    return "CALIBRATION_SETTINGS_BOARD_FEATURE_COLUMNS";
}

std::string Calibration::Settings::Board::Features::Columns::GetEntryValue() const{
    return Number::ToString(this->value_);
}

std::string Calibration::Settings::Board::Features::Columns::GetEntryDefault() const{
    return "0";
}

void Calibration::Settings::Board::Features::Columns::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}


Calibration::Settings::Board::Features::Columns::Distance::Distance(){
    this->value_ = 0;
}

Calibration::Settings::Board::Features::Columns::Distance::Distance(const double &distance){
    this->value_ = distance;
}

void Calibration::Settings::Board::Features::Columns::Distance::Set(const double &distance){
    this->value_ = distance;
}

double Calibration::Settings::Board::Features::Columns::Distance::Get() const{
    return this->value_;
}

/** \brief Gets entry name for column distance
 *  \retval CALIBRATION_SETTINGS_BOARD_FEATURE_COLUMNS_DISTANCE
 */
std::string Calibration::Settings::Board::Features::Columns::Distance::GetEntryName() const{
    return "CALIBRATION_SETTINGS_BOARD_FEATURE_COLUMNS_DISTANCE";
}

std::string Calibration::Settings::Board::Features::Columns::Distance::GetEntryValue() const{
    return Number::ToString(this->value_);
}

std::string Calibration::Settings::Board::Features::Columns::Distance::GetEntryDefault() const{
    return "0";
}

void Calibration::Settings::Board::Features::Columns::Distance::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<double>(value);
}


Calibration::Settings::Board::Features::Columns::DistanceInPixels::DistanceInPixels(){
    this->value_ = 0;
}

Calibration::Settings::Board::Features::Columns::DistanceInPixels::DistanceInPixels(const unsigned int &pixels){
    this->value_ = pixels;
}

void Calibration::Settings::Board::Features::Columns::DistanceInPixels::Set(const unsigned int &pixels){
    this->value_ = pixels;
}

unsigned int Calibration::Settings::Board::Features::Columns::DistanceInPixels::Get() const{
    return this->value_;
}

/** \brief Gets entry name for column distance in pixels
 *  \retval CALIBRATION_SETTINGS_BOARD_FEATURE_COLUMN_DISTANCE_IN_PIXELS
 */
std::string Calibration::Settings::Board::Features::Columns::DistanceInPixels::GetEntryName() const{
    return "CALIBRATION_SETTINGS_BOARD_FEATURE_COLUMN_DISTANCE_IN_PIXELS";
}

std::string Calibration::Settings::Board::Features::Columns::DistanceInPixels::GetEntryValue() const{
    return Number::ToString(this->value_);
}

std::string Calibration::Settings::Board::Features::Columns::DistanceInPixels::GetEntryDefault() const{
    return "0";
}

void Calibration::Settings::Board::Features::Columns::DistanceInPixels::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}

Calibration::Settings::Board::NumberRequired::NumberRequired(){
    this->value_ = 0;
}

Calibration::Settings::Board::NumberRequired::NumberRequired(const unsigned int &boards){
    this->value_ = boards;
}

void Calibration::Settings::Board::NumberRequired::Set(const unsigned int &boards){
    this->value_ = boards;
}

unsigned int Calibration::Settings::Board::NumberRequired::Get() const{
    return this->value_;
}

/** \brief Gets entry name for number of calibration boards required
 *  \retval CALIBRATION_BOARD_NUMBER_REQUIRED
 */
std::string Calibration::Settings::Board::NumberRequired::GetEntryName() const{
    return "CALIBRATION_BOARD_NUMBER_REQUIRED";
}

std::string Calibration::Settings::Board::NumberRequired::GetEntryValue() const{
    return Number::ToString(this->value_);
}

std::string Calibration::Settings::Board::NumberRequired::GetEntryDefault() const{
    return "0";
}

void Calibration::Settings::Board::NumberRequired::SetEntryValue(std::string value){
    this->value_ = String::ToNumber<unsigned int>(value);
}


/** \brief foreground default constructor set to white ie RGB = 255*/
Calibration::Settings::Board::Color::Foreground::Foreground(){
    this->value_.r = 255;
    this->value_.g = 255;
    this->value_.b = 255;
}

/** \brief constructor with color input
 * \param[in] color PixelRGB object that contains color to set*/
Calibration::Settings::Board::Color::Foreground::Foreground(const dlp::PixelRGB &color){
    this->value_ = color;
}

/** \brief set color
 * \param[in] color PixelRGB object that contains color to set*/
void Calibration::Settings::Board::Color::Foreground::Set(const dlp::PixelRGB &color){
    this->value_ = color;
}

/** \brief get color of foreground*/
dlp::PixelRGB Calibration::Settings::Board::Color::Foreground::Get() const{
    return this->value_;
}

/** \brief return entry name for board foreground color
 *  \retval CALIBRATION_SETTINGS_BOARD_COLOR_FOREGROUND
 */
std::string Calibration::Settings::Board::Color::Foreground::GetEntryName() const{
    return "CALIBRATION_SETTINGS_BOARD_COLOR_FOREGROUND";
}

/** \brief get entry value as string*/
std::string Calibration::Settings::Board::Color::Foreground::GetEntryValue() const{
    return dlp::Number::ToString(this->value_.r) + ", " + dlp::Number::ToString(this->value_.g) + ", " + dlp::Number::ToString(this->value_.b);
}

/** \brief get entry default of "255, 255, 255"*/
std::string Calibration::Settings::Board::Color::Foreground::GetEntryDefault() const{
    return "255, 255, 255";
}

/** \brief set entry value from string
 * \param[in] value RGB values deliminated by a comma*/
void Calibration::Settings::Board::Color::Foreground::SetEntryValue(std::string value){
    std::vector<std::string> colors = dlp::String::SeparateDelimited(value,',');

    this->value_.r = dlp::String::ToNumber<unsigned char>(colors.at(0));
    this->value_.g = dlp::String::ToNumber<unsigned char>(colors.at(1));
    this->value_.b = dlp::String::ToNumber<unsigned char>(colors.at(2));
}


Calibration::Settings::Board::Color::Background::Background(){
    this->value_.r = 0;
    this->value_.g = 0;
    this->value_.b = 0;
}

Calibration::Settings::Board::Color::Background::Background(const dlp::PixelRGB &color){
    this->value_ = color;
}

void Calibration::Settings::Board::Color::Background::Set(const dlp::PixelRGB &color){
    this->value_ = color;
}

dlp::PixelRGB Calibration::Settings::Board::Color::Background::Get() const{
    return this->value_;
}

/** \brief return entry name for board background color
 *  \retval CALIBRATION_SETTINGS_BOARD_COLOR_BACKGROUND
 */
std::string Calibration::Settings::Board::Color::Background::GetEntryName() const{
    return "CALIBRATION_SETTINGS_BOARD_COLOR_BACKGROUND";
}

std::string Calibration::Settings::Board::Color::Background::GetEntryValue() const{
    return dlp::Number::ToString(this->value_.r) + ", " + dlp::Number::ToString(this->value_.g) + ", " + dlp::Number::ToString(this->value_.b);
}

std::string Calibration::Settings::Board::Color::Background::GetEntryDefault() const{
    return "0,0,0";
}

void Calibration::Settings::Board::Color::Background::SetEntryValue(std::string value){
    std::vector<std::string> colors = dlp::String::SeparateDelimited(value,',');

    this->value_.r = dlp::String::ToNumber<unsigned char>(colors.at(0));
    this->value_.g = dlp::String::ToNumber<unsigned char>(colors.at(1));
    this->value_.b = dlp::String::ToNumber<unsigned char>(colors.at(2));
}

Calibration::Settings::TangentDistortion::TangentDistortion(){
    this->value_ = Options::NORMAL;
}

Calibration::Settings::TangentDistortion::TangentDistortion(const Options &setting){
    this->value_ = setting;
}

void Calibration::Settings::TangentDistortion::Set(const Options &setting){
    this->value_ = setting;
}

Calibration::Settings::TangentDistortion::Options Calibration::Settings::TangentDistortion::Get() const{
    return this->value_;
}

/** \brief return entry name for tangent distortion
 *  \retval CALIBRATION_TANGENT_DISTORTION
 */
std::string Calibration::Settings::TangentDistortion::GetEntryName() const{
    return "CALIBRATION_TANGENT_DISTORTION";
}

std::string Calibration::Settings::TangentDistortion::GetEntryValue() const{
    switch(this->value_){
    case Options::SET_TO_ZERO:  return "SET_TO_ZERO";
    case Options::NORMAL:
    default:                    return "NORMAL";
    }
}

std::string Calibration::Settings::TangentDistortion::GetEntryDefault() const{
    return "NORMAL";
}

void Calibration::Settings::TangentDistortion::SetEntryValue(std::string value){
    if(value.compare("SET_TO_ZERO") == 0){
        this->value_ = Options::SET_TO_ZERO;
    }
    else{
        this->value_ = Options::NORMAL;
    }
}

Calibration::Settings::SixthOrderDistortion::SixthOrderDistortion(){
    this->value_ = Options::NORMAL;
}

Calibration::Settings::SixthOrderDistortion::SixthOrderDistortion(const Options &setting){
    this->value_ = setting;
}

/** \brief Sets the distortion
 * \param[in] setting \ref Options object that contains whether or not to have distortion*/
void Calibration::Settings::SixthOrderDistortion::Set(const Options &setting){
    this->value_ = setting;
}

/** \brief Returns the \ref Options object of the sixth order distortion*/
Calibration::Settings::SixthOrderDistortion::Options Calibration::Settings::SixthOrderDistortion::Get() const{
    return this->value_;
}

/** \brief returns entry name for sixth order distortion setting
 *  \retval CALIBRATION_SIXTH_ORDER_DISTORTION
 */
std::string Calibration::Settings::SixthOrderDistortion::GetEntryName() const{
    return "CALIBRATION_SIXTH_ORDER_DISTORTION";
}

/** \brief Returns the entry value, either FIX_RADIAL_DISTORTION_COEFFICIENT or NORMAL*/
std::string Calibration::Settings::SixthOrderDistortion::GetEntryValue() const{
    switch(this->value_){
    case Options::FIX_RADIAL_DISTORTION_COEFFICIENT:    return "FIX_RADIAL_DISTORTION_COEFFICIENT";
    case Options::NORMAL:
    default:                                            return "NORMAL";
    }
}

/** \brief return "NORMAL"
 *  \retval NORMAL
 */
std::string Calibration::Settings::SixthOrderDistortion::GetEntryDefault() const{
    return "NORMAL";
}

/** \brief set value from string input for sixth order distortion
 *  \param[in] value value to set*/
void Calibration::Settings::SixthOrderDistortion::SetEntryValue(std::string value){
    if(value.compare("FIX_RADIAL_DISTORTION_COEFFICIENT") == 0){
        this->value_ = Options::FIX_RADIAL_DISTORTION_COEFFICIENT;
    }
    else{
        this->value_ = Options::NORMAL;
    }
}

}
