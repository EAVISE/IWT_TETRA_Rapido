/** \file   gray_code_settings.cpp
 *  \brief  Contains methods to set gray code setting entries
 */

#include <common/returncode.hpp>
#include <common/debug.hpp>
#include <common/parameters.hpp>
#include <common/capture/capture.hpp>
#include <common/pattern/pattern.hpp>

#include <structured_light/structured_light.hpp>
#include <structured_light/gray_code/gray_code.hpp>


namespace dlp{

/** \brief Default constructor, sets value to 0*/
GrayCode::Settings::Pixel::Threshold::Threshold(){
    this->value_ = 0;
}

/** \brief Set value constructor
 * \param[in] threshold The value to set the threshold of a pixel to for disparity map creation */
GrayCode::Settings::Pixel::Threshold::Threshold(const unsigned char &threshold){
    this->value_ = threshold;
}

/** \brief Set method
 * \param[in] threshold The value to set the threshold of a pixel to for disparity map creation */
void GrayCode::Settings::Pixel::Threshold::Set(const unsigned char &threshold){
    this->value_ = threshold;
}

/** \brief Get method */
unsigned char GrayCode::Settings::Pixel::Threshold::Get() const{
    return this->value_;
}

/** \brief Get method, returns "GRAYCODE_SETTING_PIXEL_THRESHOLD"
 *  \retval GRAYCODE_SETTING_PIXEL_THRESHOLD
 */
std::string GrayCode::Settings::Pixel::Threshold::GetEntryName() const{
    return "GRAYCODE_SETTING_PIXEL_THRESHOLD";
}

/** \brief Get method, returns value as a string */
std::string GrayCode::Settings::Pixel::Threshold::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief Returns default entry value of 0*/
std::string GrayCode::Settings::Pixel::Threshold::GetEntryDefault() const{
    return dlp::Number::ToString(0);
}

/** \brief Set threshold to the value of this string*/
void GrayCode::Settings::Pixel::Threshold::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<Settings::Pixel::Threshold::Type>(value);
}



}



