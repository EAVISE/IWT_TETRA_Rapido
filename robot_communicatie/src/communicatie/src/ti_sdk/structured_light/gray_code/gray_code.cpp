/** \file   gray_code.cpp
 *  \brief  Contains methods to gray code
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */


#include <common/returncode.hpp>
#include <common/debug.hpp>
#include <common/parameters.hpp>
#include <common/capture/capture.hpp>
#include <common/pattern/pattern.hpp>
#include <common/disparity_map.hpp>

#include <structured_light/structured_light.hpp>
#include <structured_light/gray_code/gray_code.hpp>

#include <math.h>

#include <sstream>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief Constructs object */
GrayCode::GrayCode(){
    this->debug_.SetName("STRUCTURED_LIGHT_GRAY_CODE (" + dlp::Number::ToString(this)+ "): ");
    this->debug_.Msg("Constructing object...");
    this->is_setup_ = false;
    this->disparity_map_.Clear();

    this->debug_.Msg("Object constructed");
}

/** \brief Destroys object and deallocates memory */
GrayCode::~GrayCode(){
    this->debug_.Msg("Deconstructing object...");
    this->disparity_map_.Clear();
    this->debug_.Msg("Object deconstructed");
}

/** \brief Retrieves settings from \dlp::Parameter object to configure
 *  \ref dlp::Pattern::Sequence generation and \ref dlp::Capture::Sequence decoding
 *  \param[in]  settings    \ref dlp::Parameters object to retrieve settings from
 *
 * \retval STRUCTURED_LIGHT_SETTINGS_SEQUENCE_COUNT_MISSING             \ref dlp::Parameters list missing \ref dlp::StructuredLight::Settings::Sequence::Count
 * \retval STRUCTURED_LIGHT_SETTINGS_SEQUENCE_INCLUDE_INVERTED_MISSING  \ref dlp::Parameters list missing \ref dlp::StructuredLight::Settings::Sequence::IncludeInverted
 * \retval STRUCTURED_LIGHT_SETTINGS_IMAGE_ROWS_MISSING                 \ref dlp::Parameters list missing \ref dlp::StructuredLight::Settings::Image::Size::Rows
 * \retval STRUCTURED_LIGHT_SETTINGS_IMAGE_COLUMNS_MISSING              \ref dlp::Parameters list missing \ref dlp::StructuredLight::Settings::Image::Size::Columns
 * \retval STRUCTURED_LIGHT_SETTINGS_PATTERN_COLOR_MISSING              \ref dlp::Parameters list missing \ref dlp::StructuredLight::Settings::Pattern::Color
 * \retval STRUCTURED_LIGHT_SETTINGS_PATTERN_ROWS_MISSING               \ref dlp::Parameters list missing \ref dlp::StructuredLight::Settings::Pattern::Size::Rows
 * \retval STRUCTURED_LIGHT_SETTINGS_PATTERN_COLUMNS_MISSING            \ref dlp::Parameters list missing \ref dlp::StructuredLight::Settings::Pattern::Size::Columns
 * \retval STRUCTURED_LIGHT_SETTINGS_PATTERN_ORIENTATION_MISSING        \ref dlp::Parameters list missing \ref dlp::StructuredLight::Settings::Pattern::Orientation
 * \retval GRAY_CODE_PIXEL_THRESHOLD_MISSING                            \ref dlp::Parameters list missing \ref dlp::GrayCode::Settings::Pixel::Threshold
 */
ReturnCode GrayCode::Setup(const Parameters &settings){
    ReturnCode ret;

    ret = settings.Get(&this->sequence_count_);
    if(ret.hasErrors())
        return ret.AddError(STRUCTURED_LIGHT_SETTINGS_SEQUENCE_COUNT_MISSING);

    if(settings.Get(&this->sequence_include_inverted_).hasErrors())
        return ret.AddError(STRUCTURED_LIGHT_SETTINGS_SEQUENCE_INCLUDE_INVERTED_MISSING);

    if(this->sequence_include_inverted_.Get()){
        this->sequence_count_total_ = this->sequence_count_.Get() * 2;
    }
    else{
        this->sequence_count_total_ = this->sequence_count_.Get() + 2; // Add 2 for all on and all off albedo calculation images
    }

    if((settings.Get(&this->pattern_color_)).hasErrors())
        return ret.AddError(STRUCTURED_LIGHT_SETTINGS_PATTERN_COLOR_MISSING);

    if(!this->projector_set_){
        if(settings.Get(&this->pattern_rows_).hasErrors())
            return ret.AddError(STRUCTURED_LIGHT_SETTINGS_PATTERN_ROWS_MISSING);

        if((settings.Get(&this->pattern_columns_).hasErrors()))
            return ret.AddError(STRUCTURED_LIGHT_SETTINGS_PATTERN_COLUMNS_MISSING);
    }

    if(settings.Get(&this->pattern_orientation_).hasErrors())
        return ret.AddError(STRUCTURED_LIGHT_SETTINGS_PATTERN_ORIENTATION_MISSING);

    if(settings.Get(&this->pixel_threshold_).hasErrors())
        return ret.AddError(GRAY_CODE_PIXEL_THRESHOLD_MISSING);


    // If vertical patterns are used the resolution of the gray coded patterns
    // is determined by the number of columns. If horizontal patterns are used
    // the resolution of the gray coded patterns is determined by the number
    // of rows
    switch(this->pattern_orientation_.Get()){
    case StructuredLight::Settings::Pattern::Orientation::Type::VERTICAL:
        this->resolution_ = this->pattern_columns_.Get();
        break;
    case StructuredLight::Settings::Pattern::Orientation::Type::HORIZONTAL:
        this->resolution_ = this->pattern_rows_.Get();
        break;
    case StructuredLight::Settings::Pattern::Orientation::Type::INVALID:
    default:
        return ret.AddError(STRUCTURED_LIGHT_NOT_SETUP);
        break;
    }

    // The maximum disparity is determined by the resolution and needs to be
    // a power of two for the decoding process
    this->maximum_patterns_  = (unsigned int)ceil(log2((double)this->resolution_));
    this->maximum_disparity_ = 1ul << this->maximum_patterns_;

    // The MSB pattern value is determined by the maximum disparity value
    this->msb_pattern_value_ = this->maximum_disparity_ >> 1;

    // Since the maximum disparity value will be larger than the resolution
    // an offset must be stored to adjust the decoded values to the correct
    // disparity values on the DMD
    this->offset_ = floor((this->maximum_disparity_ - this->resolution_)/2);

    // Check that request pattern count is valid
    if(this->sequence_count_.Get() > this->maximum_patterns_)
        return ret.AddError(STRUCTURED_LIGHT_CAPTURE_SEQUENCE_SIZE_INVALID);

    // Setup has been completed
    this->is_setup_ = true;

    return ret;
}

/** \brief Generates a \ref dlp::Pattern::Sequence based on the settings specified
 *  \param[out] pattern_sequence Return pointer to \ref dlp::Pattern::Sequence
 *  \retval STRUCTURED_LIGHT_NULL_POINTER_ARGUMENT  Return argument is NULL
 *  \retval STRUCTURED_LIGHT_NOT_SETUP              Module has NOT been setup
 *  \retval STRUCTURED_LIGHT_CAPTURE_SEQUENCE_SIZE_INVALID  Requested number of patterns is NOT possible with the specified DMD resolution
 */
ReturnCode GrayCode::GeneratePatternSequence(Pattern::Sequence *pattern_sequence){
    ReturnCode ret;

    // Check that argument is not null
    if(!pattern_sequence)
    {
        std::cout<<"argument pattern_sequence is null"<<std::endl;
        return ret.AddError(STRUCTURED_LIGHT_NULL_POINTER_ARGUMENT);
    }

    // Clear the pattern sequence
    pattern_sequence->Clear();
    std::vector<cv::Mat> patronen;
    std::vector<cv::Mat> patronen_dummy;

    switch(this->pattern_orientation_.Get()){
    case StructuredLight::Settings::Pattern::Orientation::Type::VERTICAL:
        patronen = patronen_dummy;
        ///Alle images inlezen die we tot de pattern laten horen. -->Juiste path opgeven
        ///er zijn 24 patronen (patroon0.png - patroon23.png) die we gaan inlezen
        for(int teller = 0; teller <= 23; teller++)
        {
            std::ostringstream convert;
            convert << teller;
            patronen.push_back(cv::imread("/home/sgr/Documents/amazon-picking-challenge/robot_communicatie/src/communicatie/SL/patronen_sl/patroon"+convert.str()+".bmp", 0));
        }
        ///cv::Mats omvormen naar dlp::Images, waarna we de images omvormen tot dlp::Patterns

        ///voor elk patroon + zijn inverse een pattern maken en toevoegen aan de totale pattern
        for(int i = 0; i<=23; i+=2)
        {
            dlp::Pattern pattern;
            dlp::Image   image = dlp::Image(patronen[i]);

            dlp::Pattern pattern_inverted;
            dlp::Image   image_inverted  = dlp::Image(patronen[i+1]);

            pattern.image_data = image;

            pattern.type.Set(dlp::Pattern::Settings::Data::Type::IMAGE_DATA);
            pattern.bitdepth.Set(dlp::Pattern::Settings::Bitdepth::Format::MONO_1BPP);

            ///de color hangt af van de firmware. elk patroon heeft daar een specifieke kleur gekregen (blauw, rood of groen) en deze moeten overeenkomen
            dlp::Pattern::Settings::Color color;

            color = dlp::Pattern::Settings::Color(dlp::Pattern::Settings::Color::Type::WHITE);

            pattern.color = color;
            pattern_sequence->Add(pattern);


            //inverted:

            pattern_inverted.image_data = image_inverted;

            pattern_inverted.type.Set(dlp::Pattern::Settings::Data::Type::IMAGE_DATA);
            pattern_inverted.bitdepth.Set(dlp::Pattern::Settings::Bitdepth::Format::MONO_1BPP);
            ///de color hangt af van de firmware. elk patroon heeft daar een specifieke kleur gekregen (blauw, rood of groen) en deze moeten overeenkomen
            pattern_inverted.color = color;

            pattern_sequence->Add(pattern_inverted);

            image.Clear();
            image_inverted.Clear();

        }

        break;
    case StructuredLight::Settings::Pattern::Orientation::Type::HORIZONTAL:
        patronen = patronen_dummy;
        ///Alle images inlezen die we tot de pattern laten horen. -->Juiste path opgeven

        ///er zijn 24 patronen (patroon24.png - patroon47.png) die we gaan inlezen
        for(int teller = 24; teller <= 47; teller++)
        {
            std::ostringstream convert;
            convert << teller;

            patronen.push_back(cv::imread("/home/sgr/Documents/amazon-picking-challenge/robot_communicatie/src/communicatie/SL/patronen_sl/patroon"+convert.str()+".bmp", 0));

        }

        ///cv::Mats omvormen naar dlp::Images, waarna we de images omvormen tot dlp::Patterns

        ///voor elk patroon + zijn inverse een pattern maken en toevoegen aan de totale pattern
        for(int i = 0; i<24; i+=2)
        {
            dlp::Pattern pattern;
            dlp::Image   image = dlp::Image(patronen[i]);

            dlp::Pattern pattern_inverted;
            dlp::Image   image_inverted  = dlp::Image(patronen[i+1]);

            pattern.image_data = image;

            pattern.type.Set(dlp::Pattern::Settings::Data::Type::IMAGE_DATA);
            pattern.bitdepth.Set(dlp::Pattern::Settings::Bitdepth::Format::MONO_1BPP);
            dlp::Pattern::Settings::Color color;
            color = dlp::Pattern::Settings::Color(dlp::Pattern::Settings::Color::Type::WHITE);

            pattern.color = color;

            pattern_sequence->Add(pattern);


            //inverted:

            pattern_inverted.image_data = image_inverted;

            pattern_inverted.type.Set(dlp::Pattern::Settings::Data::Type::IMAGE_DATA);
            pattern_inverted.bitdepth.Set(dlp::Pattern::Settings::Bitdepth::Format::MONO_1BPP);
            pattern_inverted.color = color;

            pattern_sequence->Add(pattern_inverted);

            image.Clear();
            image_inverted.Clear();
        }

        break;
    case StructuredLight::Settings::Pattern::Orientation::Type::INVALID:
    default:
        return ret.AddError(STRUCTURED_LIGHT_NOT_SETUP);
        break;
    }


    /// return de foutcode
    return ret;
}



/** \brief Decodes the \ref dlp::Capture::Sequence and returns the \ref dlp::DisparityMap
 *  \param[in] capture_sequence \ref dlp::Capture::Sequence to be decoded
 *  \param[in] disparity_map Return pointer for generated \ref dlp::DisparityMap
 *  \retval STRUCTURED_LIGHT_NULL_POINTER_ARGUMENT      Input arguments NULL
 *  \retval STRUCTURED_LIGHT_NOT_SETUP                  Module has NOT been setup
 *  \retval STRUCTURED_LIGHT_CAPTURE_SEQUENCE_EMPTY     Supplied sequence is empty
 *  \retval STRUCTURED_LIGHT_CAPTURE_SEQUENCE_SIZE_INVALID  Supplied sequence has a difference count than what was generated
 *  \retval STRUCTURED_LIGHT_DATA_TYPE_INVALID          Supplied sequence does NOT contain valid image data or a image file name
*/
ReturnCode GrayCode::DecodeCaptureSequence(Capture::Sequence *capture_sequence, dlp::DisparityMap *disparity_map){
    ReturnCode ret;

    // Check the pointers
    if(!capture_sequence || !disparity_map)
        return ret.AddError(STRUCTURED_LIGHT_NULL_POINTER_ARGUMENT);

    // Check that GrayCode object is setup
    if(!this->isSetup())
        return ret.AddError(STRUCTURED_LIGHT_NOT_SETUP);

    // Check that CaptureSequence is not empty
    if(capture_sequence->GetCount() == 0)
        return ret.AddError(STRUCTURED_LIGHT_CAPTURE_SEQUENCE_EMPTY);

    // Check that correct number of images present
    if(capture_sequence->GetCount() != this->sequence_count_total_)
        return ret.AddError(STRUCTURED_LIGHT_CAPTURE_SEQUENCE_SIZE_INVALID);

    // Create a vector of the images to decode
    std::vector<dlp::Image> images_coded;

    // Store the image resolution
    unsigned int image_rows    = 0;
    unsigned int image_columns = 0;

    for(unsigned int iCapture = 0; iCapture < this->sequence_count_total_; iCapture++){
        dlp::Capture capture;
        dlp::Image   image;
        unsigned int capture_rows;
        unsigned int capture_columns;

        // Grab the capture from the sequence
        ReturnCode ret_error = capture_sequence->Get(iCapture, &capture);

        // Check that capture was grabbed
        if(ret_error.hasErrors())
            return ret_error;

        // Check the capture type
        switch(capture.image_type.Get()){
        case dlp::Capture::Settings::Data::Type::IMAGE_FILE:
        {
            // Check that the file exists
            if(!dlp::File::Exists(capture.image_file))
                return ret.AddError(FILE_DOES_NOT_EXIST);

            // Load the file and check the resolution
            ret_error = image.Load(capture.image_file);
            if(ret_error.hasErrors())
                return ret_error;

            break;
        }
        case dlp::Capture::Settings::Data::Type::IMAGE_DATA:
        {
            // Check that the image data is not empty
            if(capture.image_data.isEmpty())
                return ret.AddError(IMAGE_EMPTY);

            // Load the file and check the resolution
            cv::Mat temp_image_data;
            capture.image_data.Unsafe_GetOpenCVData(&temp_image_data);
            ret_error = image.Create(temp_image_data);
            if(ret_error.hasErrors())
                return ret_error;
            temp_image_data.release();
            break;
        }
        case dlp::Capture::Settings::Data::Type::INVALID:
        default:
            return ret.AddError(STRUCTURED_LIGHT_DATA_TYPE_INVALID);
        }

        // Get the image resolution
        image.GetColumns(&capture_columns);
        image.GetRows(&capture_rows);

        // If on the first capture store the resolution
        if(iCapture == 0){
            image_columns = capture_columns;
            image_rows    = capture_rows;
        }

        // Check that each image has the same resolution
        if( (capture_rows    != image_rows) ||
                (capture_columns != image_columns))
            return ret.AddError(STRUCTURED_LIGHT_PATTERN_SIZE_INVALID);

        // Convert the image to monochrome
        image.ConvertToMonochrome();

        // Add the image to the list
        images_coded.push_back(image);

        // Clear the image
        image.Clear();
    }


    // All images from the CaptureSequence have been loaded

    // Allocate memory for the disparity images
    this->disparity_map_.Create( image_columns, image_rows);

    // Check is the inverted patterns are included
    unsigned int image_increment;
    if(this->sequence_include_inverted_.Get()){
        // Each "Pattern" has a normal and an inverted pattern (i.e. 2 images per pattern)
        image_increment = 2;
    }
    else{
        // Each "Pattern" only has a normal pattern (i.e. 1 images per pattern)
        image_increment = 1;
    }

    // Calculate the value the MSB pattern
    unsigned int pattern_value = this->msb_pattern_value_;
    unsigned int kImage    = 0;
    unsigned int threshold = this->pixel_threshold_.Get();
    cv::Mat temp_image_data;

    for(unsigned int iPattern = 0; iPattern < this->sequence_count_.Get(); iPattern++){
        dlp::Image image_normal;
        dlp::Image image_inverted;

        // Copy the image
        images_coded.at(kImage).Unsafe_GetOpenCVData(&temp_image_data);
        image_normal.Create(temp_image_data);

        temp_image_data.release();

        // Convert the image to monochrome
        image_normal.ConvertToMonochrome();

        // If inverted is included load the next image, if not, set to zero
        if(this->sequence_include_inverted_.Get()){
            // Add the inverted image and convert it to monochrome
            images_coded.at(kImage+1).Unsafe_GetOpenCVData(&temp_image_data);
            image_inverted.Create(temp_image_data);
            image_inverted.ConvertToMonochrome();
            temp_image_data.release();
        }

        // Decode each pixel
        for(     unsigned int yRow = 0; yRow < image_rows;    yRow++){
            for( unsigned int xCol = 0; xCol < image_columns; xCol++){

                // Get the pixel from the normal and inverted image
                unsigned char normal   = 0;
                unsigned char inverted = 0;

                image_normal.Unsafe_GetPixel(   xCol, yRow, &normal);

                if(this->sequence_include_inverted_.Get()){
                    image_inverted.Unsafe_GetPixel( xCol, yRow, &inverted);
                }
                else{
                    inverted = 255 - normal;
                }


                // Calculate the difference
                int difference = normal - inverted;

                // Get the current disparity pixel's value
                int value;
                this->disparity_map_.Unsafe_GetPixel(xCol,yRow,&value);

                // Check that point is not invalid
                if(value != dlp::DisparityMap::INVALID_PIXEL){

                    // If the disparity pixel value is empty set it to zero
                    if(value == dlp::DisparityMap::EMPTY_PIXEL)
                        value = 0;

                    // Calculate gray code value
                    int gray_code_value = 0;

                    if(difference > 0){
                        gray_code_value = pattern_value;
                    }
                    else{
                        gray_code_value = 0;
                        difference = - difference;
                    }

                    if((unsigned int)difference < threshold){
                        // Mark as invalid pixel
                        value = dlp::DisparityMap::INVALID_PIXEL;
                    }
                    else{
                        value |= gray_code_value ^ ((value >> 1) & pattern_value);

                        // If on the last pattern subtract the offset and check that the value is within the resolution
                        if(iPattern == this->sequence_count_.Get() - 1){
                            value = value - this->offset_;

                            // Check that value is not above resolution
                            if((value >= (int) this->resolution_) || (value < 0))
                                value = dlp::DisparityMap::INVALID_PIXEL;
                        }

                    }
                    this->disparity_map_.Unsafe_SetPixel(xCol,yRow,value);
                }
            }
        }

        // Shift the pattern value
        pattern_value = pattern_value >> 1;

        // Clear the images
        image_normal.Clear();
        image_inverted.Clear();

        // Increment kImage
        kImage = kImage + image_increment;
    }

    // Copy the disparity map to the pointer
    disparity_map->Create(this->disparity_map_);

    temp_image_data.release();

    return ret;
}

/** \brief      Retrieves module settings
 *  \param[in]  settings Pointer to return settings
 *  \retval     STRUCTURED_LIGHT_NULL_POINTER_ARGUMENT  Input argument is NULL
 */
ReturnCode GrayCode::GetSetup(Parameters *settings)const{
    ReturnCode ret;

    if(!settings)
        return ret.AddError(STRUCTURED_LIGHT_NULL_POINTER_ARGUMENT);

    settings->Set(this->sequence_count_);
    settings->Set(this->sequence_include_inverted_);
    settings->Set(this->pattern_rows_);
    settings->Set(this->pattern_columns_);
    settings->Set(this->pattern_color_);
    settings->Set(this->pattern_orientation_);
    settings->Set(this->pixel_threshold_);

    return ret;
}

}
