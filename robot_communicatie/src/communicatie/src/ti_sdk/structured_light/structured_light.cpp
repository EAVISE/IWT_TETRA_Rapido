/** \file   structured_light.cpp
 *  \brief  Contains setting entries and methods to set and retrieve entries
 *  for the structured light class.
 */

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/other.hpp>
#include <common/parameters.hpp>
#include <common/capture/capture.hpp>
#include <common/pattern/pattern.hpp>
#include <common/returncode.hpp>
#include <structured_light/structured_light.hpp>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief Returns the number of patterns */
unsigned int StructuredLight::GetTotalPatternCount(){
    return this->sequence_count_total_;
}

/** \brief Default Constructor
 * Sets name to "STRUCTURED LIGHT: " and leaves it in a totally uncomplete setup
 * ie no cameras/projectors images or patterns*/
StructuredLight::StructuredLight(){

    this->debug_.SetName("STRUCTURED LIGHT: ");

    this->is_setup_   = false;
    this->is_decoded_ = false;

    this->projector_set_ = false;

    this->disparity_map_.Clear();

    this->sequence_count_total_ = 0;

    this->sequence_count_.Set(0);
    this->sequence_include_inverted_.Set(false);
    this->pattern_rows_.Set(0);
    this->pattern_columns_.Set(0);
    this->pattern_color_.Set(dlp::Pattern::Settings::Color::Type::INVALID);
    this->pattern_orientation_.Set(Settings::Pattern::Orientation::Type::INVALID);
}

StructuredLight::~StructuredLight(){
    // Release the disparity map image data
    this->disparity_map_.Clear();
}

/** \brief Retrieves the DLP Platform resolution
 *  \retval STRUCTURED_LIGHT_NOT_SETUP \ref Setup() has not been run*/
ReturnCode StructuredLight::SetDlpPlatform( const dlp::DLP_Platform &platform ){
    ReturnCode ret;

    this->debug_.Msg("Retrieving DLP Platform resolution...");

    // Check that DLP_Platform is setup
    if(!platform.isPlatformSetup())
        return ret.AddError(STRUCTURED_LIGHT_NOT_SETUP);

    // Grab the resolution from DMD to set the model and image settings
    unsigned int rows;
    unsigned int columns;

    platform.GetRows(&rows);
    platform.GetColumns(&columns);

    // Save these values to the model and image settings
    this->pattern_rows_.Set(rows);
    this->pattern_columns_.Set(columns);

    this->debug_.Msg("Projector resolution  = " +
                     this->pattern_columns_.GetEntryValue() +
                     " by " +
                     this->pattern_rows_.GetEntryValue());

    this->projector_set_ = true;

    return ret;
}

/** \brief Default Constructor, initializes to 0 */
StructuredLight::Settings::Sequence::Count::Count(){
    this->value_ = 0;
}

/** \brief Custom Constructor, intializes count to set value
  * \param[in] count The count desired for the sequence
*/
StructuredLight::Settings::Sequence::Count::Count(const unsigned int &count){
    this->value_ = count;
}

/** \brief Sets the sequence count
  * \param[in] count The count to be set
*/
void StructuredLight::Settings::Sequence::Count::Set(const unsigned int &count){
    this->value_ = count;
}

/** \brief Gets the sequence count entry*/
unsigned int StructuredLight::Settings::Sequence::Count::Get() const{
    return this->value_;
}

std::string StructuredLight::Settings::Sequence::Count::GetEntryName() const{
    return "STRUCTURED_LIGHT_SETTING_SEQUENCE_COUNT";
}

std::string StructuredLight::Settings::Sequence::Count::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

std::string StructuredLight::Settings::Sequence::Count::GetEntryDefault() const{
    return dlp::Number::ToString(0);
}

/** \brief Set entry value
 * \param[in] value the string to be converted to an integer for entry value*/
void StructuredLight::Settings::Sequence::Count::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<Settings::Sequence::Count::Type>(value);
}

//StructuredLight::Settings::Pattern::Color::Color(){
//    this->value_ = dlp::Pattern::Color::WHITE;
//}

//StructuredLight::Settings::Pattern::Color::Color(const dlp::Pattern::Settings::Color::Type &color){
//    this->value_ = color;
//}

//void StructuredLight::Settings::Pattern::Color::Set(const dlp::Pattern::Settings::Color::Type &color){
//    this->value_ = color;
//}

//dlp::Pattern::Color StructuredLight::Settings::Pattern::Color::Get() const{
//    return this->value_;
//}

//std::string StructuredLight::Settings::Pattern::Color::ParameterGetName() const{
//    return "STRUCTURED_LIGHT_SETTING_IMAGE_COLOR";
//}

//std::string StructuredLight::Settings::Pattern::Color::ParameterGetValue() const{

//    switch(this->value_){
//    case dlp::Pattern::Color::RED:      return "RED";
//    case dlp::Pattern::Color::GREEN:    return "GREEN";
//    case dlp::Pattern::Color::BLUE:     return "BLUE";
//    case dlp::Pattern::Color::CYAN:     return "CYAN";
//    case dlp::Pattern::Color::YELLOW:   return "YELLOW";
//    case dlp::Pattern::Color::MAGENTA:  return "MAGENTA";
//    case dlp::Pattern::Color::WHITE:    return "WHITE";
//    case dlp::Pattern::Color::BLACK:    return "BLACK";
//    case dlp::Pattern::Color::RGB:      return "RGB";
//    case dlp::Pattern::Color::NONE:     return "NONE";
//    case dlp::Pattern::Color::INVALID:
//    default:                            return "INVALID";
//    }
//}

//std::string StructuredLight::Settings::Pattern::Color::ParameterGetDefault() const{
//    return dlp::Number::ToString("WHITE");
//}

//void StructuredLight::Settings::Pattern::Color::ParameterSetValue(std::string value){
//    this->value_ = dlp::String::ToNumber<Pattern::Color::Type>(value);
//}


/** \brief Default Constructor, initializes to 0*/
StructuredLight::Settings::Pattern::Size::Rows::Rows(){
    this->value_ = 0;
}

/** \brief Custom Constructor, intializes rows to set value
  * \param[in] rows Number of rows desired
*/
StructuredLight::Settings::Pattern::Size::Rows::Rows(const unsigned int &rows){
    this->value_ = rows;
}

/** \brief Sets number of rows to value
  * \param[in] rows Number of rows to set
*/
void StructuredLight::Settings::Pattern::Size::Rows::Set(const unsigned int &rows){
    this->value_ = rows;
}

/** \brief Gets number of rows in object*/
StructuredLight::Settings::Pattern::Size::Rows::Type StructuredLight::Settings::Pattern::Size::Rows::Get() const{
    return this->value_;
}

std::string StructuredLight::Settings::Pattern::Size::Rows::GetEntryName() const{
    return "STRUCTURED_LIGHT_SETTINGS_PATTERN_SIZE_ROWS";
}

std::string StructuredLight::Settings::Pattern::Size::Rows::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

std::string StructuredLight::Settings::Pattern::Size::Rows::GetEntryDefault() const{
    return dlp::Number::ToString(0);
}

void StructuredLight::Settings::Pattern::Size::Rows::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<Pattern::Size::Rows::Type>(value);
}

/** \brief Default Constructor, initializes to 0*/
StructuredLight::Settings::Pattern::Size::Columns::Columns(){
    this->value_ = 0;
}

/** \brief Custom Constructor, intializes columns to set value
  * \param[in] columns Number of columns desired
*/
StructuredLight::Settings::Pattern::Size::Columns::Columns(const unsigned int &columns){
    this->value_ = columns;
}

/** \brief Sets number of columns to value
  * \param[in] columns Number of columns to set
*/
void StructuredLight::Settings::Pattern::Size::Columns::Set(const unsigned int &columns){
    this->value_ = columns;
}

/** \brief Gets number of columns in object*/
unsigned int StructuredLight::Settings::Pattern::Size::Columns::Get() const{
    return this->value_;
}

std::string StructuredLight::Settings::Pattern::Size::Columns::GetEntryName() const{
    return "STRUCTURED_LIGHT_SETTINGS_PATTERN_SIZE_COLUMNS";
}

std::string StructuredLight::Settings::Pattern::Size::Columns::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

std::string StructuredLight::Settings::Pattern::Size::Columns::GetEntryDefault() const{
    return dlp::Number::ToString(0);
}

void StructuredLight::Settings::Pattern::Size::Columns::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<Pattern::Size::Columns::Type>(value);
}

/** \brief Default Constructor, initializes to horizontal orientation*/
StructuredLight::Settings::Pattern::Orientation::Orientation(){
    this->value_ = Type::HORIZONTAL;
}

/** \brief Custom Constructor, intializes orientation to set value
  * \param[in] orientation Orientation desired from \ref dlp::StructuredLight::Settings::Pattern::Orientation::Type
*/
StructuredLight::Settings::Pattern::Orientation::Orientation(const Type &orientation){
    this->value_ = orientation;
}

/** \brief Sets orientation
  * \param[in] orientation Orientation desired from \ref dlp::StructuredLight::Settings::Pattern::Orientation::Type
*/
void StructuredLight::Settings::Pattern::Orientation::Set(const Type &orientation){
    this->value_ = orientation;
}

/** \brief Gets orientation of object*/
StructuredLight::Settings::Pattern::Orientation::Type StructuredLight::Settings::Pattern::Orientation::Get() const{
    return this->value_;
}

std::string StructuredLight::Settings::Pattern::Orientation::GetEntryName() const{
    return "STRUCTURED_LIGHT_SETTINGS_IMAGE_ORIENTATION";
}

std::string StructuredLight::Settings::Pattern::Orientation::GetEntryDefault() const{
    return "HORIZONTAL";
}

std::string StructuredLight::Settings::Pattern::Orientation::GetEntryValue() const{
    switch (this->value_){
    case Type::HORIZONTAL:	return "HORIZONTAL";
    case Type::VERTICAL:	return "VERTICAL";
    default:				return "INVALID";
    }
}

void StructuredLight::Settings::Pattern::Orientation::SetEntryValue(std::string value){
    if (value.compare("HORIZONTAL") == 0){
        this->value_ = Type::HORIZONTAL;
    }
    else if (value.compare("VERTICAL") == 0){
        this->value_ = Type::VERTICAL;
    }
    else{
        this->value_ = Type::INVALID;
    }
}

/** \brief Default Constructor, initializes object to include inverted gray code patterns for resolution increase*/
StructuredLight::Settings::Sequence::IncludeInverted::IncludeInverted(){
    this->value_ = true;
}

/** \brief Custom Constructor, intializes inverted pattern inclusion to set value
  * \param[in] include_inverted boolean parameter, true to include inverted patterns, false to exclude
*/
StructuredLight::Settings::Sequence::IncludeInverted::IncludeInverted(bool include_inverted){
    this->value_ = include_inverted;
}

/** \brief Sets inverted pattern inclusion
  * \param[in] include_inverted boolean parameter, true to include inverted patterns, false to exclude
*/
void StructuredLight::Settings::Sequence::IncludeInverted::Set(bool include_inverted){
    this->value_ = include_inverted;
}

/** \brief Gets the value for IncludeInverted entry*/
bool StructuredLight::Settings::Sequence::IncludeInverted::Get() const{
    return this->value_;
}

std::string StructuredLight::Settings::Sequence::IncludeInverted::GetEntryName() const{
    return "STRUCTURED_LIGHT_SETTINGS_IMAGE_INCLUDE_INVERTED";
}

std::string StructuredLight::Settings::Sequence::IncludeInverted::GetEntryDefault() const{
    return "false";
}

std::string StructuredLight::Settings::Sequence::IncludeInverted::GetEntryValue() const{
    return Number::ToString(this->value_);
}

/** \brief Set entry value
 * \param[in] value the string to be converted to an integer for entry value*/
void StructuredLight::Settings::Sequence::IncludeInverted::SetEntryValue(std::string value) {
    this->value_ = String::ToNumber<bool>(value);
}
}
