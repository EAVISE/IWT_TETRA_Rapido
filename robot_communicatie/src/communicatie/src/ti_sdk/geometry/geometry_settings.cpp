/** \file   geometry_settings.cpp
 *  \brief  Contains dlp::Parameters::Entry objects for geometry module
 */

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/capture/capture.hpp>
#include <common/pattern/pattern.hpp>
#include <common/point_cloud.hpp>
#include <calibration/calibration.hpp>
#include <geometry/geometry.hpp>

#include <vector>
#include <string>
#include <math.h>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \brief Construcs with a scale of 1.0*/
Geometry::Settings::ScaleXYZ::ScaleXYZ(){
    this->value_ = 1.0;
}

/** \brief Constructs with the given percent (eg .5 = 50%)
 *  \param[in] percent Float representing the precent to scale*/
Geometry::Settings::ScaleXYZ::ScaleXYZ(const double &percent){
    this->value_ = percent;
}

/** \brief Sets the percent of \ref dlp::Geometry::Settings::ScaleXYZ
 *  \param[in] percent The percent to set to*/
void Geometry::Settings::ScaleXYZ::Set(const double &percent){
    this->value_ = percent;
}

/** \brief Returns the XYZ scaling*/
double Geometry::Settings::ScaleXYZ::Get()const{
    return this->value_;
}

/** \brief Returns the entry name "GEOMETRY_SETTINGS_SCALE_XYZ" as a string*/
std::string Geometry::Settings::ScaleXYZ::GetEntryName() const{
    return "GEOMETRY_SETTINGS_SCALE_XYZ";
}

/** \brief Returns the scale value as a string */
std::string Geometry::Settings::ScaleXYZ::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}

/** \brief Returns the default value as a string "1.0"*/
std::string Geometry::Settings::ScaleXYZ::GetEntryDefault() const{
    return "1.0";
}

/** \brief Sets the XYZ scale from a string
 *  \param[in] value String of a value to be converted to a float*/
void Geometry::Settings::ScaleXYZ::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<double>(value);
}

/** \brief Default constructs the maximum to be 0*/
Geometry::Settings::OriginPointDistance::Maximum::Maximum(){
    this->value_ = 0;
}

/** \brief Constructs the maximum based on a given double
 *  \param[in] distance The distance to set as maximum*/
Geometry::Settings::OriginPointDistance::Maximum::Maximum(const double &distance){
    this->value_ = distance;
}

/** \brief Sets the distance based on a given double
 *  \param[in] distance The distance to set as maximum*/
void Geometry::Settings::OriginPointDistance::Maximum::Set(const double &distance){
    this->value_ = distance;
}

/** \brief Returns the maximum distance*/
double Geometry::Settings::OriginPointDistance::Maximum::Get()const{
    return this->value_;
}

/** \brief Returns the entry string "GEOMETRY_SETTINGS_ORIGIN_POINT_DISTANCE_MAXIMUM"*/
std::string Geometry::Settings::OriginPointDistance::Maximum::GetEntryName()    const{
    return "GEOMETRY_SETTINGS_ORIGIN_POINT_DISTANCE_MAXIMUM";
}

/** \brief Returns the Maximum value as a string*/
std::string Geometry::Settings::OriginPointDistance::Maximum::GetEntryValue()   const{
    return dlp::Number::ToString(this->value_);
}

/** \brief Returns the default value of "0" as a string*/
std::string Geometry::Settings::OriginPointDistance::Maximum::GetEntryDefault() const{
    return "0";
}

/** \brief Sets the Maximum from a string entry value
 *  \param[in] value String that is converted to a double to set the Maximum*/
void Geometry::Settings::OriginPointDistance::Maximum::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<double>(value);
}

/** \brief Default constructs the maximum to be 0*/
Geometry::Settings::OriginPointDistance::Minimum::Minimum(){
    this->value_ = 0;
}

/** \brief Sets the distance based on a given double
 *  \param[in] distance The distance to set as minimum*/
Geometry::Settings::OriginPointDistance::Minimum::Minimum(const double &distance){
    this->value_ = distance;
}

/** \brief Sets the distance based on a given double
 *  \param[in] distance The distance to setas mimimum*/
void Geometry::Settings::OriginPointDistance::Minimum::Set(const double &distance){
    this->value_ = distance;
}

/** \brief Returns the minimum distance*/
double Geometry::Settings::OriginPointDistance::Minimum::Get()const{
    return this->value_;
}

/** \brief Returns the entry string "GEOMETRY_SETTINGS_ORIGIN_POINT_DISTANCE_MINIMUM"*/
std::string Geometry::Settings::OriginPointDistance::Minimum::GetEntryName()    const{
    return "GEOMETRY_SETTINGS_ORIGIN_POINT_DISTANCE_MINIMUM";
}

/** \brief Returns the Minimum value as a string*/
std::string Geometry::Settings::OriginPointDistance::Minimum::GetEntryValue()   const{
    return dlp::Number::ToString(this->value_);
}

/** \brief Returns the default value of "0" as a string*/
std::string Geometry::Settings::OriginPointDistance::Minimum::GetEntryDefault() const{
    return "0";
}

/** \brief Sets the Maximum from a string entry value
 *  \param[in] value String that is converted to a double to set the Minimum*/
void Geometry::Settings::OriginPointDistance::Minimum::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<double>(value);
}

Geometry::Settings::FilterViewpointRays::FilterViewpointRays(){
    this->value_ = true;
}

Geometry::Settings::FilterViewpointRays::FilterViewpointRays(const bool &enable){
    this->value_ = enable;
}

void Geometry::Settings::FilterViewpointRays::Set(const bool &enable){
    this->value_ = enable;
}

bool Geometry::Settings::FilterViewpointRays::Get()const{
    return this->value_;
}

std::string Geometry::Settings::FilterViewpointRays::GetEntryName() const{
    return "GEOMETRY_SETTINGS_FILTER_VIEWPOINT_RAYS";
}

std::string Geometry::Settings::FilterViewpointRays::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}
std::string Geometry::Settings::FilterViewpointRays::GetEntryDefault() const{
    return "1";
}
void Geometry::Settings::FilterViewpointRays::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<bool>(value);
}


Geometry::Settings::FilterViewpointRays::MaximumPercentError::MaximumPercentError(){
    this->value_ = 1.0;
}

Geometry::Settings::FilterViewpointRays::MaximumPercentError::MaximumPercentError(const double &percent){
    this->value_ = percent;
}

void Geometry::Settings::FilterViewpointRays::MaximumPercentError::Set(const double &percent){
    this->value_ = percent;
}

double Geometry::Settings::FilterViewpointRays::MaximumPercentError::Get()const{
    return this->value_;
}

std::string Geometry::Settings::FilterViewpointRays::MaximumPercentError::GetEntryName() const{
    return "GEOMETRY_SETTINGS_FILTER_PERCENT_ERROR_MAXIMUM";
}

std::string Geometry::Settings::FilterViewpointRays::MaximumPercentError::GetEntryValue() const{
    return dlp::Number::ToString(this->value_);
}
std::string Geometry::Settings::FilterViewpointRays::MaximumPercentError::GetEntryDefault() const{
    return "1.0";
}
void Geometry::Settings::FilterViewpointRays::MaximumPercentError::SetEntryValue(std::string value){
    this->value_ = dlp::String::ToNumber<double>(value);
}

}
