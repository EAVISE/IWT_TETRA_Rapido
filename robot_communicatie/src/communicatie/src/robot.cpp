#include "ros/ros.h"
#include "std_msgs/String.h"
#include "communicatie/Point_data.h"
#include "communicatie/Robot_coord.h"
#include <sensor_msgs/PointCloud2.h>
#include <iostream>
#include <sstream>
#include "../include/communicatie/visie.hpp"

/*
 *
 * Listens to commados, given by the vision side.
 *
 */
class Listener_commando
{
private:
    bool move;
    bool finished;
    bool calib;

public:
    Listener_commando();
    void callback(const std_msgs::String::ConstPtr msg);
    bool getMove();
    bool getFinished();
    bool getCalib();
    void setMove(bool a);
    void setFinished(bool a);
    void setCalib(bool a);
};
/*
 *
 * Constructor
 *
 */
Listener_commando::Listener_commando()
{
    move = false;
    finished = false;
    calib = false;
}

/*
 *
 * IN: msg: new message
 *
 * When a string_message from the vision node arrives, this function is called.
 * It will save the data from the message.
 *
 */

void Listener_commando::callback(const std_msgs::String::ConstPtr msg)
{
    //std::cout<<"In de callback robot"<< msg->data<<std::endl;
    if(msg->data == "finished")
        setFinished(true);
    else if(msg->data == "move" || msg->data == "move_calib")
    {
        setMove(true);
        if(msg->data == "move_calib")
            setCalib(true);
    }
}

bool Listener_commando::getMove()
{
    return move;
}

bool Listener_commando::getFinished()
{
    return finished;
}

bool Listener_commando::getCalib()
{
    return calib;
}

void Listener_commando::setMove(bool a){
    move = a;
}

void Listener_commando::setFinished(bool a){
    finished = a;
}

void Listener_commando::setCalib(bool a){
    calib = a;
}

/*
 *
 * Class to save data received from the vision node
 *
 */

class Listener_robot_data
{
private:
    int id;
    sensor_msgs::PointCloud2 scene;
    cv::Point3d translatie;
    cv::Point3d rotatie;

public:
    Listener_robot_data();
    void callback(const communicatie::Point_data::ConstPtr msg);
    sensor_msgs::PointCloud2  getScene();
    cv::Point3d getTrans();
    cv::Point3d getRot();
    int getID();
    void setScene(sensor_msgs::PointCloud2);
    void setTrans(cv::Point3d);
    void setRot(cv::Point3d);
    void setID(int);
};
/*
 *
 * Constructor
 *
 */
Listener_robot_data::Listener_robot_data()
{
    sensor_msgs::PointCloud2 p;
    scene = p;
    translatie = cv::Point3d(0,0,0);
    rotatie = cv::Point3d(0,0,0);
    id = -1;
}
/*
 *
 * IN: msg: new message
 *
 * When a point_data_message from the vision node arrives, this function is called.
 * It will save the data from the message.
 *
 */
void Listener_robot_data::callback(const communicatie::Point_data::ConstPtr msg)
{
    setScene(msg->pointcloud);
    cv::Point3d trans = cv::Point3d(msg->x, msg->y, msg->z);
    setTrans(trans);
    cv::Point3d rot = cv::Point3d(msg->rx, msg->ry, msg->rz);
    setRot(rot);
    setID(msg->point_id);
}

sensor_msgs::PointCloud2  Listener_robot_data::getScene()
{
    return scene;
}

cv::Point3d Listener_robot_data::getTrans()
{
    return translatie;
}

cv::Point3d Listener_robot_data::getRot()
{
    return rotatie;
}

int Listener_robot_data::getID()
{
    return id;
}

void Listener_robot_data::setScene(sensor_msgs::PointCloud2 a)
{
    scene = a;
}

void Listener_robot_data::setTrans(cv::Point3d a)
{
    translatie = a;
}

void Listener_robot_data::setRot(cv::Point3d a)
{
    rotatie = a;
}

void Listener_robot_data::setID(int a)
{
    id = a;
}

void mySigintHandler(int sig)
{
    std::cout<<"Shutting down robot node"<<std::endl;
    ros::shutdown();
    exit(-1);
}

/*
 *
 * Main function of the robot node.
 * From here movement of the robot is controlled.
 *
 */
int main(int argc, char **argv)
{
    ros::init(argc,argv,"robot_controller");
    ros::NodeHandle listener_node, talker_node, data_listener_node, data_talker_node;

    signal(SIGINT, mySigintHandler);

    ros::Publisher robot_pub = talker_node.advertise<std_msgs::String>("robot_commando", 1000);
    ros::Publisher robot_data_pub = data_talker_node.advertise<communicatie::Robot_coord>("robot_data", 1000);

    Listener_commando listener;
    ros::Subscriber robot_sub = listener_node.subscribe("visie_commando", 1000, &Listener_commando::callback, &listener);

    Listener_robot_data data_listener;
    ros::Subscriber robot_data = data_listener_node.subscribe("visie_data", 1000, &Listener_robot_data::callback, &data_listener);

    ros::Rate poll_rate(100);
    while(robot_pub.getNumSubscribers() == 0)
        poll_rate.sleep();

    std_msgs::String msg_start;
    std::stringstream ss_start;
    ss_start << "start";
    msg_start.data = ss_start.str();

    std_msgs::String msg_moved;
    std::stringstream ss_moved;
    ss_moved << "moved";
    msg_moved.data = ss_moved.str();

    int teller=0;

    bool vlag = false;
    while(!vlag)
    {
        while(!listener.getFinished())
        {
            ///De robot laat de visiekant weten dat we kunnen starten.
            std::cout<<"Robot: Start vision"<<std::endl;
            robot_pub.publish(msg_start);
            ros::spinOnce();
            ///Robot wacht op commando op te bewegen (uit het zichtveld van de camera)
            while(!listener.getMove())
            {
                if(listener.getFinished())
                    break;

                ros::spinOnce();
            }
            if(listener.getFinished())
                break;

            listener.setMove(false);
            if(listener.getCalib())
            {
                ///Visie laat weten dat we robot-visie gaan calibreren.
                /// Verplaats de robot(met calibratieplaatje) naar een plek binnen het gezichtsveld van de camera
                /// Robot coordinaten terugsturen naar Visie (zelf in te vullen, deze zijn dummy values)
                communicatie::Robot_coord robot_points;
                robot_points.x = 100;
                robot_points.y = 50;
                robot_points.z = 10;
                robot_points.rx = 5;
                robot_points.ry = 20;
                robot_points.rz = 30;
                robot_points.seq_number = 1;
                robot_data_pub.publish(robot_points);
            }
            else
            {    /// Visie laat weten dat het een scan van de bak/scene wil nemen
                /// Code aanroepen om de robot te verplaatsen naar een plek buiten het gezichtsveld van de camera. (vooraf te bepalen?)
            }

            std::cout<<"Moving robot"<<std::endl;
            ///De robot laat aan de visie weten dat hij verplaatst is
            robot_pub.publish(msg_moved);
            ros::spinOnce();
        }

        while(data_listener.getID() < 0)
        {
            ros::spinOnce();
        }

        std::cout<<"Received scene + pickposes, iteration "<<teller<<std::endl;
        std::cout<<data_listener.getTrans()<<data_listener.getRot()<<std::endl;

        ///de doorgestuurde info zit in het data_listener object. Via de getters kan je er nu aan.

        /// Code voor het picken van een object.

        listener.setFinished(false);
        ///ID terug op -1 zetten zodat er de volgende keer weer gewacht wordt tot er nieuwe data in data_listener zit.
        data_listener.setID(-1);
        teller++;
        if(teller>5)
        {
            /// Met deze laatste "start" laat de robot aan de visie weten dat het picken gelukt is, en er nog een pose mag doorgegeven worden"
            robot_pub.publish(msg_start);
            ros::spinOnce();
            break;
        }
    }

    std::cout<<"Shutting down Robot Node"<<std::endl;
    ros::shutdown();
}

