#include <vector>
#include <string>
#include <sstream>
#include <iostream>

#include <opencv2/opencv.hpp>

#include <pcl/io/ensenso_grabber.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>

using namespace pcl;
using namespace std;

///Ensenso:
void get_en_image(cv::Mat &cloud);
void getCalibRGBImage(int i);
void getCalibImage(cv::Mat &cloud, cv::Mat &image);
cv::Point3d getCenterPointEN(cv::Mat cloud, cv::Mat image, int boardw, int boardh);

//cv::Mat calculateTransMat(cv::Mat origin, cv::Mat dest);
//bool calibrate_en_r(std::vector<cv::Point3d> robot, std::vector<cv::Point3d> sensor);
//cv::Mat convert(cv::Mat origin);
