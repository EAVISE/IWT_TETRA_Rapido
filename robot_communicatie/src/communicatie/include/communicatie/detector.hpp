#include "visie.hpp"

#include <vector>
#include "detector/surface_matching.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include "detector/ppf_helpers.hpp"
//#include "detector/utility.hpp"
#include <opencv2/core/utility.hpp>
#include <opencv2/core/core.hpp>
//#include <pcl/features/integral_image_normal.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_types.h>

struct Pickpoint
{
    pcl::PointNormal point;
    std::vector<double> quaternion;\
    cv::Mat model_pc;
} ;


class Detector
{
public:
    virtual void trainModel(std::string path) =0;
    virtual Pickpoint getPickPoint(cv::Mat pc) =0;
};

class PPFDetector: public Detector
{
private:
    //Voor training van de detector:
    double RelativeSamplingStep;
    double RelativeDistanceStep;
    double NumAngles;
    cv::ppf_match_3d::PPF3DDetector *detector;

    //Voor matches te vinden:
    double positionThreshold;
    double rotationThreshold;
    double useWeightedClustering;
    double relativeSceneSampleStep;
    double relativeSceneDistance;

public:
    PPFDetector();
    PPFDetector(double RDS, double RSS, double NA,  double PT, double RT, double UWC, double RSD, double RSSS);
    void trainModel(std::string path);
    Pickpoint getPickPoint(cv::Mat pc);
};
