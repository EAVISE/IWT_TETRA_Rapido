/*! \file   camera.hpp
 *  \brief  Contains definitions for the DLP SDK camera base class
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#ifndef DLP_SDK_CAMERA_HPP
#define DLP_SDK_CAMERA_HPP

#include <common/returncode.hpp>
#include <common/debug.hpp>
#include <common/other.hpp>
#include <common/image/image.hpp>
#include <common/capture/capture.hpp>
#include <common/parameters.hpp>
#include <common/module.hpp>

#include <iostream>

#define CAMERA_NOT_CONNECTED        "CAMERA_NOT_CONNECTED"
#define CAMERA_NOT_SETUP            "CAMERA_NOT_SETUP"
#define CAMERA_NOT_STARTED          "CAMERA_NOT_STARTED"
#define CAMERA_NOT_STOPPED          "CAMERA_NOT_STOPPED"
#define CAMERA_NOT_DISCONNECTED     "CAMERA_NOT_DISCONNECTED"
#define CAMERA_RESOLUTION_INVALID   "CAMERA_RESOLUTION_INVALID"
#define CAMERA_TRIGGER_INVALID      "CAMERA_TRIGGER_INVALID"
#define CAMERA_FRAME_GRAB_FAILED    "CAMERA_FRAME_GRAB_FAILED"
#define CAMERA_FRAME_RATE_INVALID   "CAMERA_FRAME_RATE_INVALID"
#define CAMERA_EXPOSURE_INVALID     "CAMERA_EXPOSURE_INVALID"
// Add more #defines for camera errors

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{
/** \defgroup   Camera
 *  \brief      Base class definition for interfacing with cameras
 *
 *  @{
 */

/** \class      Camera
 *  \brief
 */
class Camera: public dlp::Module{
public:


    /** \class      Settings
     *  \brief      Contains \ref dlp::Parameters::Entry objects needed for \ref Camera::Setup().
     */
	class Settings {
	public:

        /** \class  Trigger
         *  \brief  Determines if the camera triggers (commands) pattern projection or if pattern projection triggers the camera
         */
        class Trigger: public dlp::Parameters::Entry{
			friend class Camera;
		public:
            enum class Type{
                SOFTWARE_SLAVE,     /**< Camera capture controlled via software */
                HARDWARE_SLAVE,     /**< Camera capture controlled via projector pattern trigger output */
                HARDWARE_MASTER,    /**< Camera capture controls projector pattern display */
				INVALID
			};

			Trigger();
            Trigger(const Type &Trigger);
            void Set(const Type &Trigger);
            Type Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);

		private:
            Type value_;
		};
    };


    // Define by subclass
    virtual ReturnCode Connect(int camera_id) = 0;
    virtual ReturnCode Disconnect() = 0;

    virtual ReturnCode Start() = 0;
    virtual ReturnCode Stop() = 0;

    virtual ReturnCode GetFrame(Image* ret_frame) = 0;
    virtual ReturnCode GetCaptureSequence(const unsigned int &arg_number_captures,
                                         Capture::Sequence* ret_capture_sequence) = 0;

    // Used for scanner validation
    virtual bool isConnected() const = 0;
    virtual bool isStarted() const = 0;

    virtual ReturnCode GetID(      unsigned int* ret_id) const = 0;
    virtual ReturnCode GetRows(    unsigned int* ret_rows) const = 0;
    virtual ReturnCode GetColumns( unsigned int* ret_columns) const = 0;

    virtual ReturnCode GetFrameRate( float* ret_framerate) const = 0;
    virtual ReturnCode GetExposure(  float* ret_exposure) const = 0;

    virtual ReturnCode GetTrigger(Settings::Trigger::Type *ret_trigger) const = 0;
};
/** @}*/

}
#endif //DLP_SDK_CAMERA_HPP

