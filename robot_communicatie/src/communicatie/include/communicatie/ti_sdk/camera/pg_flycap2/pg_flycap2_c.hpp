/** \file      pg_flycap2_c.hpp
 *  \brief     Simple wrapper class for Point Grey Research USB Flea cameras
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */


#ifndef DLP_SDK_PG_FLYCAP2_HPP
#define DLP_SDK_PG_FLYCAP2_HPP


#include <common/returncode.hpp>
#include <common/capture/capture.hpp>
#include <camera/camera.hpp>

// Include Flycap2 C headers
#include <flycapture/C/FlyCapture2_C.h>
#include <flycapture/C/FlyCapture2Defs_C.h>


#define PG_FLYCAP_C_NO_CAMERAS_DETECTED     "PG_FLYCAP_C_NO_CAMERAS_DETECTED"
#define PG_FLYCAP_C_NO_CONTEXT_CREATED      "PG_FLYCAP_C_NO_CONTEXT_CREATED"
#define PG_FLYCAP_C_INVALID_CAMERA_ID       "PG_FLYCAP_C_INVALID_CAMERA_ID"
#define PG_FLYCAP_C_STROBE_INVALID          "PG_FLYCAP_C_STROBE_INVALID"
#define PG_FLYCAP_C_STROBE_POLARITY_INVALID "PG_FLYCAP_C_STROBE_POLARITY_INVALID"
#define PG_FLYCAP_C_NULL_POINTER            "PG_FLYCAP_C_NULL_POINTER"

namespace dlp {

/** \class PG_FlyCap2_C
 *  \brief Contains classes for Point Grey Research camera configuration and methods to connect,configure
 *          and capture frames using Point Grey Reasearch Flea USB Cameras.
 *
 *  The PG_FlyCap2_C class contains several classes used for camera configuration including: Rows, Columns,
 *  Pixel Format, Video mode, Strobe settings.
 *
 *  This class also includes the wrapper functions for Point Grey Research Flea USB camera Connect/Disconnect, Start/Stop,
 *  SetUp and Capture APIs.
 *
 */

class PG_FlyCap2_C : public Camera
{
public:


    /** \ingroup   Camera
    *  \brief      Contains classes for camera configuration
    *  @{
    */

    /** \class      Settings
     *  \brief      Contains all \ref dlp::Parameters::Entry objects needed for
     *              \ref PG_FlyCap2_C::SetUp().
     */
    class Settings{
    public:

        /** \class  Rows
         *  \brief  %Number of active rows on camera sensor
         */
        class Rows : public dlp::Parameters::Entry{

        public:
            Rows();
            Rows(const unsigned int &rows);
            void Set(const unsigned int &rows);
            unsigned int Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            unsigned int value_;
        };

        /** \class  Columns
         *  \brief  %Number of active columns on camera sensor
         */
        class Columns : public dlp::Parameters::Entry{

        public:
            Columns();
            Columns(const unsigned int &columns);
            void Set(const unsigned int &columns);
            unsigned int Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            unsigned int value_;
        };

        /** \class  OffsetX
         *  \brief  Horizontal offset in pixels to change first active column on the camera sensor
         */
        class OffsetX : public dlp::Parameters::Entry{

        public:
            OffsetX();
            OffsetX(const unsigned int &offsetX);
            void Set(const unsigned int &offsetX);
            unsigned int Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            unsigned int value_;
        };

        /** \class  OffsetY
         *  \brief  Vertical offset in pixels to change first active row on the camera sensor
         */
        class OffsetY : public dlp::Parameters::Entry{

        public:
            OffsetY();
            OffsetY(const unsigned int &offsetY);
            void Set(const unsigned int &offsetY);
            unsigned int Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            unsigned int value_;
        };

        /** \class  ShutterSpeed
         *  \brief  Duration of time in milliseconds that the camera sensor is exposed
         */
        class ShutterSpeed : public dlp::Parameters::Entry{

        public:
            ShutterSpeed();
            ShutterSpeed(const float &shutter_speed);
            void Set(const float &shutter_speed);
            float Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            float value_;
        };

        /** \class  FrameRate
         *  \brief  Number of captured frames per second
         */
        class FrameRate : public dlp::Parameters::Entry{

        public:
            FrameRate();
            FrameRate(const float &frame_rate);
            void Set(const float &frame_rate);
            float Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            float value_;
        };

        /** \class  Exposure
         *  \brief  Sets auto exposure value
         */
        class Exposure : public dlp::Parameters::Entry{

        public:
            Exposure();
            Exposure(const float &exposure);
            void Set(const float &exposure);
            float Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            float value_;
        };

        /** \class  Gain
         *  \brief  Sets gain value
         */
        class Gain : public dlp::Parameters::Entry{
        public:
            Gain();
            Gain(const float &gain);
            void Set(const float &gain);
            float Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            float value_;
        };

        /** \class Sharpness
         *  \brief Sets the Sharpness value
         *  Values < 1000 blurs the image
         *  Values > 1000 sharpens the image
         */
        class Sharpness : public dlp::Parameters::Entry{

        public:
            Sharpness();
            Sharpness(const unsigned int &sharpness);
            void Set(const unsigned int &sharpness);
            unsigned int Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            unsigned int value_;
        };

        /** \class  VideoMode
         *  \brief  Determines if full camera resolution or half resolution is utilized
         */
        class VideoMode : public dlp::Parameters::Entry{

        public:
            enum class Options{
                MODE0_FULL_RESOLUTION,
                MODE4_HALF_RESOLUTION,
                INVALID
            };

            VideoMode();
            VideoMode(const Options &video_mode);
            void Set(const Options &video_mode);
            Options Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);

        private:
            Options value_;
        };

        /** \class  PixelFormat
         *  \brief  Camera output pixel format
         */
        class PixelFormat : public dlp::Parameters::Entry{

        public:
            enum class Options{
                PIXEL_FORMAT_RAW8,      /*!< 8 bit raw data output of camera sensor         */
                PIXEL_FORMAT_MONO8,     /*!< 8 bit mono(grayscale) output of camera sensor  */
                PIXEL_FORMAT_RGB8,      /*!< R=G=B=8 bits RGB color output of camera sensor */
                INVALID
            };

            PixelFormat();
            PixelFormat(const Options &pixel_format);
            void Set(const Options &pixel_format);
            Options Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);

        private:
            Options value_;
        };

        /** \class  ImageFormat
         *  \brief  Camera output pixel format
         */
        class ImageFormat : public dlp::Parameters::Entry{

        public:
            enum class Options{
                IMAGE_FORMAT_MONO8,     /*!< 8 bit mono(grayscale) output of camera sensor  */
                IMAGE_FORMAT_RGB8,      /*!< R=G=B=8 bits RGB color output of camera sensor */
                INVALID
            };

            ImageFormat();
            ImageFormat(const Options &pixel_format);
            void Set(const Options &pixel_format);
            Options Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);

        private:
            Options value_;
        };

        /** \class  TriggerPolarity
         *  \brief  Output trigger polarity for the camera
         */
        class TriggerPolarity : public dlp::Parameters::Entry{

        public:
            enum class Options{
                FALLING_EDGE,
                RISING_EDGE,
                INVALID
            };
            TriggerPolarity();
            TriggerPolarity(const Options &trigger_polarity);
            void Set(const Options &trigger_polarity);
            Options Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);

        private:
            Options value_;
        };

        /** \class  TriggerEnable
         *  \brief  Master control to enable/disable the camera's input trigger
         *  \note   Enabling the input trigger reduces maximum frame rate
         */
        class TriggerEnable : public dlp::Parameters::Entry{

        public:
            TriggerEnable();
            TriggerEnable(const bool &trigger_enable);
            void Set(const bool &trigger_enable);
            bool Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);

        private:
            bool value_;
        };

        /** \class  StrobeSource
         *  \brief  Specifies GPIO pin to use
         */
        class StrobeSource: public dlp::Parameters::Entry{

        public:
            enum class Options{
                GPIO_1,
                GPIO_2,
                GPIO_3,
                INVALID
            };
            StrobeSource();
            StrobeSource(const Options &strobe_source);
            void Set(const Options &strobe_source);
            Options Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);

        private:
            Options value_;

        };

        /** \class  StrobeEnable
         *  \brief  Master control to enable/disable the GPIO to send output trigger/strobe
         */
        class StrobeEnable: public dlp::Parameters::Entry{

        public:

            StrobeEnable();
            StrobeEnable(const bool &strobe_enable);
            void Set(const bool &strobe_enable);
            bool Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);

        private:
            bool value_;
        };

        /** \class  StrobePolarity
         *  \brief  Specifies the output polarity fo the strobe/trigger pulse
         */
        class StrobePolarity : public dlp::Parameters::Entry{

        public:
            enum class Options{
                LOW,
                HIGH,
                INVALID
            };
            StrobePolarity();
            StrobePolarity(const Options &strobe_polarity);
            void Set(const Options &strobe_polarity);
            Options Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);

        private:
            Options value_;
        };
    };

    PG_FlyCap2_C();

    ReturnCode DetectNumOfCameras(unsigned int* num_cameras);

    // Define pure virtual functions
    ReturnCode Connect(int camera_id);
    ReturnCode Disconnect();
    ReturnCode Setup(const dlp::Parameters &settings);
    ReturnCode GetSetup(Parameters *settings)const;
    ReturnCode Start();
    ReturnCode Stop();
    ReturnCode GetFrame(Image* ret_frame);
    ReturnCode GetCaptureSequence(const unsigned int &arg_number_captures,
                                  Capture::Sequence* ret_capture_sequence);

    bool isConnected() const;
    bool isStarted() const;

    ReturnCode GetID(unsigned int* ret_id) const;
    ReturnCode GetRows(unsigned int* ret_rows) const;
    ReturnCode GetColumns(unsigned int* ret_columns) const;

    ReturnCode GetFrameRate(float* ret_framerate) const;
    ReturnCode GetExposure(float* ret_exposure) const;

    ReturnCode GetTrigger( Camera::Settings::Trigger::Type *ret_trigger) const;

protected:

    //Setting Objects
    Settings::Rows              rows_;
    Settings::Columns           columns_;
    Settings::OffsetX           offsetX_;
    Settings::OffsetY           offsetY_;
    Settings::ShutterSpeed      shutter_speed_;
    Settings::Exposure          exposure_;
    Settings::Gain              gain_;
    Settings::FrameRate         frame_rate_;
    Settings::Sharpness         sharpness_;
    Settings::VideoMode         video_mode_;
    Settings::PixelFormat       pixel_format_;
    Settings::ImageFormat       image_format_;
    Settings::TriggerPolarity   trigger_polarity_;
    Camera::Settings::Trigger   trigger_type_;
    Settings::TriggerEnable     trigger_enable_;
    Settings::StrobeEnable      strobe_enable_;
    Settings::StrobePolarity    strobe_polarity_;
    Settings::StrobeSource      strobe_source_;

    // Members to document whether camera is connected or started
    bool is_connected_;
    bool is_started_;

private:

    fc2Error                error_;
    fc2CameraInfo           camInfo_;
    fc2Context              context_;
    fc2Format7ImageSettings init_settings_;
    fc2Property             init_property_;
    fc2TriggerMode          init_trigger_settings_;
    fc2Image                grabbed_image_;
    fc2StrobeControl        strobe_settings_;
    int                     cam_id_;

};
}

#endif // DLP_SDK_PG_FLYCAP2_HPP


