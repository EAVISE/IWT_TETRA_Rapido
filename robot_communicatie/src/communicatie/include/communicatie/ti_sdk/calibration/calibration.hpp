/** \file   calibration.hpp
 *  \brief  Contains definitions for the DLP SDK calibration classes
 *  \copyright 2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 *
 *  The calibration.hpp file defines the camera and projector calibration
 *  models for use in 3D metrology applications using DLP technology. It
 *  also defines a calibration data container which can store and load
 *  the data to avoid recalibrating a system upon every use.
 *
 */

#ifndef DLP_SDK_CALIBRATION_HPP
#define DLP_SDK_CALIBRATION_HPP

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/capture/capture.hpp>
#include <common/pattern/pattern.hpp>
#include <common/parameters.hpp>
#include <common/point_cloud.hpp>

#include <camera/camera.hpp>
#include <dlp_platforms/dlp_platform.hpp>

#include <opencv2/opencv.hpp>

#include <vector>
#include <string>

#define DLP_CV_INTRINSIC_SETUP      3, 3, CV_64FC1
#define DLP_CV_EXTRINSIC_SETUP      2, 3, CV_64FC1
#define DLP_CV_DISTORTION_SETUP     5, 1, CV_64FC1
#define DLP_CV_HOMOGRAPHY_SETUP     3, 3, CV_64FC1


#define CALIBRATION_DATA_NULL_POINTER_INTRINSIC             "CALIBRATION_DATA_NULL_POINTER_INTRINSIC"
#define CALIBRATION_DATA_NULL_POINTER_EXTRINSIC             "CALIBRATION_DATA_NULL_POINTER_EXTRINSIC"
#define CALIBRATION_DATA_NULL_POINTER_DISTORTION            "CALIBRATION_DATA_NULL_POINTER_DISTORTION"
#define CALIBRATION_DATA_NULL_POINTER_REPROJECTION_ERROR    "CALIBRATION_DATA_NULL_POINTER_REPROJECTION_ERROR"

#define CALIBRATION_DATA_NULL_POINTER_COLUMNS               "CALIBRATION_DATA_NULL_POINTER_COLUMNS"
#define CALIBRATION_DATA_NULL_POINTER_ROWS                  "CALIBRATION_DATA_NULL_POINTER_ROWS"
#define CALIBRATION_DATA_NOT_COMPLETE                       "CALIBRATION_DATA_NOT_COMPLETE"

#define CALIBRATION_DATA_FILE_EXTENSION_INVALID             "CALIBRATION_DATA_FILE_EXTENSION_INVALID"
#define CALIBRATION_DATA_FILE_SAVE_FAILED                   "CALIBRATION_DATA_FILE_SAVE_FAILED"
#define CALIBRATION_DATA_FILE_LOAD_FAILED                   "CALIBRATION_DATA_FILE_LOAD_FAILED"
#define CALIBRATION_DATA_FILE_INVALID                       "CALIBRATION_DATA_FILE_INVALID"

#define CALIBRATION_NOT_SETUP                               "CALIBRATION_NOT_SETUP"
#define CALIBRATION_NOT_COMPLETE                            "CALIBRATION_NOT_COMPLETE"
#define CALIBRATION_NOT_FROM_CAMERA                         "CALIBRATION_NOT_FROM_CAMERA"
#define CALIBRATION_NULL_POINTER_SETTINGS                   "CALIBRATION_NULL_POINTER_SETTINGS"
#define CALIBRATION_NULL_POINTER_SUCCESS                    "CALIBRATION_NULL_POINTER_SUCCESS"
#define CALIBRATION_NULL_POINTER_SUCCESSFUL                 "CALIBRATION_NULL_POINTER_SUCCESSFUL"
#define CALIBRATION_NULL_POINTER_TOTAL_REQUIRED             "CALIBRATION_NULL_POINTER_TOTAL_REQUIRED"
#define CALIBRATION_NULL_POINTER_DATA                       "CALIBRATION_NULL_POINTER_DATA"
#define CALIBRATION_NULL_POINTER_CALIBRATION_IMAGE          "CALIBRATION_NULL_POINTER_CALIBRATION_IMAGE"
#define CALIBRATION_NULL_POINTER_REPROJECTION_ERROR         "CALIBRATION_NULL_POINTER_REPROJECTION_ERROR"
#define CALIBRATION_NULL_POINTER_PROJECTED_BOARD            "CALIBRATION_NULL_POINTER_PROJECTED_BOARD"

#define CALIBRATION_SETTINGS_MODEL_SIZE_MISSING                         "CALIBRATION_SETTINGS_MODEL_SIZE_MISSING"
#define CALIBRATION_SETTINGS_IMAGE_SIZE_MISSING                         "CALIBRATION_SETTINGS_IMAGE_SIZE_MISSING"

#define CALIBRATION_SETTINGS_NUMBER_BOARDS_MISSING                      "CALIBRATION_SETTINGS_NUMBER_BOARDS_MISSING"
#define CALIBRATION_SETTINGS_NUMBER_BOARDS_INVALID                      "CALIBRATION_SETTINGS_NUMBER_BOARDS_INVALID"

#define CALIBRATION_SETTINGS_IMAGE_SIZE_MISSING                         "CALIBRATION_SETTINGS_IMAGE_SIZE_MISSING"
#define CALIBRATION_SETTINGS_IMAGE_SIZE_INVALID                         "CALIBRATION_SETTINGS_IMAGE_SIZE_INVALID"
#define CALIBRATION_SETTINGS_BOARD_TYPE_MISSING                         "CALIBRATION_SETTINGS_BOARD_TYPE_MISSING"
#define CALIBRATION_SETTINGS_BOARD_TYPE_INVALID                         "CALIBRATION_SETTINGS_BOARD_TYPE_INVALID"
#define CALIBRATION_SETTINGS_BOARD_FEATURE_SIZE_MISSING                 "CALIBRATION_SETTINGS_BOARD_FEATURE_SIZE_MISSING"
#define CALIBRATION_SETTINGS_BOARD_FEATURE_SIZE_INVALID                 "CALIBRATION_SETTINGS_BOARD_FEATURE_SIZE_INVALID"
#define CALIBRATION_SETTINGS_BOARD_FEATURE_DISTANCE_MISSING             "CALIBRATION_SETTINGS_BOARD_FEATURE_DISTANCE_MISSING"
#define CALIBRATION_SETTINGS_BOARD_FEATURE_DISTANCE_INVALID             "CALIBRATION_SETTINGS_BOARD_FEATURE_DISTANCE_INVALID"
#define CALIBRATION_SETTINGS_BOARD_FEATURE_DISTANCE_IN_PIXELS_MISSING   "CALIBRATION_SETTINGS_BOARD_FEATURE_DISTANCE_IN_PIXELS_MISSING"
#define CALIBRATION_SETTINGS_BOARD_FEATURE_DISTANCE_IN_PIXELS_INVALID   "CALIBRATION_SETTINGS_BOARD_FEATURE_DISTANCE_IN_PIXELS_INVALID"
#define CALIBRATION_SETTINGS_PATTERN_TYPE_NOT_SUPPORTED                 "CALIBRATION_SETTINGS_PATTERN_TYPE_NOT_SUPPORTED"
#define CALIBRATION_SETTINGS_PATTERN_SIZE_MISSING                       "CALIBRATION_SETTINGS_PATTERN_SIZE_MISSING"
#define CALIBRATION_SETTINGS_PATTERN_SIZE_INVALID                       "CALIBRATION_SETTINGS_PATTERN_SIZE_INVALID"
#define CALIBRATION_SETTINGS_PATTERN_POINT_DISTANCE_MISSING             "CALIBRATION_SETTINGS_PATTERN_POINT_DISTANCE_MISSING"
#define CALIBRATION_SETTINGS_PATTERN_POINT_DISTANCE_INVALID             "CALIBRATION_SETTINGS_PATTERN_POINT_DISTANCE_INVALID"
#define CALIBRATION_SETTINGS_PATTERN_POINT_LOCATION_OUT_OF_RANGE        "CALIBRATION_SETTINGS_PATTERN_POINT_LOCATION_OUT_OF_RANGE"
#define CALIBRATION_SETTINGS_PATTERN_BORDER_DISTANCE_MISSING            "CALIBRATION_SETTINGS_PATTERN_BORDER_DISTANCE_MISSING"
#define CALIBRATION_SETTINGS_PATTERN_BORDER_DISTANCE_INVALID            "CALIBRATION_SETTINGS_PATTERN_BORDER_DISTANCE_INVALID"
#define CALIBRATION_SETTINGS_TANGENT_DISTORTION_MISSING                 "CALIBRATION_SETTINGS_TANGENT_DISTORTION_MISSING"
#define CALIBRATION_SETTINGS_SIXTH_ORDER_DISTORTION_MISSING             "CALIBRATION_SETTINGS_SIXTH_ORDER_DISTORTION_MISSING"

#define CALIBRATION_IMAGE_EMPTY                     "CALIBRATION_IMAGE_EMPTY"
#define CALIBRATION_PRINTED_IMAGE_EMPTY             "CALIBRATION_PRINTED_IMAGE_EMPTY"
#define CALIBRATION_COMBO_IMAGE_EMPTY               "CALIBRATION_COMBO_IMAGE_EMPTY"
#define CALIBRATION_IMAGE_RESOLUTION_INVALID        "CALIBRATION_IMAGE_RESOLUTION_INVALID"
#define CALIBRATION_IMAGE_RESOLUTION_MISMATCH       "CALIBRATION_IMAGE_RESOLUTION_MISMATCH"
#define CALIBRATION_IMAGE_CONVERT_TO_MONO_FAILED    "CALIBRATION_IMAGE_CONVERT_TO_MONO_FAILED"
#define CALIBRATION_IMAGE_VECTOR_SIZE_MISMATCH      "CALIBRATION_IMAGE_VECTOR_SIZE_MISMATCH"

#define CALIBRATION_BOARD_NOT_DETECTED              "CALIBRATION_BOARD_NOT_DETECTED"
#define CALIBRATION_NO_BOARDS_ADDED                 "CALIBRATION_NO_BOARDS_ADDED"

#define CALIBRATION_CAMERA_CALIBRATION_MISSING                  "CALIBRATION_CAMERA_CALIBRATION_MISSING"
#define CALIBRATION_CAMERA_CALIBRATION_HOMOGRAPHIES_MISSING     "CALIBRATION_CAMERA_CALIBRATION_HOMOGRAPHIES_MISSING"

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \class  Calibration
 *  \brief  Contains classes for camera and projector calibration using
 *          the OpenCV camera calibration routines.
 *
 *  The Calibration class contains several classes used for system calibration
 *  including: Camera and Projector models, Settings, Errors, and Data container.
 *
 *  No instances of the Calibration class should be made. It was created as a
 *  class instead of a namespace so that the private calibration data could
 *  be modified by the nested camera and projector model classes.
 *
 */
class Calibration{
public:
    /** \defgroup   Calibration
     *  \brief      Contains classes for camera and projector calibration
     *  @{
     */

    /** \class      Settings
     *  \brief      Contains all \ref dlp::Parameters::Entry objects needed for
     *              \ref Calibration::Camera::Setup() and \ref Calibration::Projector::Setup().
     *  \warning    Any change to these settings for a particular setup requires the entire
     *              calibration process to be rerun.
     */
    class Settings{
    public:

        /** \class Model
         *  \brief Describes the pixel resolution of a calibration object and focal point offset.
         *
         *  Model settings are used to generate the geometrical optical rays of
         *  a calibration object in 3-dimensional space with the \ref dlp::Geometry
         *  class. Each pixel represents an optical ray. This ultimately means
         *  higher resolution cameras and projectors will yield a larger number of
         *  3d points in a point cloud. Higher camera resolution yields more spatial
         *  information, but not necessarily better depth accuracy. Higher projector
         *  resolution yields better depth accuracy.
         *
         *  Cameras typically do not have much offset (\ref Model::VerticalOffsetPercent or
         *  \ref Model::HorizontalOffsetPercent) between their sensor and the attached optics.
         *  However, many projectors have a vertical offset between the attached optics and
         *  image generating plane. Typical values are between 80% and 110%. This offset changes
         *  from projector to projector and should be measured for individual devices.
         */
        class Model{
        public:

            /** \class  Rows
             *  \brief  %Number of rows a calibration object contains (pixels).
             *  \note   Also known as resolution height
             */
            class Rows: public dlp::Parameters::Entry{
            public:
                Rows();
                Rows(const unsigned int &rows);
                void Set(const unsigned int &rows);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;
            };

            /** \class  Columns
             *  \brief  %Number of columns a calibration object contains (pixels).
             *  \note   Also known as resolution width
             */
            class Columns: public dlp::Parameters::Entry{
            public:
                Columns();
                Columns(const unsigned int &columns);
                void Set(const unsigned int &columns);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;
            };

            /** \class EffectivePixelSize
             *  \brief The orthogonal distance between pixels in micrometers
             */
            class EffectivePixelSize: public dlp::Parameters::Entry{
            public:
                EffectivePixelSize();
                EffectivePixelSize(const float &micrometers);
                void Set(const float &micrometers);
                float Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                float value_;
            };

            /** \class EstimatedFocalLength
             *  \brief The orthogonal distance between pixels in micrometers
             */
            class EstimatedFocalLength: public dlp::Parameters::Entry{
            public:
                EstimatedFocalLength();
                EstimatedFocalLength(const float &millimeters);
                void Set(const float &millimeters);
                float Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                float value_;
            };

            /** \class VerticalOffsetPercent
             *  \brief Percentage of vertical offset between image plane and optics
             *         relative to the physical size of the image plane (100 = 100%).
             *  \note  The physical image plane aspect ratio may not match pixel
             *         resolution aspect ratio. Mismatched aspect ratios could be
             *         caused by pixel size and/or orientation.
             */
            class VerticalOffsetPercent: public dlp::Parameters::Entry{
            public:
                VerticalOffsetPercent();
                VerticalOffsetPercent(const float &offset);
                void Set(const float &offset);
                float Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                float value_;
            };

            /** \class HorizontalOffsetPercent
             *  \brief Percentage of horizontal offset between image plane and optics
             *         relative to the physical size of the image plane (100 = 100%).
             *  \note  The physical image plane aspect ratio may not match pixel
             *          resolution aspect ratio. Mismatched aspect ratios could be
             *         caused by pixel size and/or orientation.
             */
            class HorizontalOffsetPercent: public dlp::Parameters::Entry{
            public:
                HorizontalOffsetPercent();
                HorizontalOffsetPercent(const float &offset);
                void Set(const float &offset);
                float Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                float value_;
            };
        };

        /** \class Image
         *  \brief Describes the pixel resolution of images used for calibration
         *  \note This class is NOT the same \ref dlp::Image.
         *
         *  For camera calibration, the image resolution is the same as the camera
         *  resolution.
         *
         *  For projector calibration, the image resolution should be set to the
         *  resolution of the camera used to capture the projected calibration
         *  boards.
         *
         */
        class Image{
        public:

            /** \class Rows
             *  \brief %Number of rows in the calibration images (pixels).
             *  \note   Also known as resolution height.
             */
            class Rows: public dlp::Parameters::Entry{
                friend class Calibration;
            public:
                Rows();
                Rows(const unsigned int &rows);
                void Set(const unsigned int &rows);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;
            };

            /** \class Columns
             *  \brief %Number of columns in the calibration images (pixels).
             *  \note   Also known as resolution width.
             */
            class Columns: public dlp::Parameters::Entry{
                friend class Calibration;
            public:
                Columns();
                Columns(const unsigned int &columns);
                void Set(const unsigned int &columns);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;
            };
        };

        /** \class Board
         *  \brief Describes the calibration board settings used for
         *         calibration such as NumberRequired, Features, and Color.
         *
         *  The SDK calibration module uses printed and projected chessboard
         *  calibration boards for Camera and Projector Calibration respectively.
         */
        class Board{
        public:

            /** \class Features
             *  \brief Describes the features of a calibration board so that the
             *         Camera::AddCalibrationBoard() and Projector::RemovePrinted_AddProjectedBoard()
             *         methods can find the calibration board within an image.
             *
             *  Feature points are designated points, edges, objects or some other entity
             *  that the searching algorithm looks for in an image to identify a location.
             *  For example, a chessboard calibration board feature is the point
             *  between the adjacent corners between four squares.
             */
            class Features{
            public:

                /** \class Rows
                 *  \brief %Number of feature rows in the calibration board (features).
                 *  \note   Also known as resolution height.
                 */
                class Rows: public dlp::Parameters::Entry{
                public:
                    Rows();
                    Rows(const unsigned int &rows);
                    void Set(const unsigned int &rows);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;

                public:

                    /** \class Distance
                     *  \brief Distance between row features in the calibration board (distance units).
                     *
                     *  For camera calibration, the distance should be measured as accurately as
                     *  possible. This measurement affects the overall accuracy of the generated
                     *  \ref dlp::Point::Cloud. The units used in this setting (i.e. millimeters,
                     *  inches, etc.) also determine the units of the \ref dlp::Point::Cloud.
                     *
                     *  For projector calibration, this value is calculated by the SDK according to the
                     *  projector's pixel resolution and physical image plane size.
                     */
                    class Distance: public dlp::Parameters::Entry{
                    public:
                        Distance();
                        Distance(const double &distance);
                        void Set(const double &distance);
                        double Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        double value_;
                    };

                    /** \class DistanceInPixels
                     *  \brief %Number of pixels between row features used to generate the
                     *          calibration board (pixels).
                     *
                     *  For camera calibration, the distance in pixels dicates the resolution of the
                     *  generated calibration board by \ref dlp::Calibration::Camera::GenerateCalibrationBoard()
                     *  used for printing.
                     *
                     *  For projector calibration, the SDK calculates this value according to the
                     *  projector's pixel resolution and calibration board features.
                     */
                    class DistanceInPixels: public dlp::Parameters::Entry{
                    public:
                        DistanceInPixels();
                        DistanceInPixels(const unsigned int &pixels);
                        void Set(const unsigned int &pixels);
                        unsigned int Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        unsigned int value_;
                    };                    
                };


                /** \class Columns
                 *  \brief %Number of feature columns in the calibration board (features).
                 *  \note   Also known as resolution width.
                 */
                class Columns: public dlp::Parameters::Entry{
                public:
                    Columns();
                    Columns(const unsigned int &columns);
                    void Set(const unsigned int &columns);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;

                public:
                    /** \class Distance
                     *  \brief Distance between column features in the calibration board (distance units).
                     *
                     *  For camera calibration, the distance should be measured as accurately as
                     *  possible. This measurement affects the overall accuracy of the generated
                     *  \ref dlp::Point::Cloud. The units used in this setting (i.e. millimeters,
                     *  inches, etc.) also determine the units of the \ref dlp::Point::Cloud.
                     *
                     *  For projector calibration, this value is calculated by the SDK according to the
                     *  projector's pixel resolution and physical image plane size.
                     */
                    class Distance: public dlp::Parameters::Entry{
                    public:
                        Distance();
                        Distance(const double &distance);
                        void Set(const double &distance);
                        double Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        double value_;
                    };

                    /** \class DistanceInPixels
                     *  \brief %Number of pixels between column features used to generate the calibration board (pixels).
                     *
                     *  For camera calibration, the distance in pixels dicates the resolution of the
                     *  generated calibration board by \ref dlp::Calibration::Camera::GenerateCalibrationBoard()
                     *  used for printing.
                     *
                     *  For projector calibration, this value is calculated by the SDK according to the
                     *  projector's pixel resolution and calibration board features.
                     */
                    class DistanceInPixels: public dlp::Parameters::Entry{
                        friend class Calibration;
                    public:
                        DistanceInPixels();
                        DistanceInPixels(const unsigned int &pixels);
                        void Set(const unsigned int &pixels);
                        unsigned int Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        unsigned int value_;
                    };
                };
            };

            /** \class  NumberRequired
             *  \brief  Number of calibration board images needed for calibration. Typical
             *          values are between 4 and 20.
             */
            class NumberRequired: public dlp::Parameters::Entry{
            public:
                NumberRequired();
                NumberRequired(const unsigned int &boards);
                void Set(const unsigned int &boards);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;
            };


            /** \class Color
             *  \brief Determines the color of the generated calibration board when using
             *         \ref dlp::Calibration::Camera::GenerateCalibrationBoard() or
             *         \ref dlp::Calibration::Projector::GenerateCalibrationBoard()
             *
             *  This setting could be used to print the camera calibration board a different
             *  color than the projected calibration board. This could improve seperation of
             *  the printed and projected calibration boards.
             */
            class Color{
            public:

                /** \class Foreground
                 *  \brief Color of the generated calibration boards foreground (\ref dlp::PixelRGB).
                 */
                class Foreground: public dlp::Parameters::Entry{
                public:
                    Foreground();
                    Foreground(const dlp::PixelRGB &color);
                    void Set(const dlp::PixelRGB &color);
                    dlp::PixelRGB Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    dlp::PixelRGB value_;
                };

                /** \class Background
                 *  \brief Color of the generated calibration boards background (\ref dlp::PixelRGB).
                 */
                class Background: public dlp::Parameters::Entry{
                public:
                    Background();
                    Background(const dlp::PixelRGB &color);
                    void Set(const dlp::PixelRGB &color);
                    dlp::PixelRGB Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    dlp::PixelRGB value_;
                };
            };

        };

        /** \class TangentDistortion
         *  \brief Determines if tangent distortion is set to zero or not during calibration.
         */
        class TangentDistortion: public dlp::Parameters::Entry{
        public:
            enum class Options{
                SET_TO_ZERO,        /**< Tangent distortion set to zero */
                NORMAL              /**< Tangent distortion calculated */
            };

            TangentDistortion();
            TangentDistortion(const Options &setting);

            void Set(const Options &setting);
            Options Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            Options value_;
        };

        /** \class SixthOrderDistortion
         *  \brief Determines if sixth order radial distortion coefficient is fixed or not during calibration.
         */
        class SixthOrderDistortion: public dlp::Parameters::Entry{
        public:
            enum class Options{
                FIX_RADIAL_DISTORTION_COEFFICIENT,  /**< Sixth order radial distortion coefficient is fixed */
                NORMAL                              /**< Sixth order radial distortion coefficient calculated */
            };

            SixthOrderDistortion();
            SixthOrderDistortion(const Options &setting);
            void Set(const Options &setting);
            Options Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            Options value_;
        };
    };


    /** \class Data
     *  \brief Container for calibration model distortion coefficients and
     *         intrinsic/extrinsic parameters.
     *
     *  In addition to containing calibration data, this class can save and load the
     *  data using XML files using \ref Data::Save() and \ref Data::Load().
     *
     *  This calibration data object contains the information required to generate
     *  the geometrical rays of a model for the system geometry. See \ref dlp::Geometry for
     *  more information.
     */
    class Data{
        friend class dlp::Calibration; /**< Allows \ref Calibration::Camera and \ref Calibration::Projector to modify private calibration data */
    public:
        /** \brief Enumeration to access rotation and translation rows from cv::Mat extrinsic object */
        enum ExtrinsicRow{
            EXTRINSIC_ROW_ROTATION    = 0,      /**< Row location of rotation vector in extrinsic cv::Mat object */
            EXTRINSIC_ROW_TRANSLATION = 1       /**< Row location of translation vector in extrinsic cv::Mat object */
        };

        Data();
        ~Data();
        Data(const Data &data);
        Data & operator=(const Data &data);

        void Clear();

        bool isComplete() const;
        bool isCamera() const;

        ReturnCode GetData(cv::Mat *intrinsic,
                          cv::Mat *extrinsic,
                          cv::Mat *distortion,
                          double  *reprojection_error)const;
        ReturnCode GetImageResolution(unsigned int *columns,
                                     unsigned int *rows)const;
        ReturnCode GetModelResolution(unsigned int *columns,
                                     unsigned int *rows)const;

        ReturnCode Save(const std::string &filename);
        ReturnCode Load(const std::string &filename);

    private:
        bool calibration_complete_;
        bool calibration_of_camera_;

        // Calibration image resolution
        unsigned int image_columns_;
        unsigned int image_rows_;

        // Model resolution
        unsigned int model_columns_;
        unsigned int model_rows_;

        // Final calibration data
        cv::Mat intrinsic_;
        cv::Mat distortion_;
        cv::Mat extrinsic_;
        std::vector<cv::Mat> homography_;   // not saved or loaded

        double reprojection_error_;
    };


    /** \class Camera
     *  \brief Contains methods for calibrating a camera.
     */
    class Camera: public dlp::Module{
    public:
        Camera();
        ~Camera();

        // Methods to clear the calibration object
        void ClearAll();
        void ClearCalibrationData();
        void ClearCalibrationImagePoints();

        // Object status methods
        bool isCalibrationComplete() const;

        // Object Get methods
        ReturnCode GetSetup(dlp::Parameters *settings) const;
        ReturnCode GetCalibrationProgress(unsigned int *successful, unsigned int *total_required) const;
        ReturnCode GetCalibrationData(dlp::Calibration::Data *data ) const;

        // Object setup methods
        ReturnCode SetCamera(const dlp::Camera &camera);
        ReturnCode SetCalibrationData(dlp::Calibration::Data &data );
        ReturnCode Setup(const dlp::Parameters &settings);

        // Calibration board generation
        ReturnCode GenerateCalibrationBoard( dlp::Image *calibration_pattern ) const;

        // Add calibration board methods
        ReturnCode AddCalibrationBoard(const dlp::Image   &board_image,   bool *success);

        // Update extrinsic data with single calibration board
        ReturnCode UpdateExtrinsicsWithCalibrationBoard(const dlp::Image &board_image, bool *success);

        // Method to remove most recent calibration board added
        ReturnCode RemoveLastCalibrationBoard();


        // Calibrate methods
        ReturnCode Calibrate(double *reprojection_error);
        ReturnCode Calibrate(double *reprojection_error,
                            const bool &update_intrinsic,
                            const bool &update_distortion,
                            const bool &update_extrinsic);


    protected:

        // Setting objects
        Settings::Model::Rows       model_rows_;
        Settings::Model::Columns    model_columns_;

        Settings::Image::Rows       image_rows_;
        Settings::Image::Columns    image_columns_;

        Settings::Board                     board_type_;
        Settings::Board::NumberRequired     board_number_required_;

        Settings::Board::Color::Foreground  board_color_foreground_;
        Settings::Board::Color::Background  board_color_background_;

        Settings::Board::Features::Columns                  board_columns_;
        Settings::Board::Features::Columns::Distance        board_column_distance_;
        Settings::Board::Features::Columns::DistanceInPixels   board_column_distance_in_pixels_;

        Settings::Board::Features::Rows                     board_rows_;
        Settings::Board::Features::Rows::Distance           board_row_distance_;
        Settings::Board::Features::Rows::DistanceInPixels      board_row_distance_in_pixels_;

        Settings::TangentDistortion     tangent_distortion_;
        Settings::SixthOrderDistortion  sixth_order_distortion_;

        // Member to track is SetCamera() was used
        bool camera_set_;

        // Calibration Data
        Data calibration_data_;

        // Calibration status
        unsigned int board_number_successes_;

        // calibration_board_feature_points_xyz_ stores the physical x, y, z position of the calibration board features.
        // Values are added during Setup() method. Assumes board is planar and that z = 0.
        std::vector<cv::Point3f> calibration_board_feature_points_xyz_;

        // Vector to store calibration_board_feature_points_xyz_ for each calibration image
        // The same calibration board must be used for all calibration images!
        // This creates the asummption the the camera is moving and the calibraiton
        // board is always in the same position in space
        std::vector<std::vector<cv::Point3f>> object_points_xyz_;

        // image_points_xy_ stores the x, y locaiton of the features in the calibration image
        // which is the camera pixel location of the feature.
        std::vector<std::vector<cv::Point2f>> image_points_xy_;

    private:
        DISALLOW_COPY_AND_ASSIGN(Camera);
    };


    /** \class Projector
     *  \brief Contains methods for calibrating a projector by using a camera.
     */
    class Projector : public dlp::Calibration::Camera{
    public:
        Projector();
        ~Projector();

        // Override the ClearAll and ClearCalibration to account for addition projector calibration data
        void ClearAll();
        void ClearCalibrationData();

        // Add setup for DlpPlatform
        ReturnCode SetDlpPlatform( const dlp::DLP_Platform &platform );  // Grabs the model resolution and mirror orientation

        // Required method to set camera calibration data
        ReturnCode SetCameraCalibration(const dlp::Calibration::Data &camera_calibration);

        // Override the camera Setup() method so that the DLP_Platform mirror type
        // type is used for the calibration_board_feature_points_xyz_ generation and also to check that
        // camera calibration has been provided
        ReturnCode Setup(const dlp::Parameters &settings);



        // These methods automatically remove the printed board from the combination
        ReturnCode RemovePrinted_AddProjectedBoard(const dlp::Image &projector_all_on,
                                                   const dlp::Image &projector_all_off,
                                                   const dlp::Image &board_image_printed_and_projected,
                                                         dlp::Image *board_image_projected,
                                                               bool *success);

        // Update extrinsic data with single calibration board
        ReturnCode RemovePrinted_UpdateExtrinsicsWithProjectedBoard(const dlp::Image &projector_all_on,
                                                                    const dlp::Image &projector_all_off,
                                                                    const dlp::Image &board_image_printed_and_projected,
                                                                          dlp::Image *board_image_projected,
                                                                                bool *success);


        // Overload the camera calibration methods because projector calibration is different
        ReturnCode Calibrate(double *reprojection_error);
        ReturnCode Calibrate(double *reprojection_error,
                            const bool &update_intrinsic,
                            const bool &update_distortion,
                            const bool &update_extrinsic);



    protected:
        // Settings for vertical and horizontal offset
        Settings::Model::VerticalOffsetPercent   offset_vertical_;
        Settings::Model::HorizontalOffsetPercent offset_horizontal_;

        // Variables for the effective model size
        float effective_model_width_;
        float effective_model_height_;


        Settings::Model::EffectivePixelSize     pixel_size_;
        Settings::Model::EstimatedFocalLength   focal_length_;

        float effective_pixel_size_um_;
        float estimated_focal_length_mm_;

        dlp::Calibration::Data camera_calibration_data_;


        // Member to track if SetDlpPlatform() was used
        bool projecter_set_;

        // Memeber to track DLP_Platform mirror type for calibration_board_feature_points_xyz_ generation
        DLP_Platform::Mirror   projector_mirror_type_;

    private:
        DISALLOW_COPY_AND_ASSIGN(Projector);
    };
    /** @}*/
};
}

#endif  //#ifndef DLP_SDK_CALIBRATION_HPP

