/** \file       dlp_platform.hpp
 *  \brief      This header file declares the DLP_Platform base class.
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#ifndef DLP_SDK_DLP_PLATFORM_HPP
#define DLP_SDK_DLP_PLATFORM_HPP

#include <common/returncode.hpp>
#include <common/debug.hpp>
#include <common/other.hpp>
#include <common/image/image.hpp>
#include <common/pattern/pattern.hpp>
#include <common/parameters.hpp>
#include <common/module.hpp>

#include <iostream>

#define DLP_PLATFORM_NOT_SETUP              "DLP_PLATFORM_NOT_SETUP"
#define DLP_PLATFORM_NULL_INPUT_ARGUMENT    "DLP_PLATFORM_NULL_INPUT_ARGUMENT"

/** \brief dlp is the namespace for all DLP SDK classes, enumerations, functions, etc. */
namespace dlp{


/** \class DLP_Platform
 *  \defgroup DLP_Platforms
    \brief Base class definition for interfacing with DLP based platforms

    DLP_Platform has several pure virtual functions so that all subclasses
    have the same basic interface and functionality.
*/
class DLP_Platform: public dlp::Module{
public:
    enum class Mirror{
        ORTHOGONAL,         /*!< The DLP Platform DMD has an orthogonal mirror array                            */
        DIAMOND,            /*!< The DLP Platform DMD has a diamond mirror array                                */
        INVALID             /*!< The DLP Platform has not been selected and the mirror array type is unknown    */
    };

    enum class Platform{
        LIGHTCRAFTER_3000,       /*!< The DLP Platform is based on the LightCrafter which features the DLPC300 controller, DLP3000 DMD, DM365, and FPGA */
        LIGHTCRAFTER_4500,  /*!< The DLP Platform is based on the LightCrafter 4500 which features the DLPC350 controller and DLP4500 DMD */
        INVALID             /*!< The DLP Platform has not been selected */
    };

    /** \class Settings
     *  \brief Contains \ref dlp::Parameters::Entry objects needed for DLP_Platform
     */
    class Settings{
    public:

        /** \class  PatternSequence
         *  \brief  Group of \ref dlp::Parameters::Entry objects for PatternSequence
         *          which include if the system has been prepared previously and the
         *          sequence exposure and period times in microseconds
         */
        class PatternSequence
        {
        public:
            /** \class Prepared
             *  \brief Stores boolean value to determine if the DLP_Platform
             *         has been prepared for a \ref dlp::Pattern::Sequence
             */
            class Prepared : public dlp::Parameters::Entry{
            public:
                Prepared();
                Prepared(bool prep);

                void Set(bool prep);
                bool Get()const;
                void SetEntryValue(std::string value);

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
            private:
                bool prepared_;
            };

            /** \class Exposure
             *  \brief Stores the exposure time in microseconds for a \ref dlp::Pattern::Sequence
             */
            class Exposure: public dlp::Parameters::Entry{
            public:
                Exposure();
                Exposure(unsigned long long time);

                void Set(unsigned long long time);
                unsigned long long Get()const;


                void SetEntryValue(std::string value);

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
            private:
                unsigned long long exposure_;
            };

            /** \class Period
             *  \brief Stores the period time in microseconds for a \ref dlp::Pattern::Sequence
             */
            class Period: public dlp::Parameters::Entry{
            public:
                Period();
                Period(unsigned long long time);

                void Set(unsigned long long time);
                unsigned long long Get()const;

                void SetEntryValue(std::string value);

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
            private:
                unsigned long long period_;
            };
        };
    };

    DLP_Platform();

    virtual ReturnCode Connect(unsigned int device) = 0;
    virtual ReturnCode Disconnect() = 0;
    virtual bool       isConnected() const = 0;

    virtual ReturnCode ProjectSolidWhitePattern() = 0;
    virtual ReturnCode ProjectSolidBlackPattern() = 0;

    virtual ReturnCode PreparePatternSequence(const Pattern::Sequence &arg_pattern_sequence) = 0;
    virtual ReturnCode StartPatternSequence(const unsigned int &start, const unsigned int &patterns, const bool &repeat) = 0;
    virtual ReturnCode DisplayPatternInSequence(const unsigned int &pattern_index, const bool &repeat) = 0;
    virtual ReturnCode StopPatternSequence() = 0;

    bool isPlatformSetup() const;
    bool isSetup() const;

    bool ImageResolutionCorrect(const std::string &arg_image_filename)const;
    bool ImageResolutionCorrect(const Image &arg_image)const;

    // Used for scanner validation
    ReturnCode GetPlatform(         Platform *ret_platform) const;
    ReturnCode GetMirrorType(         Mirror *ret_mirror) const;
    ReturnCode GetEffectiveMirrorSize(float *ret_mirror_size) const;

    ReturnCode GetRows(         unsigned int *ret_rows) const;
    ReturnCode GetColumns(      unsigned int *ret_columns) const;

    ReturnCode GetDevice( unsigned int *ret_device ) const;

protected:
    // Methods that subclasses call during setup
    void SetDevice(unsigned int device);

    // Should be called in object construction
    ReturnCode  SetPlatform(Platform arg_platform);

    Settings::PatternSequence::Prepared sequence_prepared_;
    Settings::PatternSequence::Exposure sequence_exposure_;
    Settings::PatternSequence::Period   sequence_period_;

private:
    bool            is_platform_set_;
    unsigned int    device_;
    unsigned int    rows_;
    unsigned int	columns_;
    Platform        platform_;
    Mirror          mirror_;
    float           mirror_effective_size_um_;
};

}

#endif
