/** \file       lcr4500.hpp
 *  \brief      Contains definitions for the DLP SDK LightCrafter 4500 class
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#ifndef DLP_LCR4500_HPP
#define DLP_LCR4500_HPP

#include <atomic>

#include <common/returncode.hpp>
#include <common/parameters.hpp>
#include <common/pattern/pattern.hpp>
#include <common/image/image.hpp>

#include <dlp_platforms/dlp_platform.hpp>
#include <dlp_platforms/lightcrafter_4500/lcr4500_defaults.hpp>
#include <dlp_platforms/lightcrafter_4500/flashdevice.hpp>

#define LCR4500_FLASH_FW_VERSION_ADDRESS    0xF902C000

#define LCR4500_TRUE    1
#define LCR4500_FALSE   0

#define LCR4500_COMMAND_FAILED -1

#define LCR4500_PATTERN_SEQUENCE_VALIDATION_FAILED          "LCR4500_PATTERN_SEQUENCE_VALIDATION_FAILED"
#define LCR4500_PATTERN_SEQUENCE_START_FAILED               "LCR4500_PATTERN_SEQUENCE_START_FAILED"

#define LCR4500_CONNECTION_FAILED                           "LCR4500_CONNECTION_FAILED"
#define LCR4500_NOT_CONNECTED                               "LCR4500_NOT_CONNECTED"
#define LCR4500_SETUP_FAILURE                               "LCR4500_SETUP_FAILURE"

#define LCR4500_SETUP_POWER_STANDBY_FAILED                  "LCR4500_SETUP_POWER_STANDBY_FAILED"
#define LCR4500_SETUP_SHORT_AXIS_FLIP_FAILED                "LCR4500_SETUP_SHORT_AXIS_FLIP_FAILED"
#define LCR4500_SETUP_LONG_AXIS_FLIP_FAILED                 "LCR4500_SETUP_LONG_AXIS_FLIP_FAILED"
#define LCR4500_SETUP_LED_SEQUENCE_AND_ENABLES_FAILED       "LCR4500_SETUP_LED_SEQUENCE_AND_ENABLES_FAILED"
#define LCR4500_SETUP_INVERT_LED_PWM_FAILED                 "LCR4500_SETUP_INVERT_LED_PWM_FAILED"
#define LCR4500_SETUP_LED_CURRENTS_FAILED                   "LCR4500_SETUP_LED_CURRENTS"
#define LCR4500_SETUP_LED_RED_EDGE_DELAYS_FAILED            "LCR4500_SETUP_LED_RED_EDGE_DELAYS_FAILED"
#define LCR4500_SETUP_LED_GREEN_EDGE_DELAYS_FAILED          "LCR4500_SETUP_LED_GREEN_EDGE_DELAYS_FAILED"
#define LCR4500_SETUP_LED_BLUE_EDGE_DELAYS_FAILED           "LCR4500_SETUP_LED_BLUE_EDGE_DELAYS_FAILED"
#define LCR4500_SETUP_INPUT_SOURCE_FAILED                   "LCR4500_SETUP_INPUT_SOURCE_FAILED"
#define LCR4500_SETUP_PARALLEL_PORT_CLOCK_FAILED            "LCR4500_SETUP_PARALLEL_PORT_CLOCK_FAILED"
#define LCR4500_SETUP_DATA_SWAP_FAILED                      "LCR4500_SETUP_DATA_SWAP_FAILED"
#define LCR4500_SETUP_INVERT_DATA_FAILED                    "LCR4500_SETUP_INVERT_DATA_FAILED"
#define LCR4500_SETUP_DISPLAY_MODE_FAILED                   "LCR4500_SETUP_DISPLAY_MODE_FAILED"
#define LCR4500_SETUP_TEST_PATTERN_COLOR_FAILED             "LCR4500_SETUP_TEST_PATTERN_COLOR_FAILED"
#define LCR4500_SETUP_TEST_PATTERN_FAILED                   "LCR4500_SETUP_TEST_PATTERN_FAILED"
#define LCR4500_SETUP_FLASH_IMAGE_FAILED                    "LCR4500_SETUP_FLASH_IMAGE_FAILED"
#define LCR4500_SETUP_TRIGGER_INPUT_1_DELAY_FAILED          "LCR4500_SETUP_TRIGGER_INPUT_1_DELAY_FAILED"
#define LCR4500_SETUP_TRIGGER_OUTPUT_1_FAILED               "LCR4500_SETUP_TRIGGER_OUTPUT_1_FAILED"
#define LCR4500_SETUP_TRIGGER_OUTPUT_2_FAILED               "LCR4500_SETUP_TRIGGER_OUTPUT_2_FAILED"

#define LCR4500_CALIBRATION_PATTERNS_NOT_PREPARED           "LCR4500_CALIBRATION_PATTERNS_NOT_PREPARED"
#define LCR4500_PATTERN_SEQUENCE_NOT_PREPARED               "LCR4500_PATTERN_SEQUENCE_NOT_PREPARED"
#define LCR4500_IN_CALIBRATION_MODE                         "LCR4500_IN_CALIBRATION_MODE"

#define LCR4500_IMAGE_RESOLUTION_INVALID                    "LCR4500_IMAGE_RESOLUTION_INVALID"
#define LCR4500_IMAGE_FORMAT_INVALID                        "LCR4500_IMAGE_FORMAT_INVALID"

#define LCR4500_PATTERN_NUMBER_PARAMETER_MISSING            "LCR4500_PATTERN_NUMBER_PARAMETER_MISSING"
#define LCR4500_PATTERN_FLASH_INDEX_PARAMETER_MISSING       "LCR4500_PATTERN_FLASH_INDEX_PARAMETER_MISSING"


#define LCR4500_PATTERN_SEQUENCE_BUFFERSWAP_TIME_ERROR      "LCR4500_PATTERN_SEQUENCE_BUFFERSWAP_TIME_ERROR"

#define LCR4500_IMAGE_FILE_FORMAT_INVALID                   "LCR4500_IMAGE_FILE_FORMAT_INVALID"
#define LCR4500_IMAGE_LIST_TOO_LONG                         "LCR4500_IMAGE_LIST_TOO_LONG"
#define LCR4500_IMAGE_MEMORY_ALLOCATION_FAILED              "LCR4500_IMAGE_MEMORY_ALLOCATION_FAILED"
#define LCR4500_FLASH_IMAGE_INDEX_INVALID                   "LCR4500_FLASH_IMAGE_INDEX_INVALID"
#define LCR4500_FIRMWARE_UPLOAD_IN_PROGRESS                 "LCR4500_FIRMWARE_UPLOAD_IN_PROGRESS"
#define LCR4500_FIRMWARE_FILE_INVALID                       "LCR4500_FIRMWARE_FILE_INVALID"
#define LCR4500_FIRMWARE_FILE_NOT_FOUND                     "LCR4500_FIRMWARE_FILE_NOT_FOUND"
#define LCR4500_FIRMWARE_FILE_NAME_INVALID                  "LCR4500_FIRMWARE_FILE_NAME_INVALID"
#define LCR4500_DLPC350_FIRMWARE_FILE_NOT_FOUND             "LCR4500_DLPC350_FIRMWARE_FILE_NOT_FOUND"
#define LCR4500_FIRMWARE_FLASH_PARAMETERS_FILE_NOT_FOUND    "LCR4500_FIRMWARE_FLASH_PARAMETERS_FILE_NOT_FOUND"
#define LCR4500_UNABLE_TO_ENTER_PROGRAMMING_MODE            "LCR4500_UNABLE_TO_ENTER_PROGRAMMING_MODE"
#define LCR4500_GET_FLASH_MANUFACTURER_ID_FAILED            "LCR4500_GET_FLASH_MANUFACTURER_ID_FAILED"
#define LCR4500_GET_FLASH_DEVICE_ID_FAILED                  "LCR4500_GET_FLASH_DEVICE_ID_FAILED"
#define LCR4500_FLASHDEVICE_PARAMETERS_NOT_FOUND            "LCR4500_FLASHDEVICE_PARAMETERS_NOT_FOUND"
#define LCR4500_FIRMWARE_FLASH_ERASE_FAILED                 "LCR4500_FIRMWARE_FLASH_ERASE_FAILED"
#define LCR4500_FIRMWARE_MEMORY_ALLOCATION_FAILED           "LCR4500_FIRMWARE_MEMORY_ALLOCATION_FAILED"
#define LCR4500_FIRMWARE_NOT_ENOUGH_MEMORY                  "LCR4500_FIRMWARE_NOT_ENOUGH_MEMORY"
#define LCR4500_FIRMWARE_UPLOAD_FAILED                      "LCR4500_FIRMWARE_UPLOAD_FAILED"
#define LCR4500_FIRMWARE_CHECKSUM_VERIFICATION_FAILED       "LCR4500_FIRMWARE_CHECKSUM_VERIFICATION_FAILED"
#define LCR4500_FIRMWARE_CHECKSUM_MISMATCH                  "LCR4500_FIRMWARE_CHECKSUM_MISMATCH"
#define LCR4500_FIRMWARE_IMAGE_BASENAME_EMPTY               "LCR4500_FIRMWARE_IMAGE_BASENAME_EMPTY"
#define LCR4500_NULL_POINT_ARGUMENT_PARAMETERS              "LCR4500_NULL_POINT_ARGUMENT_PARAMETERS"
#define LCR4500_NULL_POINT_ARGUMENT_MINIMUM_EXPOSURE        "LCR4500_NULL_POINT_ARGUMENT_MINIMUM_EXPOSURE"
#define LCR4500_MEASURE_FLASH_LOAD_TIMING_FAILED            "LCR4500_MEASURE_FLASH_LOAD_TIMING_FAILED"
#define LCR4500_READ_FLASH_LOAD_TIMING_FAILED               "LCR4500_READ_FLASH_LOAD_TIMING_FAILED"
#define LCR4500_GET_STATUS_FAILED                           "LCR4500_GET_STATUS_FAILED"

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

struct LCR4500_LUT_Entry{
    int     trigger_type;
    int     pattern_number;
    int     bit_depth;
    int     LED_select;
    bool    invert_pattern;
    bool    insert_black;
    bool    buffer_swap;
    bool    trigger_out_share_prev;
    unsigned int exposure;
    unsigned int period;
};

/** \class  LCr4500
 *  \ingroup DLP_Platforms
 *  \brief  Contains classes for LightCrafter 4500 projector settings and operations.
 *
 *  The LCr4500 class contains several classes used for setting projector parameters,
 *  and controlling projector operation.
 *
 *  The LCr4500 class is specific to the DLP LightCrafter 4500 and should not be used
 *  with other DLP platforms.
 *
 */
class LCr4500 : public DLP_Platform{
public:

    /** \class      Settings
     *  \brief      Contains all \ref dlp::Parameters::Entry objects needed for LCr4500.
     */
    class Settings{
    public:
        /** \class File
         *  \brief Contains objects to read and modify LightCrafter 4500 firmware files.
         *
         *  Current SDK implementation utilizes flash images stored on the LightCrafter 4500
         *  for display.  In order to achieve this, images are inserted into a firmware file
         *  and programmed into the flash memory of the device.  This class contains objects to
         *  point to files on a physical drive.
         */
        class File{
        public:

            /** \class DLPC350_Firmware
             *  \brief File path and name of DLPC350 firmware without any images.
             */
            class DLPC350_Firmware: public dlp::Parameters::Entry{
            public:
                DLPC350_Firmware();
                DLPC350_Firmware(const std::string &file);
                void Set(const std::string &file);
                std::string Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                std::string value_;
            };

            /** \class DLPC350_FlashParameters
             *  \brief The path and name of a file that defines parameters of flash memory
             *         attached to the DLPC350.
             */
            class DLPC350_FlashParameters: public dlp::Parameters::Entry{
            public:
                DLPC350_FlashParameters();
                DLPC350_FlashParameters(const std::string &file);
                void Set(const std::string &file);
                std::string Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                std::string value_;
            };

            /** \class PatternSequenceFirmware
             *  \brief The path and name of the firmware containing the required images
             *         for a pattern sequence.
             */
            class PatternSequenceFirmware: public dlp::Parameters::Entry{
            public:
                PatternSequenceFirmware();
                PatternSequenceFirmware(const std::string &file);
                void Set(const std::string &file);
                std::string Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                std::string value_;
            };

        };

        /** \class UseDefault
         *  \brief This class sets a group of default settings to the LightCrafter 4500
         *         object during \ref Setup()
         */
        class UseDefault: public dlp::Parameters::Entry{
        public:
            UseDefault();
            UseDefault(const bool &setup);
            void Set(const bool &setup);
            bool Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            bool value_;
        };

        /** \class PowerStandby
         *  \brief Object for setting the LightCrafter 4500 into standby mode for power saving,
         *         or back into normal power mode.
         */
        class PowerStandby: public dlp::Parameters::Entry{
        public:
            enum Mode: bool{
                STANDBY = true, /**< Power standby mode */
                NORMAL  = false /**< Normal power mode */
            };
            PowerStandby();
            PowerStandby(const bool &mode);
            void Set(const bool &mode);
            bool Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            bool value_;
        };

        /** \class ImageFlip
         *  \brief Object for setting the LightCrafter 4500 image flip mode
         *         allowing the displayed image to be flipped on its long axis
         *         or short axis.
         */
        class ImageFlip{
        public:

            /** \class ShortAxis
             *  \brief Sets the image flip on the short axis.  This is considered a
             *         "North-South" flip.
             */
            class ShortAxis: public dlp::Parameters::Entry{
            public:
                enum Mode: bool{
                    FLIP   = true,  /**< Flip image enabled  */
                    NORMAL = false  /**< Flip image disabled */
                };
                ShortAxis();
                ShortAxis(const bool &mode);
                void Set(const bool &mode);
                bool Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                bool value_;
            };

            /** \class LongAxis
             *  \brief Sets the image flip on the long axis.  This is considered a
             *         "East-West" flip.
             */
            class LongAxis: public dlp::Parameters::Entry{
            public:
                enum Mode: bool{
                    FLIP   = true,  /**< Flip image enabled  */
                    NORMAL = false  /**< Flip image disabled */
                };
                LongAxis();
                LongAxis(const bool &mode);
                void Set(const bool &mode);
                bool Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                bool value_;
            };

            typedef ShortAxis NorthSouth;
            typedef LongAxis  EastWest;
        };

        /** \class InputSource
         *  \brief Object for setting of the video input source on the LightCrafter 4500.
         *  \note  The SDK does not support the display of images in any method except flash
         *         images. The SDK can still be used to control the LightCrafter 4500 and set
         *         its video input source.
         */
        class InputSource: public dlp::Parameters::Entry{
        public:
            enum Source : unsigned int{
                PARALLEL_INTERFACE      = 0,    /**< Parallel port input source set */
                INTERNAL_TEST_PATTERNS  = 1,    /**< Internal test pattern source set */
                FLASH_IMAGES            = 2,    /**< Flash images source set */
                FPD_LINK                = 3     /**< FPD input source set */
            };

            InputSource();
            InputSource(const unsigned int &source);
            void Set(const unsigned int &source);
            unsigned int Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            unsigned int value_;
        };

        /** \class ParallelPortWidth
         *  \brief Object for setting the width of the parallel port
         *         connection in bits.
         */
        class ParallelPortWidth: public dlp::Parameters::Entry{
        public:
            enum Width : unsigned int{
                BITS_30 = 0,    /**< Parallel port width of 30 bits */
                BITS_24 = 1,    /**< Parallel port width of 24 bits */
                BITS_20 = 2,    /**< Parallel port width of 20 bits */
                BITS_16 = 3,    /**< Parallel port width of 16 bits */
                BITS_10 = 4,    /**< Parallel port width of 10 bits */
                BITS_8  = 5     /**< Parallel port width of 8 bits */
            };
            ParallelPortWidth();
            ParallelPortWidth(const unsigned int &width);
            void Set(const unsigned int &width);
            unsigned int Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            unsigned int value_;
        };

        /** \class ParallelClockSelect
         *  \brief Object for setting the parallel clock source.
         */
        class ParallelClockSelect: public dlp::Parameters::Entry{
        public:
            enum ClockPort : unsigned int{
                PORT_1_CLOCK_A = 0, /**< Set clock A as parallel clock source */
                PORT_1_CLOCK_B = 1, /**< Set clock B as parallel clock source */
                PORT_1_CLOCK_C = 2  /**< Set clock C as parallel clock source */
            };
            ParallelClockSelect();
            ParallelClockSelect(const unsigned int &clock);
            void Set(const unsigned int &clock);
            unsigned int Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            unsigned int value_;
        };

        /** \class InputDataSwap
         *  \brief Object for swapping input channels from an input port.
         *
         *  This is typically viewed as swapping RGB values from an input source.
         */
        class InputDataSwap: public dlp::Parameters::Entry{
        public:
            enum DataSubChannels: unsigned int{
                ABC_TO_ABC = 0, /**< No swapping of data subchannels */
                ABC_TO_CAB = 1, /**< Data subchannels are right shifted and circularly rotated  */
                ABC_TO_BCA = 2, /**< Data subchannels are left shifted and circularly rotated   */
                ABC_TO_ACB = 3, /**< Data subchannels B and C are swapped x4 wr                 */
                ABC_TO_BAC = 4, /**< Data subchannels A and B are swapped                       */
                ABC_TO_CBA = 5, /**< Data subchannels A and C are swapped                       */
                INVALID
            };
            enum Port: bool{
                PARALLEL_INTERFACE = false, /**< Sets the input port to be swapped to parallel port */
                FPD_LINK           = true   /**< Sets the input port to be swapped to FPD link      */
            };

            InputDataSwap();
            InputDataSwap(const bool &port, const unsigned int &data_channel);
            void Set(const bool &port, const unsigned int &data_channel);
            bool GetPort() const;
            unsigned int GetDataChannel() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            bool value_port_;
            unsigned int value_data_channel_;
        };

        /** \class DisplayMode
         *  \brief Object for setting the LightCrafter 4500 in pattern sequence
         *         mode or video mode.
         *
         *  Pattern sequence mode allows pixel accurate control of the LightCrafter 4500.
         *  Video mode enables image processing algorithms inside the DLPC350 to run on
         *  the displayed images.
         */
        class DisplayMode: public dlp::Parameters::Entry{
        public:
            enum Mode: bool{
                PATTERN_SEQUENCE = true,    /**< Sets the LightCrafter 4500 to pattern sequence mode */
                VIDEO            = false    /**< Sets the LightCrafter 4500 to video mode            */
            };

            DisplayMode();
            DisplayMode(const bool &mode);
            void Set(const bool &mode);
            bool Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            bool value_;
        };

        /** \class InvertData
         *  \brief Object for inverting data displayed on the LightCrafter 4500.
         */
        class InvertData: public dlp::Parameters::Entry{
        public:
            InvertData();
            InvertData(const bool &invert);
            void Set(const bool &invert);
            bool Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            bool value_;
        };

        /** \class Led
         *  \brief Contains objects for setting values controlling the LEDs in the
         *         LightCrafter 4500 light engine.
         */
        class Led{
        public:
            static const unsigned char MAXIMUM_CURRENT;

            /** \class Sequence
             *  \brief Sets the control of the LEDs in the LightCrafter to automatic or manual modes.
             */
            class Sequence: public dlp::Parameters::Entry{
            public:
                enum Mode: bool{
                    AUTO   = true,  /**< Allows the LightCrafter 4500 to automatically control LED sequence */
                    MANUAL = false  /**< Sets the LED sequence to manual control */
                };
                Sequence();
                Sequence(const bool &mode);
                void Set(const bool &mode);
                bool Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                bool value_;
            };

            /** \class InvertPWM
             *  \brief Inverts the PWM signal controlling the LEDs in the LightCrafter 4500 if set to true.
             */
            class InvertPWM: public dlp::Parameters::Entry{
            public:
                InvertPWM();
                InvertPWM(const bool &invert);
                void Set(const bool &invert);
                bool Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                bool value_;
            };

            /** \class Red
             *  \brief Contains objects for setting parameters specific to the red LED.
             */
            class Red{
            public:

                /** \class Enable
                 *  \brief Enables the driver output to the LED.  True if the LED is enabled.
                 */
                class Enable: public dlp::Parameters::Entry{
                public:
                    Enable();
                    Enable(const bool &enable);
                    void Set(const bool &enable);
                    bool Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    bool value_;
                };

                /** \class Current
                 *  \brief Sets the current level of the LED.  Value must fall from 0 to 255.
                 */
                class Current: public dlp::Parameters::Entry{
                public:
                    Current();
                    Current(const unsigned char &current);
                    void Set(const unsigned char &current);
                    unsigned char Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned char value_;
                };

                /** \class  EdgeDelay
                 *  \brief  An object to set the edge delay for the LED.
                 *  \note   This is set by default and does not have to be changed unless
                 *          special operation of the LED is required OR a different emitter
                 *          has been inserted in the light engine.
                 */
                class EdgeDelay{
                public:

                    /** \class Rising
                     *  \brief Sets the delay for the rising edge of the LED driver signal.
                     */
                    class Rising: public dlp::Parameters::Entry{
                    public:
                        Rising();
                        Rising(const unsigned char &delay);
                        void Set(const unsigned char &delay);
                        unsigned char Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        unsigned char value_;
                    };

                    /** \class Falling
                     *  \brief Sets the delay for the falling edge of the LED driver signal.
                     */
                    class Falling: public dlp::Parameters::Entry{
                    public:
                        Falling();
                        Falling(const unsigned char &delay);
                        void Set(const unsigned char &delay);
                        unsigned char Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        unsigned char value_;
                    };
                };
            };

            /** \class Green
             *  \brief Contains objects for setting parameters specific to the green LED.
             */
            class Green{
            public:

                /** \class Enable
                 *  \brief Enables the driver output to the LED.  True if the LED is enabled.
                 */
                class Enable: public dlp::Parameters::Entry{
                public:
                    Enable();
                    Enable(const bool &enable);
                    void Set(const bool &enable);
                    bool Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    bool value_;
                };

                /** \class Current
                 *  \brief Sets the current level of the LED.  Value must fall from 0 to 255.
                 */
                class Current: public dlp::Parameters::Entry{
                public:
                    Current();
                    Current(const unsigned char &current);
                    void Set(const unsigned char &current);
                    unsigned char Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned char value_;
                };

                /** \class  EdgeDelay
                 *  \brief  An object to set the edge delay for the LED.
                 *  \note   This is set by default and does not have to be changed unless
                 *          special operation of the LED is required OR a different emitter
                 *          has been inserted in the light engine.
                 */
                class EdgeDelay{
                public:

                    /** \class Rising
                     *  \brief Sets the delay for the rising edge of the LED driver signal.
                     */
                    class Rising: public dlp::Parameters::Entry{
                    public:
                        Rising();
                        Rising(const unsigned char &delay);
                        void Set(const unsigned char &delay);
                        unsigned char Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        unsigned char value_;
                    };

                    /** \class Falling
                     *  \brief Sets the delay for the falling edge of the LED driver signal.
                     */
                    class Falling: public dlp::Parameters::Entry{
                    public:
                        Falling();
                        Falling(const unsigned char &delay);
                        void Set(const unsigned char &delay);
                        unsigned char Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        unsigned char value_;
                    };
                };
            };

            /** \class Blue
             *  \brief Contains objects for setting parameters specific to the blue LED.
             */
            class Blue{
            public:

                /** \class Enable
                 *  \brief Enables the driver output to the LED.  True if the LED is enabled.
                 */
                class Enable: public dlp::Parameters::Entry{
                public:
                    Enable();
                    Enable(const bool &enable);
                    void Set(const bool &enable);
                    bool Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    bool value_;
                };

                /** \class Current
                 *  \brief Sets the current level of the LED.  Value must fall from 0 to 255.
                 */
                class Current: public dlp::Parameters::Entry{
                public:
                    Current();
                    Current(const unsigned char &current);
                    void Set(const unsigned char &current);
                    unsigned char Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned char value_;
                };

                /** \class EdgeDelay
                 *  \brief An object to set the edge delay for the LED.
                 *
                 *  \note This is set by default and does not have to be changed unless
                 *  special operation of the LED is required OR a different emitter has been
                 *  inserted in the light engine.
                 */
                class EdgeDelay{
                public:

                    /** \class Rising
                     *  \brief Sets the delay for the rising edge of the LED driver signal.
                     */
                    class Rising: public dlp::Parameters::Entry{
                    public:
                        Rising();
                        Rising(const unsigned char &delay);
                        void Set(const unsigned char &delay);
                        unsigned char Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        unsigned char value_;
                    };

                    /** \class Falling
                     *  \brief Sets the delay for the falling edge of the LED driver signal.
                     */
                    class Falling: public dlp::Parameters::Entry{
                    public:
                        Falling();
                        Falling(const unsigned char &delay);
                        void Set(const unsigned char &delay);
                        unsigned char Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        unsigned char value_;
                    };
                };
            };

        };

        /** \class TestPattern
         *  \brief This class contains the objects to set a test pattern to be projected
         *         by a LightCrafter 4500.
         *  \note The test pattern is generated by the DLPC350 and requires no flash images to be
         *        uploaded.
         */
        class TestPattern: public dlp::Parameters::Entry{
        public:
            enum Pattern : unsigned int{
                SOLID_FIELD       = 0,  /**< Sets the test pattern to a single color for every pixel in the field   */
                HORIZONTAL_RAMP   = 1,  /**< Sets the test pattern to a horizontal ramp of intensities              */
                VERTICAL_RAMP     = 2,  /**< Sets the test pattern to a vertical ramp of intensities                */
                HORIZONTAL_LINES  = 3,  /**< Sets the test pattern to display a series of horizontal lines          */
                DIAGONAL_LINES    = 4,  /**< Sets the test pattern to display a series of diagonal lines            */
                VERTICAL_LINES    = 5,  /**< Sets the test pattern to display a series of vertical lines            */
                GRID              = 6,  /**< Sets the test pattern to display a grid                                */
                CHECKERBOARD      = 7,  /**< Sets the test pattern to display a checkerboard definable by the user  */
                RGB_RAMP          = 8,  /**< Sets the test pattern to display an RGB ramp                           */
                COLOR_BARS        = 9,  /**< Sets the test pattern to display colored bars                          */
                STEP_BARS         = 10  /**< Sets the test pattern to display stepped bars                          */
            };

            TestPattern();
            TestPattern(const unsigned int &pattern);
            void Set(const unsigned int &pattern);
            unsigned int Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            unsigned int value_;

        public:

            /** \class Color
             *  \brief Contains classes and objects for setting the colors used in TestPattern objects.
             */
            class Color{
            public:
                static const unsigned int MAXIMUM;

                /** \class Foreground
                 *  \brief Allows setting of RGB values for foreground of TestPattern.
                 *  \note RGB values for test patterns are 10-bit per color.  Valid inputs are 0 to 1023.
                 */
                class Foreground: public dlp::Parameters::Entry{
                public:
                    Foreground();
                    Foreground(const unsigned int &red, const unsigned int &green, const unsigned int &blue);
                    void Set(const unsigned int &red, const unsigned int &green, const unsigned int &blue);
                    unsigned int GetRed() const;
                    unsigned int GetGreen() const;
                    unsigned int GetBlue() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_red_;
                    unsigned int value_green_;
                    unsigned int value_blue_;
                };

                /** \class Background
                 *  \brief Allows setting of RGB values for background of TestPattern.
                 *  \note RGB values for test patterns are 10-bit per color.  Valid inputs are 0 to 1023.
                 */
                class Background: public dlp::Parameters::Entry{
                public:
                    Background();
                    Background(const unsigned int &red, const unsigned int &green, const unsigned int &blue);
                    void Set(const unsigned int &red, const unsigned int &green, const unsigned int &blue);
                    unsigned int GetRed() const;
                    unsigned int GetGreen() const;
                    unsigned int GetBlue() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_red_;
                    unsigned int value_green_;
                    unsigned int value_blue_;
                };
            };
        };

        /** \class FlashImage
         *  \brief This class contains the objects needed to control a flash image to be displayed
         *  by the LightCrafter 4500.
         *
         *  Flash images are indexed inside of the DLPC350 firmware file.
         */
        class FlashImage: public dlp::Parameters::Entry{
        public:
            FlashImage();
            FlashImage(const unsigned int &index);
            void Set(const unsigned int &index);
            unsigned int Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);

            static const unsigned int MAXIMUM_INDEX;
        private:
            unsigned int value_;
        };

        /** \class Pattern
         *  \brief Class containing objects for the settings of individual patterns.
         */
        class Pattern{
        public:

            /** \class Source
             *  \brief Object for setting the input source of the pattern in the sequence.
             *
             *  DLP SDK does not currently support using a source other than flash images.
             *  A setting of true uses video port input, false sets flash image input.
             */
            class Source: public dlp::Parameters::Entry{
            public:
                enum Input: bool{
                    FLASH_IMAGES = false,   /**< Setting for flash image input */
                    VIDEO_PORT   = true     /**< Setting for video port input */
                };

                Source();
                Source(const bool &source);
                void Set(const bool &source);
                bool Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                bool value_;
            };

            /** \class Display
             *  \brief Class containing setting objects that control the display of
             *         pattern sequences by the LightCrafter 4500.
             */
            class Display{
            public:
                enum Control{
                    STOP  = 0,  /**< Stops the display of pattern sequence */
                    PAUSE = 1,  /**< Pauses the display of pattern sequence */
                    START = 2   /**< Starts the display of pattern sequence */
                };

                /** \class Repeat
                 *  \brief Setting object for the repetition of a pattern sequence. True
                 *         sets the sequence to repeat indefinitely.
                 */
                class Repeat: public dlp::Parameters::Entry{
                public:
                    Repeat();
                    Repeat(const bool &repeat);
                    void Set(const bool &repeat);
                    bool Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    bool value_;
                };

                /** \class VerifyImageLoadTimes
                 *  \brief Setting object to verify that a pattern sequence will
                 *         display as expected considering pattern periods and
                 *         the image load times
                 */
                class VerifyImageLoadTimes: public dlp::Parameters::Entry{
                public:
                    VerifyImageLoadTimes();
                    VerifyImageLoadTimes(const unsigned int &count);
                    void Set(const unsigned int &count);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;
                };
            };

            /** \class Invert
             *  \brief Setting object for the inversion of a pattern's color. True
             *         inverts the patterns colors.
             */
            class Invert: public dlp::Parameters::Entry{
            public:
                Invert();
                Invert(const bool &invert);
                void Set(const bool &invert);
                bool Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                bool value_;
            };

            /** \class FlashImage
             *  \brief This setting displays an entire 24-bit RGB flash image as a pattern.
             *
             *  This method would not be used typically.  This method should only be used to generate RGB
             *  images outside of the sequence class.
             */
            class FlashImage: public dlp::Parameters::Entry{
            public:
                FlashImage();
                FlashImage(const unsigned int &index);
                void Set(const unsigned int &index);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;

            public:
                /** \class Red
                 *  \brief This setting determines where the red bit plane is found for the flash image.
                 */
                class Red: public dlp::Parameters::Entry{
                public:
                    Red();
                    Red(const unsigned int &index);
                    void Set(const unsigned int &index);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;
                };

                /** \class Green
                 *  \brief This setting determines where the green bit plane is found for the flash image.
                 */
                class Green: public dlp::Parameters::Entry{
                public:
                    Green();
                    Green(const unsigned int &index);
                    void Set(const unsigned int &index);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;
                };

                /** \class Blue
                 *  \brief This setting determines where the blue bit plane is found for the flash image.
                 */
                class Blue: public dlp::Parameters::Entry{
                public:
                    Blue();
                    Blue(const unsigned int &index);
                    void Set(const unsigned int &index);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;
                };
            };

            /** \class Number
             *  \brief This class contains objects to create patterns of monochrome type with a certain bit depth.
             */
            class Number: public dlp::Parameters::Entry{
            public:
                Number();
                Number(const unsigned int &index);
                void Set(const unsigned int &index);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;

            public:

                /** \class Red
                 *  \brief This setting specifies the red bit plane for the pattern.
                 */
                class Red: public dlp::Parameters::Entry{
                public:
                    Red();
                    Red(const unsigned int &index);
                    void Set(const unsigned int &index);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;
                };

                /** \class Green
                 *  \brief This setting specifies the green bit plane for the pattern.
                 */
                class Green: public dlp::Parameters::Entry{
                public:
                    Green();
                    Green(const unsigned int &index);
                    void Set(const unsigned int &index);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;
                };

                /** \class Blue
                 *  \brief This setting specifies the blue bit plane for the pattern.
                 */
                class Blue: public dlp::Parameters::Entry{
                public:
                    Blue();
                    Blue(const unsigned int &index);
                    void Set(const unsigned int &index);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;
                };

                /** \class Mono_1BPP
                 *  \brief This object sets the pattern to a bit depth of 1.
                 */
                class Mono_1BPP{
                public:
                    enum Bitplanes : int{
                        G0 = 0,     /**< Bitplane G0 */
                        G1 = 1,     /**< Bitplane G1 */
                        G2 = 2,     /**< Bitplane G2 */
                        G3 = 3,     /**< Bitplane G3 */
                        G4 = 4,     /**< Bitplane G4 */
                        G5 = 5,     /**< Bitplane G5 */
                        G6 = 6,     /**< Bitplane G6 */
                        G7 = 7,     /**< Bitplane G7 */
                        R0 = 8,     /**< Bitplane R0 */
                        R1 = 9,     /**< Bitplane R1 */
                        R2 = 10,    /**< Bitplane R2 */
                        R3 = 11,    /**< Bitplane R3 */
                        R4 = 12,    /**< Bitplane R4 */
                        R5 = 13,    /**< Bitplane R5 */
                        R6 = 14,    /**< Bitplane R6 */
                        R7 = 15,    /**< Bitplane R7 */
                        B0 = 16,    /**< Bitplane B0 */
                        B1 = 17,    /**< Bitplane B1 */
                        B2 = 18,    /**< Bitplane B2 */
                        B3 = 19,    /**< Bitplane B3 */
                        B4 = 20,    /**< Bitplane B4 */
                        B5 = 21,    /**< Bitplane B5 */
                        B6 = 22,    /**< Bitplane B6 */
                        B7 = 23,    /**< Bitplane B7 */
                        BLACK = 24  /**< All bitplanes */
                    };
                };

                /** \class Mono_2BPP
                 *  \brief This object sets the pattern to a bit depth of 2.
                 */
                class Mono_2BPP{
                public:
                    enum Bitplanes : int{
                        G1_G0 = 0,  /**< Bitplane G1 to G0 */
                        G3_G2 = 1,  /**< Bitplane G3 to G2 */
                        G5_G4 = 2,  /**< Bitplane G5 to G4 */
                        G7_G6 = 3,  /**< Bitplane G7 to G6 */
                        R1_R0 = 4,  /**< Bitplane R1 to R0 */
                        R3_R2 = 5,  /**< Bitplane R3 to R2 */
                        R5_R4 = 6,  /**< Bitplane R5 to R4 */
                        R7_R6 = 7,  /**< Bitplane R7 to R6 */
                        B1_B0 = 8,  /**< Bitplane B1 to B0 */
                        B3_B2 = 9,  /**< Bitplane B3 to B2 */
                        B5_B4 = 10, /**< Bitplane B5 to B4 */
                        B7_B6 = 11  /**< Bitplane B7 to B6 */
                    };
                };

                /** \class Mono_3BPP
                 *  \brief This object sets the pattern to a bit depth of 3.
                 */
                class Mono_3BPP{
                public:
                    enum Bitplanes : int{
                        G2_G1_G0 = 0,   /**< Bitplane G2 to G0 */
                        G5_G4_G3 = 1,   /**< Bitplane G5 to G3 */
                        R0_G7_G6 = 2,   /**< Bitplane R0 to G6 */
                        R3_R2_R1 = 3,   /**< Bitplane R3 to R1 */
                        R6_R5_R4 = 4,   /**< Bitplane R6 to R4 */
                        B1_B0_R7 = 5,   /**< Bitplane B1 to R7 */
                        B4_B3_B2 = 6,   /**< Bitplane B4 to B2 */
                        B7_B6_B5 = 7    /**< Bitplane B7 to B5 */
                    };
                };

                /** \class Mono_4BPP
                 *  \brief This object sets the pattern to a bit depth of 4.
                 */
                class Mono_4BPP{
                public:
                    enum Bitplanes : int{
                        G3_G2_G1_G0 = 0,    /**< Bitplane G3 to G0 */
                        G7_G6_G5_G4 = 1,    /**< Bitplane G7 to G4 */
                        R3_R2_R1_R0 = 2,    /**< Bitplane R3 to R0 */
                        R7_R6_R5_R4 = 3,    /**< Bitplane R7 to G4 */
                        B3_B2_B1_B0 = 4,    /**< Bitplane B3 to B0 */
                        B7_B6_B5_B4 = 5 	/**< Bitplane B7 to B4 */
                    };
                };

                /** \class Mono_5BPP
                 *  \brief This object sets the pattern to a bit depth of 5.
                 */
                class Mono_5BPP{
                public:
                    enum Bitplanes : int{
                        G5_G4_G3_G2_G1 = 0, /**< Bitplane G5 to G1 */
                        R3_R2_R1_R0_G7 = 1, /**< Bitplane R3 to G7 */
                        B1_B0_R7_R6_R5 = 2, /**< Bitplane B1 to R5 */
                        B7_B6_B5_B4_B3 = 3  /**< Bitplane B7 to B3 */
                    };
                };

                /** \class Mono_6BPP
                 *  \brief This object sets the pattern to a bit depth of 6.
                 */
                class Mono_6BPP{
                public:
                    enum Bitplanes : int{
                        G5_G4_G3_G2_G1_G0 = 0,  /**< Bitplane G5 to G0 */
                        R3_R2_R1_R0_G7_G6 = 1,  /**< Bitplane R3 to G6 */
                        B1_B0_R7_R6_R5_R4 = 2,  /**< Bitplane B1 to R4 */
                        B7_B6_B5_B4_B3_B2 = 3   /**< Bitplane B7 to B2 */
                    };
                };

                /** \class Mono_7BPP
                 *  \brief This object sets the pattern to a bit depth of 7.
                 */
                class Mono_7BPP{
                public:
                    enum Bitplanes : int{
                        G7_G6_G5_G4_G3_G2_G1 = 0,   /**< Bitplane G7 to G1 */
                        R7_R6_R5_R4_R3_R2_R1 = 1,   /**< Bitplane R7 to R1 */
                        B7_B6_B5_B4_B3_B2_B1 = 2    /**< Bitplane B7 to B1 */
                    };
                };

                /** \class Mono_8BPP
                 *  \brief This object sets the pattern to a bit depth of 8.
                 */
                class Mono_8BPP{
                public:
                    enum Bitplanes : int{
                        G7_G6_G5_G4_G3_G2_G1_G0 = 0,    /**< Bitplane G7 to G0 */
                        R7_R6_R5_R4_R3_R2_R1_R0 = 1,    /**< Bitplane R7 to R0 */
                        B7_B6_B5_B4_B3_B2_B1_B0 = 2 	/**< Bitplane B7 to B0 */
                    };
                };
            };

            /** \class Led
             *  \brief This object sets the Led to be used for the monochrome pattern.
             *  \note  The led can be any combination of the 3 LEDs installed in the LightCrafter 4500.
             */
            class Led: public dlp::Parameters::Entry{
            public:
                enum Color: unsigned int{
                    NONE     = 0,   /**< No LED on                                  */
                    RED      = 1,   /**< Red LED on                                 */
                    GREEN    = 2,   /**< Green LED on                               */
                    YELLOW   = 3,   /**< Red and Green LEDs on simultaneously       */
                    BLUE     = 4,   /**< Blue LED on simultaneously                 */
                    MAGENTA  = 5,   /**< Red and Blue LEDs on simultaneously        */
                    CYAN     = 6,   /**< Green and Blue LEDs on simultaneously      */
                    WHITE    = 7,   /**< Red, Green and Blue LEDs on simultaneously */
                    INVALID  = 8
                };

                Led();
                Led(const unsigned int &led);
                void Set(const unsigned int &led);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;
            };

            /** \class Bitdepth
             *  \brief Objects that define the bitdepth of the pattern.
             */
            class Bitdepth: public dlp::Parameters::Entry{
            public:
                enum Format: unsigned int{
                    MONO_1BPP = 1,  /**< 1 bit per pixel, 1 color */
                    MONO_2BPP = 2,  /**< 2 bit per pixel, 1 color */
                    MONO_3BPP = 3,  /**< 3 bit per pixel, 1 color */
                    MONO_4BPP = 4,  /**< 4 bit per pixel, 1 color */
                    MONO_5BPP = 5,  /**< 5 bit per pixel, 1 color */
                    MONO_6BPP = 6,  /**< 6 bit per pixel, 1 color */
                    MONO_7BPP = 7,  /**< 7 bit per pixel, 1 color */
                    MONO_8BPP = 8,  /**< 8 bit per pixel, 1 color */
                    INVALID   = 9
                };

                Bitdepth();
                Bitdepth(const unsigned int &format);
                void Set(const unsigned int &format);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;
            };

            /** \class Trigger
             *  \brief Defines the trigger to be used to display the pattern. If the
             *         pattern is externally triggered, level definition is included as well.
             */
            class Trigger: public dlp::Parameters::Entry{
            public:
                enum Type: unsigned int{
                    INTERNAL          = 0,  /**< Internally triggered pattern */
                    EXTERNAL_POSITIVE = 1,  /**< Externally triggered pattern by a low to high transition */
                    EXTERNAL_NEGATIVE = 2,  /**< Externally triggered pattern by a high to low transition */
                    NONE              = 3,  /**< No trigger */
                    INVALID           = 4
                };

                Trigger();
                Trigger(const unsigned int &trigger);
                void Set(const unsigned int &trigger);
                unsigned int Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                unsigned int value_;

            public:

                /** \class Mode
                 *  \brief Defines the trigger mode to be used to display the pattern.
                 *         Please see DLPC350 programmer's guide for trigger mode descriptions.
                 */
                class Mode: public dlp::Parameters::Entry{
                public:
                    enum Trigger: unsigned int{
                        MODE_0_VSYNC            = 0,  /**< Mode 0 triggering, uses Vsync signal         */
                        MODE_1_INT_OR_EXT       = 1,  /**< Mode 1 internal or external trigger          */
                        MODE_2                  = 2,  /**< Mode 2 triggering                            */
                        MODE_3_EXP_INT_OR_EXT   = 3,  /**< Mode 3 expanded internal or external trigger */
                        MODE_4_EXP_VSYNC        = 4,  /**< Mode 4 expanded triggering with VSYNC        */
                        INVALID                 = 5
                    };

                    Mode();
                    Mode(const unsigned int &mode);
                    void Set(const unsigned int &mode);
                    unsigned int Get() const;

                    std::string GetEntryName() const;
                    std::string GetEntryValue() const;
                    std::string GetEntryDefault() const;
                    void SetEntryValue(std::string value);
                private:
                    unsigned int value_;
                };

                enum Output:unsigned int{
                    TRIGGER_OUT_1 = 1,  /**< Trigger output 1 */
                    TRIGGER_OUT_2 = 2   /**< Trigger output 2 */
                };

                /** \class Input1
                 *  \brief Objects for the settings of parameters specific to input trigger 1.
                 */
                class Input1{
                public:

                    /** \class Delay
                     *  \brief Allows for the setting of a delay for input trigger 1.
                     *  \note  Each bit adds 107.136 ns of delay up to a maximum 4 bytes = 4294967295 ns delay.
                     */
                    class Delay: public dlp::Parameters::Entry{
                    public:
                        Delay();
                        Delay(const unsigned int &delay);
                        void Set(const unsigned int &delay);
                        unsigned int Get() const;
                        double GetNanoSeconds();

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        unsigned int value_;
                    };
                };

                /** \class Output1
                 *  \brief Objects for the settings of parameters specific to output trigger 1.
                 */
                class Output1{
                public:

                    /** \class Invert
                     *  \brief Object to invert the output of output trigger 1.  True means output is inverted.
                     */
                    class Invert: public dlp::Parameters::Entry{
                    public:
                        Invert();
                        Invert(const bool &invert);
                        void Set(const bool &invert);
                        bool Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        bool value_;
                    };

                    /** \class EdgeDelay
                     *  \brief Class containing objects to set the edge delays for output trigger 1.
                     */
                    class EdgeDelay{
                    public:
                        static const unsigned int MAXIMUM; // 0xD5

                        /** \class Rising
                         *  \brief Changes the delay for the rising edge of output trigger 1.
                         *  \note Input the delay in nanoseconds.
                         */
                        class Rising: public dlp::Parameters::Entry{
                        public:
                            Rising();
                            Rising(const unsigned int &delay);
                            void Set(const unsigned int &delay);
                            unsigned int Get() const;
                            double GetNanoSeconds(); //107.2ns * val -20.05 µs

                            std::string GetEntryName() const;
                            std::string GetEntryValue() const;
                            std::string GetEntryDefault() const;
                            void SetEntryValue(std::string value);
                        private:
                            unsigned int value_;
                        };

                        /** \class Falling
                         *  \brief Changes the delay for the falling edge of output trigger 1.
                         *  \note  Input the delay in nanoseconds.
                         */
                        class Falling: public dlp::Parameters::Entry{
                        public:
                            Falling();
                            Falling(const unsigned int &delay);
                            void Set(const unsigned int &delay);
                            unsigned int Get() const;
                            double GetNanoSeconds(); //107.2ns * val -20.05 µs

                            std::string GetEntryName() const;
                            std::string GetEntryValue() const;
                            std::string GetEntryDefault() const;
                            void SetEntryValue(std::string value);
                        private:
                            unsigned int value_;
                        };
                    };

                };

                /** \class Output2
                 *  \brief Objects for the settings of parameters specific to output trigger 2.
                 */
                class Output2{
                public:

                    /** \class Invert
                     *  \brief Object to invert the output of output trigger 2.  True means output is inverted.
                     */
                    class Invert: public dlp::Parameters::Entry{
                    public:
                        Invert();
                        Invert(const bool &invert);
                        void Set(const bool &invert);
                        bool Get() const;

                        std::string GetEntryName() const;
                        std::string GetEntryValue() const;
                        std::string GetEntryDefault() const;
                        void SetEntryValue(std::string value);
                    private:
                        bool value_;
                    };

                    /** \class EdgeDelay
                     *  \brief Class containing objects to set the edge delays for output trigger 2.
                     */
                    class EdgeDelay{
                    public:
                        static const unsigned int MAXIMUM; // 0xFF

                        /** \class Rising
                         *  \brief Changes the delay for the rising edge of output trigger 2.
                         *  \note Input the delay in nanoseconds.
                         */
                        class Rising: public dlp::Parameters::Entry{
                        public:
                            Rising();
                            Rising(const unsigned int &delay);
                            void Set(const unsigned int &delay);
                            unsigned int Get() const;
                            double GetNanoSeconds(); //107.2ns * val -20.05 µs

                            std::string GetEntryName() const;
                            std::string GetEntryValue() const;
                            std::string GetEntryDefault() const;
                            void SetEntryValue(std::string value);
                        private:
                            unsigned int value_;
                        };
                    };
                };
            };

            /** \class ShareExposure
             *  \brief Class of settings objects which allow patterns to share exposure time.
             */
            class ShareExposure: public dlp::Parameters::Entry{
            public:
                ShareExposure();
                ShareExposure(const bool &share);
                void Set(const bool &share);
                bool Get() const;

                std::string GetEntryName() const;
                std::string GetEntryValue() const;
                std::string GetEntryDefault() const;
                void SetEntryValue(std::string value);
            private:
                bool value_;
            };

            /** \class Exposure
             *  \brief Sets the exposure time of the pattern.  Exposure time is set in ns.
             */
            class Exposure{
            public:
                static unsigned long int MININUM(const dlp::Pattern::Settings::Bitdepth::Format &bitdepth);

                static const unsigned long int MAXIMUM;
                static const unsigned long int PERIOD_DIFFERENCE_MINIMUM;
            };
        };

        static const unsigned int PATTERN_LUT_SIZE;
        static const unsigned int IMAGE_LUT_SIZE;
        static const unsigned int BUFFER_IMAGE_SIZE;
    };

    LCr4500();

    // DLP_Platform Pure Virtual Functions
    ReturnCode Connect(unsigned int device);
    ReturnCode Disconnect();
    bool       isConnected() const;

    ReturnCode Setup(const Parameters &arg_parameters);
    ReturnCode GetSetup(Parameters* ret_parameters)const;

    ReturnCode ProjectSolidWhitePattern();
    ReturnCode ProjectSolidBlackPattern();

    ReturnCode PreparePatternSequence(const Pattern::Sequence &arg_pattern_sequence);
    ReturnCode StartPatternSequence(const unsigned int &start, const unsigned int &patterns, const bool &repeat);
    ReturnCode DisplayPatternInSequence(const unsigned int &pattern_index, const bool &repeat);
    ReturnCode StopPatternSequence();


    // LightCrafter 4500 Specific methods
    ReturnCode PatternSettingsValid(Pattern &arg_pattern);

    ReturnCode UploadFirmware(std::string firmware_filename);
    ReturnCode CreateFirmware(const std::string              &new_firmware_filename,
                             const std::vector<std::string> &image_filenames);


    // Methods that need to be written
    ReturnCode CreateFirmwareImages(const Pattern::Sequence  &arg_pattern_sequence,
                                   const std::string        &arg_image_filename_base,
                                   Pattern::Sequence          &ret_pattern_sequence,
                                   std::vector<std::string> &ret_image_filename_list );



    ReturnCode GetImageLoadTime(const unsigned int &index, const unsigned int &load_count, double *max_microseconds);


    bool StartPatternImageStorage(const unsigned char &bitplane_position,
                                  const unsigned char &mono_bpp,
                                        unsigned char &ret_pattern_number);

    bool FirmwareUploadInProgress();

    long long GetFirmwareUploadPercentComplete();
    long long GetFirmwareFlashEraseComplete();

private:

    // Pattern Sequence related methods
    static int DlpPatternColorToLCr4500Led(const Pattern::Settings::Color &color);
    static int DlpPatternBitdepthToLCr4500Bitdepth(const dlp::Pattern::Settings::Bitdepth &depth);

    // Firmware upload related methods
    bool ProcessFlashParamsLine(const std::string &line);
    int  GetSectorNum(unsigned int Addr);

    ReturnCode SavePatternIntImageAsRGBfile(Image &image_int, const std::string &filename);
    ReturnCode CreateSendStartSequenceLut(const dlp::Pattern::Sequence &arg_pattern_sequence);

    // Setting members
    Settings::File::DLPC350_Firmware            dlpc350_firmware_;
    Settings::File::DLPC350_FlashParameters     dlpc350_flash_parameters_;
    Settings::File::PatternSequenceFirmware     pattern_sequence_firmware_;

    Settings::UseDefault                        use_default_;
    Settings::PowerStandby                      power_standby_;

    Settings::DisplayMode                       display_mode_;
    Settings::InputSource                       input_source_;

    Settings::ParallelPortWidth                 parallel_port_width_;
    Settings::ParallelClockSelect               parallel_port_clock_;

    Settings::InputDataSwap                     input_data_swap_;

    Settings::InvertData                        invert_data_;

    Settings::ImageFlip::ShortAxis              short_axis_flip_;
    Settings::ImageFlip::LongAxis               long_axis_flip_;

    Settings::Led::Sequence                     led_sequence_mode_;
    Settings::Led::InvertPWM                    led_invert_pwm_;

    Settings::Led::Red::Enable                  led_red_enable_;
    Settings::Led::Red::Current                 led_red_current_;
    Settings::Led::Red::EdgeDelay::Rising       led_red_edge_delay_rising_;
    Settings::Led::Red::EdgeDelay::Falling      led_red_edge_delay_falling_;

    Settings::Led::Green::Enable                led_green_enable_;
    Settings::Led::Green::Current               led_green_current_;
    Settings::Led::Green::EdgeDelay::Rising     led_green_edge_delay_rising_;
    Settings::Led::Green::EdgeDelay::Falling    led_green_edge_delay_falling_;

    Settings::Led::Blue::Enable                 led_blue_enable_;
    Settings::Led::Blue::Current                led_blue_current_;
    Settings::Led::Blue::EdgeDelay::Rising      led_blue_edge_delay_rising_;
    Settings::Led::Blue::EdgeDelay::Falling     led_blue_edge_delay_falling_;

    Settings::Pattern::Display::VerifyImageLoadTimes    verify_image_load_;
    Settings::Pattern::Trigger                  trigger_source_;
    Settings::Pattern::Trigger::Input1::Delay   trigger_in_1_delay_;
    Settings::Pattern::Trigger::Output1::Invert trigger_out_1_invert_;
    Settings::Pattern::Trigger::Output2::Invert trigger_out_2_invert_;

    Settings::Pattern::Trigger::Output1::EdgeDelay::Rising  trigger_out_1_rising_;
    Settings::Pattern::Trigger::Output1::EdgeDelay::Falling trigger_out_1_falling_;
    Settings::Pattern::Trigger::Output2::EdgeDelay::Rising  trigger_out_2_rising_;


    FlashDevice myFlashDevice;
    std::string firmwarePath;

    bool        previous_command_in_progress;

    bool        firmware_upload_restart_needed;


    std::atomic_flag        firmware_upload_in_progress = ATOMIC_FLAG_INIT;
    std::atomic <long long> firmware_upload_percent_erased_;
    std::atomic <long long> firmware_upload_percent_complete_;

    unsigned char status_hw_;
    unsigned char status_sys_;
    unsigned char status_main_;

    bool                pattern_sequence_prepared_;
    Pattern::Sequence   pattern_sequence_;

    bool                calibration_sequence_prepared_;
    Pattern::Sequence   calibration_sequence_;

};

}

#endif // DLP_SDK_LCR4500_HPP
