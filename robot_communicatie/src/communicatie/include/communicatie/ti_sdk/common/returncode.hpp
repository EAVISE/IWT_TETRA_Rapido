/*! \file       returncode.hpp
 *  \ingroup    Common
 *  \brief      Defines ReturnCode class for all SDK modules for errors and warning messages
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#ifndef DLP_SDK_RETURNCODE_HPP
#define DLP_SDK_RETURNCODE_HPP

#include <string>
#include <vector>

/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/** \class      ReturnCode
 *  \ingroup    Common
 *  \brief      Return type for most DLP SDK methods.
 *  \warning    NOT functional with switch() statements
 */
class ReturnCode{
public:
    ReturnCode& AddError(   const std::string &msg );
    ReturnCode& AddWarning( const std::string &msg );
    ReturnCode& Add(const ReturnCode &code);

    bool hasErrors();
    bool hasWarnings();

    bool ContainsError(   std::string msg );
    bool ContainsWarning( std::string msg );

    std::vector<std::string> GetErrors();
    std::vector<std::string> GetWarnings();
    unsigned int GetErrorCount();
    unsigned int GetWarningCount();

    std::string ToString();

    operator bool() const{
        if(this->errors_.size()>0)  return false;
        else                        return true;
    }


private:
    std::vector<std::string> errors_;
    std::vector<std::string> warnings_;
};

}

#endif //#ifndef DLP_SDK_RETURNCODE_HPP











