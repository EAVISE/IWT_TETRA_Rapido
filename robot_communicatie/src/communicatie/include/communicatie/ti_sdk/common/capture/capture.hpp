/*! \file       capture.hpp
 *  \ingroup    Common
 *  \brief      Defines \ref dlp::Capture and \ref dlp::Capture::Sequence classes
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */

#ifndef DLP_SDK_CAPTURE_HPP
#define DLP_SDK_CAPTURE_HPP

#include <common/debug.hpp>
#include <common/returncode.hpp>
#include <common/image/image.hpp>
#include <common/other.hpp>
#include <common/parameters.hpp>

#include <iostream>
#include <string>
#include <vector>

#define CAPTURE_TYPE_INVALID                    "CAPTURE_TYPE_INVALID"
#define CAPTURE_SEQUENCE_EMPTY                  "CAPTURE_SEQUENCE_EMPTY"
#define CAPTURE_SEQUENCE_TOO_LONG               "CAPTURE_SEQUENCE_TOO_LONG"
#define CAPTURE_SEQUENCE_INDEX_OUT_OF_RANGE     "CAPTURE_SEQUENCE_INDEX_OUT_OF_RANGE"
#define CAPTURE_SEQUENCE_TYPES_NOT_EQUAL        "CAPTURE_SEQUENCE_TYPES_NOT_EQUAL"


/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{


/** \class      Capture
 *  \ingroup    Common
 *  \brief      Container class for image data or file name for transfer between \ref dlp::Calibration,
 *              \ref dlp::Camera, and \ref dlp::StructuredLight classes.
 */
class Capture{
public:

    /** \class      Settings
     *  \ingroup    Settings
     *  \brief      Contains all \ref dlp::Parameters::Entry objects needed for Capture objects.
     */
    class Settings{
    public:
        /** \class Data
         *  \brief Dictates if the Capture uses \ref Capture.image_data or \ref Capture.image_file
         */
        class Data: public dlp::Parameters::Entry{
        public:
            enum class Type{
                IMAGE_FILE,     /*!< Capture contains dlp::Image instance */
                IMAGE_DATA,     /*!< Capture contains image filename      */
                INVALID         /*!< Capture type has not been set        */
            };
            Data();
            Data(const Type &image_type);
            void Set(const Type &image_type);
            Type Get() const;

            std::string GetEntryName() const;
            std::string GetEntryValue() const;
            std::string GetEntryDefault() const;
            void SetEntryValue(std::string value);
        private:
            Type value_;
        };
    };

    /** \class      Sequence
     *  \ingroup    Common
     *  \brief      Container class used to group multiple \ref dlp::Capture objects
     */
    class Sequence{
    public:

        Sequence();
        ~Sequence();
        Sequence(const Capture  &capture);
        Sequence(const Sequence &capture_seq);
        Sequence& operator=(const Sequence& capture_seq);

        unsigned int GetCount() const;
        void Clear();

        ReturnCode Add( const Capture  &new_capture);
        ReturnCode Add( const Sequence &sequence);
        ReturnCode Get(    const unsigned int &index, Capture* ret_capture) const;
        ReturnCode Set(    const unsigned int &index, Capture &arg_capture);
        ReturnCode Remove( const unsigned int &index);

        bool EqualDataTypes() const;

        Parameters parameters;  /*!< Nonrequired member to store any addition information required about the \ref dlp::Capture::Sequence */
    private:
        std::vector<Capture> captures_;
    };


    Capture();
    ~Capture();

    // Capture Information
    int camera_id;          /*!< Nonrequired member to notate which camera created the Capture */
    int pattern_id;         /*!< Nonrequired member to notate which projected pattern image the Capture contains */

    // Capture Data
    Settings::Data  image_type;     /*!< \b Required member to notate if Capture contains image data or an image filename */
    Image           image_data;     /*!< \ref dlp::Image member. Empty when Capture instance is constructed. */
    std::string     image_file;     /*!< \ref std::string to store an image filename. Empty when Capture instance is constructed. */
};

}


#endif // DLP_SDK_CAPTURE_HPP
