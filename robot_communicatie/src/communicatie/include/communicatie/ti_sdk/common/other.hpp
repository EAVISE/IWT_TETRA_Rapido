/** \file       other.hpp
 *  \ingroup    Common
 *  \brief      Contains common functions relating to strings, numbers, and time
 *  \copyright  2014 Texas Instruments Incorporated - http://www.ti.com/ ALL RIGHTS RESERVED
 */


#ifndef DLP_SDK_OTHER_HPP
#define DLP_SDK_OTHER_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip> // for setprecision()

#define NUM_TO_STRING_PRECISION     16
#define FILE_DOES_NOT_EXIST         "FILE_DOES_NOT_EXIST"


/** \brief  Contains all DLP SDK classes, functions, etc. */
namespace dlp{

/* DISALLOW_COPY_AND_ASSIGN Macro from google
 * http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml?showone=Copy_Constructors#Copy_Constructors
 *
 */
// A macro to disallow the copy constructor and operator= functions
// This should be used in the private: declarations for a class
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
  TypeName(const TypeName&);               \
  void operator=(const TypeName&)

/** \brief  Contains sleep functions and time tracking \ref dlp::Time::Chronograph class
 *  \ingroup Common
 */
namespace Time{

    /** \brief  Contains methods to pause program or thread execution */
    namespace Sleep{
        void Microseconds(unsigned time);
        void Milliseconds(unsigned time);
        void Seconds(unsigned time);
    }

    /** \class Chronograph
     *  \brief  Measures time between laps and total time in milliseconds
     *  \ingroup Common
     */
    class Chronograph{
    public:
        Chronograph();
        Chronograph(bool start);

        unsigned long long Reset();
        unsigned long long Lap();

        std::vector<unsigned long long> GetLapTimes();
        unsigned long long              GetTotalTime();

    private:
        unsigned long long start_;
        unsigned long long last_lap_;
        std::vector<unsigned long long> laps_;
    };
}

/** \brief  Contains common functions related to files
 *  \ingroup Common
 */
namespace File{
    bool Exists( std::string filename );
    unsigned long long GetSize(std::string filename);
    std::vector<std::string> ReadLines(std::string filename);
}

/** \brief  Contains common functions related to string manipulation
 *  \ingroup Common
 */
namespace String{

    /** \brief Converts an ASCII string number to a signed or unsigned numerical variable
     *
     *  For example, to convert a string to an int perform the following:
     *  int value = dlp::String::ToNumber<int>("123");
     */
    template <typename T>
    T ToNumber( const std::string &text ){
        std::istringstream ss(text);
        T result;
        long double number;

        ss >> number;          // Convert string to number
        result = (T) number;   // Convert double to return type

        return result;
    }

    /** \brief Converts an ASCII string hexadecimal number to an unsigned numerical variable
     *
     *  For example, to convert a hexadecimal string to an int perform the following:
     *  unsigned int value = dlp::String::HEXtoNumber_unsigned<unsigned int>("0x1A");
     */
    template <typename T>
    T HEXtoNumber_unsigned(const std::string &text){
        unsigned long long temp = std::stoull(text, nullptr, 16);
        T result = (T) temp;
        return result;
    }

    /** \brief Converts an ASCII string hexadecimal number to a signed numerical variable
     *
     *  For example, to convert a hexadecimal string to an int perform the following:
     *  int value = dlp::String::HEXtoNumber_unsigned<int>("0x1A");
     */
    template <typename T>
    T HEXtoNumber_signed(const std::string &text){
        long long temp = std::stoll(text, nullptr, 16);
        T result =  (T) temp;
        return result;
    }

    std::string Trim(const std::string &string);
    std::string ToUpperCase(const std::string &string);
    std::string ToLowerCase(const std::string &string);

    std::vector<std::string> SeparateDelimited(const std::string &string, const char &delimiter);
}

/** \brief  Contains common functions to convert numbers to strings
 *  \ingroup common
 */
namespace Number{

    /** \brief Converts a numerical variable to its ASCII string equivalent */
    template <typename T>
    std::string ToString( T number ){
        std::ostringstream ss;
        ss << std::setprecision(NUM_TO_STRING_PRECISION);
        ss << number;
        return ss.str();
    }

    template <> std::string ToString<char>( char number );
    template <> std::string ToString<unsigned char>( unsigned char number );

}

}

#endif // DLP_SDK_OTHER_HPP
