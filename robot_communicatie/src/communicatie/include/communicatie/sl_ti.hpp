#ifndef TI_SL_H_INCLUDED
#define TI_SL_H_INCLUDED

#include "visie.hpp"

using namespace FlyCapture2;


dlp::ReturnCode ConnectAndSetupProjector(dlp::DLP_Platform *projector, const std::string &settings_file);
dlp::ReturnCode ConnectAndSetupCamera(dlp::Camera *camera, const std::string &settings_file);
void PrepareProjectorPatterns( dlp::DLP_Platform    *projector,
                               const std::string    &projector_calib_settings_file,
                               dlp::StructuredLight *structured_light_vertical,
                               const std::string    &structured_light_vertical_settings_file,
                               dlp::StructuredLight *structured_light_horizontal,
                               const std::string    &structured_light_horizontal_settings_file,
                               const bool            previously_prepared,
                               unsigned int         *total_pattern_count);


bool get_ti_images(std::string path, int width, int height, bool &setup, dlp::PG_FlyCap2_C &camera, dlp::LCr4500 &projector,
                   dlp::GrayCode &structured_light_vertical, dlp::GrayCode &structured_light_horizontal, unsigned int &total_pattern_count);
#endif
