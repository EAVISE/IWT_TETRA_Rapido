#ifndef STRUCTUREDLIGHT_H_INCLUDED
#define STRUCTUREDLIGHT_H_INCLUDED

using namespace std;
using namespace cv;
using namespace pcl;
using namespace boost;
using namespace FlyCapture2;

///Structured light:
struct Decoder
{
    vector<Mat> minimum;
    vector<Mat> maximum;
    vector<Mat> Ld;
    vector<Mat> Lg;
    vector<Mat> pattern_image;
} ;

struct Paar
{
    vector<Point2d> camera;
    Point2d projector;
} ;

struct Visualizer
{
    Mat pointcloud;
    vector<Point2d> cam_points;
} ;

Decoder init_decoder();
vector<Mat> generate_pattern(int NOP_v, int NOP_h, int projector_width, int projector_height);
bool get_sl_images(int delay, string path, int serie, int width, int height);
//bool get_vimba(int delay, string path, int serie, int width, int height);
bool get_pointgrey(int delay, string path, int serie, int width, int height);
int getdir (string dir, vector<string> &files);
bool findcorners(vector<vector<Point2f> > &chessboardcorners, string path, int aantalseries, int width, int height, int boardw, int boardh);
int check_bit(float value1, float value2, float Ld, float Lg, float m);
void calculate_light_components(Decoder &d, vector<Mat> beelden, int dir, float b);
void get_pattern_image(Decoder &d, vector<Mat> beelden, int dir, float m, float thresh);
void colorize_pattern(Decoder &d, vector<Mat> &image, int dir, int projector_width, int projector_height);
bool decode(int serienummer, Decoder &d, bool draw, string path, float b, float m, float thresh, int p_w, int p_h);
bool decode_all(int aantalseries, vector<Decoder> &dec, bool draw, string path, float b, float m, float thresh, int projector_width, int projector_height);
bool calibrate_sl(vector<Decoder> dec, vector<vector<Point2f> > corners, int aantalseries, int projector_width, int projector_height, int boardw, int boardh);
Mat calculate3DPoints_all(string path, int aantalseries, float b, float m, float thresh, int projector_width, int projector_height);
Mat calculate3DPoints(vector<Point2f> &chessboardcorners, Decoder d);
Mat getRobotPoints(int height, int width);
Mat calculateTransMat(Mat origin, Mat dest);
Mat convert(Mat origin);
void save(Mat origin, std::string a);
void getRmsError(Mat transformed, Mat dest);
cv::Point3d getCenterPoint(string path, float b, float m, float thresh, int projector_width, int projector_height, int boardw, int boardh);
cv::Mat threshold_scene(cv::Mat scene_robot);
#endif // STRUCTUREDLIGHT_H_INCLUDED
