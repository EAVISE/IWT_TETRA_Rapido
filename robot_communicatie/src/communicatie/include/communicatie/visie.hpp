#ifndef VISIE_H_INCLUDED
#define VISIE_H_INCLUDED

#include "ros/ros.h"
#include <sensor_msgs/PointCloud2.h>
#include <iostream>
#include <sstream>

#include "std_msgs/String.h"
#include "communicatie/Point_data.h"
#include "communicatie/Robot_coord.h"

#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <limits>
#include <omp.h>
#include <iomanip>
#include <signal.h>
#include <math.h>

#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/registration/icp.h>
#include <pcl/filters/filter.h>
#include <pcl/features/normal_3d.h>

#include "pcl_conversions.hpp"

#include "flycapture/FlyCapture2.h"
#include <dlp_sdk.hpp>
#include "ti_sdk/camera/pg_flycap2/pg_flycap2_c.hpp"


#include <boost/timer/timer.hpp>
#include <cmath>

#include <boost/asio/serial_port.hpp>
#include <boost/asio.hpp>


///General
int getdir (std::string dir, std::vector<std::string> &files);
void tonen(cv::Mat image, cv::String naam);

void printmat(cv::Mat a);
void printmat(cv::Mat a, cv::String b);
void printmat(std::vector<cv::Mat> a, cv::String b);

void save(cv::Mat origin);

class Listener
{
private:
    bool start;
    bool moved;

public:
    Listener();
    void callback(const std_msgs::String::ConstPtr msg);
    bool getMoved();
    bool getStart();
    void setMoved(bool a);
    void setStart(bool a);


};

class Listener_data
{
private:
    int id;
    cv::Point3d translatie;
    cv::Point3d rotatie;

public:
    Listener_data();
    void callback(const communicatie::Robot_coord::ConstPtr msg);
    cv::Point3d getTrans();
    cv::Point3d getRot();
    int getID();
    void setTrans(cv::Point3d);
    void setRot(cv::Point3d);
    void setID(int);
};

#endif
