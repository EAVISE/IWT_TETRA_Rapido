#include "visie.hpp"
#include "structuredlight.hpp"
#include "sl_ti.hpp"


class Scanner
{ 
public:
    int calrows;
    int calcols;
    int boardw;
    int boardh;
    //Scanner();
    virtual void calibrate(void) =0;
    virtual cv::Point3d getCameraPoint(void) =0;
    virtual void sensor_calibrate(std::vector<cv::Point3d>, std::vector<cv::Point3d>) =0;
    virtual void scan(void) =0;

    ///Getters and Setters
    virtual cv::Mat getScene() =0;
    virtual void setScene(cv::Mat) =0;


};

class SLScanner: public Scanner
{
private:
    int width;
    int heigth;
    float b;
    float m;
    int thresh;
    int speed;
    int calib_sl_series;

    cv::Mat scene;
    cv::Mat transmat;

    void count_series();

public:
    SLScanner();
    SLScanner(int, int, float, float, int, int, int, int);
    void calibrate();
    cv::Point3d getCameraPoint(void);
    void sensor_calibrate(std::vector<cv::Point3d>, std::vector<cv::Point3d>);
    void scan();

    cv::Mat getScene();
    void setScene(cv::Mat);
};

class ENScanner: public Scanner
{
private:
    int number_of_calib_images;
    cv::Mat scene;
    cv::Mat transmat;

public:
    ENScanner(int w, int h);
    void calibrate(void);
    cv::Point3d getCameraPoint(void);
    void sensor_calibrate(std::vector<cv::Point3d>, std::vector<cv::Point3d>);
    void scan(void);

    ///Getters and Setters
    cv::Mat getScene();
    void setScene(cv::Mat);
    void setBoardw(int);
    void setBoardh(int);

};

class TIScanner: public Scanner
{
private:
    int width;
    int heigth;
    float b;
    float m;
    int thresh;
    int speed;


    cv::Mat scene;
    cv::Mat transmat;

    int count_series();

public:
    bool setup;
    int calib_ti_series;
    dlp::PG_FlyCap2_C   camera;
    dlp::LCr4500        projector;
    dlp::GrayCode structured_light_vertical;
    dlp::GrayCode structured_light_horizontal;
    unsigned int total_pattern_count;

    TIScanner();
    TIScanner(int, int, float, float, int, int, int, int);
    void calibrate();
    cv::Point3d getCameraPoint(void);
    void sensor_calibrate(std::vector<cv::Point3d>, std::vector<cv::Point3d>);
    void scan();

    cv::Mat getScene();
    void setScene(cv::Mat);
};
