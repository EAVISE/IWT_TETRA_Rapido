#include "zhistogram.hpp"

int main (int argc, char** argv)
{
    vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> cloud_collection; //(new pcl::PointCloud<pcl::PointXYZ>);
   // pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_en(new pcl::PointCloud<pcl::PointXYZ>);

    vector<string> files;

    //files.push_back("en.ply");
    //files.push_back("sl.ply");
    files.push_back("Ensenso_U.pcd");
    files.push_back("NSL_U.ply");
    files.push_back("NSL_U2.ply");
    files.push_back("OSL_U.ply");
    //files.push_back("bovenwolk.ply");

    pcl::PLYReader plyreader;
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    for(int k=0; k<files.size() ; k++)
    {
        ///Read point cloud
        ostringstream conv;
        conv << k;
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
        if(k==0)
        {
            if (pcl::io::loadPCDFile<pcl::PointXYZ> ("Ensenso_U.pcd", *cloud) == -1) //* load the file
            {
                PCL_ERROR ("Couldn't read file test_Ensenso_U.pcd \n");
                return (-1);
            }
        }
        else
            plyreader.read(files[k], *cloud);
        cout<<files[k]<<endl;
        cloud->width  = cloud->points.size();
        cloud->height = 1;
        cloud->points.resize (cloud->width * cloud->height);

        ///Rotate point cloud so it is perpendicular with the z-axis
        if(rotateCloud(cloud) )
        {
            cout<<"Point cloud "<<files[k]<<" succesfully rotated."<<endl;
        }
        else
        {
            cout<<"Could not rotate point cloud "<<files[k]<<", moving on to the next one."<<endl;
            continue;
        }

        ///Find the maximum Z-value in the desired unit. The standard unit is metres
        int maxvalue = 0;
        for(int i =0; i<cloud->points.size(); i++)
        {
            int z = cloud->points[i].z*1000; ///z in mm
            maxvalue = (maxvalue < z ? z : maxvalue);
        }

        ///Count the number of times a z-value appears in the point cloud
        vector<int> number_of_z_values;
        number_of_z_values.resize(maxvalue+1);

        if(sort_z(number_of_z_values, cloud))
        {
            cout<<"Point cloud sorted."<<endl;
        }
        else
        {
            cout<<"Could not sort point cloud."<<endl;
            continue;
        }
        ///Visualize sorted z_values in a Mat
        //visualize_z(number_of_z_values);
        ///Print the sorted z-values to .csv
        string path = "zhistogram_"+files[k]+".csv";
        print(number_of_z_values, path);
    }

    /*while (!viewer->wasStopped ())
    {
        viewer->spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }
    */
}
