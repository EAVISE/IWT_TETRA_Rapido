Nodige libraries voor Errordet:

Opencv 3.0.0
PCL 1.8
Boost : builden met vlaggen -lboost_system en -lboost_thread

Compiler moet zoeken in directories:
/usr/local/pcl-1.8  (of waar je het geinstalleerd hebt)
/usr/include/eigen3
/usr/include/vtk-5.8


Voorwaarden:
In de folder errordet moet je de .ply files zetten die je wil testen.
Wanneer je de code build moet je deze namen aanpassen in functies int rms_error_ground_plane() en int rms_error_top_plane() :
	
	vector<string> files;
    	files.push_back("naam_van_je_ply_file.ply");

Functions:

rgbVis:
	Geeft een viewer terug zodat de puntenwolk kan worden gevisualiseerd. Deze functie geeft een RGB viewer terug.

simplevis:
	Geeft een viewer terug zodat de puntenwolk kan worden gevisualiseerd. Deze functie geeft een viewer terug die kleuren negeert.

tonen:
	Toont een cv::Mat met de gespecifieerde string als naam van het venster. Resolutie is 1200x800.

rms_error_ground_plane:
	Leest alle opgegeven .ply files in. Daarna roteert deze functie de pointcloud.
	Deze functie segmenteert dan de ground plane op basis van de z-waarden.
	Hierna wordt er een vlak door de ground plane gefit. Dit vlak wordt gebruikt door de functie op de afwijking van het ground plane tot het vlak te schatten via RMS.
	Uiteindelijk zal de functie het ground plane tonen met het vlak erdoor gefit. Alle pointclouds (dus van elke .ply is er 1) worden in 1 venster gevisualiseerd.
	output:
		depthmap(ingekleurd, rood is hoog, blauw is laag)
		RMS error groundplane.


rms_error_top_plane:
	Leest alle opgegeven .ply files in. Daarna roteert deze functie de pointcloud.
	Deze functie segmenteert dan het bovenvlak op basis van de z-waarden.
	Hierna wordt er een vlak0 door het ground plane en een vlak1 door het bovenvlak gefit. Dit vlak1 wordt gebruikt door de functie op de afwijking van het bovenvlak tot het vlak1 te schatten via RMS.
	Vlak0 en Vlak1 worden gebruikt om de hoogte van het object te schatten. Deze afstand wordt in mm weergegeven. Dit is dan een maat voor de correctheid van de puntenwolk, daar deze afstand 		vergeleken kan worden met de reele hoogte van het object.
	Uiteindelijk zal de functie het bovenvlak en vlak0 tonen. Alle pointclouds (dus van elke .ply is er 1) worden in 1 venster gevisualiseerd.
	output:
		RMS error bovenvlak.
		Gemeten grootte van het object.
	

		
