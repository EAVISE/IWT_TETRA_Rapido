Nodige libraries voor multiflash:

OpenCV 3.0.0
PCL 1.8
Boost : builden met vlaggen -lboost_system en -lboost_thread

Andere vlaggen voor de linker: -lueye_api (voor de IDS camera)

Compiler moet zoeken in directories:
/usr/local/pcl-1.8  (of waar je het geinstalleerd hebt)
/usr/include/eigen3
/usr/include/vtk-5.8


Voorwaarden: 
	/

Functies:

initCam
	Initialiseer de IDS camera.

grabSingleImage
	Gebruikt de camera handler verkrijgen uit initCam om een beeld uit te lezen.
	De functie zal het uitgelezen beeld wegschrijven op het path dat je meegeeft, indien gewenst

grabMultiflashImages
	Open de camera, stuur het juiste commando naar de driver van de LEDs en neem een beeld. 	Herhaal voor elk aanzicht (momenteel nog 4).

grabMultiflashCalibImages
	Geef mee hoeveel calibratiebeelden je wil maken.
	De functie zal steeds het opgenomen beeld wegschrijven en laten zien. Wanneer je dan een 		toets indrukt zal de functie het volgende beeld nemen. Zo heb je tijd om het calibratiebord 		te verleggen.

calibrateMultiflashCamera
	Leest de net genomen calibratiebeelden uit en calibreert de camera. Zo kunnen we later 		beelden undistorten.
	Nog niet geimplementeerd.

calculateEdgeMap
	Gaat voor grijswaardenbeelden de randen berekenen.
	Volgorde van bewerkingen:
	Normaliseer elk aanzicht.
	Bereken een maximum beeld.
	Bereken een verhoudingsbeeld voor elk aanzicht (aanzicht/maximum).
	Ga met een Sobelfilter over elk verhoudingsbeeld.
	Gebruik een Canny Edge detector om de edges te vinden.
	
calculateEdgeMapRGB
	Zelfde functionaliteit als calculateEdgeMap maar dan voor RGB beelden.
	Momenteel roept de mainfunctie de calculateEdgeMapfunctie aan, en niet de calculateEdgeMapRGB.
	Dit dien je aan te passen als je met RGB beelden wil werken.

Opmerkingen:
	Momenteel zijn de LEDs en de camera nog niet gesynchroniseerd.
