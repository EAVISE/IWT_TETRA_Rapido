#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <ueye.h>

#include <boost/asio/serial_port.hpp>
#include <boost/asio.hpp>

using namespace std;
using namespace cv;
using namespace boost;

void tonen(Mat image, String naam);

///MultiFlash:
void grabMultiflashImages();
Mat grabSingleImage(String a, HIDS hCam, bool write)
void calibrateMultiflashCamera();
void calculateEdgeMap();
void calculateEdgeMapRGB();
void grabMultiflashCalibImages(int number);
Mat grabSingleImage(String a, HIDS hCam);
