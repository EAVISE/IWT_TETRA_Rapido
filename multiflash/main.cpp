#include <iostream>
#include "multiflash.hpp"

using namespace std;

int main()
{
    bool vlag = true;
    while(vlag)
    {
        cout<<"To grab images, press g\n"
            "To calibrate the camera, press c\n"
            "To calculate depth map, press d\n"
            "To quit, press q"<<endl;
        char antwoord;
        cin >> antwoord;

        if(antwoord == 'g')
        {
            grabMultiflashImages();
        }
        else if(antwoord =='c')
        {
            calibrateMultiflashCamera();
        }
        else if(antwoord == 'd')
        {
            calculateEdgeMap();
        }
        else if(antwoord=='q')
        {
            vlag=false;
        }
    }

    return 1;
}
