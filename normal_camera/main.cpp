#include "normal_camera.hpp"

int main()
{
    bool vlag = true;
    while(vlag)
    {
        cout<<"Place Images in ./calib_cam \n"
            "To calibrate the camera, press c\n"
            "Calculate 3D, press d\n"
            "To quit, press q"<<endl;
        char antwoord;
        cin >> antwoord;

        if(antwoord == 'c')
        {
            calibrate_camera();
        }
        else if(antwoord =='d')
        {
            calculate3D_single_camera();
        }
        else if(antwoord=='q')
        {
            vlag=false;
        }
    }
    return 0;
}
