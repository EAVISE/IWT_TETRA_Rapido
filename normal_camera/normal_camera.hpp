#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <dirent.h>
#include <errno.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/cloud_viewer.h>


using namespace std;
using namespace cv;
using namespace pcl;

int getdir (string dir, vector<string> &files);
void calibrate_camera();
Mat calculate3D_single_camera();
void save(Mat origin);

