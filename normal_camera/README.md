Nodige libraries voor Errordet:

Opencv 3.0.0
PCL 1.8
Boost : builden met vlaggen -lboost_system en -lboost_thread

Compiler moet zoeken in directories:
/usr/local/pcl-1.8  (of waar je het geinstalleerd hebt)
/usr/include/eigen3
/usr/include/vtk-5.8


Voorwaarden:
	In de folder calib_cam moet je de calibratiebeelden zetten die je wil gebruiken.

Functies:
	
save
	Heeft een cv::Mat als input, en zal deze wegschrijven als een pcl puntenwolk.
	De cv::Mat moet homogene coordinaten bevatten en geordend zijn volgens dimensie: 3 x N. 

calibrate_camera
	Deze functie leest de calibratiebeelden in vanuit de folder calib_cam.
	Hierna zal de functie de chessboardcorners proberen te achterhalen.
	Als dit gelukt is, wordt de camera gecalibreerd via cv::calibrateCamera.
	Uiteindelijk worden de reprojectie fout, de camera matrix, de camera distortie coefficienten, 		en de extrinsieke waarden, de rotatie vectors en de translatie vectors weggeschreven naar 		camera.xml.
	

		
